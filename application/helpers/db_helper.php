<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('getMaxId')) {

    function getMaxId($col, $tab) {
        $ci = get_instance();
        $sql = "SELECT max(" . $col . ") FROM " . $tab . "";
        $result = $ci->db->query($sql);
        $row = $result->row();
        $curr_id = 0;
        foreach ($row as $value) {
            $curr_id = $value;
        }
        if ($curr_id == null || $curr_id == NULL || $curr_id == 0) {
            $curr_id = 1;
        } else {
            $curr_id = $curr_id + 1;
        }
        return $curr_id;
    }

}

if (!function_exists('uploadProfImg')) {

    function uploadProfImg($field_name) {
        $ci = get_instance();
        $config['upload_path'] = 'public/uploads/profile_imgs';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_width'] = '120';
        $config['max_height'] = '120';
        $config['overwrite'] = TRUE;
        $config['encrypt_name'] = TRUE;

        $ci->load->library('upload', $config);
        if (!$ci->upload->do_upload($field_name)) {
            $data['imageError'] = $ci->upload->display_errors();
            return $data;
        } else {
            $imageDetailArray = $ci->upload->data();
            $image = $imageDetailArray['file_name'];
            return $image;
        }
    }

}

if (!function_exists('upload_event_file')) {

    function upload_event_file($event, $field_name, $type) {
        $ci = get_instance();
        if (!empty($event)) {
            $path = 'public/uploads/' . $event;
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0755, TRUE);
            }
        }
        if ($type === 'vid') {
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'mp4';
            $config['max_size'] = '';
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $ci->load->library('upload', $config);
            $ci->upload->initialize($config);
            if (!$ci->upload->do_upload($field_name)) {
                $data['file_error'] = $ci->upload->display_errors();
                return $data;
            } else {
                $vidRtnArray = $ci->upload->data();
                $vid['enc_vid'] = $vidRtnArray['file_name'];
                return $vid;
            }
        } elseif ($type === 'img') {
            $config1['upload_path'] = $path;
            $config1['allowed_types'] = 'gif|jpg|png';
            //$config1['max_width'] = '120';
            //$config1['max_height'] = '120';
            $config1['overwrite'] = TRUE;
            $config1['encrypt_name'] = TRUE;
            $ci->load->library('upload', $config1);
            $ci->upload->initialize($config1);
            if (!$ci->upload->do_upload($field_name)) {
                $data1['file_error'] = $ci->upload->display_errors();
                return $data1;
            } else {
                $imgRtnArray = $ci->upload->data();
                $img['enc_img'] = $imgRtnArray['file_name'];
                return $img;
            }
        }
//            elseif ($type === 'fls') {
//                $config2['upload_path'] = $path;
//                $config2['allowed_types'] = 'doc|docx|xlsx|pdf';
//                
//            }
    }

}


if (!function_exists('upload_media_file')) {

    function upload_media_file($event, $field_name, $type, $width = NULL, $height = NULL) {
        $ci = get_instance();
        if (!empty($event)) {
            $path = 'public/uploads/' . $event;
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0755, TRUE);
            }
        }
        if ($type === 'vid') {
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'mp4';
            $config['max_size'] = '';
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $ci->load->library('upload', $config);
            $ci->upload->initialize($config);
            if (!$ci->upload->do_upload($field_name)) {
                $data['file_error'] = $ci->upload->display_errors();
                return $data;
            } else {
                $vidRtnArray = $ci->upload->data();
                $vid['enc_vid'] = $vidRtnArray['file_name'];
                return $vid;
            }
        } elseif ($type === 'img') {
            $config1['upload_path'] = $path;
            $config1['allowed_types'] = 'gif|jpg|png';
            if ($width != NULL) {
                $config1['max_width'] = $width;
                $config1['min_width'] = $width;
            }
            if ($height != NULL) {
                $config1['max_height'] = $height;
                $config1['min_height'] = $height;
            }
            $config1['overwrite'] = TRUE;
            $config1['encrypt_name'] = TRUE;
            $ci->load->library('upload', $config1);
            $ci->upload->initialize($config1);
            if (!$ci->upload->do_upload($field_name)) {
                $data1['file_error'] = $ci->upload->display_errors();
                return $data1;
            } else {
                $imgRtnArray = $ci->upload->data();
                $img['enc_img'] = $imgRtnArray['file_name'];
                return $img;
            }
        }
//            elseif ($type === 'fls') {
//                $config2['upload_path'] = $path;
//                $config2['allowed_types'] = 'doc|docx|xlsx|pdf';
//                
//            }
    }

}

if (!function_exists('upload_event_multiplefile')) {

    function upload_event_multiplefile($fileobject, $event) {
        $ci = & get_instance();
        $path = "";
        if (!empty($event)) {
            $path = 'public/uploads/' . $event . '/';
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0755, TRUE);
            }
        }
        $number_of_files = sizeof($_FILES[$fileobject]['tmp_name']);
        $files = $_FILES[$fileobject];
        $errors = array();
        $file_arr = array();
        $result = array();
        for ($k = 0; $k < $number_of_files; $k++) {
            if ($_FILES[$fileobject]['error'][$k] != 0)
                $errors[$k][] = 'Couldn\'t upload file ' . $_FILES[$fileobject]['name'][$k];
        }
        if (sizeof($errors) == 0) {
            $ci->load->library('upload'); // load library
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000';
            $config['overwrite'] = TRUE;
            $config['encrypt_name'] = TRUE;

            for ($i = 0; $i < $number_of_files; $i++) {
                $_FILES['uploadedimage']['name'] = $files['name'][$i];
                $_FILES['uploadedimage']['type'] = $files['type'][$i];
                $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
                $_FILES['uploadedimage']['error'] = $files['error'][$i];
                $_FILES['uploadedimage']['size'] = $files['size'][$i];
                $ci->upload->initialize($config);
                if ($ci->upload->do_upload('uploadedimage')) {
                    $imageup = $ci->upload->data();
                    array_push($file_arr, $imageup["file_name"]);
                    $result["response"] = TRUE;
                    //$result["message"] = "Files uploaded successfully.";
                    $result["files"] = $file_arr;
                } else {
                    $result["response"] = FALSE;
                    //$result["message"][$i] = $ci->upload->display_errors();
                }
            }
        }
        return $result;
    }

}

if (!function_exists('upload_meadia_multiplefiles')) {

    function upload_meadia_multiplefiles($fileobject, $event, $width = null, $height = null) {
        $ci = & get_instance();
        $path = "";
        if (!empty($event)) {
            $path = 'public/uploads/' . $event . '/';
            if (!is_dir($path)) { //create the folder if it's not already exists
                mkdir($path, 0755, TRUE);
            }
        }
        $number_of_files = sizeof($_FILES[$fileobject]['tmp_name']);
        $files = $_FILES[$fileobject];
        $errors = array();
        $file_arr = array();
        $result = array();
        for ($k = 0; $k < $number_of_files; $k++) {
            if ($_FILES[$fileobject]['error'][$k] != 0)
                $errors[$k][] = 'Couldn\'t upload file ' . $_FILES[$fileobject]['name'][$k];
        }
        if (sizeof($errors) == 0) {
            $ci->load->library('upload'); // load library
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000';
            $config['overwrite'] = TRUE;
            $config['encrypt_name'] = TRUE;
            if ($width != NULL) {
                $config['max_width'] = $width;
                $config['min_width'] = $width;
            }
            if ($height != NULL) {
                $config['max_height'] = $height;
                $config['min_height'] = $height;
            }

            for ($i = 0; $i < $number_of_files; $i++) {
                $_FILES['uploadedimage']['name'] = $files['name'][$i];
                $_FILES['uploadedimage']['type'] = $files['type'][$i];
                $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
                $_FILES['uploadedimage']['error'] = $files['error'][$i];
                $_FILES['uploadedimage']['size'] = $files['size'][$i];
                $ci->upload->initialize($config);
                if ($ci->upload->do_upload('uploadedimage')) {
                    $imageup = $ci->upload->data();
                    array_push($file_arr, $imageup["file_name"]);
                    $result["response"] = TRUE;
                    //$result["message"] = "Files uploaded successfully.";
                    $result["files"] = $file_arr;
                } else {
                    $result["response"] = FALSE;
                    //$result["message"][$i] = $ci->upload->display_errors();
                    $result["message"] = $ci->upload->display_errors();
                }
            }
        }
        return $result;
    }

}

if (!function_exists('email_checker')) {

    function email_checker($email, $uid, $tab, $col, $col2) {
        $ci = get_instance();
        $sql = "SELECT count(u.user_id) AS cnt FROM user_tab u JOIN " . $tab . " d WHERE u.user_id = d." . $col . " AND d." . $col2 . "=" . $uid . " AND u.user_email='" . $email . "'";
        $result = $ci->db->query($sql);
        $row = $result->row_array();
        if (intval($row['cnt']) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('email_validator')) {

    function email_validator($email) {
        $ci = get_instance();
        $sql = "SELECT count(u.user_id) AS cnt FROM user_tab u WHERE u.user_email='" . $email . "'";
        $sql1 = "SELECT count(r.reg_id) AS cnt FROM registration_tab r WHERE r.reg_email = '" . $email . "'";
        $result = $ci->db->query($sql);
        $result1 = $ci->db->query($sql1);
        $row = $result->row_array();
        $row1 = $result1->row_array();
        if (intval($row['cnt']) > 0 && intval($row1['cnt']) > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

if (!function_exists('get_column')) {

    function get_column($select_col, $whr_col, $tab, $uid) {
        $ci = get_instance();
        $sql = "SELECT " . $select_col . " FROM " . $tab . " WHERE " . $whr_col . "=" . $uid . "";
        $result = $ci->db->query($sql);
        $row = $result->row_array();
        return $row[$select_col];
    }

}

if (!function_exists('get_user_id_by_user_name')) {

    function get_user_id_by_user_name($uemail) {
        $ci = get_instance();
        if ($ci->session->userdata('user_role') === '2') {
            $sql = "SELECT d.cod_id AS uid FROM dgfg_coordinator_tab d JOIN user_tab u ON u.user_id = d.cod_user_ref WHERE u.user_email='" . $uemail . "'";
        } elseif ($ci->session->userdata('user_role') === '3') {
            $sql = "SELECT d.led_id AS uid FROM dgfg_leader_tab d JOIN user_tab u ON u.user_id = d.led_user_ref WHERE u.user_email='" . $uemail . "'";
        } elseif ($ci->session->userdata('user_role') === '4') {
            $sql = "SELECT d.mem_id AS uid FROM dgfg_member_tab d JOIN user_tab u ON u.user_id = d.mem_user_ref WHERE u.user_email='" . $uemail . "'";
        }
        $result = $ci->db->query($sql);
        $row = $result->row_array();
        return $row['uid'];
    }

}

if (!function_exists('get_memeber_id_by_user_name')) {

    function get_memeber_id_by_user_name($uemail, $role) {
        $ci = get_instance();
        $sql = "";
        if ($role === '2') {
            $sql = "SELECT d.cod_id AS uid FROM dgfg_coordinator_tab d JOIN user_tab u ON u.user_id = d.cod_user_ref WHERE u.user_email='" . $uemail . "'";
        } elseif ($role === '3') {
            $sql = "SELECT d.led_id AS uid FROM dgfg_leader_tab d JOIN user_tab u ON u.user_id = d.led_user_ref WHERE u.user_email='" . $uemail . "'";
        } elseif ($role === '4') {
            $sql = "SELECT d.mem_id AS uid FROM dgfg_member_tab d JOIN user_tab u ON u.user_id = d.mem_user_ref WHERE u.user_email='" . $uemail . "'";
        }

        $result = $ci->db->query($sql);
        $row = $result->row_array();
        return $row['uid'];
    }

}

if (!function_exists('get_tot_tasks')) {

    function get_tot_tasks($eid) {
        $ci = get_instance();
        $sql = "SELECT count(e.event_task_id) AS cnt FROM event_task_tab e JOIN event_tab e1 ON e1.event_id = e.event_ref WHERE e.event_ref=" . $eid . "";
        $query = $ci->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['cnt'];
        }
    }

}

if (!function_exists('get_comp_tasks')) {

    function get_comp_tasks($eid) {
        $ci = get_instance();
        $sql = "SELECT count(e.event_task_id) AS cnt FROM event_task_tab e INNER JOIN event_tab e1 ON e1.event_id = e.event_ref WHERE e.task_stat='C' AND e.event_ref=" . $eid . "";
        $query = $ci->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['cnt'];
        }
    }

}

if (!function_exists('email_notification')) {

    function email_notification($email_data) {
        $ci = get_instance();

        $config = array();
        $config['useragent'] = "CodeIgniter";
        $config['mailpath'] = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "localhost";
        $config['smtp_port'] = "25";
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
//            'protocol' => 'smtp',
//            'smtp_host' => 'ssl://smtp.googlemail.com',
//            'smtp_port' => 465,
//            'smtp_user' => 'vtag.test@gmail.com', // change it to yours
//            'smtp_pass' => 'test123*', // change it to yours
        $ci->load->library('email', $config);
        $ci->email->set_newline("\r\n");
        $ci->email->from($email_data['from'], $email_data['name']);
        $ci->email->to($email_data['to']);
        $ci->email->subject($email_data['subject']);
        $ci->email->message($email_data['msg']);
        if ($ci->email->send()) {
            return TRUE;
        } else {
            show_error($ci->email->print_debugger());
            return FALSE;
        }
    }

}

if (!function_exists('dashboard_notification')) {

    function dashboard_notification($rec_role, $rec_id, $sent_role, $sent_id, $msg) {
        $ci = get_instance();
        $sql = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (" . $rec_role . "," . $rec_id . "," . $sent_role . "," . $sent_id . ",NOW(),'N','" . $msg . "')";
        $ci->db->query($sql);
        $insert_id = $ci->db->insert_id();
        if ($insert_id) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

