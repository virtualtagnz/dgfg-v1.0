<?php

if (!session_id()) {
    session_start();
}

require_once("Facebook/autoload.php");

class Facebooksdk {

    protected $fb;
    protected $helper;

    public function __construct() {

        $this->fb = new Facebook\Facebook([
            'app_id' => '1556633847964820',
            'app_secret' => '0617d7fd22ac2c52bf822b6511d2bee4',
            'default_graph_version' => 'v2.5',
        ]);
        $this->helper = $this->fb->getRedirectLoginHelper();
    }

    function getLoginUrl($callback_url) {

        $permissions = ['email']; // optional
        $loginUrl = $this->helper->getLoginUrl($callback_url, $permissions);
        return $loginUrl;
    }

    function getAccessToken() {
        try {
            $accessToken = $this->helper->getAccessToken();
            return $accessToken;
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    function getUserData($access_token) {
        $this->fb->setDefaultAccessToken($access_token);

        try {
            $response = $this->fb->get('/me?fields=id,name,email,first_name,last_name,gender,picture');
            $userNode = $response->getGraphUser();
            return $userNode;
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    function getCanvasHelper() {
        $this->fb->getCanvasHelper();
    }

}
