<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['create_user/(:any)'] = 'create_user/index/$1';
$route['view_user/(:any)'] = 'view_user/index/$1';
$route['view_single/(:num)/(:num)'] = 'view_single/index/$1/$2';
$route['user_edit/(:num)/(:num)'] = 'user_edit/index/$1/$2';
$route['edit_event/(:any)'] = 'edit_event/index/$1';
$route['edit_service/(:any)'] = 'edit_service/index/$1';
$route['single_event/(:any)'] = 'single_event/index/$1';
$route['single_service/(:any)'] = 'single_service/index/$1';
$route['send_invitation/(:any)'] = 'send_invitation/index/$1';
$route['edit_daily_surveys/(:any)'] = 'edit_daily_surveys/index/$1';
$route['single_daily_survey/(:any)'] = 'single_daily_survey/index/$1';
$route['email_confirmation/(:any)'] = 'email_confirmation/index/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
