<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reward_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }
    
    public function insert_reward($data_arr) {
        $sql = "INSERT INTO rewards(re_user_role, re_user_id, reward_des, reward, reward_date) "
                . "VALUES (".$data_arr['user_role'].",".$data_arr['id'].",'".$data_arr['reward_des']."',".$data_arr['reward'].",NOW())";
        $query = $this->db->query($sql);
        $insert_id = $ci->db->insert_id();
        if($insert_id){
            return TRUE;
        }else{
            return FALSE;
        }   
    }
}

