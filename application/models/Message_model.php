<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }
    
    public function get_senders() {
        $sql = "SELECT d.cod_img_link AS img,concat(d.cod_fname,' ',d.cod_lname) AS uname,u.user_email AS email FROM user_tab u INNER JOIN dgfg_coordinator_tab d ON u.user_id = d.cod_user_ref "
                . "UNION "
                . "SELECT d.led_img_link AS img,concat(d.led_fname,' ',d.led_lname) AS uname,u.user_email AS email FROM user_tab u INNER JOIN dgfg_leader_tab d ON u.user_id = d.led_user_ref "
                . "UNION "
                . "SELECT d.mem_img_link AS img,concat(d.mem_fname,' ',d.mem_lname) AS uname,u.user_email AS email FROM user_tab u INNER JOIN dgfg_member_tab d ON u.user_id = d.mem_user_ref";
        $query = $this->db->query($sql);
        $sender_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $sender_arr[$i]['img'] = $row['img'];
                $sender_arr[$i]['uname'] = $row['uname'];
                $sender_arr[$i]['email'] = $row['email'];
                $i = $i + 1;
            }
            $json_str = json_encode($sender_arr);
            return $json_str;
        }
    }
    
    public function save_message($msg_data) {
        $msg_id = getMaxId("msg_id", "private_msg");
        
        $msg_master = array();
        $msg_user = array();
        
        $msg_master['msg_id'] = $msg_id;
        $msg_master['msg_reply_id'] = $msg_data['msg_ef_id'];
        $msg_master['msg_title'] = trim($msg_data['subject']);
        $msg_master['msg_body'] = trim($msg_data['mag']);
        $date = new DateTime();
        $msg_master['msg_date'] = $date->format('Y-m-d H:i:s');
        
        $i=0;
        foreach ($msg_data['msg_to'] as $value) {
            $msg_user[$i]['msg_id_ref'] = $msg_id;
            $msg_user[$i]['sender_email'] = $this->session->userdata('username');
            $msg_user[$i]['receiver_email'] = $value;
            $msg_user[$i]['is_watch'] = 'N';
            $i++;
        }
        
        $this->db->trans_start();
        $this->db->insert('private_msg', $msg_master);
        $this->db->insert_batch('private_msg_users', $msg_user);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function load_my_messages() {
        $sql = "SELECT p.msg_id,p.msg_title,DATE_FORMAT(p.msg_date,'%D %M %Y') AS msg_date FROM private_msg p "
                . "INNER JOIN private_msg_users p1 ON p.msg_id = p1.msg_id_ref "
                . "WHERE p1.is_watch='N' AND p1.receiver_email='".$this->session->userdata('username')."' ORDER BY msg_date DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $my_msg_arr = array();
            $i=0;
            foreach ($query->result_array() as $row) {
                $my_msg_arr[$i]['msg_id'] = $this->encrypt->encode($row['msg_id']);
                $my_msg_arr[$i]['msg_title'] = $row['msg_title'];
                $my_msg_arr[$i]['msg_date'] = $row['msg_date'];
                $i++;
            }
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }  else {
            $json_str = json_encode('0');
            return $json_str;
        }
    }
    
    public function load_all_messages() {
        $sql = "SELECT p.msg_id,p1.sender_email,p.msg_title,p.msg_body,DATE_FORMAT(p.msg_date,'%D %M %Y') AS msg_date FROM private_msg p "
                . "INNER JOIN private_msg_users p1 ON p.msg_id = p1.msg_id_ref "
                . "WHERE p1.receiver_email='".$this->session->userdata('username')."' AND p.msg_reply_id IS NULL";
        $query = $this->db->query($sql);
        $my_msg_arr = array();
        if ($query->num_rows() > 0) {
            $i=0;
            foreach ($query->result_array() as $row) {
            $my_msg_arr[$i]['msg_id'] = $this->encrypt->encode($row['msg_id']);
                $my_msg_arr[$i]['sender_email'] = $row['sender_email'];
                $my_msg_arr[$i]['msg_title'] = $row['msg_title'];
                //$my_msg_arr[$i]['msg_body'] = $row['msg_body'];
                $my_msg_arr[$i]['msg_date'] = $row['msg_date'];
                $i++;
            }
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }  else {
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }
    }
    
    public function load_intermediate_messages() {
        $sql = "SELECT p.msg_id,p1.sender_email,p.msg_title,p.msg_body,DATE_FORMAT(p.msg_date,'%D %M %Y') AS msg_date FROM private_msg p "
                . "INNER JOIN private_msg_users p1 ON p.msg_id = p1.msg_id_ref "
                . "WHERE p1.receiver_email='".$this->session->userdata('username')."'";
        $query = $this->db->query($sql);
        $my_msg_arr = array();
        if ($query->num_rows() > 0) {
            $i=0;
            foreach ($query->result_array() as $row) {
            $my_msg_arr[$i]['msg_id'] = $this->encrypt->encode($row['msg_id']);
                $my_msg_arr[$i]['sender_email'] = $row['sender_email'];
                $my_msg_arr[$i]['msg_title'] = $row['msg_title'];
                //$my_msg_arr[$i]['msg_body'] = $row['msg_body'];
                $my_msg_arr[$i]['msg_date'] = $row['msg_date'];
                $i++;
            }
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }  else {
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }
    }
    
    public function load_coodinator_messages() {
        $sql = "SELECT DISTINCT(p.msg_id) AS msg_id,p1.sender_email,p.msg_title,p.msg_body,DATE_FORMAT(p.msg_date,'%D %M %Y') AS msg_date FROM private_msg p "
                . "INNER JOIN private_msg_users p1 ON p.msg_id = p1.msg_id_ref "
                . "WHERE p1.sender_email='".$this->session->userdata('username')."' AND p.msg_reply_id IS NULL";
        $query = $this->db->query($sql);
        $my_msg_arr = array();
        if ($query->num_rows() > 0) {
            $i=0;
            foreach ($query->result_array() as $row) {
            $my_msg_arr[$i]['msg_id'] = $this->encrypt->encode($row['msg_id']);
                $my_msg_arr[$i]['sender_email'] = $row['sender_email'];
                $my_msg_arr[$i]['msg_title'] = $row['msg_title'];
                //$my_msg_arr[$i]['msg_body'] = $row['msg_body'];
                $my_msg_arr[$i]['msg_date'] = $row['msg_date'];
                $i++;
            }
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }  else {
            $json_str = json_encode($my_msg_arr);
            return $json_str;
        }
    }
    
    public function load_single_message($msg_id) {
        $sql = "SELECT p.msg_title,p.msg_body,DATE_FORMAT(p.msg_date,'%D %M %Y %H:%i') AS msg_date,p.msg_id FROM private_msg p WHERE p.msg_id=".$msg_id."";
        $query = $this->db->query($sql);
        $single_msg = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $single_msg[] = $row;
            }
            return $single_msg;
        }
    }
    
    public function load_reply_msg($msg_id) {
        $sql = "SELECT p1.msg_body,DATE_FORMAT(p1.msg_date,'%D %M %Y %H:%i') AS msg_date,p1.msg_id AS uid FROM private_msg p1 WHERE p1.msg_reply_id = ".$msg_id."";
        $query = $this->db->query($sql);
        $reply_msg = array();
        if ($query->num_rows() > 0) {
            $i=0;
            foreach ($query->result_array() as $row) {
                $reply_msg[$i]['msg_body'] = $row['msg_body'];
                $reply_msg[$i]['msg_date'] = $row['msg_date'];
                $reply_msg[$i]['rep_rs'] = $this->load_senders_receivers($row['uid']);
                $i++;
            }
            return $reply_msg;
        }
        
    }
    
    public function load_senders_receivers($msg_id) {
        $sql = "SELECT p1.sender_email,p1.receiver_email FROM private_msg p "
                . "INNER JOIN private_msg_users p1 ON p.msg_id = p1.msg_id_ref "
                . "WHERE p.msg_id=".$msg_id."";
        $query = $this->db->query($sql);
        $send_res_arr = array();
        if ($query->num_rows() > 0) {
            $i=0;
            foreach ($query->result_array() as $row) {
                $send_res_arr[$i]['sender_email'] = $row['sender_email'];
                $send_res_arr[$i]['receiver_email'] = $row['receiver_email'];
                $send_res_arr[$i]['sender_img'] = $this->load_sender_image($row['sender_email']);
                $i++;
            }
            return $send_res_arr;
        }
    }
    
    public function load_sender_image($email) {
        $sql = "SELECT u.user_role_ref FROM dgfg_db.user_tab u WHERE u.user_email='".$email."'";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            if($row['user_role_ref']==='2'){
                $sql1 = "SELECT d.cod_img_link AS img FROM dgfg_coordinator_tab d WHERE d.cod_user_ref = (SELECT u.user_id FROM user_tab u WHERE u.user_email='".$email."')";
            }elseif ($row['user_role_ref']==='3') {
                $sql1 = "SELECT d.led_img_link AS img FROM dgfg_leader_tab d WHERE d.led_user_ref = (SELECT u.user_id FROM user_tab u WHERE u.user_email='".$email."')";
            }elseif ($row['user_role_ref']==='4') {
                $sql1 = "SELECT d.mem_img_link AS img FROM dgfg_member_tab d WHERE d.mem_user_ref = (SELECT u.user_id FROM user_tab u WHERE u.user_email='".$email."')";
            }
            $query1 = $this->db->query($sql1);
            if($query1->num_rows() > 0){
                $row1 = $query1->row_array();
                return $row1['img'];
            }
        }
    }
    
    public function update_message_flag($msg_id) {
        $sql = "UPDATE private_msg_users SET is_watch='Y' WHERE msg_id_ref=".$msg_id." AND receiver_email='".$this->session->userdata('username')."'";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

