<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_member extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }

    public function addMember($user_data) {
        //$this->load->helper('db_helper');
        $mem_id = getMaxId("mem_id", "dgfg_member_tab");
        $user_id = getMaxId("user_id", "user_tab");
        $sql1 = "INSERT INTO user_tab(user_id, user_role_ref, user_email, user_pwd, active_stat, date_create, role_create, rcode_create, date_modify, role_mod, rcode_mod) "
                . "VALUES (" . $user_id . ",4,'" . $user_data['email'] . "','" . $user_data['pwd'] . "','" . $user_data['stat'] . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "')";
        $sql2 = "INSERT INTO dgfg_member_tab(mem_id, mem_user_ref, mem_fname, mem_lname, mem_gender, mem_mobile, mem_img_link, mem_description, mem_fb_link, mem_ig_link, mem_ethnicity, create_by, role_create, date_create, mod_by, mod_role, mod_date) "
                . "VALUES (" . $mem_id . "," . $user_id . ",'" . $user_data['fname'] . "','" . $user_data['lname'] . "','" . $user_data['gender'] . "','" . $user_data['mob'] . "','" . $user_data['prof_img'] . "','" . $user_data['des'] . "','" . $user_data['fb'] . "','" . $user_data['inst'] . "','" . $user_data['ethnicity'] . "','" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW())";
        $this->db->trans_start();
        $this->db->query($sql1);
        $this->db->query($sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function addFbMember($user_data) {
        //$this->load->helper('db_helper');
        $mem_id = getMaxId("mem_id", "dgfg_member_tab");
        $user_id = getMaxId("user_id", "user_tab");
        $sql1 = "INSERT INTO user_tab(user_id, user_role_ref, user_email, user_pwd, active_stat, date_create, role_create, rcode_create, date_modify, role_mod, rcode_mod) "
                . "VALUES (" . $user_id . ",4,'" . $user_data['email'] . "','" . " " . "','" . "1" . "',NOW(),'" . "sys-facebook" . "','" . " " . "',NOW(),'" . " " . "','" . " " . "')";
        $sql2 = "INSERT INTO dgfg_member_tab(mem_id, mem_user_ref, mem_fname, mem_lname, mem_gender, mem_mobile, mem_img_link, mem_description, mem_fb_link, mem_ig_link, mem_ethnicity, create_by, role_create, date_create, mod_by, mod_role, mod_date) "
                . "VALUES (" . $mem_id . "," . $user_id . ",'" . $user_data['first_name'] . "','" . $user_data['last_name'] . "','" . $user_data['gender'] . "','" . " " . "','" . $user_data['picture']['url'] . "','" . " " . "','" . " " . "','" . " " . "','" . " "  . "','" . " " . "','" . "sys-facebook" . "',NOW(),'" . " " . "','" . " " . "',NOW())";
        $this->db->trans_start();
        $this->db->query($sql1);
        $this->db->query($sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getMember() {
        $select_sql = "SELECT d.mem_id,concat(d.mem_fname,' ',d.mem_lname) as u_name,d.mem_img_link,CASE WHEN (u.active_stat = '1') THEN 'Active' ELSE 'Deactive' END AS act_stat "
                . "FROM dgfg_member_tab d JOIN user_tab u ON d.mem_user_ref=u.user_id";
        $query = $this->db->query($select_sql);
        if ($query->num_rows() > 0) {
            $cod_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $cod_arr[$i]['uid'] = $row['mem_id'];
                $cod_arr[$i]['u_name'] = $row['u_name'];
                $cod_arr[$i]['img'] = $row['mem_img_link'];
                $cod_arr[$i]['act_stat'] = $row['act_stat'];
                $i = $i + 1;
            }
            $json_str = json_encode($cod_arr);
            return $json_str;
        }
    }

    public function getSingleMember($mem_id) {
        $sql = "SELECT u.user_email,u.active_stat,d.mem_fname,d.mem_lname,d.mem_gender,d.mem_mobile,d.mem_img_link,d.mem_description,d.mem_fb_link,d.mem_ig_link,d.mem_ethnicity "
                . "FROM dgfg_member_tab d JOIN user_tab u WHERE u.user_id = d.mem_user_ref AND d.mem_id=" . $mem_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $cod_arr2 = array();
            foreach ($query->result_array() as $row) {
//                  $cod_arr2[] = $row;
                $cod_arr2['uemail'] = $row['user_email'];
                $cod_arr2['act_stat'] = $row['active_stat'];
                $cod_arr2['fname'] = $row['mem_fname'];
                $cod_arr2['lname'] = $row['mem_lname'];
                $cod_arr2['gender'] = $row['mem_gender'];
                $cod_arr2['mob'] = $row['mem_mobile'];
                $cod_arr2['img'] = $row['mem_img_link'];
                $cod_arr2['des'] = $row['mem_description'];
                $cod_arr2['fb'] = $row['mem_fb_link'];
                $cod_arr2['inst'] = $row['mem_ig_link'];
                $cod_arr2['sm'] = $row['mem_ethnicity'];
            }
            return $cod_arr2;
        }
    }

    public function modify_member($mod_info) {
        //$this->load->helper('db_helper');
        $cod_id = get_column("mem_user_ref", "mem_id", "dgfg_member_tab", $mod_info['uid']);
        $mod_sql1 = "UPDATE user_tab SET "
                . "user_email='" . $mod_info['email'] . "',"
                . "user_pwd='" . $mod_info['pwd'] . "',"
                . "active_stat='" . $mod_info['stat'] . "',"
                . "date_modify=NOW(),"
                . "role_mod='" . $this->session->userdata('username') . "',"
                . "rcode_mod='" . $this->session->userdata('user_role') . "' WHERE user_id=" . $cod_id . "";

        $mod_sql2 = "UPDATE dgfg_member_tab SET "
                . "mem_fname='" . $mod_info['fname'] . "',"
                . "mem_lname='" . $mod_info['lname'] . "',"
                . "mem_gender='" . $mod_info['gender'] . "',"
                . "mem_mobile='" . $mod_info['mob'] . "',"
                . "mem_img_link='" . $mod_info['prof_img'] . "',"
                . "mem_description='" . $mod_info['des'] . "',"
                . "mem_fb_link='" . $mod_info['fb'] . "',"
                . "mem_ig_link='" . $mod_info['inst'] . "',"
                . "mem_ethnicity ='" . $mod_info['ethnicity'] . "',"
                . "mod_by='" . $this->session->userdata('username') . "',"
                . "mod_role='" . $this->session->userdata('user_role') . "',"
                . "mod_date=NOW() WHERE mem_id=" . $mod_info['uid'] . "";

        $this->db->trans_start();
        $this->db->query($mod_sql1);
        $this->db->query($mod_sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function load_unselected_members($eid) {
        //$sql = "SELECT d.mem_id,concat(d.mem_fname,'',d.mem_lname) AS u_name FROM dgfg_member_tab d WHERE d.mem_id NOT IN (SELECT x.sel_mem_id_ref FROM selected_members x)";
        //$sql = "SELECT e.member_id_ref,concat(d.mem_fname,'',d.mem_lname) AS u_name FROM events_interest e INNER JOIN dgfg_member_tab d ON d.mem_id = e.member_id_ref WHERE e.events_id_ref=".$eid."";
        $sql = "SELECT e.member_id_ref, concat(d.mem_fname,' ',d.mem_lname) AS u_name FROM dgfg_db.events_interest e "
                . "INNER JOIN dgfg_db.dgfg_member_tab d ON d.mem_id = e.member_id_ref "
                . "WHERE e.events_id_ref = ".$eid." AND e.member_id_ref NOT IN (SELECT s.sel_mem_id_ref FROM selected_members s WHERE s.sel_event_id_ref =".$eid.")";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $mem_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $mem_arr[$i]['uid'] = $this->encrypt->encode($row['member_id_ref']);
                $mem_arr[$i]['u_name'] = $row['u_name'];
                $i = $i + 1;
            }
            $json_str = json_encode($mem_arr);
            return $json_str;
        }
    }

    public function set_events_available_members($arr) {
        $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (4,".$arr['sel_mem_id_ref'].",".$this->session->userdata('user_role').",".$sent_id.",NOW(),'N','You have 1 event assigned')";
        $this->db->trans_start();
        $this->db->insert('selected_members', $arr);
        $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function load_selected_members($eid) {
        $sql = "SELECT s.sel_mem_id_ref,concat(d.mem_fname,' ',d.mem_lname) AS u_name FROM selected_members s INNER JOIN dgfg_member_tab d ON d.mem_id = s.sel_mem_id_ref WHERE s.sel_event_id_ref=" . $eid . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $cod_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $cod_arr[$i]['uid'] = $this->encrypt->encode($row['sel_mem_id_ref']);
                $cod_arr[$i]['u_name'] = $row['u_name'];
                $i = $i + 1;
            }
            $json_str = json_encode($cod_arr);
            return $json_str;
        }
    }

}
