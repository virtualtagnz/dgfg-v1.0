<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }
    
    public function load_my_notification() {
        $uid = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "SELECT n.notify_id,n.notification_msg FROM notification_tab n "
                . "WHERE n.notification_recive_role=".$this->session->userdata('user_role')." AND n.notification_receive_id=".$uid." AND n.notification_sent_role <> ".$this->session->userdata('user_role')." AND n.notification_sent_id <> ".$uid." AND is_watch='N' "
                . "ORDER BY  notification_sent_date DESC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $notify_arr = array();
            foreach ($query->result_array() as $row) {
                $notify_arr[] = $row;
            }
            $json_str = json_encode($notify_arr);
            return $json_str;
        }  else {
            $json_str = json_encode('0');
            return $json_str;
        }
        
    }
    
    public function update_view_flag($nid) {
        $sql = "UPDATE notification_tab SET is_watch='Y' WHERE notify_id=".$nid."";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
}
