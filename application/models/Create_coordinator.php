<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_coordinator extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function addCoodinator($user_data) {
        $this->load->helper('db_helper');
        $cod_id = getMaxId("cod_id", "dgfg_coordinator_tab");
        $user_id = getMaxId("user_id", "user_tab");
        $sql1 = "INSERT INTO user_tab(user_id, user_role_ref, user_email, user_pwd, active_stat, date_create, role_create, rcode_create, date_modify, role_mod, rcode_mod) "
                . "VALUES (" . $user_id . ",2,'" . $user_data['email'] . "','" . $user_data['pwd'] . "','" . $user_data['stat'] . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "')";

        $sql2 = "INSERT INTO dgfg_coordinator_tab(cod_id, cod_user_ref, cod_fname, cod_lname, cod_gender, cod_mobile, cod_img_link, cod_description, cod_fb_link, cod_ig_link, cod_sm_challenge, create_by, role_create, date_create, mod_by, mod_role, mod_date) "
                . "VALUES (" . $cod_id . "," . $user_id . ",'" . $user_data['fname'] . "','" . $user_data['lname'] . "','" . $user_data['gender'] . "','" . $user_data['mob'] . "','" . $user_data['prof_img'] . "','" . $user_data['des'] . "','" . $user_data['fb'] . "','" . $user_data['inst'] . "','" . $user_data['smc'] . "','" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW())";

        $this->db->trans_start();
        $this->db->query($sql1);
        $this->db->query($sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getCoordinator() {
        $select_sql = "SELECT c.cod_id,concat(cod_fname,' ',cod_lname) as u_name,c.cod_img_link,CASE WHEN (u.active_stat = '1') THEN 'Active' ELSE 'Deactive' END AS act_stat "
                . "FROM dgfg_coordinator_tab c JOIN user_tab u ON c.cod_user_ref=u.user_id";
        $query = $this->db->query($select_sql);
        if ($query->num_rows() > 0) {
            $cod_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $cod_arr[$i]['uid'] = $row['cod_id'];
                $cod_arr[$i]['u_name'] = $row['u_name'];
                $cod_arr[$i]['img'] = $row['cod_img_link'];
                $cod_arr[$i]['act_stat'] = $row['act_stat'];
                $i = $i + 1;
            }
            $json_str = json_encode($cod_arr);
            return $json_str;
        }
    }

    public function getSingleCoodinator($cod_id) {
        $sql = "SELECT u.user_email,u.active_stat,d.cod_fname,d.cod_lname,d.cod_gender,d.cod_mobile,d.cod_img_link,d.cod_description,d.cod_fb_link,d.cod_ig_link,d.cod_sm_challenge "
                . "FROM dgfg_coordinator_tab d JOIN user_tab u WHERE u.user_id = d.cod_user_ref AND d.cod_id=" . $cod_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $cod_arr2 = array();
            foreach ($query->result_array() as $row) {
//                  $cod_arr2[] = $row;
                $cod_arr2['uemail'] = $row['user_email'];
                $cod_arr2['act_stat'] = $row['active_stat'];
                $cod_arr2['fname'] = $row['cod_fname'];
                $cod_arr2['lname'] = $row['cod_lname'];
                $cod_arr2['gender'] = $row['cod_gender'];
                $cod_arr2['mob'] = $row['cod_mobile'];
                $cod_arr2['img'] = $row['cod_img_link'];
                $cod_arr2['des'] = $row['cod_description'];
                $cod_arr2['fb'] = $row['cod_fb_link'];
                $cod_arr2['inst'] = $row['cod_ig_link'];
                $cod_arr2['sm'] = $row['cod_sm_challenge'];
            }
            return $cod_arr2;
        }
    }

    public function modify_coodinator($mod_info) {
        $this->load->helper('db_helper');
        $cod_id = get_column("cod_user_ref", "cod_id", "dgfg_coordinator_tab", $mod_info['uid']);
        $mod_sql1 = "UPDATE user_tab SET "
                . "user_email='" . $mod_info['email'] . "',"
                . "user_pwd='" . $mod_info['pwd'] . "',"
                . "active_stat='" . $mod_info['stat'] . "',"
                . "date_modify=NOW(),"
                . "role_mod='" . $this->session->userdata('username') . "',"
                . "rcode_mod='" . $this->session->userdata('user_role') . "' WHERE user_id=".$cod_id."";

        $mod_sql2 = "UPDATE dgfg_coordinator_tab SET "
                . "cod_fname='" . $mod_info['fname'] . "',"
                . "cod_lname='" . $mod_info['lname'] . "',"
                . "cod_gender='" . $mod_info['gender'] . "',"
                . "cod_mobile='" . $mod_info['mob'] . "',"
                . "cod_img_link='" . $mod_info['prof_img'] . "',"
                . "cod_description='" . $mod_info['des'] . "',"
                . "cod_fb_link='" . $mod_info['fb'] . "',"
                . "cod_ig_link='" . $mod_info['inst'] . "',"
                . "cod_sm_challenge='" . $mod_info['smc'] . "',"
                . "mod_by='" . $this->session->userdata('username') . "',"
                . "mod_role='" . $this->session->userdata('user_role') . "',"
                . "mod_date=NOW() WHERE cod_id=".$mod_info['uid']."";
        
        $this->db->trans_start();
        $this->db->query($mod_sql1);
        $this->db->query($mod_sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
