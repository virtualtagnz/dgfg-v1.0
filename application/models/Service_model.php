<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }

    public function create_service($data) {
        $sp_id = getMaxId("sp_id", "service_tab");
        $sql = "INSERT INTO service_tab(sp_id, sp_name, sp_cat, sp_summery, sp_cost, sp_phn, sp_key, sp_list, sp_add, sp_hrs, sp_after_phn, sp_img, sp_staff_img, sp_web, sp_level, sp_create_by, sp_create_role, sp_create_date, sp_mod_by, sp_mod_role, sp_mod_date) "
                . "VALUES (" . $sp_id . ",'" . $this->db->escape_str($data['sp_name']) . "','" . $data['sp_cat'] . "','" . $this->db->escape_str($data['sp_summery']) . "','" . $data['sp_cost'] . "','" . $data['sp_phn'] . "','" . $this->db->escape_str($data['sp_key']) . "','" . $this->db->escape_str($data['sp_list']) . "','" . $this->db->escape_str($data['sp_add']) . "','" . $data['sp_hrs'] . "','" . $data['sp_after_phn'] . "','" . $data['sp_img'] . "','" . $data['sp_staff_img'] . "','" . $data['sp_web'] . "','" . $data['sp_level'] . "','" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW())";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function view_all_services() {
        $sql = "SELECT sp_id, sp_name, sp_cat, sp_summery FROM service_tab";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $service_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $service_arr[$i]['sp_id'] = $this->encrypt->encode($row['sp_id']);
                $service_arr[$i]['sp_name'] = $row['sp_name'];
                $service_arr[$i]['sp_cat'] = $row['sp_cat'];
                $service_arr[$i]['sp_summery'] = $row['sp_summery'];
                $i = $i + 1;
            }
            $json_str = json_encode($service_arr);
            return $json_str;
        }
    }

    public function get_single_service($sp_id) {
        $sql = "SELECT sp_name, sp_cat, sp_summery, sp_cost, sp_phn, sp_key, sp_list, sp_add, sp_hrs, sp_after_phn, sp_email, sp_img, sp_staff_img, sp_web, sp_level FROM service_tab WHERE sp_id=" . $sp_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $srv_arr = array();
            foreach ($query->result_array() as $row) {
                $srv_arr[] = $row;
            }
            return $srv_arr;
        }
    }

    public function edit_service($data, $sp_id) {
        $sql = "UPDATE service_tab SET "
                . "sp_name='" . $this->db->escape_str($data['sp_name']) . "',"
                . "sp_cat='" . $data['sp_cat'] . "',"
                . "sp_summery='" . $this->db->escape_str($data['sp_summery']) . "',"
                . "sp_cost='" . $data['sp_cost'] . "',"
                . "sp_phn='" . $data['sp_phn'] . "',"
                . "sp_key='" . $this->db->escape_str($data['sp_key']) . "',"
                . "sp_list='" . $this->db->escape_str($data['sp_list']) . "',"
                . "sp_add='" . $data['sp_add'] . "',"
                . "sp_hrs='" . $data['sp_hrs'] . "',"
                . "sp_after_phn='" . $data['sp_after_phn'] . "',"
                . "sp_email='" . $data['sp_email'] . "',"
                . "sp_img='" . $data['sp_img'] . "',"
                . "sp_staff_img='" . $data['sp_staff_img'] . "',"
                . "sp_web='" . $data['sp_web'] . "',"
                . "sp_level='" . $data['sp_level'] . "',"
                . "sp_mod_by='" . $this->session->userdata('username') . "',"
                . "sp_mod_role='" . $this->session->userdata('user_role') . "',"
                . "sp_mod_date=NOW() WHERE sp_id=" . $sp_id . "";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function load_services_according_survey() {
        $sql = "SELECT DISTINCT(s2.sp_id),s2.sp_name,s2.sp_img,s2.sp_summery,w1.wellness_sec_name FROM survey_answers s "
                . "JOIN survey_map s1 ON s1.survey_map_id = s.survey_no_ref "
                . "JOIN wellness_quesions w ON  w.wellness_quesions_id = s.survey_question_ref "
                . "JOIN wellnes_sections w1 ON w1.wellness_sec_id = w.wellness_quesions_sec_ref "
                . "JOIN service_tab s2 ON  s2.sp_cat = w1.wellness_sec_id "
                . "WHERE s1.survey_map_user='" . $this->session->userdata('username') . "' AND (s.survey_ans=5 AND s2.sp_level=1) UNION "
                . "SELECT DISTINCT(s2.sp_id),s2.sp_name,s2.sp_img,s2.sp_summery,w1.wellness_sec_name FROM survey_answers s "
                . "JOIN survey_map s1 ON s1.survey_map_id = s.survey_no_ref "
                . "JOIN wellness_quesions w ON  w.wellness_quesions_id = s.survey_question_ref "
                . "JOIN wellnes_sections w1 ON w1.wellness_sec_id = w.wellness_quesions_sec_ref "
                . "JOIN service_tab s2 ON  s2.sp_cat = w1.wellness_sec_id "
                . "WHERE s1.survey_map_user='" . $this->session->userdata('username') . "' AND (s.survey_ans=3 AND s2.sp_level=2)";
        $query = $this->db->query($sql);
        $service_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $service_arr[$i]['sp_id'] = $this->encrypt->encode($row['sp_id']);
                $service_arr[$i]['sp_name'] = $row['sp_name'];
                $service_arr[$i]['sp_img'] = $row['sp_img'];
                //$pos = strpos($row['sp_summery'], ' ', 100);
                $service_arr[$i]['sp_summery'] = $row['sp_summery'];
                $service_arr[$i]['wellness_sec_name'] = $row['wellness_sec_name'];
                $i++;
            }
            return $service_arr;
        } else {
            return $service_arr;
        }
    }

    public function get_service_email($sid) {
        $sql = "SELECT sp_email FROM service_tab WHERE sp_id = $sid";
        $query = $this->db->query($sql);
        $result_arr = array();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result_arr['sp_email'] = $row['sp_email'];
            }
        } else {
            $result_arr = FALSE;
        }
        return $result_arr;
    }

    public function send_email($data) {
            $email_data['from'] = $data['body_email'];
            $email_data['name'] = $data['body_email'];
            $email_data['to'] = $data['to_email'];
            $email_data['subject'] = 'Member Inquiry';
            
            $dt['body_subject'] = $data['body_subject'];
            $dt['body_message'] = $data['body_message'];
            $dt['body_from'] = $data['body_email'];
            $dt['body_telephone'] = $data['body_telephone'];
            $dt['body_email'] = $data['body_email'];
            
            $email_data['msg'] = $this->load->view('email_templates/service_message_temp', $dt, TRUE);
            //$this->load->view('email_templates/service_message_temp', $dt);
            $rtn = email_notification($email_data);
            return $rtn;
    }

}
