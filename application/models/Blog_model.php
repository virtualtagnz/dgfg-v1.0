<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }

    public function blog_view_all($pg) {
        $sql = "SELECT blog_id, blog_name, blog_body, blog_image, DATE_FORMAT(blog_created_date, '%D %M %Y') AS blog_created_date FROM blog_tab WHERE is_approved = ? ORDER BY blog_created_date DESC";
        $query = $this->db->query($sql, array(1));
        return $query->result_array();
    }

    public function blog_admin_view_all() {
        $sql = "SELECT blog_id, blog_name, CONCAT(SUBSTRING(blog_body,1,75),'...') AS blog_body, blog_image, blog_created_by, blog_created_date, blog_modified_by, blog_modified_date, is_approved FROM blog_tab WHERE is_approved IS NULL ORDER BY blog_created_date DESC";
        $query = $this->db->query($sql, array(1));
        if ($query->num_rows() > 0) {
            $blog_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $blog_arr[$i]['blog_name'] = $row['blog_name'];
                $blog_arr[$i]['blog_body'] = $row['blog_body'];
                $blog_arr[$i]['blog_created_by'] = $row['blog_created_by'];
                $blog_arr[$i]['blog_created_date'] = $row['blog_created_date'];
                $blog_arr[$i]['blog_modified_by'] = $row['blog_modified_by'];
                $blog_arr[$i]['blog_modified_date'] = $row['blog_modified_date'];
                $blog_arr[$i]['blog_id'] = $row['blog_id'];
                $blog_arr[$i]['blog_id'] = $row['blog_id'];
                $blog_arr[$i]['is_approved'] = $row['is_approved'];
                $i++;
            }
            $json_str = json_encode($blog_arr);
            return $json_str;
        }
    }

    public function blog_view_single($id) {
        $sql = "SELECT * FROM blog_tab WHERE blog_id = $id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function blog_add($data) {
        $sql = "INSERT INTO blog_tab(blog_name, blog_body, blog_image, blog_created_by, blog_created_role, blog_created_date) "
                . "VALUES('" . $data['title'] . "','" . $data['body'] . "','" . $data['img'] . "','" . $data['createdby'] . "'," . $data['createdrole'] . ",'" . $data['date'] . "')";

        $sql_notify = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";
        $notif = array();
        $date = date("Y-m-d H:i:s");
        $user = $this->session->userdata('username');
        $sender_id = get_user_id_by_user_name($user);
        $notif['notification_recive_role'] = 2;
        $notif['notification_receive_id'] = 1;
        $notif['notification_sent_role'] = $this->session->userdata('user_role');
        $notif['notification_sent_id'] = $sender_id;
        $notif['notification_sent_date'] = $date;
        $notif['is_watch'] = "N";
        $notif['notification_msg'] = "Your have a new blog-post to approve";

        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->query($sql_notify, $notif);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function blog_toggle_approve($id) {
        $return = FALSE;
        $blog_created_user = "";
        $blog_created_role = "";
        $sql3 = "SELECT blog_created_by, blog_created_role FROM blog_tab WHERE blog_id = $id";
        $query = $this->db->query($sql3);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $blog_created_user = $row['blog_created_by'];
                $blog_created_role = $row['blog_created_role'];
            }
            $sql1 = "UPDATE blog_tab SET is_approved = 1,  blog_modified_by = ?, blog_modified_role = ?, blog_modified_date = ? WHERE blog_id = ? AND is_approved IS NULL";
            $sql2 = "INSERT INTO rewards(re_user_role, re_user_id, reward_des, reward, reward_date) VALUES (?,?,?,?,?)";

            $sql4 = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";

            $user = $this->session->userdata('username');
            $role = $this->session->userdata('user_role');
            $date = date("Y-m-d H:i:s");
            $author_id = get_memeber_id_by_user_name($blog_created_user, $blog_created_role);
            $coor_id = get_user_id_by_user_name($user);

            $reward['re_user_role'] = $blog_created_role;
            $reward['re_user_id'] = $author_id;
            $reward['reward_des'] = 'Blog Post Added (id) - ' . $id;
            $reward['reward'] = 10;
            $reward['reward_date'] = $date;

            $notif['notification_recive_role'] = $blog_created_role;
            $notif['notification_receive_id'] = $author_id;
            $notif['notification_sent_role'] = $this->session->userdata('user_role');
            $notif['notification_sent_id'] = $coor_id;
            $notif['notification_sent_date'] = $date;
            $notif['is_watch'] = "N";
            $notif['notification_msg'] = "Your blog post has been approved";

            $this->db->trans_start();
            $this->db->query($sql1, array($user, $role, $date, $id));
            $this->db->query($sql2, $reward);
            $this->db->query($sql4, $notif);
            $this->db->trans_complete();

            if ($this->db->trans_status() === TRUE) {
                $return = TRUE;
            } else {
                $return = FALSE;
            }
        }
        return $return;
    }

    public function blog_toggle_reject_post($id) {
        $return = FALSE;
        $sql = "UPDATE blog_tab SET is_approved = 0 WHERE blog_id = $id AND is_approved IS NULL";
        $sql_notify = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";
        $notif = array();
        $blog_created_user = "";
        $blog_created_role = "";
        //get blog writer
        $sql_author = "SELECT blog_created_by, blog_created_role FROM blog_tab WHERE blog_id = $id";
        $query = $this->db->query($sql_author);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $blog_created_user = $row['blog_created_by'];
                $blog_created_role = $row['blog_created_role'];
            }
        }
        //get blog writer's id
        $recivers_id = get_memeber_id_by_user_name($blog_created_user, $blog_created_role);
        $user = $this->session->userdata('username');
        $sender_id = get_user_id_by_user_name($user);
        $date = date("Y-m-d H:i:s");
        $notif['notification_recive_role'] = $blog_created_role;
        $notif['notification_receive_id'] = $recivers_id;
        $notif['notification_sent_role'] = $this->session->userdata('user_role');
        $notif['notification_sent_id'] = $sender_id;
        $notif['notification_sent_date'] = $date;
        $notif['is_watch'] = "N";
        $notif['notification_msg'] = "Your blog post has been rejected";

        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->query($sql_notify, $notif);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            $return = TRUE;
        }
        return $return;
    }

}
