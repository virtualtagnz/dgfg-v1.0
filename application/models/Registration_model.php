<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }

    public function register_user($data) {
        $conf_key = md5(uniqid(rand()));
        $reg = array();
        $reg['reg_id'] = getMaxId("reg_id", "registration_tab");
        $reg['reg_email'] = $data['email'];
        $reg['reg_pwd'] = $data['pwd'];
        $reg['reg_fname'] = $data['fname'];
        $reg['reg_lname'] = $data['lname'];
        $reg['conf_code'] = $conf_key;
        $date = new DateTime();
        $reg['date_registered'] = $date->format('Y-m-d H:i:s');
        $reg['is_confirm'] = 0;
        $reg['conf_date'] = NULL;

        $this->db->trans_begin();
        $this->db->insert('registration_tab', $reg);

        if ($this->db->trans_status() === TRUE) {
            $email_data['from'] = 'vtag.test@gmail.com';
            $email_data['name'] = 'DGFG Team';
            $email_data['to'] = $data['email'];
            $email_data['subject'] = 'Welcome to DGFG';
            $dt['uname'] = $data['fname'] . ' ' . $data['lname'];
            $dt['ckey'] = $conf_key;
            $email_data['msg'] = $this->load->view('email_templates/new_registration_temp', $dt, TRUE);
            $rtn = email_notification($email_data);
            if ($rtn) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $this->db->trans_rollback();
        }
    }

    public function conf_key_compare($param) {
        $msg = "";
        $sql = "SELECT conf_code,reg_fname,reg_lname,is_confirm FROM registration_tab r WHERE r.reg_email='" . $param['uname'] . "' AND r.reg_pwd='" . $param['pwd'] . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            if ($row['is_confirm']) {
                $msg = "01ACF";
            } else {
                if ($row['conf_code'] == $param['conf_key']) {
                    $user_id = getMaxId("user_id", "user_tab");
                    $mem_id = getMaxId("mem_id", "dgfg_member_tab");

                    $sql1 = "INSERT INTO user_tab(user_id, user_role_ref, user_email, user_pwd, active_stat, date_create, role_create, rcode_create, date_modify, role_mod, rcode_mod) "
                            . "VALUES (" . $user_id . ",4,'" . $param['uname'] . "','" . $param['pwd'] . "','1',NOW(),'" . $param['uname'] . "',4,NOW(),'" . $param['uname'] . "',4)";

                    $sql2 = "INSERT INTO dgfg_member_tab(mem_id, mem_user_ref, mem_fname, mem_lname, mem_gender, mem_mobile, mem_img_link, mem_description, mem_fb_link, mem_ig_link, mem_ethnicity, create_by, role_create, date_create, mod_by, mod_role, mod_date) "
                            . "VALUES (" . $mem_id . "," . $user_id . ",'" . $row['reg_fname'] . "','" . $row['reg_lname'] . "','-','-','-','-','-','-','-','" . $param['uname'] . "',4,NOW(),'" . $param['uname'] . "',4,NOW())";

                    $sql3 = "UPDATE registration_tab SET is_confirm=1,conf_date=NOW() WHERE reg_email = '" . $param['uname'] . "' AND conf_code='" . $row['conf_code'] . "'";

                    $this->db->trans_start();
                    $this->db->query($sql1);
                    $this->db->query($sql2);
                    $this->db->query($sql3);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === TRUE) {
                        $msg = "01CFT";
                    } else {
                        $msg = "01CFF";
                    }
                }  else {
                    $msg = "01CFE";
                }
            }
        }
        return $msg;
    }

}
