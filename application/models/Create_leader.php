<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_leader extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function addLeader($user_data) {
        $this->load->helper('db_helper');
        $led_id = getMaxId("led_id", "dgfg_leader_tab");
        $user_id = getMaxId("user_id", "user_tab");
        $sql1 = "INSERT INTO user_tab(user_id, user_role_ref, user_email, user_pwd, active_stat, date_create, role_create, rcode_create, date_modify, role_mod, rcode_mod) "
                . "VALUES (" . $user_id . ",3,'" . $user_data['email'] . "','" . $user_data['pwd'] . "','" . $user_data['stat'] . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "')";
        $sql2 = "INSERT INTO dgfg_leader_tab(led_id, led_user_ref, led_fname, led_lname, led_gender, led_mobile, led_img_link, led_description, led_fb_link, led_ig_link, led_ethnicity, create_by, role_create, date_create, mod_by, mod_role, mod_date) "
                . "VALUES (" . $led_id . "," . $user_id . ",'" . $user_data['fname'] . "','" . $user_data['lname'] . "','" . $user_data['gender'] . "','" . $user_data['mob'] . "','" . $user_data['prof_img'] . "','" . $user_data['des'] . "','" . $user_data['fb'] . "','" . $user_data['inst'] . "','" . $user_data['ethnicity'] . "','" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW())";

        $this->db->trans_start();
        $this->db->query($sql1);
        $this->db->query($sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getLeader() {
        $select_sql = "SELECT d.led_id,concat(d.led_fname,' ',d.led_lname) as u_name,d.led_img_link,CASE WHEN (u.active_stat = '1') THEN 'Active' ELSE 'Deactive' END AS act_stat "
                . "FROM dgfg_leader_tab d JOIN user_tab u ON d.led_user_ref=u.user_id";
        $query = $this->db->query($select_sql);
        if ($query->num_rows() > 0) {
            $cod_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $cod_arr[$i]['uid'] = $row['led_id'];
                $cod_arr[$i]['u_name'] = $row['u_name'];
                $cod_arr[$i]['img'] = $row['led_img_link'];
                $cod_arr[$i]['act_stat'] = $row['act_stat'];
                $i = $i + 1;
            }
            $json_str = json_encode($cod_arr);
            return $json_str;
        }
    }

    public function getSingleLeader($led_id) {
        $sql = "SELECT u.user_email,u.active_stat,d.led_fname,d.led_lname,d.led_gender,d.led_mobile,d.led_img_link,d.led_description,d.led_fb_link,d.led_ig_link,d.led_ethnicity "
                . "FROM dgfg_leader_tab d JOIN user_tab u WHERE u.user_id = d.led_user_ref AND d.led_id=" . $led_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $cod_arr2 = array();
            foreach ($query->result_array() as $row) {
//                  $cod_arr2[] = $row;
                $cod_arr2['uemail'] = $row['user_email'];
                $cod_arr2['act_stat'] = $row['active_stat'];
                $cod_arr2['fname'] = $row['led_fname'];
                $cod_arr2['lname'] = $row['led_lname'];
                $cod_arr2['gender'] = $row['led_gender'];
                $cod_arr2['mob'] = $row['led_mobile'];
                $cod_arr2['img'] = $row['led_img_link'];
                $cod_arr2['des'] = $row['led_description'];
                $cod_arr2['fb'] = $row['led_fb_link'];
                $cod_arr2['inst'] = $row['led_ig_link'];
                $cod_arr2['sm'] = $row['led_ethnicity'];
            }
            return $cod_arr2;
        }
    }

    public function modify_leader($mod_info) {
        $this->load->helper('db_helper');
        $cod_id = get_column("led_user_ref", "led_id", "dgfg_leader_tab", $mod_info['uid']);
        $mod_sql1 = "UPDATE user_tab SET "
                . "user_email='" . $mod_info['email'] . "',"
                . "user_pwd='" . $mod_info['pwd'] . "',"
                . "active_stat='" . $mod_info['stat'] . "',"
                . "date_modify=NOW(),"
                . "role_mod='" . $this->session->userdata('username') . "',"
                . "rcode_mod='" . $this->session->userdata('user_role') . "' WHERE user_id=" . $cod_id . "";

        $mod_sql2 = "UPDATE dgfg_leader_tab SET "
                . "led_fname='" . $mod_info['fname'] . "',"
                . "led_lname='" . $mod_info['lname'] . "',"
                . "led_gender='" . $mod_info['gender'] . "',"
                . "led_mobile='" . $mod_info['mob'] . "',"
                . "led_img_link='" . $mod_info['prof_img'] . "',"
                . "led_description='" . $mod_info['des'] . "',"
                . "led_fb_link='" . $mod_info['fb'] . "',"
                . "led_ig_link='" . $mod_info['inst'] . "',"
                . "led_ethnicity ='" . $mod_info['ethnicity'] . "',"
                . "mod_by='" . $this->session->userdata('username') . "',"
                . "mod_role='" . $this->session->userdata('user_role') . "',"
                . "mod_date=NOW() WHERE led_id=" . $mod_info['uid'] . "";

        $this->db->trans_start();
        $this->db->query($mod_sql1);
        $this->db->query($mod_sql2);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function leader_contribute_event($eid) {
        $sql = "SELECT concat(d.led_fname,' ',d.led_lname) AS led_name FROM events_role_map e INNER JOIN event_tab e1 "
                . "ON e.event_id_ref = e1.event_id INNER JOIN dgfg_leader_tab d "
                . "ON e.role_id_ref = d.led_id "
                . "WHERE e.event_id_ref=" . $eid . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->led_name;
        }
    }

    public function get_leader_event_created($eid) {
        $sql = "SELECT d.led_id,u.user_role_ref FROM dgfg_leader_tab d "
                . "JOIN user_tab u ON d.led_user_ref = u.user_id "
                . "JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "WHERE u.user_email IN (SELECT event_create_by FROM event_tab e WHERE event_id = " . $eid . ")";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $arr =array();
            foreach ($query->result_array() as $row) {
                $arr[] = $row;
            }
            return $arr;
        }
    }
    
    public function get_leader_invited_events($eid,$mem_id) {
        $sql = "SELECT d.led_id,u.user_role_ref FROM dgfg_leader_tab d "
                . "JOIN user_tab u ON d.led_user_ref = u.user_id "
                . "JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "WHERE u.user_email IN (SELECT i.ev_invite_by FROM event_invitation i WHERE i.ev_invite_event_ref=".$eid." AND i.ev_invite_mem_ref=".$mem_id.")";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $arr =array();
            foreach ($query->result_array() as $row) {
                $arr[] = $row;
            }
            return $arr;
        }
    }
    
    public function get_leader_assign_task($eid) {
        $sql = "SELECT d.led_id,u.user_role_ref FROM dgfg_leader_tab d "
                . "JOIN user_tab u ON d.led_user_ref = u.user_id "
                . "JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "WHERE u.user_email IN (SELECT DISTINCT(a.assign_by) FROM assign_events_tab a WHERE a.aev_event_ref=".$eid.")";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $arr =array();
            foreach ($query->result_array() as $row) {
                $arr[] = $row;
            }
            return $arr;
        }
    }

}
