<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function login_dgfg($data) {
        $stat;
        $sql = "SELECT u.user_email,u.user_pwd,u.user_role_ref,u.active_stat FROM user_tab u,user_roles u1 "
                . "WHERE u.user_email='" . $data['user_name'] . "' AND u1.role_code = u.user_role_ref";
        $result = $this->db->query($sql);
        $row = $result->row();
        if ($result->num_rows() === 1) {
            if ($row->active_stat) {
                if ($row->user_pwd === md5($data['user_pwd'])) {
                    $newdata = array(
                        'username' => $row->user_email,
                        'user_role' => $row->user_role_ref,
                        'logged_in' => TRUE
                    );
                    $this->session->set_userdata($newdata);
                    return $stat = 'sux';
                } else {
                    return $stat = 'unsux';
                }
            } else {
                return $stat = 'inacx';
            }
        }
    }

    public function login_fb_dgfg_exists($data) {
        $stat;
        $sql = "SELECT user_email, user_role_ref FROM user_tab "
                . "WHERE user_email='" . $data['email'] . "'";
        //error_log($data['user_profile']['email']);
        $result = $this->db->query($sql);
        $row = $result->row();
        if ($result->num_rows() === 1) {
            $newdata = array(
                'username' => $row->user_email,
                'user_role' => $row->user_role_ref,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            $stat = TRUE;
        } else {
            $stat = FALSE;
        }
        return $stat;
    }

    public function get_user_img($uemail, $role) {
        $sql = '';
        if ($role === '2') {
            $sql = "SELECT d.cod_img_link AS prof_img,concat(d.cod_fname,' ',d.cod_lname) AS uname FROM dgfg_coordinator_tab d JOIN user_tab u JOIN user_roles u1 "
                    . "WHERE u.user_id = d.cod_user_ref AND u1.role_code = u.user_role_ref AND u.user_email='" . $uemail . "' AND u1.role_code=" . $role . "";
        } elseif ($role === '3') {
            $sql = "SELECT d.led_img_link AS prof_img,concat(d.led_fname,' ',d.led_lname) AS uname FROM dgfg_leader_tab d JOIN user_tab u JOIN user_roles u1 "
                    . "WHERE u.user_id = d.led_user_ref AND u1.role_code = u.user_role_ref AND u.user_email='" . $uemail . "' AND u1.role_code=" . $role . "";
        } elseif ($role === '4') {
            $sql = "SELECT d.mem_img_link AS prof_img,concat(d.mem_fname,' ',d.mem_lname) AS uname FROM dgfg_member_tab d JOIN user_tab u JOIN user_roles u1 "
                    . "WHERE u.user_id = d.mem_user_ref AND u1.role_code = u.user_role_ref AND u.user_email='" . $uemail . "' AND u1.role_code=" . $role . "";
        }

        $result = $this->db->query($sql);
        $row = $result->row();
        if ($result->num_rows() === 1) {
            $this->session->set_userdata('user_img', $row->prof_img);
            $this->session->set_userdata('real_name', $row->uname);
        }
    }
    
}
