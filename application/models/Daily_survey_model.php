<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daily_survey_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
    }

    public function create_daily_survey($data) {
        $dsurvery_id = getMaxId("daily_survey_id", "daily_survey_main");

        $main_arr = array();
        $qa_arr = array();

        $main_arr['daily_survey_id'] = $dsurvery_id;
        $main_arr['daily_survey_name'] = $data['dsurvey_name'];
        $main_arr['daily_survey_date'] = $data['dsurvey_date'];
        $main_arr['daily_survey_points'] = $data['dsurvey_points'];
        $main_arr['created_by'] = $this->session->userdata('username');
        $date = new DateTime();
        $main_arr['create_date'] = $date->format('Y-m-d H:i:s');

        $qa_list = json_decode($data['dsurvey_json']);
        if (count($qa_list) > 0) {
            $i = 0;
            foreach ($qa_list as $value) {
                $temp['daily_survey_main_ref'] = $dsurvery_id;
                $temp['question'] = $value->col1;
                $temp['ans1'] = $value->col2;
                $temp['ans2'] = $value->col3;
                $temp['ans3'] = $value->col4;
                $temp['ans4'] = $value->col5;
                $qa_arr[$i] = $temp;
                $i++;
            }
        }
        $this->db->trans_start();
        $this->db->insert('daily_survey_main', $main_arr);
        $this->db->insert_batch('daily_survey_qa', $qa_arr);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_all_daily_surveys() {
        $sql = "SELECT d.daily_survey_id,d.daily_survey_name,DATE_FORMAT(d.daily_survey_date,'%d-%m-%Y') AS daily_survey_date FROM daily_survey_main d";
        $query = $this->db->query($sql);
        $dsur_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $dsur_arr[$i]['daily_survey_id'] = $this->encrypt->encode($row['daily_survey_id']);
                $dsur_arr[$i]['daily_survey_name'] = $row['daily_survey_name'];
                $dsur_arr[$i]['daily_survey_date'] = $row['daily_survey_date'];
                $i++;
            }
            $json_str = json_encode($dsur_arr);
            return $json_str;
        }
    }

    public function get_single_daily_survey($sur_id) {
        $sql = "SELECT daily_survey_name, daily_survey_date, daily_survey_points FROM daily_survey_main WHERE daily_survey_id = " . $sur_id . "";
        $query = $this->db->query($sql);
        $main_sur_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $main_sur_arr['s_name'] = $row['daily_survey_name'];
                $main_sur_arr['s_date'] = $row['daily_survey_date'];
                $main_sur_arr['s_points'] = $row['daily_survey_points'];
            }
            return $main_sur_arr;
        }
    }

    public function get_survey_qa($sur_id) {
        $sql = "SELECT qa_id, question, ans1, ans2, ans3, ans4 FROM daily_survey_qa WHERE daily_survey_main_ref = " . $sur_id . "";
        $query = $this->db->query($sql);
        $sur_qa_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $sur_qa_arr[$i]['qa_id'] = $row['qa_id'];
                $sur_qa_arr[$i]['question'] = $row['question'];
                $sur_qa_arr[$i]['ans1'] = $row['ans1'];
                $sur_qa_arr[$i]['ans2'] = $row['ans2'];
                $sur_qa_arr[$i]['ans3'] = $row['ans3'];
                $sur_qa_arr[$i]['ans4'] = $row['ans4'];
                $i = $i + 1;
            }
            $json_str = json_encode($sur_qa_arr);
            return $json_str;
        }
    }

    public function edit_daily_survey($data, $sid) {
//        $sql = "UPDATE daily_survey_main SET "
//                . "daily_survey_name='".$data['']."',"
//                . "daily_survey_date=[value-3],"
//                . "daily_survey_points=[value-4],"
//                . "mod_by=[value-7],"
//                . "mod_date=[value-8] "
//                . "WHERE daily_survey_id";
        $main_arr = array();
        $qa_arr = array();

        $main_arr['daily_survey_name'] = $data['dsurvey_name'];
        $main_arr['daily_survey_date'] = $data['dsurvey_date'];
        $main_arr['daily_survey_points'] = $data['dsurvey_points'];
        $main_arr['created_by'] = $this->session->userdata('username');
        $date = new DateTime();
        $main_arr['create_date'] = $date->format('Y-m-d H:i:s');

        $qa_list = json_decode($data['dsurvey_json']);
        if (count($qa_list) > 0) {
            $i = 0;
            foreach ($qa_list as $value) {
                $temp['daily_survey_main_ref'] = $sid;
                $temp['question'] = $value->col1;
                $temp['ans1'] = $value->col2;
                $temp['ans2'] = $value->col3;
                $temp['ans3'] = $value->col4;
                $temp['ans4'] = $value->col5;
                $qa_arr[$i] = $temp;
                $i++;
            }
        }
        $this->db->trans_start();
        $this->db->update('daily_survey_main', $main_arr, array('daily_survey_id' => $sid));
        if (count($qa_arr) > 0) {
            $this->db->delete('daily_survey_qa', array('daily_survey_main_ref' => $sid));
            $this->db->insert_batch('daily_survey_qa', $qa_arr);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_questions_count($sid) {
        $sql = "SELECT count(d.qa_id) AS cnt_per_q FROM dgfg_db.daily_survey_qa d WHERE d.daily_survey_main_ref=" . $sid . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['cnt_per_q'];
        }
    }

    public function get_daily_survey_points($sur_id) {
        $sql = "SELECT daily_survey_points FROM daily_survey_main WHERE daily_survey_id=" . $sur_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['daily_survey_points'];
        }
    }

    public function save_daily_survey_answers($data, $sid) {
        $map_id = getMaxId("ds_map_id", "daily_survey_map");
        $map_arr = array();
        $ans_arr = array();
        $reward_arr = array();

        $map_arr['ds_map_id'] = $map_id;
        $map_arr['ds_map_survey_ref'] = $sid;
        $map_arr['ds_map_user'] = $this->session->userdata('username');
        $date = new DateTime();
        $map_arr['ds_map_sur_date'] = $date->format('Y-m-d H:i:s');

        $i = 0;
        foreach ($data as $value) {
            $ans_arr[$i]['ds_ans_main_ref'] = $map_id;
            $ans_arr[$i]['ds_ans_question_ref'] = $value['q'];
            $ans_arr[$i]['ds_ans'] = $value['a'];
            $i++;
        }

        $reward_arr['re_user_role'] = $this->session->userdata('user_role');
        $reward_arr['re_user_id'] = get_user_id_by_user_name($this->session->userdata('username'));
        ;
        $reward_arr['reward_des'] = 'Survey id: ' . $sid . ' Survey Done';
        $reward_arr['reward'] = $this->get_daily_survey_points($sid);
        $reward_arr['reward_date'] = $date->format('Y-m-d H:i:s');

        $this->db->trans_start();
        $this->db->insert('daily_survey_map', $map_arr);
        $this->db->insert_batch('daily_survey_answers', $ans_arr);
        $this->db->insert('rewards', $reward_arr);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_all_submitted_surveys() {
        $sql = "SELECT d.ds_map_survey_ref AS sid,d1.led_img_link AS img,concat(d1.led_fname,' ',d1.led_lname) AS uname, DATE_FORMAT(d.ds_map_sur_date,'%D %M %Y') AS dt,'SKWAD Leader' AS role, d.ds_map_user AS email FROM dgfg_db.daily_survey_map d "
                . "INNER JOIN dgfg_db.user_tab u ON d.ds_map_user = u.user_email "
                . "INNER JOIN dgfg_db.user_roles u1 ON u1.role_code = u.user_role_ref "
                . "INNER JOIN dgfg_db.dgfg_leader_tab d1 ON u.user_id = d1.led_user_ref "
                . "UNION "
                . "SELECT d.ds_map_survey_ref AS sid,d2.mem_img_link AS img,concat(d2.mem_fname,' ',d2.mem_lname) AS uname, DATE_FORMAT(d.ds_map_sur_date,'%D %M %Y') AS dt,'SKWAD Member' AS role, d.ds_map_user AS email FROM dgfg_db.daily_survey_map d "
                . "INNER JOIN dgfg_db.user_tab u ON d.ds_map_user = u.user_email "
                . "INNER JOIN dgfg_db.user_roles u1 ON u1.role_code = u.user_role_ref "
                . "INNER JOIN dgfg_db.dgfg_member_tab d2 ON u.user_id = d2.mem_user_ref";
        $query = $this->db->query($sql);
        $sub_sur_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $sub_sur_arr[$i]['sid'] = $this->encrypt->encode($row['sid']);
                $sub_sur_arr[$i]['img'] = $row['img'];
                $sub_sur_arr[$i]['uname'] = $row['uname'];
                $sub_sur_arr[$i]['dt'] = $row['dt'];
                $sub_sur_arr[$i]['role'] = $row['role'];
                $sub_sur_arr[$i]['email'] = $row['email'];
                $i = $i + 1;
            }
            $json_str = json_encode($sub_sur_arr);
            return $json_str;
        }
    }
    
    public function load_daily_surveys_answers($email,$id) {
        $sql = "SELECT d2.qa_id,d2.question,d2.ans1,d2.ans2,d2.ans3,d2.ans4,d.ds_ans FROM dgfg_db.daily_survey_answers d "
                . "INNER JOIN dgfg_db.daily_survey_map d1 ON d1.ds_map_id = d.ds_ans_main_ref "
                . "INNER JOIN dgfg_db.daily_survey_qa d2 ON d2.qa_id = d.ds_ans_question_ref "
                . "WHERE d1.ds_map_user='".$email."' AND d1.ds_map_survey_ref=".$id."";
        $query = $this->db->query($sql);
        $ans_arr = array();
        if ($query->num_rows() > 0) {
            $i=0;
            foreach ($query->result_array() as $row) {
                $ans_arr[$i]['qa_id'] = $row['qa_id'];
                $ans_arr[$i]['question'] = $row['question'];
                $ans_arr[$i]['ans1'] = $row['ans1'];
                $ans_arr[$i]['ans2'] = $row['ans2'];
                $ans_arr[$i]['ans3'] = $row['ans3'];
                $ans_arr[$i]['ans4'] = $row['ans4'];
                $ans_arr[$i]['ds_ans'] = $row['ds_ans'];
                $i = $i + 1;
            }
            $json_str = json_encode($ans_arr);
            return $json_str;
        }
    }

}
