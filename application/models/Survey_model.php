<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Survey_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_survey($sec_id) {
        $this->db->select('w.wellness_quesions_id,w.wellness_quesion,w.wellness_ans1,w.wellness_ans1_support,w.wellness_ans2,w.wellness_ans2_support,w.wellness_ans3,w.wellness_ans3_support,w.wellness_ans4,w.wellness_ans4_support,w.wellness_ans5,w.wellness_ans5_support');
        $this->db->from('wellness_quesions w');
        $this->db->join('wellnes_sections w1', 'w1.wellness_sec_id = w.wellness_quesions_sec_ref');
        $this->db->where('wellness_quesions_sec_ref', $sec_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $survey_arr = array();
            foreach ($query->result_array() as $row) {
                $survey_arr[] = $row;
            }
            return $survey_arr;
        }
    }

    public function get_survey_section() {
        $query = $this->db->get('wellnes_sections');
        if ($query->num_rows() > 0) {
            $sur_section_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['wellness_sec_name'] = $row['wellness_sec_name'];
                $temp['wellness_quesions'] = $this->get_survey($row['wellness_sec_id']);
                $sur_section_arr[$i] = $temp;
                $i++;
            }
            return $sur_section_arr;
        }
    }

    public function is_survey_done($u_email) {
        $sql = "SELECT count(s.survey_map_id) AS sur_cnt FROM survey_map s WHERE s.survey_map_user='" . $u_email . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['sur_cnt'];
        }
    }

    public function save_survey_data($data) {
        $this->load->helper('db_helper');
        $survey_id = getMaxId("survey_map_id", "survey_map");
        $date = new DateTime();
        $map_arr = array();
        $ans_arr = array();
        $map_arr['survey_map_id'] = $survey_id;
        $map_arr['survey_map_user'] = $this->session->userdata('username');
        $map_arr['survey_date'] = $date->format('Y-m-d H:i:s');

        $i = 0;
        foreach ($data as $value) {
            $temp['survey_no_ref'] = $survey_id;
            $temp['survey_question_ref'] = $i + 1;
            $temp['survey_ans'] = $value[0];
            $ans_arr[] = $temp;
            $i++;
        }

        $this->db->trans_start();
        $this->db->insert('survey_map', $map_arr);
        $this->db->insert_batch('survey_answers', $ans_arr);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
//            $email_data['from']=$this->session->userdata('username');
//            $email_data['name']=$this->session->userdata('real_name');
//            $email_data['to']='vtag.test@gmail.com';
//            $email_data['subject']='Testing Email';
//            $email_data['msg']='Testing Email Notification function';
//            $rtn=email_notification($email_data);
//            if($rtn){
//                return TRUE;
//            }else{
//                return FALSE;
//            }
            //send email
            $email_data['from'] = $this->session->userdata('username');
            $email_data['name'] = $this->session->userdata('real_name');
            $email_data['to'] = 'vtag.test@gmail.com';
            $email_data['subject'] = 'Wellness Survey Results';
            $dt['user'] = $this->session->userdata('real_name');
            $dt['critic_arr'] = $this->get_critical_area($this->session->userdata('username'), $survey_id);
            $email_data['msg'] = $this->load->view('email_templates/critical_area_temp', $dt, TRUE);
            $rtn = email_notification($email_data);
            $rtn = TRUE;
            $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
            $dn_rtn = dashboard_notification('2', '1', $this->session->userdata('user_role'), $sent_id, 'Wellness survey completed by: '.$this->session->userdata('real_name'));
            if ($rtn && $dn_rtn) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function get_critical_area($uname, $sid) {
        $sql = "SELECT DISTINCT(w1.wellness_sec_name) AS critical_sections FROM survey_answers s "
                . "INNER JOIN survey_map s1 ON s1.survey_map_id = s.survey_no_ref "
                . "INNER JOIN wellness_quesions w ON  w.wellness_quesions_id = s.survey_question_ref "
                . "INNER JOIN wellnes_sections w1 ON  w1.wellness_sec_id = w.wellness_quesions_sec_ref "
                . "WHERE s.survey_ans = '5' AND s1.survey_map_user='" . $uname . "' AND s1.survey_map_id=" . $sid . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $critical_sec = array();
            foreach ($query->result_array() as $row) {
                $critical_sec[] = $row['critical_sections'];
            }
            return $critical_sec;
        }
    }

    public function load_survey_id() {
        $sql = "SELECT s.survey_map_id FROM survey_map s WHERE s.survey_map_user='" . $this->session->userdata('username') . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['survey_map_id'];
        }
    }

    public function load_survey_result($sid) {
        $sql = "SELECT s.survey_question_ref,s.survey_ans FROM survey_answers s "
                . "JOIN survey_map s1 ON s.survey_no_ref = s1.survey_map_id "
                . "WHERE s1.survey_map_id=" . $sid . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $sel_arr = array();
            foreach ($query->result_array() as $row) {
                $sel_arr[$row['survey_question_ref']] = $row['survey_ans'];
            }
            return $sel_arr;
        }
    }

    public function load_submited_surveys($utype) {
        if ($utype === '3') {
            $sql = "SELECT concat(d.led_fname,' ',d.led_lname) AS name,s.survey_map_id, DATE_FORMAT(s.survey_date,'%Y-%m-%d') AS sub_date FROM user_tab u "
                    . "JOIN dgfg_leader_tab d ON u.user_id = d.led_user_ref "
                    . "JOIN survey_map s ON s.survey_map_user = u.user_email "
                    . "WHERE u.user_email IN (SELECT s.survey_map_user FROM survey_map s)";
        } elseif ($utype === '4') {
            $sql = "SELECT concat(d.mem_fname,' ',d.mem_lname) AS name,s.survey_map_id, DATE_FORMAT(s.survey_date,'%Y-%m-%d') AS sub_date FROM user_tab u "
                    . "JOIN dgfg_member_tab d  ON u.user_id = d.mem_user_ref "
                    . "JOIN survey_map s ON s.survey_map_user = u.user_email "
                    . "WHERE u.user_email IN (SELECT s.survey_map_user FROM survey_map s)";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $user_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $user_arr[$i]['survey_id'] = $this->encrypt->encode($row['survey_map_id']);
                $user_arr[$i]['user_name'] = $row['name'];
                $user_arr[$i]['sub_date'] = $row['sub_date'];
                $i = $i + 1;
            }
            $json_str = json_encode($user_arr);
            return $json_str;
        }
    }
    
    public function load_survey_section() {
        $query = $this->db->get('wellnes_sections');
        if ($query->num_rows() > 0) {
            $sur_section_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['wellness_sec_name'] = $row['wellness_sec_name'];
                $temp['wellness_sec_id'] = $row['wellness_sec_id'];
                $sur_section_arr[$i] = $temp;
                $i++;
            }
            return $sur_section_arr;
        }
    }

}
