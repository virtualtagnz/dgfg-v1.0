<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
        $this->load->model('create_leader');
    }

    public function create_event($event_data) {
        //$this->load->helper('db_helper');
        $event_id = getMaxId("event_id", "event_tab");
        $approve = array();
        if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') {
            $approve['stat'] = 'A';
            $approve['by'] = $this->session->userdata('username');
            $date = new DateTime();
            $approve['date'] = $date->format('Y-m-d H:i:s');;
            $approve['role'] = $this->session->userdata('user_role');
            $approve['ev_stat'] = 'A';
        } else {
            $approve['stat'] = 'P';
            $approve['by'] = "Pending";
            $approve['date'] = "";
            $approve['role'] = "";
            $approve['ev_stat'] = 'P';
        }
        $task_list = json_decode($event_data['tasks']);
        $task_ass = array();
        if (count($task_list) > 0) {
            $i = 0;
            foreach ($task_list as $value) {
                $temp['event_ref'] = $event_id;
                $temp['task_des'] = $this->db->escape_str($value->col1);
                $temp['due_date'] = $value->col2;
                $temp['task_point'] = $value->col3;
                $temp['task_stat'] = 'O';
                $temp['date_complete'] = '';
                $task_ass[$i] = $temp;
                $i++;
            }
        }
        $event_sql = "INSERT INTO event_tab(event_id, event_name, event_date, event_start, event_end, event_loc, event_promo1, event_promo2, event_promo3, event_promo4, event_plan, event_comments, event_video_linq, event_img_linq, event_stat, event_approve_stat, event_approved_by, event_approved_date, event_approved_role, event_create_by, event_create_role, event_create_date, event_mod_by, event_mod_role, event_mod_date) "
                . "VALUES (" . $event_id . ",'" . $this->db->escape_str($event_data['name']) . "','" . $event_data['dt'] . "','" . $event_data['stime'] . "','" . $event_data['etime'] . "','" . $this->db->escape_str($event_data['loc']) . "','" . $event_data['promo1'] . "','" . $event_data['promo2'] . "','" . $event_data['promo3'] . "','" . $event_data['promo4'] . "','" . $event_data['evplan'] . "','" . $this->db->escape_str($event_data['cmt']) . "','" . $event_data['evid'] . "','" . $event_data['img'] . "','" . $approve['ev_stat'] . "','" . $approve['stat'] . "','" . $approve['by'] . "','" . $approve['date'] . "','" . $approve['role'] . "','" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW(),'" . $this->session->userdata('username') . "','" . $this->session->userdata('user_role') . "',NOW())";


        $this->db->trans_start();
        $this->db->query($event_sql);
        $this->db->insert_batch('event_task_tab', $task_ass);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_event_data() {
        if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') {
            $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc,"
                    . "CASE WHEN event_approve_stat='A' THEN 'Approved' WHEN event_approve_stat='P' THEN 'Pending' WHEN event_approve_stat='R' THEN 'Rejected' END AS approve_stat,"
                    . "CASE WHEN event_stat='A' THEN 'Available' WHEN event_stat='P' THEN 'Pending' WHEN event_stat='C' THEN 'Completed' WHEN event_stat='N' THEN 'Not Available'  WHEN event_stat='S' THEN 'Processing' END AS ev_stat FROM event_tab";
        } else {
            $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc,"
                    . "CASE WHEN event_approve_stat='A' THEN 'Approved' WHEN event_approve_stat='P' THEN 'Pending' WHEN event_approve_stat='R' THEN 'Rejected' END AS approve_stat,"
                    . "CASE WHEN event_stat='A' THEN 'Available' WHEN event_stat='P' THEN 'Pending' WHEN event_stat='C' THEN 'Completed' WHEN event_stat='N' THEN 'Not Available'  WHEN event_stat='S' THEN 'Processing' END AS ev_stat FROM event_tab "
                    . "WHERE event_create_by='" . $this->session->userdata('username') . "'";
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $event_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $event_arr[$i]['event_id'] = $this->encrypt->encode($row['event_id']);
                $event_arr[$i]['event_name'] = $row['event_name'];
                $event_arr[$i]['event_date'] = $row['event_date'];
                $event_arr[$i]['event_start'] = $row['event_start'];
                $event_arr[$i]['event_end'] = $row['event_end'];
                $event_arr[$i]['event_loc'] = $row['event_loc'];
                $event_arr[$i]['approve_stat'] = $row['approve_stat'];
                $event_arr[$i]['ev_stat'] = $row['ev_stat'];
                $i = $i + 1;
            }
            $json_str = json_encode($event_arr);
            return $json_str;
        }
    }

    public function get_event_dates() {
        $sql = "SELECT e.event_id AS id,DATE_FORMAT(e.event_date,'%Y-%m-%d') AS start,DATE_FORMAT(e.event_date,'%Y-%m-%d') AS end,e.event_name AS title, "
                . "CASE e.event_stat WHEN 'C' THEN 'green' WHEN 'S' THEN 'blue' WHEN 'N' THEN 'red' END AS color "
                . "FROM event_tab e";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $event_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $event_arr[$i]['id'] = $this->encrypt->encode($row['id']);
                $event_arr[$i]['start'] = $row['start'];
                $event_arr[$i]['end'] = $row['end'];
                $event_arr[$i]['title'] = $row['title'];
                $event_arr[$i]['color'] = $row['color'];
                $i++;
            }
            $json_str = json_encode($event_arr);
            return $json_str;
        }
    }

    public function get_single_event($event_id) {
        $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc, event_promo1, event_promo2, event_promo3, event_promo4, event_plan, event_comments, event_video_linq, event_img_linq "
                . "FROM event_tab WHERE event_id=" . $event_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $event_arr = array();
            foreach ($query->result_array() as $row) {
                $event_arr['event_id'] = $row['event_id'];
                $event_arr['event_name'] = $row['event_name'];
                $event_arr['event_date'] = $row['event_date'];
                $event_arr['event_start'] = $row['event_start'];
                $event_arr['event_end'] = $row['event_end'];
                $event_arr['event_loc'] = $row['event_loc'];
                $event_arr['event_promo1'] = $row['event_promo1'];
                $event_arr['event_promo2'] = $row['event_promo2'];
                $event_arr['event_promo3'] = $row['event_promo3'];
                $event_arr['event_promo4'] = $row['event_promo4'];
                $event_arr['event_plan'] = $row['event_plan'];
                $event_arr['event_comments'] = $row['event_comments'];
                $event_arr['event_video_linq'] = $row['event_video_linq'];
                $event_arr['event_img_linq'] = $row['event_img_linq'];
            }
            return $event_arr;
        }
    }

    public function get_single_task_list($event_id) {
        $sql = "SELECT task_des, DATE_FORMAT(due_date,'%Y-%m-%d') AS due_date,task_point FROM event_task_tab WHERE event_ref=" . $event_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $task_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $task_arr[$i]['task_des'] = $row['task_des'];
                $task_arr[$i]['due_date'] = $row['due_date'];
                $task_arr[$i]['task_point'] = $row['task_point'];
                $i = $i + 1;
            }
            $json_str = json_encode($task_arr);
            return $json_str;
        }
    }

    public function modify_event($event_data, $event_id) {
        $sql = "UPDATE event_tab SET "
                . "event_name='" . $this->db->escape_str($event_data['event_name']) . "',"
                . "event_date='" . $event_data['event_date'] . "',"
                . "event_start='" . $event_data['event_start'] . "',"
                . "event_end='" . $event_data['event_end'] . "',"
                . "event_loc='" . $this->db->escape_str($event_data['event_loc']) . "',"
                . "event_promo1='" . $event_data['event_promo1'] . "',"
                . "event_promo2='" . $event_data['event_promo2'] . "',"
                . "event_promo3='" . $event_data['event_promo3'] . "',"
                . "event_promo4='" . $event_data['event_promo4'] . "',"
                . "event_plan='" . $event_data['event_plan'] . "',"
                . "event_comments='" . $this->db->escape_str($event_data['event_comments']) . "',"
                . "event_video_linq='" . $event_data['event_video_linq'] . "',"
                . "event_img_linq='" . $event_data['event_img_linq'] . "',"
                . "event_mod_by='" . $this->session->userdata('username') . "',"
                . "event_mod_role='" . $this->session->userdata('user_role') . "',"
                . "event_mod_date=NOW() WHERE event_id=" . $event_id . "";

        $task_list = json_decode($event_data['tasks']);
        $task_ass = array();
        if (count($task_list) > 0) {
            $i = 0;
            foreach ($task_list as $value) {
                $temp['event_ref'] = $event_id;
                $temp['task_des'] = $this->db->escape_str($value->col1);
                $temp['due_date'] = $value->col2;
                $temp['task_point'] = $value->col3;
                $temp['task_stat'] = 'O';
                $temp['date_complete'] = '';
                $task_ass[$i] = $temp;
                $i++;
            }
        }
        $this->db->trans_start();
        $this->db->query($sql);
        if (count($task_ass) > 0) {
            if ($this->delete_event_tasks($event_id)) {
                $this->db->insert_batch('event_task_tab', $task_ass);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function delete_event($event_id) {
        $this->db->where('event_id', $event_id);
        $this->db->delete('event_tab');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function delete_event_tasks($event_id) {
        $this->db->where('event_ref', $event_id);
        $this->db->delete('event_task_tab');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_pending_events() {
        $sql = "SELECT e.event_id, e.event_name,e.event_date, e.event_start, e.event_end, e.event_loc FROM event_tab e WHERE e.event_approve_stat = 'P'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $pvent_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $pvent_arr[$i]['event_id'] = $this->encrypt->encode($row['event_id']);
                $pvent_arr[$i]['event_name'] = $row['event_name'];
                $pvent_arr[$i]['event_date'] = $row['event_date'];
                $pvent_arr[$i]['event_start'] = $row['event_start'];
                $pvent_arr[$i]['event_end'] = $row['event_end'];
                $pvent_arr[$i]['event_loc'] = $row['event_loc'];
                $i++;
            }
            $json_str = json_encode($pvent_arr);
            return $json_str;
        }
    }

    public function approve_events($eid, $stat, $ev_stat, $ev_created_arr, $sent_id, $msg) {
        $sql = "UPDATE event_tab SET "
                . "event_stat='" . $ev_stat . "',"
                . "event_approve_stat='" . $stat . "',"
                . "event_approved_by='" . $this->session->userdata('username') . "',"
                . "event_approved_date=NOW(),"
                . "event_approved_role='" . $this->session->userdata('user_role') . "' "
                . "WHERE event_id=" . $eid . "";
        $sql1 = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (" . $ev_created_arr[0]['user_role_ref'] . "," . $ev_created_arr[0]['led_id'] . "," . $this->session->userdata('user_role') . "," . $sent_id . ",NOW(),'N','" . $msg . "')";
        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->query($sql1);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_available_events() {
        $sql = "SELECT e.event_id, e.event_name,e.event_date, e.event_start, e.event_end, e.event_loc FROM event_tab e WHERE e.event_approve_stat='A' AND e.event_stat='A'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $avt_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $avt_arr[$i]['event_id'] = $this->encrypt->encode($row['event_id']);
                $avt_arr[$i]['event_name'] = $row['event_name'];
                $avt_arr[$i]['event_date'] = $row['event_date'];
                $avt_arr[$i]['event_start'] = $row['event_start'];
                $avt_arr[$i]['event_end'] = $row['event_end'];
                $avt_arr[$i]['event_loc'] = $row['event_loc'];
                $i++;
            }
            $json_str = json_encode($avt_arr);
            return $json_str;
        }
    }

    public function select_events_by_leader($eid) {
        //$this->load->helper('db_helper');
        $tab_id = getMaxId("tab_id", "events_role_map");
        $led_id = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "INSERT INTO events_role_map(tab_id, event_id_ref, role_id_ref, selected_date) VALUES (" . $tab_id . "," . $eid . "," . $led_id . ",NOW())";
        $sql_up = "UPDATE event_tab SET event_stat='S' WHERE event_id=" . $eid . "";
        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->query($sql_up);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_task_list($event_id) {
        $sql = "SELECT event_task_id,task_des FROM event_task_tab WHERE event_ref=" . $event_id . " AND task_stat='O'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $task_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $task_arr[$i]['event_task_id'] = $row['event_task_id'];
                $task_arr[$i]['task_des'] = $row['task_des'];
                $i = $i + 1;
            }
            $json_str = json_encode($task_arr);
            return $json_str;
        }
    }

    public function get_selected_event_by_leader() {
        //$this->load->helper('db_helper');
        $led_id = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "SELECT e.event_id_ref,e1.event_name FROM events_role_map e INNER JOIN event_tab e1 ON e1.event_id = e.event_id_ref WHERE e.role_id_ref=" . $led_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $ev_arr = array();
            foreach ($query->result_array() as $row) {
                $temp['event_id'] = $this->encrypt->encode($row['event_id_ref']);
                $temp['event_name'] = $row['event_name'];
                $ev_arr[] = $temp;
            }
            return $ev_arr;
        }
    }

    public function get_active_events() {
        //$this->load->helper('db_helper');
        if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') {
            $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc FROM event_tab e WHERE e.event_approve_stat='A' AND e.event_stat='S'";
        } elseif ($this->session->userdata('user_role') === '3') {
            $led_id = get_user_id_by_user_name($this->session->userdata('username'));
            $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc FROM event_tab e INNER JOIN events_role_map m ON e.event_id = m.event_id_ref WHERE e.event_approve_stat='A' AND e.event_stat='S' AND m.role_id_ref=" . $led_id . "";
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $arr[$i]['event_id'] = $this->encrypt->encode($row['event_id']);
                $arr[$i]['event_name'] = $row['event_name'];
                $arr[$i]['task_cnt'] = get_tot_tasks($row['event_id']);
                $arr[$i]['task_comp'] = get_comp_tasks($row['event_id']);

                if (intval($arr[$i]['task_cnt']) > 0 && intval($arr[$i]['task_comp']) > 0) {
                    $temp = (intval($arr[$i]['task_comp']) / intval($arr[$i]['task_cnt'])) * 100;
                    $arr[$i]['precent'] = round($temp, 2);
                } else {
                    $arr[$i]['precent'] = '0';
                }

                $i++;
            }
            $json_str = json_encode($arr);
            return $json_str;
        }
    }

    public function get_complete_events() {
        //$this->load->helper('db_helper');
        if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') {
            $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc FROM event_tab e WHERE e.event_approve_stat='A' AND e.event_stat='C'";
        } elseif ($this->session->userdata('user_role') === '3') {
            $led_id = get_user_id_by_user_name($this->session->userdata('username'));
            $sql = "SELECT event_id, event_name, event_date, event_start, event_end, event_loc FROM event_tab e INNER JOIN events_role_map m ON e.event_id = m.event_id_ref WHERE e.event_approve_stat='A' AND e.event_stat='C' AND m.role_id_ref=" . $led_id . "";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $arr[$i]['event_id'] = $this->encrypt->encode($row['event_id']);
                $arr[$i]['event_name'] = $row['event_name'];
                $arr[$i]['task_cnt'] = get_tot_tasks($row['event_id']);
                $arr[$i]['task_comp'] = get_comp_tasks($row['event_id']);

                if (intval($arr[$i]['task_cnt']) > 0 && intval($arr[$i]['task_comp']) > 0) {
                    $temp = (intval($arr[$i]['task_comp']) / intval($arr[$i]['task_cnt'])) * 100;
                    $arr[$i]['precent'] = round($temp, 2);
                } else {
                    $arr[$i]['precent'] = '0';
                }

                $i++;
            }
            $json_str = json_encode($arr);
            return $json_str;
        }
    }

    public function interest_events($eid) {
        $ins_dat = array();
        $ins_dat['events_id_ref'] = $eid;
        $ins_dat['member_id_ref'] = get_user_id_by_user_name($this->session->userdata('username'));
        $date = new DateTime();
        $ins_dat['date_send_interest'] = $date->format('Y-m-d H:i:s');

        $this->db->insert('events_interest', $ins_dat);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function is_already_interested($eid, $mem_id) {
        $sql = "SELECT count(events_interest_id) AS inst_cnt FROM events_interest WHERE events_id_ref=" . $eid . " AND member_id_ref=" . $mem_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['inst_cnt'];
        }
    }

    public function add_invitation($arr, $umail, $sent_id) {
        $sql = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (4," . $arr['ev_invite_mem_ref'] . "," . $this->session->userdata('user_role') . "," . $sent_id . ",NOW(),'N','You have project invitation')";
        $this->db->trans_start();
        $this->db->insert('event_invitation', $arr);
        $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $email_data['from'] = $this->session->userdata('username');
            $email_data['name'] = $this->session->userdata('real_name');
            $email_data['to'] = 'vtag.test@gmail.com'; //repalace with $umail;
            $email_data['subject'] = 'Event Invitation';
            $email_data['msg'] = $this->load->view('email_templates/event_invitation_temp', '', TRUE);
            $rtn = email_notification($email_data);
            if ($rtn) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function already_invited($mem_id, $evid) {
        $sql = "SELECT count(e.ev_invite_id) AS invite_cnt FROM event_invitation e WHERE e.ev_invite_event_ref=" . $evid . " AND e.ev_invite_mem_ref=" . $mem_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['invite_cnt'];
        }
    }

    public function load_invited_events() {
        $mem_id = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "SELECT e.ev_invite_event_ref,e1.event_name,e1.event_date,e1.event_start,e1.event_end,e1.event_loc FROM event_invitation e "
                . "INNER JOIN event_tab e1 ON e1.event_id = e.ev_invite_event_ref "
                . "WHERE e.ev_invite_mem_ref=" . $mem_id . " AND e.ev_invite_accept='N'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $avt_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $avt_arr[$i]['event_id'] = $this->encrypt->encode($row['ev_invite_event_ref']);
                $avt_arr[$i]['event_name'] = $row['event_name'];
                $avt_arr[$i]['event_date'] = $row['event_date'];
                $avt_arr[$i]['event_start'] = $row['event_start'];
                $avt_arr[$i]['event_end'] = $row['event_end'];
                $avt_arr[$i]['event_loc'] = $row['event_loc'];
                $i++;
            }
            $json_str = json_encode($avt_arr);
            return $json_str;
        }
    }

    public function update_invited_event($eid) {
        $mem_id = get_user_id_by_user_name($this->session->userdata('username'));
        $ins_dat = array();
        $ins_dat['events_id_ref'] = $eid;
        $ins_dat['member_id_ref'] = $mem_id;
        $date = new DateTime();
        $ins_dat['date_send_interest'] = $date->format('Y-m-d H:i:s');
        $led_arr = $this->create_leader->get_leader_invited_events($eid, $mem_id);
        $ev_arr = $this->get_single_event($eid);
        $sql = "UPDATE event_invitation SET ev_invite_accept='Y' WHERE ev_invite_event_ref=" . $eid . " AND ev_invite_mem_ref=" . $mem_id . "";
        $sql_noti = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (" . $led_arr[0]['user_role_ref'] . "," . $led_arr[0]['led_id'] . "," . $this->session->userdata('user_role') . "," . $mem_id . ",NOW(),'N','Your invitation accepted for event- " . $ev_arr['event_name'] . "')";
        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->insert('events_interest', $ins_dat);
        $this->db->query($sql_noti);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_events_points($eid) {
        $sql = "SELECT task_point FROM event_task_tab WHERE event_task_id=" . $eid . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['task_point'];
        }
    }

    public function load_gallery_controls() {
        $current_user = $this->session->userdata('username');
        $current_role = $this->session->userdata('user_role');
        if ($current_role === '4') {
            $sql = "SELECT evt.event_id, evt.event_name, evt.event_date, evt.event_start, evt.event_end, evt.event_loc, COUNT(evtg.galary_id) AS num_of_images, "
                    . "CASE WHEN evt.event_stat='A' THEN 'Available' WHEN evt.event_stat='P' THEN 'Pending' WHEN evt.event_stat='C' THEN 'Completed' WHEN evt.event_stat='N' THEN 'Not Available' WHEN evt.event_stat='S' THEN 'Processing' END AS ev_stat "
                    . "FROM event_tab evt "
                    . "INNER JOIN selected_members smem ON evt.event_id = smem.sel_event_id_ref "
                    . "INNER JOIN dgfg_member_tab mem ON smem.sel_mem_id_ref = mem.mem_id "
                    . "INNER JOIN user_tab usr ON mem.mem_user_ref = usr.user_id "
                    . "LEFT JOIN event_galary evtg ON evt.event_id = evtg.event_id "
                    . "WHERE usr.user_email = '" . $current_user . "' "
                    . "GROUP BY evt.event_id";
        } elseif ($current_role === '3') {
            $sql = "SELECT evt.event_id, evt.event_name, evt.event_date, evt.event_start, evt.event_end, evt.event_loc, COUNT(evtg.galary_id) AS num_of_images, "
                    . "CASE WHEN evt.event_stat='A' THEN 'Available' WHEN evt.event_stat='P' THEN 'Pending' WHEN evt.event_stat='C' THEN 'Completed' WHEN evt.event_stat='N' THEN 'Not Available' WHEN evt.event_stat='S' THEN 'Processing' END AS ev_stat "
                    . "FROM event_tab evt "
                    . "INNER JOIN events_role_map evrm ON evt.event_id = evrm.event_id_ref "
                    . "INNER JOIN dgfg_leader_tab led ON evrm.role_id_ref = led.led_id "
                    . "INNER JOIN user_tab usr ON led.led_user_ref = usr.user_id "
                    . "LEFT JOIN event_galary evtg ON evt.event_id = evtg.event_id "
                    . "WHERE usr.user_email = '" . $current_user . "' "
                    . "GROUP BY evt.event_id";
        } elseif ($current_role === '2' || $current_role === '1') {
            $sql = "SELECT evt.event_id, evt.event_name, evt.event_date, evt.event_start, evt.event_end, evt.event_loc, COUNT(evtg.galary_id) AS num_of_images, "
                    . "CASE WHEN evt.event_stat='A' THEN 'Available' WHEN evt.event_stat='P' THEN 'Pending' WHEN evt.event_stat='C' THEN 'Completed' WHEN evt.event_stat='N' THEN 'Not Available' WHEN evt.event_stat='S' THEN 'Processing' END AS ev_stat "
                    . "FROM event_tab evt "
                    . "LEFT JOIN event_galary evtg ON evt.event_id = evtg.event_id "
                    . "GROUP BY evt.event_id";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $event_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $event_arr[$i]['event_id'] = $this->encrypt->encode($row['event_id']);
                $event_arr[$i]['event_name'] = $row['event_name'];
                $event_arr[$i]['event_date'] = $row['event_date'];
                $event_arr[$i]['event_start'] = $row['event_start'];
                $event_arr[$i]['event_end'] = $row['event_end'];
                $event_arr[$i]['event_loc'] = $row['event_loc'];
                $event_arr[$i]['ev_stat'] = $row['ev_stat'];
                $event_arr[$i]['event_image_count'] = $row['num_of_images'];
                $i = $i + 1;
            }
            $json_str = json_encode($event_arr);
            return $json_str;
        }
    }

    public function add_images_eventgalary($event_arr) {
        $author_email = $this->session->userdata('username');
        $author_role = $this->session->userdata('user_role');
        $sql = "INSERT INTO event_galary(event_id, img_linq, is_approved, is_visible, is_submited, create_by, create_role, create_date) "
                . "VALUES (?,?,?,?,?,?,?,NOW())";
//        $author_fn = "";
//        $author_ln = "";
//        $sql_get_author = "";
//        if ($author_role == 4) {
//            $sql_get_author .= "SELECT mem.mem_fname AS fname, mem.mem_lname AS lname FROM dgfg_member_tab mem "
//                    . "INNER JOIN user_tab u ON mem.mem_user_ref = u.user_id "
//                    . "WHERE u.user_email = '".$author_email."'";
//        } else if ($author_role == 3) {
//            $sql_get_author .= "SELECT led.led_fname AS fname, led.led_lname AS lname FROM dgfg_leader_tab led "
//                    . "INNER JOIN user_tab u ON led.led_user_ref = u.user_id "
//                    . "WHERE u.user_email = '".$author_email."'";
//        } else if ($author_role == 2) {
//            $sql_get_author .= "SELECT cod.cod_fname AS fname, cod.cod_lname AS lname FROM dgfg_coordinator_tab cod "
//                    . "INNER JOIN user_tab u ON cod.cod_user_ref = u.user_id "
//                    . "WHERE u.user_email = '".$author_email."'";
//        }
//        $query2 = $this->db->query($sql_get_author);
//        if ($query2->num_rows() > 0) {
//            foreach ($query2->result_array() as $row) {
//                $author_fn = $row['fname'];
//                $author_ln = $row['lname'];
//            }
//        }

        $event_id = $event_arr['event_id'];
        $event_name = $event_arr['event_name'];
        $path = base_url() . "public/uploads/" . $event_name . "/";
        $this->db->trans_start();
        foreach ($event_arr['files'] AS $data_arr) {
            $ev_arr = array();
            $ev_arr['event_id'] = $event_id;
            $ev_arr['img_linq'] = $path . $data_arr;
            $ev_arr['is_approved'] = 0;
            $ev_arr['is_visible'] = 1;
            $ev_arr['is_submited'] = 0;
//            $ev_arr['create_by'] = $author_fn . " " . $author_ln;
            $ev_arr['create_by'] = $author_email;
            $ev_arr['create_role'] = $author_role;
            $this->db->query($sql, $ev_arr);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function view_authors_images_eventgalary($event_id, $status) {
        $sql = "SELECT * FROM event_galary WHERE is_visible = 1 AND event_id = ? AND is_submited = ? AND create_by = ? AND create_role = ?";
        $ev_arr = array();
        $ev_arr['event_id'] = $event_id;
        $ev_arr['is_submited'] = $status;
        $ev_arr['created_by'] = $this->session->userdata('username');
        $ev_arr['create_role'] = $this->session->userdata('user_role');
        $query = $this->db->query($sql, $ev_arr);
        if ($query->num_rows() > 0) {
            $data_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $data_arr[$i]['galary_id'] = $row['galary_id'];
                $data_arr[$i]['event_id'] = $row['event_id'];
                $data_arr[$i]['img_linq'] = $row['img_linq'];
                $data_arr[$i]['is_approve'] = $row['is_approved'];
                $data_arr[$i]['create_date'] = $row['create_date'];
                $i++;
            }
            $json_str = json_encode($data_arr);
            return $json_str;
        }
    }

    public function view_all_images_eventgalary($event_id, $status) {
        $current_role = $this->session->userdata('user_role');
        if ($current_role === '1' || $current_role === '2') {
            $sql = "SELECT * FROM event_galary WHERE is_visible = 1 AND is_submited = 1 AND event_id = ? AND is_approved = ?";
        } elseif ($current_role === '3') {
            $sql = "SELECT * FROM event_galary WHERE is_visible = 1 AND create_role = 4 AND is_submited = 1 AND event_id = ? AND is_approved = ?";
        }

        $ev_arr = array();
        $ev_arr['event_id'] = $event_id;
        $ev_arr['is_approved'] = $status;
        $query = $this->db->query($sql, $ev_arr);
        if ($query->num_rows() > 0) {
            $data_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $data_arr[$i]['galary_id'] = $row['galary_id'];
                $data_arr[$i]['event_id'] = $row['event_id'];
                $data_arr[$i]['img_linq'] = $row['img_linq'];
                $data_arr[$i]['is_approve'] = $row['is_approved'];
                $data_arr[$i]['create_by'] = $row['create_by'];
                $data_arr[$i]['create_date'] = $row['create_date'];
                $i++;
            }
            $json_str = json_encode($data_arr);
            return $json_str;
        }
    }

    public function submit_single_image($gallery_id) {
        $result = FALSE;
        $current_userid = get_user_id_by_user_name($this->session->userdata('username'));

        $date = date("Y-m-d H:i:s");
        $sql = "UPDATE event_galary SET is_submited = 1 WHERE is_visible = 1 AND galary_id = $gallery_id";
        $sql_noti = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";

        //get receiver 1 (leader)
//        $sql3 = "SELECT erm.role_id_ref AS leader_id, u.user_role_ref AS leader_role  FROM events_role_map erm "
//                ."INNER JOIN dgfg_leader_tab l ON erm.role_id_ref = l.led_id "
//                ."INNER JOIN user_tab u ON l.led_user_ref = u.user_id "
//                ."WHERE erm.event_id_ref = $event_id";
//        $query3 = $this->db->query($sql3);
//        if ($query3->num_rows() > 0) {
//            foreach ($query3->result_array() as $row) {
//                $receiver_id = $row['leader_id'];
//                $receiver_role = $row['leader_role'];
//            }
//        }
//        $notif1['notification_recive_role'] = $receiver_role;
//        $notif1['notification_receive_id'] = $receiver_id;
//        $notif1['notification_sent_role'] = $this->session->userdata('user_role');
//        $notif1['notification_sent_id'] = $author_id;
//        $notif1['notification_sent_date'] = $date;
//        $notif1['is_watch'] = "N";
//        $notif1['notification_msg'] = "You have new image(s) to approve from event galary";

        $notif2['notification_recive_role'] = 2;
        $notif2['notification_receive_id'] = 1;
        $notif2['notification_sent_role'] = $this->session->userdata('user_role');
        $notif2['notification_sent_id'] = $current_userid;
        $notif2['notification_sent_date'] = $date;
        $notif2['is_watch'] = "N";
        $notif2['notification_msg'] = "You have new image(s) to approve";

        $this->db->trans_start();
        $this->db->query($sql);
//        $this->db->query($sql_noti, $notif1);
        $this->db->query($sql_noti, $notif2);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $result = TRUE;
        }
        return $result;
    }

    public function submit_images($event_id, $author) {
        $result = FALSE;
        $gallery_created_role = "";
        $receiver_id = "";
        $receiver_role = "";
        $date = date("Y-m-d H:i:s");
        $sql = "UPDATE event_galary SET is_submited = 1 WHERE is_visible = 1 AND event_id = $event_id AND create_by = '" . $author . "'";
        $sql_noti = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";
        //get role from event_galary
        $sql2 = "SELECT create_role FROM event_galary WHERE event_id = ? AND create_by = ?";
        $arr_sql2 = array();
        $arr_sql2['event_id'] = $event_id;
        $arr_sql2['create_bye'] = $author;
        $query2 = $this->db->query($sql2, $arr_sql2);

        if ($query2->num_rows() > 0) {
            foreach ($query2->result_array() as $row) {
                $gallery_created_role = $row['create_role'];
            }
        }
        //get user id from user_tab
        $author_id = get_memeber_id_by_user_name($author, $gallery_created_role);
        //get receiver 1 (leader)
//        $sql3 = "SELECT erm.role_id_ref AS leader_id, u.user_role_ref AS leader_role  FROM events_role_map erm "
//                ."INNER JOIN dgfg_leader_tab l ON erm.role_id_ref = l.led_id "
//                ."INNER JOIN user_tab u ON l.led_user_ref = u.user_id "
//                ."WHERE erm.event_id_ref = $event_id";
//        $query3 = $this->db->query($sql3);
//        if ($query3->num_rows() > 0) {
//            foreach ($query3->result_array() as $row) {
//                $receiver_id = $row['leader_id'];
//                $receiver_role = $row['leader_role'];
//            }
//        }
//        $notif1['notification_recive_role'] = $receiver_role;
//        $notif1['notification_receive_id'] = $receiver_id;
//        $notif1['notification_sent_role'] = $this->session->userdata('user_role');
//        $notif1['notification_sent_id'] = $author_id;
//        $notif1['notification_sent_date'] = $date;
//        $notif1['is_watch'] = "N";
//        $notif1['notification_msg'] = "You have new image(s) to approve from event galary";

        $notif2['notification_recive_role'] = 2;
        $notif2['notification_receive_id'] = 1;
        $notif2['notification_sent_role'] = $this->session->userdata('user_role');
        $notif2['notification_sent_id'] = $author_id;
        $notif2['notification_sent_date'] = $date;
        $notif2['is_watch'] = "N";
        $notif2['notification_msg'] = "You have new image(s) to approve";

        $this->db->trans_start();
        $this->db->query($sql);
//        $this->db->query($sql_noti, $notif1);
        $this->db->query($sql_noti, $notif2);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $result = TRUE;
        }
        return $result;
    }

    public function approve_single_image($galary_id) {
        $result = FALSE;
        $notif_arr = array();
        $current_user = $this->session->userdata('username');
        $current_role = $this->session->userdata('user_role');
        $current_userid = get_user_id_by_user_name($current_user);
        $date = date("Y-m-d H:i:s");
        $reciver_role = "";
        $reciver_id = "";
        $sql_get_notifyreciever = "SELECT create_role, create_by FROM event_galary WHERE galary_id = $galary_id";
        $query = $this->db->query($sql_get_notifyreciever);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $reciver_role = $row['create_role'];
                $reciver_id = get_memeber_id_by_user_name($row['create_by'], $reciver_role);
            }
        }
        $sql = "UPDATE event_galary SET is_approved = 1, approve_by = '" . $current_user . "', approve_role = " . $current_role . ", approve_date = NOW() WHERE galary_id = $galary_id AND is_visible = 1";
        $sql_notif = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";
        $query = $this->db->query($sql_get_notifyreciever);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $reciver_role = $row['create_role'];
                $reciver_id = get_memeber_id_by_user_name($row['create_by'], $reciver_role);
            }
        }
        $notif_arr['notification_recive_role'] = $reciver_role;
        $notif_arr['notification_receive_id'] = $reciver_id;
        $notif_arr['notification_sent_role'] = $current_role;
        $notif_arr['notification_sent_id'] = $current_userid;
        $notif_arr['notification_sent_date'] = $date;
        $notif_arr['is_watch'] = "N";
        $notif_arr['notification_msg'] = "Your gallery image(s) approved";

        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->query($sql_notif, $notif_arr);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $result = TRUE;
        }
        return $result;
    }

    public function approve_all_image($event_id) {
        $result = FALSE;
        $current_user = $this->session->userdata('username');
        $current_role = $this->session->userdata('user_role');
        $current_userid = get_user_id_by_user_name($current_user);
        $date = date("Y-m-d H:i:s");
        $notif_multi_arr = array();
        $sql = "";
        if ($current_role === '3') {
            $sql .= "UPDATE event_galary SET is_approved = 1, approve_by = '" . $current_user . "', approve_role = " . $current_role . ", approve_date = NOW() WHERE event_id = $event_id AND is_visible = 1 AND is_approved = 0 AND create_role = 4";
        } elseif ($current_role === '2' || $current_role === '1') {
            $sql .= "UPDATE event_galary SET is_approved = 1, approve_by = '" . $current_user . "', approve_role = " . $current_role . ", approve_date = NOW() WHERE event_id = $event_id AND is_visible = 1 AND is_approved = 0";
        }
        $sql_get_notifyreciever = "SELECT create_role, create_by FROM event_galary WHERE event_id = $event_id AND is_visible = 1 AND is_approved = 0 GROUP BY create_role";
        $query = $this->db->query($sql_get_notifyreciever);
        if ($query->num_rows() > 0) {
            $i = 1;
            foreach ($query->result_array() as $row) {
                $reciver_role = $row['create_role'];
                $reciver_id = get_memeber_id_by_user_name($row['create_by'], $reciver_role);
                $notif_multi_arr[$i] = array('notification_recive_role' => $reciver_role, 'notification_receive_id' => $reciver_id, 'notification_sent_role' => $current_role, 'notification_sent_id' => $current_userid, 'notification_sent_date' => $date, 'is_watch' => "N", 'notification_msg' => "Your gallery image(s) approved");
                $i++;
            }
        }
        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->insert_batch('notification_tab', $notif_multi_arr);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $result = TRUE;
        }
        return $result;
    }

    public function reject_single_image($galary_id) {
        $result = FALSE;
        $notif_arr = array();
        $current_user = $this->session->userdata('username');
        $current_role = $this->session->userdata('user_role');
        $current_userid = get_user_id_by_user_name($current_user);
        $date = date("Y-m-d H:i:s");
        $reciver_role = "";
        $reciver_id = "";
        $sql_get_notifyreciever = "SELECT create_role, create_by FROM event_galary WHERE galary_id = $galary_id";
        $query = $this->db->query($sql_get_notifyreciever);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $reciver_role = $row['create_role'];
                $reciver_id = get_memeber_id_by_user_name($row['create_by'], $reciver_role);
            }
        }
        $sql = "UPDATE event_galary SET is_approved = 0 WHERE galary_id = $galary_id AND is_visible = 1";
        $sql_notif = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) VALUES (?,?,?,?,?,?,?)";
        $notif_arr['notification_recive_role'] = $reciver_role;
        $notif_arr['notification_receive_id'] = $reciver_id;
        $notif_arr['notification_sent_role'] = $current_role;
        $notif_arr['notification_sent_id'] = $current_userid;
        $notif_arr['notification_sent_date'] = $date;
        $notif_arr['is_watch'] = "N";
        $notif_arr['notification_msg'] = "Your gallery image(s) rejected";

        $this->db->trans_start();
        $this->db->query($sql);
        $this->db->query($sql_notif, $notif_arr);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $result = TRUE;
        }
        return $result;
    }

    public function delete_image($galary_id) {
        $result = FALSE;
        $sql = "UPDATE event_galary SET is_visible = 0 WHERE galary_id = $galary_id";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            $result = TRUE;
        }
        return $result;
    }

    public function load_approved_images($ev_id) {
        $sql = "SELECT e.img_linq FROM event_galary e WHERE e.event_id=" . $ev_id . " AND e.is_approved=1 AND e.is_visible=1";
        $query = $this->db->query($sql);
        $img_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $img_arr[] = $row;
            }
            $json_str = json_encode($img_arr);
            return $json_str;
        }
    }

//    public function get_tot_tasks($eid) {
//        $sql = "SELECT count(e.event_task_id) AS cnt FROM event_task_tab e JOIN event_tab e1 ON e1.event_id = e.event_ref WHERE e.event_ref=" . $eid . "";
//        $query = $this->db->query($sql);
//        if ($query->num_rows() > 0) {
//            $row = $query->row_array();
//            return $row['cnt'];
//        }
//    }
//
//    public function get_comp_tasks($eid) {
//        $sql = "SELECT count(e.event_task_id) AS cnt FROM event_task_tab e INNER JOIN event_tab e1 ON e1.event_id = e.event_ref WHERE e.task_stat='C' AND e.event_ref=" . $eid . "";
//        $query = $this->db->query($sql);
//        if ($query->num_rows() > 0) {
//            $row = $query->row_array();
//            return $row['cnt'];
//        }
//    }
}
