<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
        $this->load->model('task_model');
    }

    public function load_newly_registered_mem() {
        $sql = "SELECT m.mem_id,m.mem_img_link,concat(m.mem_fname,' ',m.mem_lname) AS mem_name FROM dgfg_member_tab m "
                . "ORDER BY m.date_create DESC "
                . "LIMIT 10";
        $query = $this->db->query($sql);
        $mem_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $mem_arr[] = $row;
            }
            return $mem_arr;
        } else {
            return $mem_arr;
        }
    }

    public function load_top_performing_events() {

        $sql = "SELECT e.event_id,e.event_name, TRUNCATE(((COUNT(CASE e1.task_stat WHEN 'C' THEN 1 END)/ count(e1.event_task_id))*100),1) AS precent "
                . "FROM event_tab e INNER JOIN event_task_tab e1 ON e.event_id = e1.event_ref "
                . "GROUP BY e.event_name "
                . "ORDER BY precent DESC";
        $query = $this->db->query($sql);
        $ev_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['event_id'] = $this->encrypt->encode($row['event_id']);
                $temp['event_name'] = $row['event_name'];
                $temp['precent'] = $row['precent'];
                $ev_arr[$i] = $temp;
                $i++;
            }
            return $ev_arr;
        } else {
            return $ev_arr;
        }
    }

    public function survey_allerts() {
        $sql = "SELECT s.survey_map_id,d.led_img_link,concat(d.led_fname,' ',d.led_lname) AS name,count(CASE s1.survey_ans WHEN '5' THEN 1 END) AS cnt FROM survey_map s "
                . "INNER JOIN survey_answers s1 ON s.survey_map_id = s1.survey_no_ref "
                . "INNER JOIN user_tab u ON  u.user_email = s.survey_map_user "
                . "INNER JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "INNER JOIN dgfg_leader_tab d ON u.user_id = d.led_user_ref "
                . "GROUP BY s.survey_map_user "
                . "UNION "
                . "(SELECT s.survey_map_id,d1.mem_img_link,concat(d1.mem_fname,' ',d1.mem_lname) AS name,count(CASE s1.survey_ans WHEN '5' THEN 1 END) AS cnt FROM survey_map s "
                . "INNER JOIN survey_answers s1 ON s.survey_map_id = s1.survey_no_ref "
                . "INNER JOIN user_tab u ON  u.user_email = s.survey_map_user "
                . "INNER JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "INNER JOIN dgfg_member_tab d1 ON u.user_id = d1.mem_user_ref "
                . "GROUP BY s.survey_map_user) "
                . "ORDER BY cnt DESC "
                . "LIMIT 10";
        $query = $this->db->query($sql);
        $alerts_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['survey_map_id'] = $this->encrypt->encode($row['survey_map_id']);
                $temp['led_img_link'] = $row['led_img_link'];
                $temp['name'] = $row['name'];
                $temp['cnt'] = $row['cnt'];
                $alerts_arr[$i] = $temp;
                $i++;
            }
            return $alerts_arr;
        } else {
            return $alerts_arr;
        }
    }

    public function recent_surveys() {
        $sql = "SELECT survey_map_id,dt,uname,role_code FROM "
                . "(SELECT s.survey_map_id,DATE_FORMAT(survey_date,'%Y-%m-%d') AS dt,concat(d.led_fname,' ',d.led_lname) AS uname,u1.role_code FROM survey_map s "
                . "INNER JOIN user_tab u ON s.survey_map_user = u.user_email "
                . "INNER JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "INNER JOIN dgfg_leader_tab d ON u.user_id = d.led_user_ref) AS XX "
                . "UNION "
                . "(SELECT s.survey_map_id,DATE_FORMAT(survey_date,'%Y-%m-%d') AS dt,concat(d1.mem_fname,' ',d1.mem_lname) AS uname,u1.role_code FROM survey_map s "
                . "INNER JOIN user_tab u ON s.survey_map_user = u.user_email "
                . "INNER JOIN user_roles u1 ON u1.role_code = u.user_role_ref "
                . "INNER JOIN dgfg_member_tab d1 ON  u.user_id = d1.mem_user_ref) "
                . "ORDER BY dt DESC "
                . "LIMIT 10";
        $query = $this->db->query($sql);
        $alerts_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['survey_map_id'] = $this->encrypt->encode($row['survey_map_id']);
                $temp['dt'] = $row['dt'];
                $temp['uname'] = $row['uname'];
                $temp['role_code'] = $row['role_code'];
                $alerts_arr[$i] = $temp;
                $i++;
            }
            return $alerts_arr;
        } else {
            return $alerts_arr;
        }
    }

    public function top_leaders() {
        $sql = "SELECT concat(d.led_fname,' ',d.led_lname) AS lname,d.led_img_link,d.led_id,count(e.event_id_ref) AS ev_cnt FROM events_role_map e "
                . "INNER JOIN dgfg_leader_tab d ON d.led_id = e.role_id_ref "
                . "INNER JOIN event_tab e1 ON e1.event_id = e.event_id_ref "
                . "WHERE e1.event_stat = 'C' "
                . "GROUP BY lname "
                . "ORDER BY ev_cnt DESC "
                . "LIMIT 10";
        $query = $this->db->query($sql);
        $led_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $led_arr[] = $row;
            }
            return $led_arr;
        } else {
            return $led_arr;
        }
    }

    public function top_members() {
        $sql = "SELECT TRUNCATE(((count(CASE e.task_stat WHEN 'C' THEN 1 END)/count(aev_task_id))*100),1) AS task_completion_precentage,concat(d1.mem_fname,' ',d1.mem_lname) AS mem_name,d1.mem_img_link,d1.mem_id "
                . "FROM assign_events_tab a "
                . "INNER JOIN event_task_tab e ON e.event_task_id = a.aev_task_id "
                . "INNER JOIN dgfg_member_tab d1 ON d1.mem_id = a.aev_uid "
                . "WHERE a.aev_role_code=4 "
                . "GROUP BY d1.mem_fname "
                . "ORDER BY task_completion_precentage DESC";
        $query = $this->db->query($sql);
        $mem_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $mem_arr[] = $row;
            }
            return $mem_arr;
        } else {
            return $mem_arr;
        }
    }

    public function up_coming_events() {
        $sql = "SELECT e.event_id,e.event_name,e.event_date "
                . "FROM event_tab e "
                . "WHERE event_date >= NOW() AND e.event_stat='A' ORDER BY e.event_date ASC";
        $query = $this->db->query($sql);
        $up_ev_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['event_id'] = $this->encrypt->encode($row['event_id']);
                $temp['event_name'] = $row['event_name'];
                $temp['event_date'] = $row['event_date'];
                $up_ev_arr[$i] = $temp;
                $i++;
            }
            return $up_ev_arr;
        } else {
            return $up_ev_arr;
        }
    }

    /*
     * Leader dashboard functions
     */

    public function load_my_skawd_events() {
        $my_id = get_user_id_by_user_name($this->session->userdata('username'));
        if ($this->session->userdata('user_role') === '3') {
            $sql = "SELECT e.event_id,e.event_name,e.event_date,e.event_start,e.event_end,e.event_loc,e.event_comments,e.event_img_linq FROM events_role_map m "
                    . "INNER JOIN event_tab e ON e.event_id = m.event_id_ref "
                    . "INNER JOIN dgfg_leader_tab d ON d.led_id = m.role_id_ref "
                    . "WHERE d.led_id=" . $my_id . "";
        } elseif ($this->session->userdata('user_role') === '4') {
            $sql = "SELECT e.event_id,e.event_name,e.event_date,e.event_start,e.event_end,e.event_loc,e.event_comments,e.event_img_linq FROM selected_members s "
                    . "INNER JOIN event_tab e ON e.event_id = s.sel_event_id_ref "
                    . "INNER JOIN dgfg_member_tab d ON d.mem_id = s.sel_mem_id_ref "
                    . "WHERE d.mem_id = " . $my_id . "";
        }
        $query = $this->db->query($sql);
        $ev_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['event_id'] = $this->encrypt->encode($row['event_id']);
                $temp['event_name'] = $row['event_name'];
                $temp['event_date'] = $row['event_date'];
                $temp['event_start'] = $row['event_start'];
                $temp['event_end'] = $row['event_end'];
                $temp['event_loc'] = $row['event_loc'];
                $temp['event_comments'] = $row['event_comments'];
                $temp['event_img_linq'] = $row['event_img_linq'];
                $temp['my_task_list'] = $this->load_my_all_task($row['event_id'], $my_id);
                $ev_arr[$i] = $temp;
                $i++;
            }
            return $ev_arr;
        } else {
            return $ev_arr;
        }
    }

    public function load_my_all_task($ev_id, $uid) {
        $sql = "SELECT a.aev_task_id,e.task_des, CASE e.task_stat WHEN 'A' THEN 'Processing' WHEN 'C' THEN 'Completed' END AS stat "
                . "FROM assign_events_tab a "
                . "INNER JOIN event_task_tab e ON e.event_task_id = a.aev_task_id  "
                . "INNER JOIN event_tab e1 ON e1.event_id = a.aev_event_ref  "
                . "WHERE a.aev_event_ref=" . $ev_id . " AND a.aev_role_code=" . $this->session->userdata('user_role') . " AND a.aev_uid=" . $uid . "";
        $query = $this->db->query($sql);
        $tsk_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $temp['aev_task_id'] = $row['aev_task_id'];
                $temp['task_des'] = $row['task_des'];
                $temp['stat'] = $row['stat'];
                $tsk_arr[$i] = $temp;
                $i++;
            }
            return $tsk_arr;
        } else {
            return $tsk_arr;
        }
    }

    public function load_leaders_not_have_project() {
        $sql = "SELECT d.led_id, concat(d.led_fname,' ',d.led_lname) AS uname,d.led_img_link "
                . "FROM dgfg_leader_tab d WHERE d.led_id NOT IN (SELECT DISTINCT(e.role_id_ref) FROM events_role_map e)";
        $query = $this->db->query($sql);
        $led_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $led_arr[] = $row;
            }
            return $led_arr;
        } else {
            return $led_arr;
        }
    }

    public function load_leaders_directory() {
        $sql = "SELECT d.led_id,concat(d.led_fname,' ',d.led_lname) as u_name,d.led_img_link,d.led_mobile FROM dgfg_leader_tab d ORDER BY u_name ASC";
        $query = $this->db->query($sql);
        $led_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $led_arr[] = $row;
            }
            return $led_arr;
        } else {
            return $led_arr;
        }
    }
    
    public function load_blog_posts(){
        $sql = "SELECT blog_id, blog_name, blog_body, blog_image, DATE_FORMAT(blog_created_date, '%D %M %Y') AS blog_created_date FROM blog_tab WHERE is_approved = 1 ORDER BY blog_created_date DESC";
        $query = $this->db->query($sql);
        $blg_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $blg_arr[] = $row;
            }
            return $blg_arr;
        } else {
            return $blg_arr;
        }
    }
    
    public function load_daily_surveys_on_public() {
        $sql = "SELECT d.daily_survey_id,d.daily_survey_name,DATE_FORMAT(d.daily_survey_date,'%d-%m-%Y') AS daily_survey_date FROM daily_survey_main d "
                . "WHERE d.daily_survey_date >= NOW() AND d.daily_survey_id NOT IN (SELECT d.ds_map_survey_ref FROM daily_survey_map d WHERE d.ds_map_user='".$this->session->userdata('username')."')";
        $query = $this->db->query($sql);
        $dsur_arr = array();
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                $dsur_arr[$i]['daily_survey_id'] = $this->encrypt->encode($row['daily_survey_id']);
                $dsur_arr[$i]['daily_survey_name'] = $row['daily_survey_name'];
                $dsur_arr[$i]['daily_survey_date'] = $row['daily_survey_date'];
                $i++;
            }
            return $dsur_arr;
        }
    }
    
    public function load_highest_rewards() {
        $sql = "SELECT img,uname,tot_points,role,uid FROM "
                . "(SELECT (d.mem_img_link) AS img,concat(d.mem_fname,' ',d.mem_lname) AS uname, sum(r.reward) AS tot_points,r.re_user_role AS role,r.re_user_id AS uid FROM rewards r "
                . "INNER JOIN dgfg_member_tab d ON r.re_user_id = d.mem_id "
                . "WHERE r.re_user_role = 4 "
                . "GROUP BY uname) AS X "
                . "UNION "
                . "(SELECT (d1.led_img_link) AS img,concat(d1.led_fname,' ',d1.led_lname) AS uname, sum(r.reward) AS tot_points,r.re_user_role AS role,r.re_user_id AS uid FROM rewards r "
                . "INNER JOIN dgfg_leader_tab d1 ON r.re_user_id = d1.led_id "
                . "WHERE r.re_user_role = 3 "
                . "GROUP BY uname) "
                . "ORDER BY tot_points DESC LIMIT 10";
        $query = $this->db->query($sql);
        $rwds_arr = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $rwds_arr[] = $row;
            }
        }
        return $rwds_arr;
    }

}
