<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Task_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('db_helper');
        $this->load->model('create_leader');
        $this->load->model('event_model');
    }

    public function assign_task($data) {

        $batc_array = array();
        $up_array = array();
        $i = 0;
        foreach ($data['sel_tsk'] as $value) {
            $temp['aev_event_ref'] = $data['event'];
            $temp1['event_ref'] = $data['event'];
            $temp['aev_task_id'] = $value;
            $temp1['event_task_id'] = $value;
            $temp['aev_role_code'] = $data['role'];
            $temp['aev_uid'] = $data['user_id'];
            $date = new DateTime();
            $temp['assign_date'] = $date->format('Y-m-d H:i:s');
            $temp['assign_role'] = $this->session->userdata('user_role');
            $temp['assign_by'] = $this->session->userdata('username');
            $temp1['task_stat'] = 'A';
            $batc_array[$i] = $temp;
            $up_array[$i] = $temp1;
            $i++;
        }
        $cnt = count($data['sel_tsk']);
        $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (".$data['role'].",".$data['user_id'].",".$this->session->userdata('user_role').",".$sent_id.",NOW(),'N','You have assigned ".$cnt." tasks')";
        $this->db->trans_start();
        $this->db->insert_batch('assign_events_tab', $batc_array);
        $this->db->update_batch('event_task_tab', $up_array, 'event_task_id');
        $this->db->query($sql);
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function load_events() {
        //$this->load->helper('db_helper');
        $uid = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "SELECT DISTINCT(a.aev_event_ref) AS evid,e1.event_name FROM assign_events_tab a "
                . "INNER JOIN event_task_tab e ON e.event_task_id = a.aev_task_id "
                . "INNER JOIN event_tab e1 ON e1.event_id = a.aev_event_ref "
                . "WHERE a.aev_role_code=" . $this->session->userdata('user_role') . " AND a.aev_uid=" . $uid . " AND e.task_stat='A'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $my_tasks = array();
            foreach ($query->result_array() as $row) {
                $my_tasks[$this->encrypt->encode($row['evid'])] = $row['event_name'];
            }
            return $my_tasks;
        }
    }

    public function load_tasks($evid) {
        //$this->load->helper('db_helper');
        $uid = get_user_id_by_user_name($this->session->userdata('username'));
        $sql = "SELECT a.aev_task_id,e.task_des FROM assign_events_tab a "
                . "INNER JOIN event_task_tab e ON e.event_task_id = a.aev_task_id "
                . "INNER JOIN event_tab e1 ON e1.event_id = a.aev_event_ref "
                . "WHERE a.aev_event_ref=" . $evid . " AND a.aev_role_code=" . $this->session->userdata('user_role') . " AND a.aev_uid=" . $uid . " AND e.task_stat='A'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $my_tasks = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $my_tasks[$i]['event_task_id'] = $this->encrypt->encode($row['aev_task_id']);
                $my_tasks[$i]['task_des'] = $row['task_des'];
                $i = $i + 1;
            }
            $json_str = json_encode($my_tasks);
            return $json_str;
        }
    }

    public function up_task_complete($dat) {
        $arr = array();
        $rewards = array();
        $i = 0;
        $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
        foreach ($dat['event_task_id'] as $value) {
            $temp['event_ref'] = $this->encrypt->decode($dat['event_ref']);
            $temp['event_task_id'] = $this->encrypt->decode($value);
            $temp['task_stat'] = 'C';
            $date = new DateTime();
            $temp['date_complete'] = $date->format('Y-m-d H:i:s');
            
            $temp1['re_user_role'] = $this->session->userdata('user_role');
            $temp1['re_user_id'] = $sent_id;
            $temp1['reward_des'] = $this->encrypt->decode($value).' Task Complete';
            $temp1['reward'] = $this->event_model->get_events_points($this->encrypt->decode($value));
            $temp1['reward_date'] = $date->format('Y-m-d H:i:s');
            $arr[$i] = $temp;
            $rewards[$i] = $temp1;
            $i++;
        }
        $led_arr = $this->create_leader->get_leader_assign_task($this->encrypt->decode($dat['event_ref']));
        $cnt = count($arr);
        $sql = "INSERT INTO notification_tab(notification_recive_role, notification_receive_id, notification_sent_role, notification_sent_id, notification_sent_date, is_watch, notification_msg) "
                . "VALUES (".$led_arr[0]['user_role_ref'].",".$led_arr[0]['led_id'].",".$this->session->userdata('user_role').",".$sent_id.",NOW(),'N','".$cnt." Task completed by: ".$this->session->userdata('real_name')."')";

        $this->db->trans_start();
        $this->db->update_batch('event_task_tab', $arr, 'event_task_id');
        $this->db->query($sql);
        $this->db->insert_batch('rewards',$rewards);
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $rtn_val = $this->mark_event_complete($this->encrypt->decode($dat['event_ref']));
            if ($rtn_val) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    public function mark_event_complete($eid) {
        //$this->load->helper('db_helper');
        $tot_tasks_count = get_tot_tasks($eid);
        $completed_task = get_comp_tasks($eid);
        if ($tot_tasks_count === $completed_task) {
            $sql = "UPDATE event_tab SET  event_stat='C' WHERE event_id=" . $eid . "";
            $this->db->query($sql);
            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }  else {
            return TRUE;
        }
    }

    public function get_active_task_list($event_id) {
        $sql = "SELECT e.task_des,DATE_FORMAT(e.due_date,'%Y-%m-%d') AS due_date,"
                . "CASE e.task_stat WHEN 'A' THEN 'Active' WHEN 'C' THEN 'Complete' WHEN 'O' THEN 'Not Assign' END AS stat "
                . "FROM event_task_tab e "
                . "WHERE e.event_ref=" . $event_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $task_arr = array();
            $i = 0;
            foreach ($query->result_array() as $row) {
                $task_arr[$i]['task_des'] = $row['task_des'];
                $task_arr[$i]['due_date'] = $row['due_date'];
                $task_arr[$i]['stat'] = $row['stat'];
                $i = $i + 1;
            }
            $json_str = json_encode($task_arr);
            return $json_str;
        }
    }

}
