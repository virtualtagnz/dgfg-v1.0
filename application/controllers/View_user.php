<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_user extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $data['user_type'] = end($this->uri->segments);
        switch ($data['user_type']) {
            case '1':
                $data['page_title'] = 'VIEW YOUTH COORDINATOR';
                break;
            case '2':
                $data['page_title'] = 'VIEW SKWAD LEADER';
                break;
            case '3':
                $data['page_title'] = 'VIEW SKWAD MEMBER';
                break;
        }
        $data['main_content'] = 'view_user_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function view_coordinator(){
        $this->load->model('create_coordinator');
        $cod_arr = $this->create_coordinator->getCoordinator();
        echo $cod_arr;
    }
    
    public function view_leader(){
        $this->load->model('create_leader');
        $cod_arr = $this->create_leader->getLeader();
        echo $cod_arr;
    }
    
    public function view_member(){
        $this->load->model('create_member');
        $cod_arr = $this->create_member->getMember();
        echo $cod_arr;
    }

}
