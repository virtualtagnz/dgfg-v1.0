<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Single_event extends CI_Controller{
    
    public $event_id;


    public function __construct() {
        parent::__construct();
        $this->event_id = $this->encrypt->decode(end($this->uri->segments));
        $this->load->library('facebooksdk');
        $this->fb = $this->facebooksdk;
    }
    
    public function index() {
        $this->load->model('event_model');
        $rtn_arr = $this->event_model->get_single_event($this->event_id);

        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $data['img_json'] = $this->event_model->load_approved_images($this->event_id);
        $data['json_str'] = $this->event_model->get_single_task_list($this->event_id);
        $cb = "http://dgfg.websitepreview.co.nz/index.php/socialmedia_api/share_event_fb";
        $url = $this->fb->getLoginUrl($cb);
        $data['return_url'] = $url;
        //$data['main_content'] = 'single_event_view';
        $this->load->view('single_event_view', $data);
    }
    
    public function active_events_view() {
        $this->load->model('event_model');
        $this->load->model('task_model');
        $this->load->model('create_leader');
        $this->load->model('create_member');
        $rtn_arr = $this->event_model->get_single_event($this->event_id);
        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $data['skwad_leder']=$this->create_leader->leader_contribute_event($this->event_id);
        $data['skwad_members']=$this->create_member->load_selected_members($this->event_id);
        $data['json_str'] = $this->task_model->get_active_task_list($this->event_id);
        $this->load->view('single_active_event_view', $data);
    }
}

