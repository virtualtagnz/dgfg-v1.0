<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Single_daily_survey extends CI_Controller {

    public $sid;

    public function __construct() {
        parent::__construct();
        $this->sid = $this->encrypt->decode(end($this->uri->segments));
        $this->load->model('service_model');
        $this->load->model('dashboard_model');
        $this->load->model('daily_survey_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $rtn_arr = $this->daily_survey_model->get_single_daily_survey($this->sid);
        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $data['json_str'] = $this->daily_survey_model->get_survey_qa($this->sid);
        $this->load->view('single_daily_survey_view', $data);
    }

    public function save_daily_survey_answers() {
        $cnt = $this->daily_survey_model->get_questions_count($this->sid);
        for ($i = 0; $i < $cnt; $i++) {
            //$this->form_validation->set_rules('Q' . ($i + 1), 'Answer', 'callback_radio_btn_attemps[' . 'Q' . ($i + 1) . ']');
            $this->form_validation->set_rules('Q' . ($i + 1), 'answer', 'required');
        }
        if ($this->form_validation->run() == FALSE) {
            if ($this->session->userdata('user_role') === '3') {
                $data['my_events'] = $this->dashboard_model->load_my_skawd_events();
                $data['service_cat'] = $this->service_model->load_services_according_survey();
                $data['top_leaders'] = $this->dashboard_model->top_leaders();
                $data['top_members'] = $this->dashboard_model->top_members();
                $data['leads_not_project'] = $this->dashboard_model->load_leaders_not_have_project();
                $data['leaders_derectory'] = $this->dashboard_model->load_leaders_directory();
                $data['blog_post'] = $this->dashboard_model->load_blog_posts();
                $data['daily_survey'] = $this->dashboard_model->load_daily_surveys_on_public();
                $data['main_content'] = 'leader_dashboard';
                $this->load->view('include/dashboard_template', $data);

                $rtn_arr = $this->daily_survey_model->get_single_daily_survey($this->sid);
                foreach ($rtn_arr as $key => $value) {
                    $data1[$key] = $value;
                }
                $data1['json_str'] = $this->daily_survey_model->get_survey_qa($this->sid);
                $this->load->view('single_daily_survey_view', $data1);
            } elseif ($this->session->userdata('user_role') === '4') {
                $data['my_events'] = $this->dashboard_model->load_my_skawd_events();
                $data['service_cat'] = $this->service_model->load_services_according_survey();
                $data['top_leaders'] = $this->dashboard_model->top_leaders();
                $data['top_members'] = $this->dashboard_model->top_members();
                $data['blog_post'] = $this->dashboard_model->load_blog_posts();
                $data['daily_survey'] = $this->dashboard_model->load_daily_surveys_on_public();
                $data['main_content'] = 'member_dashboard';
                $this->load->view('include/dashboard_template', $data);

                $rtn_arr = $this->daily_survey_model->get_single_daily_survey($this->sid);
                foreach ($rtn_arr as $key => $value) {
                    $data1[$key] = $value;
                }
                $data1['json_str'] = $this->daily_survey_model->get_survey_qa($this->sid);
                $this->load->view('single_daily_survey_view', $data1);
            }
        } else {
            $sur_ans = array();
            for ($j = 0; $j < $cnt; $j++) {
                $temp[0] = $this->input->post('real_qv' . ($j + 1));
                $temp[1] = $this->input->post('Q' . ($j + 1));
                $sur_ans[$j]['q'] = $temp[0];
                $sur_ans[$j]['a'] = $temp[1][0];
            }
            $rtn_arr = $this->daily_survey_model->save_daily_survey_answers($sur_ans, $this->sid);
            if ($rtn_arr) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

    public function radio_btn_attemps($str, $r_name) {
        $checked_arr = $this->input->post($r_name);
        $count = count($checked_arr);
        if ($count == 0) {
            $error = 'Please select one answer!';
            $this->form_validation->set_message('radio_btn_attemps', $error);
            return FALSE;
        }
    }

    public function view_completed_single_daily_surveys() {
        $uemail = $this->input->post('email');
        $rtn_arr = $this->daily_survey_model->get_single_daily_survey($this->sid);
        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $data['json_str'] = $this->daily_survey_model->load_daily_surveys_answers($uemail,$this->sid);
        $this->load->view('view_submitted_single_daily_survey', $data);
    }

}
