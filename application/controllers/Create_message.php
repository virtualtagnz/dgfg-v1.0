<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_message extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $data['senders_arr'] = $this->message_model->get_senders();
        $data['main_content'] = 'create_message_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function create_msg() {
        $this->form_validation->set_rules('msg_subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('msg_to', 'To', 'trim|required');
        $this->form_validation->set_rules('msg', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $msg_data = array();
            $msg_data['msg_ef_id'] = NULL;
            $msg_data['subject'] = $this->input->post('msg_subject');
            $msg_data['mag'] = $this->input->post('msg');
            $sender_list = preg_replace('/\s+/', '', $this->input->post('msg_to'));
            $clear_whitespace = explode(",", $sender_list);
            $arr['temp'] = array_filter($clear_whitespace);
            $i = 0;
            foreach ($arr['temp'] as $value) {
                preg_match('~<(.*?)>~', $value, $output);
                $msg_data['msg_to'][$i] = $output[1];
                $i++;
            }
            $rtn_msg = $this->message_model->save_message($msg_data);
            if ($rtn_msg) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

    public function reply_msg($msg_id) {
        $this->form_validation->set_rules('msg_to', 'To', 'trim|required');
        $this->form_validation->set_rules('msg', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data_arr['msg'] = $this->message_model->load_single_message($msg_id);
            $data_arr['send_res'] = $this->message_model->load_senders_receivers($msg_id);
            $data_arr['reply_msg'] = $this->message_model->load_reply_msg($msg_id);
            $data_arr['senders_arr'] = $this->message_model->get_senders();
            $data_arr['main_content'] = 'single_message';
            $this->load->view('include/dashboard_template', $data_arr);
        } else {
            $msg_data = array();
            $msg_data['msg_ef_id'] = $msg_id;
            $msg_data['subject'] = $this->input->post('msg_sub');
            $msg_data['mag'] = $this->input->post('msg');
            $sender_list = preg_replace('/\s+/', '', $this->input->post('msg_to'));
            $clear_whitespace = explode(",", $sender_list);
            $arr['temp'] = array_filter($clear_whitespace);
            $i = 0;
            foreach ($arr['temp'] as $value) {
                preg_match('~<(.*?)>~', $value, $output);
                $msg_data['msg_to'][$i] = $output[1];
                $i++;
            }
            $rtn_msg = $this->message_model->save_message($msg_data);
            if ($rtn_msg) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

}
