<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_services extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['main_content'] = 'view_services';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function get_services() {
        $this->load->model('service_model');
        $cod_arr = $this->service_model->view_all_services();
        echo $cod_arr;
    }
}

