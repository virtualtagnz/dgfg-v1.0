<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_daily_surveys extends CI_Controller {
    
    public $sid;
    public function __construct() {
        parent::__construct();
        $this->sid = $this->encrypt->decode(end($this->uri->segments));
        $this->load->model('daily_survey_model');
        $this->load->library('form_validation');
    }
    
    public function index() {
        $rtn_arr = $this->daily_survey_model->get_single_daily_survey($this->sid);
        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $data['json_str'] = $this->daily_survey_model->get_survey_qa($this->sid);
        $data['main_content'] = 'edit_daily_surveys';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function mod_daily_survey() {
        $this->form_validation->set_rules('dsurvey_name', 'Survey Name', 'trim|required');
        $this->form_validation->set_rules('dsurvey_date', 'Date of survey', 'trim|required');
        $this->form_validation->set_rules('dsurvey_pnts', 'Points for survey', 'trim|required');
        $this->form_validation->set_rules('dse_tab', 'dse_tab', 'callback_task_validator');
        $temp = $this->input->post('json_str');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        }  else {
            $ds_arr = array();
            $ds_arr['dsurvey_name'] = $this->input->post('dsurvey_name');
            $ds_arr['dsurvey_date'] = $this->input->post('dsurvey_date'); 
            $ds_arr['dsurvey_points'] = $this->input->post('dsurvey_pnts');
            $ds_arr['dsurvey_json'] = $this->input->post('json_str');
            $rtn_val = $this->daily_survey_model->edit_daily_survey($ds_arr,  $this->sid);
            if($rtn_val){
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            }  else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }
    
    public function task_validator(){
        if($this->input->post('json_str')=="[]"){
            $this->form_validation->set_message('task_validator', 'Please enter question(s) and answer(s)');
            return FALSE;
        }  else {
            return TRUE;
        }
    }
    
}

