<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('survey_model');
        $data['survey_info'] = $this->survey_model->get_survey_section();
        $this->load->view('include/header');
        $this->load->view('survey_view', $data);
    }

    public function save_survey() {
        $this->load->library('form_validation');

        for ($i = 0; $i < 26; $i++) {
            $this->form_validation->set_rules('Q' . ($i + 1), 'Answer', 'callback_radio_btn_attemps[' . 'Q' . ($i + 1) . ']');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $sur_ans = array();
            for ($j = 0; $j < 26; $j++) {
                $sur_ans[$j] = $this->input->post('Q' . ($j + 1));
            }
            $this->load->model('survey_model');
            $rt_msg = $this->survey_model->save_survey_data($sur_ans);
            if ($rt_msg) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

    public function radio_btn_attemps($str, $r_name) {
        $checked_arr = $this->input->post($r_name);
        $count = count($checked_arr);
        if ($count == 0) {
            $error = 'Please select one answer!';
            $this->form_validation->set_message('radio_btn_attemps', $error);
            return FALSE;
        }
    }
    
    public function create_url() {
        $this->load->model('survey_model');
        $sur_id = $this->survey_model->load_survey_id();
        echo 'survey/display_survey/'.$sur_id;
    }
    

    public function display_survey() {
        $this->load->model('survey_model');
        $sur_id = $this->survey_model->load_survey_id();
        $data['survey_info'] = $this->survey_model->get_survey_section();
        $data['survey_answers'] = $this->survey_model->load_survey_result($sur_id);
        $data['main_content'] = 'view_completed_survey';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function display_survey_cod($sur_id) {
        $this->load->model('survey_model');
        $data['survey_info'] = $this->survey_model->get_survey_section();
        $data['survey_answers'] = $this->survey_model->load_survey_result($this->encrypt->decode($sur_id));
        $data['main_content'] = 'view_completed_survey';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function display_single_survay_completed($sur_id) {
        $this->load->model('survey_model');
        $data['survey_info'] = $this->survey_model->get_survey_section();
        $data['survey_answers'] = $this->survey_model->load_survey_result($this->encrypt->decode($sur_id));
        $this->load->view('single_complete_survey',$data);
    }
    
    
    public function load_all_surveys($uid) {
        $this->load->model('survey_model');
        $data['json_str'] = $this->survey_model->load_submited_surveys($uid);
        $data['main_content'] = 'view_submitted_surveys';
        $this->load->view('include/dashboard_template', $data);
    }

}
