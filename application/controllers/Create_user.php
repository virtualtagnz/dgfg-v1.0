<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_user extends CI_Controller {

    public $utype = array();
    public $img;

    public function __construct() {
        parent::__construct();
        $this->utype['curr_user'] = end($this->uri->segments);
    }

    public function index() {
        $data['user_type'] = $this->utype['curr_user'];
        switch ($this->utype['curr_user']) {
            case '1':
                $data['page_title'] = 'CREATE YOUTH COORDINATOR';
                break;
            case '2':
                $data['page_title'] = 'CREATE SKWAD LEADER';
                break;
            case '3':
                $data['page_title'] = 'CREATE SKWAD MEMBER';
                break;
        }
        $data['main_content'] = 'create_user_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function createusers() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_email', 'Email', 'trim|valid_email|required|is_unique[user_tab.user_email]');
        $this->form_validation->set_rules('pwd', 'Password', 'trim|required');
        $this->form_validation->set_rules('re_pwd', 'Confirm Password', 'trim|required|matches[re_pwd]');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');

        if ($this->input->post('fb_linq') != "") {
            $this->form_validation->set_rules('fb_linq', 'Facebook Link', 'trim|callback_valid_url');
        }
        if ($this->input->post('inst_linq') != "") {
            $this->form_validation->set_rules('inst_linq', 'Instagram Link', 'trim|callback_valid_url');
        }
        if (!empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Upload image', 'trim|callback_img_check');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $add_user = array();
            $add_user['email'] = $this->input->post('user_email');
            $add_user['pwd'] = md5($this->input->post('pwd'));
            $add_user['re_pwd'] = md5($this->input->post('re_pwd'));
            $add_user['stat'] = $this->input->post('status');
            $add_user['fname'] = $this->input->post('first_name');
            $add_user['lname'] = $this->input->post('last_name');
            $add_user['gender'] = $this->input->post('gender');
            $add_user['mob'] = $this->input->post('mobile');
            $add_user['prof_img'] = $this->img;
            $add_user['des'] = $this->input->post('des');
            $add_user['fb'] = $this->input->post('fb_linq');
            $add_user['inst'] = $this->input->post('inst_linq');
            if ($this->utype['curr_user'] === '1') {
                $add_user['smc'] = $this->input->post('smc');
            } else {
                $add_user['ethnicity'] = $this->input->post('ethnicity');
            }
            /*
             * create youth coodinator
             */
            if ($this->utype['curr_user'] === '1') {
                $this->load->model('create_coordinator');
                $rt_msg = $this->create_coordinator->addCoodinator($add_user);
                if ($rt_msg) {
                    $this->session->set_flashdata('success_msg', 'Youth coordinator created successfully');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                    redirect(current_url());
                }
            }
            /*
             * create SKWAD Leader
             */ elseif ($this->utype['curr_user'] === '2') {
                $this->load->model('create_leader');
                $rt_msg = $this->create_leader->addLeader($add_user);
                if ($rt_msg) {
                    $this->session->set_flashdata('success_msg', 'SKAWD Leader created successfully');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                    redirect(current_url());
                }
            }

            /*
             * create SKWAD Member
             */ elseif ($this->utype['curr_user'] === '3') {
                $this->load->model('create_member');
                $rt_msg = $this->create_member->addMember($add_user);
                if ($rt_msg) {
                    $this->session->set_flashdata('success_msg', 'SKAWD Member created successfully');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                    redirect(current_url());
                }
            }
        }
    }

    public function img_check() {
        $this->load->helper('db_helper');
        $enc_image = uploadProfImg("image");
        if (isset($enc_image['imageError'])) {
            $this->form_validation->set_message('img_check', $enc_image['imageError']);
            return FALSE;
        } else {
            $this->img = base_url().'public/uploads/profile_imgs/'.$enc_image;
            return $enc_image;
        }
    }

    /* validate url (Facebook, instgram */

    public function valid_url($url) {
        return (filter_var($url, FILTER_VALIDATE_URL) !== FALSE);
    }

}
