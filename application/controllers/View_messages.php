<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_messages extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('message_model');
    }
    
    public function index() {
        $data['main_content'] = 'view_my_messages';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function load_msg() {
        $data_arr="";
        if($this->session->userdata('user_role')==='2'){
            $data_arr = $this->message_model->load_coodinator_messages();
        }  else {
            $data_arr = $this->message_model->load_all_messages();
            if($data_arr == '[]'){
                $data_arr = $this->message_model->load_intermediate_messages();
            }
        }
        echo $data_arr;
    }
    
    public function get_single_msg($msg_id) {
        $data_arr['msg'] = $this->message_model->load_single_message($this->encrypt->decode($msg_id));
        $data_arr['send_res'] = $this->message_model->load_senders_receivers($this->encrypt->decode($msg_id));
        $data_arr['reply_msg'] = $this->message_model->load_reply_msg($this->encrypt->decode($msg_id));
        $data_arr['senders_arr'] = $this->message_model->get_senders();
        $data_arr['main_content'] = 'single_message';
        $this->load->view('include/dashboard_template', $data_arr);
    }
}

