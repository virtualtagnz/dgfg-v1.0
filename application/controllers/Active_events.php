<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Active_events extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['main_content'] = 'active_events_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function load_active_events() {
        $this->load->model('event_model');
        $cod_arr = $this->event_model->get_active_events();
        echo $cod_arr;
    }
}

