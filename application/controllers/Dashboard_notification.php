<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_notification extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('notification_model');
        $this->load->model('message_model');
    }

    public function index() {
        $data_arr = $this->notification_model->load_my_notification();
        $data_arr1 = $this->message_model->load_my_messages();
        echo $data_arr.'+'.$data_arr1;
    }

    public function mark_as_view($nid) {
        $rt_val = $this->notification_model->update_view_flag($nid);
        if ($rt_val) {
            $data_arr = $this->notification_model->load_my_notification();
            echo $data_arr;
        }
    }
    
        public function set_as_mark($msg_id) {
        $rtn_msg = $this->message_model->update_message_flag($this->encrypt->decode($msg_id));
         if ($rtn_msg) {
            $data_arr = $this->message_model->load_my_messages();
            echo $data_arr;
        }
    }

}
