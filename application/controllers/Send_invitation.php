<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Send_invitation extends CI_Controller {

    public $user_id;

    public function __construct() {
        parent::__construct();
        $this->user_id = end($this->uri->segments);
        $this->load->model('event_model');
        $this->load->model('create_member');
        $this->load->library('form_validation');
        $this->load->helper('db_helper');
    }

    public function index() {
        $data['my_event'] = $this->event_model->get_selected_event_by_leader();
        $temp = $this->create_member->getSingleMember($this->user_id);
        $data['email'] = $temp['uemail'];
        $data['uid'] = $this->user_id;
        $this->load->view('send_invitation_view', $data);
    }

    public function send_member_invitation() {
        $this->form_validation->set_rules('ev_name', 'Event Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_err', 'error');
            redirect(site_url('view_user/3'));
        } else {
            $invite_arr = array();
            $invite_arr['ev_invite_event_ref'] = $this->encrypt->decode($this->input->post('event_id'));
            $invite_arr['ev_invite_mem_ref'] = $this->input->post('u_id');
            $invite_arr['ev_invite_accept'] = 'N';
            $date = new DateTime();
            $invite_arr['ev_invite_date'] = $date->format('Y-m-d H:i:s');
            $invite_arr['ev_invite_by'] = $this->session->userdata('username');
            $mem_email = $this->input->post('user_email');
            $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
            $rtn_cnt = $this->event_model->is_already_interested($invite_arr['ev_invite_event_ref'], $invite_arr['ev_invite_mem_ref']);
            if (intval($rtn_cnt) > 0) {
                $this->session->set_flashdata('already_interest', 'error');
                redirect(site_url('view_user/3'));
            } else {
                $cnt = $this->event_model->already_invited($invite_arr['ev_invite_mem_ref'], $invite_arr['ev_invite_event_ref']);
                if (intval($cnt) > 0) {
                    $this->session->set_flashdata('already_invite', 'error');
                    redirect(site_url('view_user/3'));
                } else {
                    $msg_arr = $this->event_model->add_invitation($invite_arr, $mem_email, $sent_id);
                    if ($msg_arr) {
                        $this->session->set_flashdata('success_msg', 'success');
                        redirect(site_url('view_user/3'));
                    } else {
                        $this->session->set_flashdata('error_msg', 'error');
                        redirect(site_url('view_user/3'));
                    }
                }
            }
        }
    }

}
