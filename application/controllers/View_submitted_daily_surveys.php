<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_submitted_daily_surveys extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('daily_survey_model');
    }
    
    public function index() {
        $data['main_content'] = 'view_submitted_daily_survey';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function load_submitted_daily_surveys() {
        $json_str = $this->daily_survey_model->get_all_submitted_surveys();
        echo $json_str;
    }
}

