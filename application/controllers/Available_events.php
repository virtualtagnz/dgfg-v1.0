<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Available_events extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['main_content'] = 'available_events_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function available_events_view() {
        $this->load->model('event_model');
        $rtn_arr = $this->event_model->get_available_events();
        echo $rtn_arr;
    }
    
}

