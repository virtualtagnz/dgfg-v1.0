<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_edit extends CI_Controller {

    public $para1 = array();
    public $para2 = array();
    public $img;

    public function __construct() {
        parent::__construct();
        $slice_arr = array_slice($this->uri->segment_array(), -2, 2, true);
        $segment_arr = array();
        foreach ($slice_arr as $value) {
            $segment_arr[] = $value;
        }
        $this->para1 = $segment_arr[0];
        $this->para2 = $segment_arr[1];
    }

    public function index() {
        $rtn_arr = array();
        if ($this->para2 === '1') {
            $this->load->model('create_coordinator');
            $rtn_arr = $this->create_coordinator->getSingleCoodinator($this->para1);
        } elseif ($this->para2 === '2') {
            $this->load->model('create_leader');
            $rtn_arr = $this->create_leader->getSingleLeader($this->para1);
        } elseif ($this->para2 === '3') {
            $this->load->model('create_member');
            $rtn_arr = $this->create_member->getSingleMember($this->para1);
        }

        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }

        $data['user_type'] = $this->para2;
        switch ($data['user_type']) {
            case '1':
                $data['page_title'] = 'YOUTH COORDINATOR PROFILE: ' . $data['fname'] . ' ' . $data['lname'];
                break;
            case '2':
                $data['page_title'] = 'SKWAD LEADER PROFILE: ' . $data['fname'] . ' ' . $data['lname'];
                break;
            case '3':
                $data['page_title'] = 'SKWAD MEMBER PROFILE: ' . $data['fname'] . ' ' . $data['lname'];
                break;
        }
        $data['seg1'] = $this->para1;
        $data['seg2'] = $this->para2;
        $data['main_content'] = 'user_edit_view';
        $this->load->view('include/dashboard_template', $data);
        //var_dump(array_slice($this->uri->segment_array(),-2,2,true));
    }

    public function modify_user() {

        $this->load->library('form_validation');
        $this->load->helper('db_helper');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|valid_email|required');
        $valid = TRUE;
        switch ($this->para2) {
            case '1':
                $valid = email_checker($this->input->post('user_email'), $this->para1, "dgfg_coordinator_tab","cod_user_ref","cod_id");
                break;
            case '2':
                $valid = email_checker($this->input->post('user_email'), $this->para1, "dgfg_leader_tab","led_user_ref","led_id");
                break;
            case '3':
                $valid = email_checker($this->input->post('user_email'), $this->para1, "dgfg_member_tab","mem_user_ref","mem_id");
                break;
        }

        if ($valid === FALSE) {
            $this->form_validation->set_rules('user_email', 'Email', 'trim|valid_email|required|is_unique[user_tab.user_email]');
        }

        $this->form_validation->set_rules('pwd', 'Password', 'trim|matches[re_pwd]');
        $this->form_validation->set_rules('re_pwd', 'Re-enter Password', 'trim');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        if (!empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Upload image', 'trim|callback_img_check');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $mod_info = array();
            $mod_info['uid'] = $this->para1;
            $mod_info['email'] = $this->input->post('user_email');
            if ($this->input->post('pwd') === "") {
                if($this->para2==='1'){
                    $clm_id = get_column("cod_user_ref", "cod_id", "dgfg_coordinator_tab", $mod_info['uid']);
                }elseif ($this->para2==='2') {
                    $clm_id = get_column("led_user_ref", "led_id", "dgfg_leader_tab", $mod_info['uid']);
                }elseif ($this->para2==='3') {
                    $clm_id = get_column("mem_user_ref", "mem_id", "dgfg_member_tab", $mod_info['uid']);
                }
                $pre_pwd = get_column("user_pwd", "user_id", "user_tab", $clm_id);
                $mod_info['pwd'] = $pre_pwd;
            } else {
                $mod_info['pwd'] = md5($this->input->post('pwd'));
            }
            $mod_info['stat'] = $this->input->post('status');
            $mod_info['fname'] = $this->input->post('first_name');
            $mod_info['lname'] = $this->input->post('last_name');
            $mod_info['gender'] = $this->input->post('gender');
            $mod_info['mob'] = $this->input->post('mobile');
            if (!empty($_FILES['image']['name'])) {
                $mod_info['prof_img'] = $this->img;
            } else {
                $mod_info['prof_img'] = $this->input->post('old_img');
            }

            $mod_info['des'] = $this->input->post('des');
            $mod_info['fb'] = $this->input->post('fb_linq');
            $mod_info['inst'] = $this->input->post('inst_linq');
            if ($this->para2 === '1') {
                $mod_info['smc'] = $this->input->post('smc');
            } else {
                $mod_info['ethnicity'] = $this->input->post('ethnicity');
            }

            if ($this->para2 === '1') {
                $this->load->model('create_coordinator');
                $rt_msg = $this->create_coordinator->modify_coodinator($mod_info);
                if ($rt_msg) {
                    $this->session->set_flashdata('success_msg', 'Youth coordinator modify successfully');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                    redirect(current_url());
                }
            } elseif ($this->para2 === '2') {
                $this->load->model('create_leader');
                $rt_msg = $this->create_leader->modify_leader($mod_info);
                if ($rt_msg) {
                    $this->session->set_flashdata('success_msg', 'SKWAD leader modify successfully');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                    redirect(current_url());
                }
            }elseif ($this->para2 === '3') {
                $this->load->model('create_member');
                $rt_msg = $this->create_member->modify_member($mod_info);
                if ($rt_msg) {
                    $this->session->set_flashdata('success_msg', 'SKWAD member modify successfully');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                    redirect(current_url());
                }
            }
        }
    }

    public function img_check() {
        $this->load->helper('db_helper');
        $enc_image = uploadProfImg("image");
        if (isset($enc_image['imageError'])) {
            $this->form_validation->set_message('img_check', $enc_image['imageError']);
            return FALSE;
        } else {
            $this->img = base_url().'public/uploads/profile_imgs/'.$enc_image;
            return $enc_image;
        }
    }

}
