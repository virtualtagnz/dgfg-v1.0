<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Completed_events extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['main_content'] = 'complete_events_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function load_comp_events() {
        $this->load->model('event_model');
        $cod_arr = $this->event_model->get_complete_events();
        echo $cod_arr;
    }
}
