<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_service extends CI_Controller {

    public $img=array();

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('survey_model');
        $data['service_cat'] = $this->survey_model->load_survey_section();
        $data['main_content'] = 'create_service_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function add_service() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('sprovider', 'Name of Provider', 'trim|required');
        $this->form_validation->set_rules('scategory', 'Provider Category', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('slevel', 'Level', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if (!empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Upload image', 'trim|callback_file_check[image]');
        }
        if (!empty($_FILES['staff_image']['name'])) {
            $this->form_validation->set_rules('staff_image', 'Upload image', 'trim|callback_file_check[staff_image]');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $service_dt = array();
            $service_dt['sp_name'] = $this->input->post('sprovider');
            $service_dt['sp_cat'] = $this->input->post('scategory');
            $service_dt['sp_summery'] = $this->input->post('ssummary');
            $service_dt['sp_cost'] = $this->input->post('scost');
            $service_dt['sp_phn'] = $this->input->post('scontact');
            $service_dt['sp_key'] = $this->input->post('skey');
            $service_dt['sp_list'] = $this->input->post('slist');
            $service_dt['sp_add'] = $this->input->post('saddress');
            $service_dt['sp_hrs'] = $this->input->post('shours');
            $service_dt['sp_email'] = $this->input->post('email');
            $service_dt['sp_after_phn'] = $this->input->post('safternum');
            $service_dt['sp_img'] = $this->img[0];
            $service_dt['sp_staff_img'] = $this->img[1];
            $service_dt['sp_web'] = $this->input->post('sweb');
            $service_dt['sp_level'] = $this->input->post('slevel');

            $this->load->model('service_model');
            $rt_msg = $this->service_model->create_service($service_dt);
            if ($rt_msg) {
                $this->session->set_flashdata('success_msg', 'Service directory create successfully');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                redirect(current_url());
            }
        }
    }

    public function file_check($str, $field_name) {
        $this->load->helper('db_helper');
        $enc_file = upload_media_file($this->input->post('sprovider'), $field_name, "img", 120, 120);
        if (isset($enc_file['file_error'])) {
            $this->form_validation->set_message('file_check', $enc_file['file_error']);
            return FALSE;
        } else {
            if ($field_name == "image") {
                $this->img[0] = $enc_file['enc_img'];
            } elseif ($field_name == "staff_image") {
                $this->img[1] = $enc_file['enc_img'];
            }
        }
    }

}
