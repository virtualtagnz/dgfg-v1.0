<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Load_service extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('service_model');
    }
    
    public function index() {
        $data['service_cat'] = $this->service_model->load_services_according_survey();
        $data['main_content'] = 'view_my_services';
        $this->load->view('include/dashboard_template', $data);
    }
}

