<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_daily_survey extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('daily_survey_model');
    }
    
    public function index() {
        $data['main_content'] = 'view_all_daily_survey';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function load_all_surveys() {
        $json_str = $this->daily_survey_model->get_all_daily_surveys();
        echo $json_str;
    }
    
}

