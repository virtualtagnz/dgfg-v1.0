<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Socialmedia_api extends CI_Controller {

    public $event_id;

    public function __construct() {
        parent::__construct();
        //$this->event_id = $this->encrypt->decode(end($this->uri->segments));
        $this->load->library('facebooksdk');
        $this->fb = $this->facebooksdk;
    }

    public function index() {
        
    }

    public function share_event_fb($id) {
        $this->event_id = $id;
        $this->load->model('event_model');
        $rtn_arr = $this->event_model->get_single_event($this->event_id);
        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $act = $this->fb->getAccessToken();
        $params = ['message' => $data['event_name'] . ' - ' . $data['event_comments']];
        $response = $this->fb->post("/me/feed", $params, $act);
//
//        echo "<pre>";
//        print_r($response);
//        echo "</pre>";
    }

}
