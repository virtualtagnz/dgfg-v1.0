<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('facebooksdk');
        $this->fb = $this->facebooksdk;
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->model('registration_model');
        $this->load->model('dashboard_model');
        $this->load->helper('db_helper');
    }

    public function index() {
        $this->load->view('include/header');
        $cb = "http://dgfg.websitepreview.co.nz/index.php/login/user_login_facebook";
        $url = $this->fb->getLoginUrl($cb);
        $data['login_url'] = $url;
        $data['up_coming_ev'] = $this->dashboard_model->up_coming_events();
        $this->load->view('login_view', $data);
    }

    public function user_login() {
        $this->form_validation->set_rules('user_name', 'Email', 'trim|valid_email|required');
        $this->form_validation->set_rules('user_pwd', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $u_data = array(
                'user_name' => $this->input->post('user_name'),
                'user_pwd' => $this->input->post('user_pwd')
            );
            $this->load->model('login_model');
            $stat = $this->login_model->login_dgfg($u_data);
            if ($stat === 'sux') {
                $this->load->model('survey_model');
                $is_sur_done = $this->survey_model->is_survey_done($this->input->post('user_name'));

                $this->login_model->get_user_img($this->input->post('user_name'), $this->session->userdata('user_role'));
                if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') {
//                    $data['main_content'] = 'super_dashboard';
//                    $this->load->view('include/dashboard_template', $data);
                    redirect('dashboard');
                } elseif ($this->session->userdata('user_role') === '3') {
                    if (intval($is_sur_done) > 0) {
//                        $data['main_content'] = 'leader_dashboard';
//                        $this->load->view('include/dashboard_template', $data);
                        redirect('dashboard');
                    } else {
                        $data['survey_info'] = $this->survey_model->get_survey_section();
                        $this->load->view('include/header');
                        $this->load->view('survey_view', $data);
                    }
                } elseif ($this->session->userdata('user_role') === '4') {
                    if (intval($is_sur_done) > 0) {
                        redirect('dashboard');
//                        $data['main_content'] = 'member_dashboard';
//                        $this->load->view('include/dashboard_template', $data);
                    } else {
                        $data['survey_info'] = $this->survey_model->get_survey_section();
                        $this->load->view('include/header');
                        $this->load->view('survey_view', $data);
                    }
                }
            } elseif ($stat === 'unsux') {
                $this->session->set_flashdata('error_msg1', 'User name and password not match, Try again');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg2', 'Your account has been deactivated, please contact web admin');
                redirect(current_url());
            }
        }
    }

// if the user wants to log via facebook

    function user_login_facebook() {

        $data = "";
        $act = $this->fb->getAccessToken();
        $data = $this->fb->getUserData($act);

        //$this->load->view('status', $data);
        $this->load->model('login_model');
        $this->load->model('create_member');
        //check ths user is already exists or not
        $fbUserValidate = $this->login_model->login_fb_dgfg_exists($data);
        //if not enter to the database
        if ($fbUserValidate == FALSE) {
            $fbUserAdd = $this->create_member->addFbMember($data);
            if ($fbUserAdd == FALSE) {
                $this->session->set_flashdata('error_msg2', 'Your account has been decativate, please contact web admin');
                redirect(current_url());
            } else {
                $fbUserValidate = $this->login_model->login_fb_dgfg_exists($data);
            }
        }

        if ($fbUserValidate == TRUE) {
            $this->take_survey();
        }
//        echo $data["id"];
//        echo $data["name"];
//        echo $data["email"];
//        echo $data["first_name"];
//        echo $data["last_name"];
//        echo $data["gender"];
//        echo $data["picture"]["url"];
    }

    function take_survey() {
        $this->load->model('survey_model');
        $is_sur_done = $this->survey_model->is_survey_done($this->session->userdata('username'));

        $this->login_model->get_user_img($this->session->userdata('username'), $this->session->userdata('user_role'));
        if ($this->session->userdata('user_role') === '4') {
            if (intval($is_sur_done) > 0) {
                $data['main_content'] = 'member_dashboard';
                $this->load->view('include/dashboard_template', $data);
            } else {
                $data['survey_info'] = $this->survey_model->get_survey_section();
                $this->load->view('include/header');
                $this->load->view('survey_view', $data);
            }
        }
    }

    public function user_registration() {
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('reg_user_name', 'Email', 'trim|valid_email|required|xss_clean|callback_validate_duplicate_email');
        $this->form_validation->set_rules('reg_user_pwd', 'Password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $reg_arr = array();
            $reg_arr['email'] = $this->input->post('reg_user_name');
            $reg_arr['pwd'] = md5($this->input->post('reg_user_pwd'));
            $reg_arr['fname'] = $this->input->post('first_name');
            $reg_arr['lname'] = $this->input->post('last_name');
            $rtn_msg = $this->registration_model->register_user($reg_arr);
            if ($rtn_msg) {
                $this->session->set_flashdata('success_msg', 'done');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

    public function validate_duplicate_email() {
        $email = $this->input->post('reg_user_name');
        $rtn = email_validator($email);
        if ($rtn === FALSE) {
            $this->form_validation->set_message('validate_duplicate_email', 'Sorry, that email address is already used');
            return FALSE;
        }  else {
            return TRUE;
        }
    }

}
