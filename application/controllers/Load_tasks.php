<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Load_tasks extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('task_model');
        $rt_msg = $this->task_model->load_events();
        $data['ev_info'] = $rt_msg;
        $data['main_content'] = 'view_my_tasks';
        $this->load->view('include/dashboard_template', $data);
    }

    public function load_my_tasks($ev_id) {
        $this->load->model('task_model');
        $rt_msg = $this->task_model->load_tasks($this->encrypt->decode($ev_id));
        echo $rt_msg;
    }

    public function task_complete() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('ev_name', 'Name of Event', 'trim|required');
        $this->form_validation->set_rules('ev_tasks', 'Event Tasks', 'callback_check_box_attemps');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $com_tsk = array();
            $com_tsk['event_ref'] = $this->input->post('event_id');
            $com_tsk['event_task_id'] = $this->input->post('ev_tasks');
            $com_tsk['task_stat'] = 'C';
            $date = new DateTime();
            $com_tsk['date_complete'] = $date->format('Y-m-d H:i:s');

            $this->load->model('task_model');
            $rt_msg = $this->task_model->up_task_complete($com_tsk);
            if ($rt_msg) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

    public function check_box_attemps() {
        $checked_arr = $this->input->post('ev_tasks');
        $count = count($checked_arr);
        if ($count == 0) {
            $error = 'Please select at least one task to complete';
            $this->form_validation->set_message('check_box_attemps', $error);
            return FALSE;
        }
    }

}
