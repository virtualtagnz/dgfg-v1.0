<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_event extends CI_Controller {

    public $event_id;
    public $img_file;
    public $vid_file;
    public $count;

    public function __construct() {
        parent::__construct();
        $this->event_id = $this->encrypt->decode(end($this->uri->segments));
        $this->load->model('event_model');
        $this->load->library('form_validation');
        $this->load->helper('db_helper');
        $this->load->model('create_leader');
    }

    public function index() {
        //$this->load->model('event_model');
        $rtn_arr = $this->event_model->get_single_event($this->event_id);

        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }
        $data['json_str'] = $this->event_model->get_single_task_list($this->event_id);
        $data['main_content'] = 'edit_event_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function modify_event() {
        //$this->load->library('form_validation');
        //$this->load->helper('db_helper');
//        $curr_name = get_column("event_name", "event_id", "event_tab", $this->event_id);
//        $change_name = $this->input->post('evname');
//        if ($curr_name !== $change_name) {
//            if (file_exists('public/uploads/' . $curr_name)) {
//                rename('public/uploads/' . $curr_name, 'public/uploads/' . $change_name);
//            }
//        }
//
//        $this->form_validation->set_rules('evname', 'Event Name', 'trim|required');
        $this->form_validation->set_rules('evdate', 'Event Date', 'trim|required');
        $this->form_validation->set_rules('evstime', 'Event Start Time', 'trim|required');
        $this->form_validation->set_rules('evetime', 'Event End time', 'trim|required');
        $this->form_validation->set_rules('evloc', 'Event Location', 'trim|required');
        $this->form_validation->set_rules('ev_task', 'ev_task', 'callback_task_validator');
        if ($_FILES['evid']['name']) {
            $this->form_validation->set_rules('evid', 'Upload video', 'trim|callback_file_check[vid]');
        } elseif ($this->input->post('old_vid')) {
            $this->vid_file = $this->input->post('old_vid');
        }

        if ($_FILES['image']['name']) {
            $this->form_validation->set_rules('image', 'Upload image', 'trim|callback_file_check[img]');
        } elseif ($this->input->post('old_img')) {
            $this->img_file = $this->input->post('old_img');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $event_data = array();
            $event_data['event_name'] = $this->input->post('hide_evname');
            $event_data['event_date'] = $this->input->post('evdate');
            $event_data['event_start'] = $this->input->post('evstime');
            $event_data['event_end'] = $this->input->post('evetime');
            $event_data['event_loc'] = $this->input->post('evloc');
            $event_data['event_promo1'] = $this->input->post('evpromo1');
            $event_data['event_promo2'] = $this->input->post('evpromo2');
            $event_data['event_promo3'] = $this->input->post('evpromo3');
            $event_data['event_promo4'] = $this->input->post('evpromo4');
            $event_data['event_plan'] = $this->input->post('evplan');
            $event_data['event_comments'] = $this->input->post('evcomments');
            $event_data['event_video_linq'] = $this->vid_file;
            $event_data['event_img_linq'] = $this->img_file;
            $event_data['tasks'] = $this->input->post('json_str');

            $rtn_arr = $this->event_model->modify_event($event_data, $this->event_id);
            if ($rtn_arr) {
                $this->session->set_flashdata('success_msg', 'done');
                $this->index();
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                $this->index();
            }
        }
    }

    public function upload_image_view() {
        $data['main_content'] = "event_upload_galary_view";
        $data_single_event = $this->event_model->get_single_event($this->event_id);
        $data['event_id'] = $this->encrypt->encode($data_single_event['event_id']);
        $data['event_name'] = $data_single_event['event_name'];
        //get pending images
        $data['pending_images'] = $this->event_model->view_authors_images_eventgalary($this->event_id, 0);
        //get approved images
        $data['approve_images'] = $this->event_model->view_authors_images_eventgalary($this->event_id, 1);
        $this->load->view('include/dashboard_template', $data);
    }

    public function upload_images() {
        $rtn_arr = FALSE;
        $fv_error = "";
        $this->form_validation->set_rules('evname', 'Event Name', 'trim|required');
        $this->form_validation->set_rules('files', 'Image', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('form_msg', 'error');
        } else {
            $path = $this->input->post('evname');
            //$rslt_upload = upload_event_multiplefile("image", $path);
            $rslt_upload = upload_meadia_multiplefiles("image", $path, 851, 315);
            //print_r($rslt);
            if ($rslt_upload['response'] == TRUE) {
                //enter to the database
                $rslt_upload['event_name'] = $this->input->post('evname');
                $rslt_upload['event_id'] = $this->event_id;
                $rtn_arr = $this->event_model->add_images_eventgalary($rslt_upload);
            }
            if ($rtn_arr) {
                $this->session->set_flashdata('success_msg', 'done');
            } else {
                $this->session->set_flashdata('detail', $rslt_upload['message']);
                $this->session->set_flashdata('error_msg', 'error');
            }
        }

//        $data['main_content'] = "event_upload_galary_view";
//        $data_single_event = $this->event_model->get_single_event($this->event_id);
//        $data['event_id'] = $this->encrypt->encode($data_single_event['event_id']);
//        $data['event_name'] = $data_single_event['event_name'];
//        //get pending images
//        $data['pending_images'] = $this->event_model->view_authors_images_eventgalary($this->event_id, 0);
//        //get approved images
//        $data['approve_images'] = $this->event_model->view_authors_images_eventgalary($this->event_id, 1);
//        $this->load->view('include/dashboard_template', $data);
        redirect(site_url()."/edit_event/upload_image_view/".$this->encrypt->encode($this->event_id));
    }

    public function file_check($str, $type) {
        //$this->load->helper('db_helper');
        $enc_file = array();
        if ($type === 'vid') {
            $enc_file = upload_media_file($this->input->post('evname'), "evid", $type);
        } elseif ($type === 'img') {
            $enc_file = upload_media_file($this->input->post('evname'), "image", $type, 851, 315);
        }

        if (isset($enc_file['file_error'])) {
            $this->form_validation->set_message('file_check', $enc_file['file_error']);
            return FALSE;
        } else {
            if (isset($enc_file['enc_vid'])) {
                $this->vid_file = $enc_file['enc_vid'];
            }
            if (isset($enc_file['enc_img'])) {
                $this->img_file = $enc_file['enc_img'];
            }
        }
    }

    public function task_validator() {
        if ($this->input->post('json_str') == "[]") {
            $this->form_validation->set_message('task_validator', 'Please enter task(s)');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function approve_event() {
        $ev_created_arr = $this->create_leader->get_leader_event_created($this->event_id);
        $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
        $ev_arr = $this->event_model->get_single_event($this->event_id);
        $msg = $ev_arr['event_name'] . 'has approved';
        $rtn_arr = $this->event_model->approve_events($this->event_id, 'A', 'A', $ev_created_arr, $sent_id, $msg);
        if ($rtn_arr) {
            $json_str = json_encode($rtn_arr);
            echo $json_str;
        } else {
            $json_str = json_encode($rtn_arr);
            echo $json_str;
        }
    }

    public function reject_event() {
        $ev_created_arr = $this->create_leader->get_leader_event_created($this->event_id);
        $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
        $ev_arr = $this->event_model->get_single_event($this->event_id);
        $msg = $ev_arr['event_name'] . '- has rejected';
        $rtn_arr = $this->event_model->approve_events($this->event_id, 'R', 'N', $ev_created_arr, $sent_id, $msg);
        if ($rtn_arr) {
            $json_str = json_encode($rtn_arr);
            echo $json_str;
        } else {
            $json_str = json_encode($rtn_arr);
            echo $json_str;
        }
    }

    public function select_event() {
        //$this->load->model('event_model');
        $rtn_arr = $this->event_model->select_events_by_leader($this->event_id);
        if ($rtn_arr) {
            $json_str = json_encode($rtn_arr);
            echo $json_str;
        } else {
            $json_str = json_encode($rtn_arr);
            echo $json_str;
        }
    }

    public function send_member_interest() {
        $mem_id = get_user_id_by_user_name($this->session->userdata('username'));
        $rtn_cnt = $this->event_model->is_already_interested($this->event_id, $mem_id);
        if (intval($rtn_cnt) > 0) {
            echo "AI";
        } else {
            $msg_arr = $this->event_model->interest_events($this->event_id);
            if ($msg_arr) {
                echo "TR";
            } else {
                echo "ER";
            }
        }
    }

    public function view_approve_image_gallery() {
        $data['main_content'] = "view_event_imagegalary";
        $data_single_event = $this->event_model->get_single_event($this->event_id);
        $data['event_id'] = $this->encrypt->encode($data_single_event['event_id']);
        $data['event_name'] = $data_single_event['event_name'];
        //get pending images
        $data['pending_images'] = $this->event_model->view_all_images_eventgalary($this->event_id, 0);
        //get approved images
        $data['approve_images'] = $this->event_model->view_all_images_eventgalary($this->event_id, 1);
        $this->load->view('include/dashboard_template', $data);
    }

    public function submit_single_image($gallery_id){
        $result = $this->event_model->submit_single_image($gallery_id);
        echo $result;
    }
    
    public function submit_images($event_id) {
        $user = $this->session->userdata('username');
        $result = $this->event_model->submit_images($event_id, $user);
        echo $result;
    }
    
    public function approve_single_image($gallery_id) {
        $result = $this->event_model->approve_single_image($gallery_id);
        echo $result;
    }
    
    public function approve_all_image($event_id){
        $result = $this->event_model->approve_all_image($event_id);
        echo $result;
    }

    public function reject_single_image($gallery_id) {
        $result = $this->event_model->reject_single_image($gallery_id);
        echo $result;
    }

    public function delete_image($gallery_id) {
        $result = $this->event_model->delete_image($gallery_id);
        echo $result;
    }
}
