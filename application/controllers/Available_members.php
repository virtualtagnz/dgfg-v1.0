<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Available_members extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('event_model');
        $rt_msg = $this->event_model->get_selected_event_by_leader();
        $data['ev_info'] = $rt_msg;
        $data['main_content'] = 'available_members_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function load_members($ev_id) {
        $this->load->model('create_member');
        $rtn_arr = $this->create_member->load_unselected_members($this->encrypt->decode($ev_id));
        echo $rtn_arr;
    }

    public function set_events_to_members() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('ev_name', 'Event Name', 'trim|required');
        $this->form_validation->set_rules('av_mem', 'Member Name', 'trim|required');
        $this->form_validation->set_rules('mem_id', 'Member Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $set = array();
            $set['sel_mem_id_ref'] = $this->encrypt->decode($this->input->post('mem_id'));
            $set['sel_event_id_ref'] = $this->encrypt->decode($this->input->post('event_id'));
            $set['selected_by'] = $this->session->userdata('username');
            $set['selected_role'] = $this->session->userdata('user_role');
            $date = new DateTime();
            $set['selected_date'] = $date->format('Y-m-d H:i:s');
            $this->load->model('create_member');
            $cod_arr = $this->create_member->set_events_available_members($set);
            if ($cod_arr) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

}
