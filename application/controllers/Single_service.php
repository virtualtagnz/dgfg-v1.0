<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Single_service extends CI_Controller {

    public $service_id;

    public function __construct() {
        parent::__construct();
        $this->load->model('service_model');
        $this->load->library('form_validation');
        $this->service_id = $this->encrypt->decode(end($this->uri->segments));
    }

    public function index() {

        $rtn_arr = $this->service_model->get_single_service($this->service_id);
        foreach ($rtn_arr[0] as $key => $value) {
            $data[$key] = $value;
        }
        $data['service_id'] = $this->service_id;
        //$this->load->view('include/header');
        $this->load->view('single_service_view', $data);
    }

    public function send_service_message($sid) {
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['service_cat'] = $this->service_model->load_services_according_survey();
            $data['main_content'] = 'view_my_services';
            $this->load->view('include/dashboard_template', $data);
            $rtn_arr = $this->service_model->get_single_service($sid);
            foreach ($rtn_arr[0] as $key => $value) {
                $data[$key] = $value;
            }
            $data['service_id'] = $sid;
            $this->load->view('single_service_view', $data);
        } else {
            $rtn_msg = "";
            $result = $this->service_model->get_service_email($sid);
            if ($result != FALSE) {
                $email = $result['sp_email'];
                $data_email = array();
                $data_email['sp_id'] = $sid;
                $data_email['to_email'] = $email;
                $data_email['body_subject'] = $this->input->post('subject');
                $data_email['body_message'] = $this->input->post('message');
                $data_email['body_email'] = $this->input->post('email');
                $data_email['body_telephone'] = $this->input->post('telephone');
                $rtn_msg = $this->service_model->send_email($data_email);
                if ($rtn_msg) {
                    $this->session->set_flashdata('success_msg', 'success');
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error_msg', 'error');
                    redirect(current_url());
                }
                redirect(site_url() . "/load_service");
            }
        }
    }

}
