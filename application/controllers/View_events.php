<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_events extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('event_model');
    }

    public function index() {
        $data['main_content'] = 'view_events_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function get_events() {
        $cod_arr = $this->event_model->get_event_data();
        echo $cod_arr;
    }

    public function load_events_calender() {
        $cod_arr = $this->event_model->get_event_dates();
        echo $cod_arr;
    }

    public function control_image_gallery() {
        if ($this->session->userdata('user_role') == 1 || $this->session->userdata('user_role') == 2) {
            $data['main_content'] = 'event_fullcontrol_view';
            $this->load->view('include/dashboard_template', $data);
        } elseif ($this->session->userdata('user_role') == 3) {
            $data['main_content'] = 'event_leadercontrol_view';
            $this->load->view('include/dashboard_template', $data);
        } elseif ($this->session->userdata('user_role') == 4) {
            $data['main_content'] = 'event_membercontrol_view';
            $this->load->view('include/dashboard_template', $data);
        }
    }
    
    public function load_gallery_control(){
        $cod_arr = $this->event_model->load_gallery_controls();
        echo $cod_arr;
    }
}
