<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assign_task extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('event_model');
        $rt_msg = $this->event_model->get_selected_event_by_leader();
        $data['ev_info'] = $rt_msg;
        $data['main_content'] = 'assign_task_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function get_event_tasks($event_id) {

        $this->load->model('event_model');
        $rt_msg = $this->event_model->get_task_list($this->encrypt->decode($event_id));
        echo $rt_msg; 
    }

    public function get_users($utype,$evid) {
        if ($utype === '3') {
            $this->load->helper('db_helper');
            $led_arr=array();
            $led_arr[0]['uid'] = $this->encrypt->encode(get_user_id_by_user_name($this->session->userdata('username')));
            $led_arr[0]['u_name'] = $this->session->userdata('real_name');
            echo json_encode($led_arr);
        } else {
            $eid = $this->encrypt->decode($evid);
            $this->load->model('create_member');
            $cod_arr = $this->create_member->load_selected_members($eid);
            echo $cod_arr;
        }
    }

    public function task_assign() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('ev_name', 'Name of Event', 'trim|required');
        $this->form_validation->set_rules('ev_tasks', 'Event Tasks', 'callback_check_box_attemps');
        $this->form_validation->set_rules('as_role', 'User Type', 'trim|required');
        $this->form_validation->set_rules('uname', 'User Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $tsk = array();
            $tsk['event'] = $this->encrypt->decode($this->input->post('event_id'));
            $tsk['sel_tsk'] = $this->input->post('ev_tasks');
            $tsk['role'] = $this->input->post('as_role');
            $tsk['user_id'] = $this->encrypt->decode($this->input->post('uname'));
            $this->load->model('task_model');
            $cod_arr = $this->task_model->assign_task($tsk);
            if ($cod_arr) {
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }

    public function check_box_attemps() {
        $checked_arr = $this->input->post('ev_tasks');
        $count = count($checked_arr);
        if ($count == 0) {
            $error = 'Please select at least one task';
            $this->form_validation->set_message('check_box_attemps', $error);
            return FALSE;
        }
    }

}
