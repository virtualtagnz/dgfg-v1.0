<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invited_events extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('event_model');
//        $this->load->model('create_member');
//        $this->load->library('form_validation');
    }
    
    public function index() {
        $data['main_content'] = 'invited_events_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function view_invited_events() {
        $rtn_arr = $this->event_model->load_invited_events();
        echo $rtn_arr;
    }
    
    public function accept_invitation($eid) {
        $rtn_msg = $this->event_model->update_invited_event($this->encrypt->decode($eid));
        if($rtn_msg){
            $json_str = json_encode($rtn_msg);
            echo $json_str;
        }  else {
            $json_str = json_encode($rtn_msg);
            echo $json_str;
        }
    }
    
    
}
