<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public $img_file;
    
    public function __construct() {
        parent::__construct();
        $session = $this->session->userdata('username');
        if (!isset($session)) {
            redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->load->model('blog_model');
    }

    public function index() {
        redirect(site_url()."/dashboard");
    }

    public function blog_timeline_view($pg) {
        $rt_arr = $this->blog_model->blog_view_all($pg);
        $data['info'] = $rt_arr;
        $data['main_content'] = 'blog_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function blog_single_view($id){
        $data['data'] = $this->blog_model->blog_view_single($id);
        $this->load->view('blog_view_single',$data);
    }

    public function blog_add_post() {
        $this->form_validation->set_rules('blog_title', 'Blog Title', 'trim|required');
        $this->form_validation->set_rules('blog_body', 'Blog Body', 'trim|required');
        if (!empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Upload image', 'trim|callback_file_check[img]');
        }
        
        if ($this->form_validation->run() == FALSE) {
            $data['main_content'] = 'blog_add';
            $this->load->view('include/dashboard_template', $data);
        } else {
            //str_replace  ("'", "", $data);
            $data['title'] = str_replace("'", "&#39;",$this->input->post('blog_title'));
            $data['body'] = $this->input->post('blog_body');
            $data['img'] = $this->img_file;
            $data['createdby'] = $this->session->userdata('username');
            $data['createdrole'] = $this->session->userdata('user_role');
            $data['date'] = date('Y-m-d H:i:s');

            $rt_msg = $this->blog_model->blog_add($data);

            if ($rt_msg) {
                $this->session->set_flashdata('success_msg', 'Post added successfully');
                $data['main_content'] = 'blog_add';
                $this->load->view('include/dashboard_template', $data);
            } else {
                $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                $data['main_content'] = 'blog_add';
                $this->load->view('include/dashboard_template', $data);
            }
        }
    }

    public function blog_approve_post_view() {
        $data['main_content'] = 'blog_view_admin';
        $this->load->view('include/dashboard_template', $data);
    }

    public function blog_admin_view(){
        $rtn_arr = $this->blog_model->blog_admin_view_all();
        echo $rtn_arr;
    }
    public function blog_toggle_approve_post($id) {
        $result = $this->blog_model->blog_toggle_approve($id);
        echo $result;
    }
    
    public function blog_toggle_reject_post($id){
        $result = $this->blog_model->blog_toggle_reject_post($id);
        echo $result;
    }
    
    public function file_check($str, $type) {
        $this->load->helper('db_helper');
        $enc_file = array();
        if ($type === 'img') {
            $enc_file = upload_media_file("blog", "image", $type, 851, 315);
        }

        if (isset($enc_file['file_error'])) {
            $this->form_validation->set_message('file_check', $enc_file['file_error']);
            return FALSE;
        } else {
            if (isset($enc_file['enc_img'])) {
                $this->img_file = "public/uploads/blog/".$enc_file['enc_img'];
            }
        }
    }

}
