<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_service extends CI_Controller {

    public $sp_id;
    public $img_file1;
    public $img_file2;

    public function __construct() {
        parent::__construct();
        $this->sp_id = $this->encrypt->decode(end($this->uri->segments));
    }

    public function index() {
        $this->load->model('service_model');
        $this->load->model('survey_model');
        $data['service_cat'] = $this->survey_model->load_survey_section();
        $rtn_arr = $this->service_model->get_single_service($this->sp_id);
        foreach ($rtn_arr[0] as $key => $value) {
            $data[$key] = $value;
        }
        $data['main_content'] = 'edit_service_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function modify_service() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('sprovider', 'Name of Provider', 'trim|required');
        $this->form_validation->set_rules('scategory', 'Provider Category', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('slevel', 'Level', 'trim|required|greater_than[0]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        $this->load->helper('db_helper');
        $curr_name = get_column("sp_name", "sp_id", "service_tab", $this->sp_id);
        $change_name = $this->input->post('sprovider');
        if ($curr_name !== $change_name) {
            if (file_exists('public/uploads/' . $curr_name)) {
                rename('public/uploads/' . $curr_name, 'public/uploads/' . $change_name);
            }
        }

        if (!empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Provider image', 'trim|callback_file_check[image]');
        } elseif (!empty($this->input->post('old_img'))) {
            $this->img_file1 = $this->input->post('old_img');
        }

        if (!empty($_FILES['staff_image']['name'])) {
            $this->form_validation->set_rules('image', 'Staff image', 'trim|callback_file_check[staff_image]');
        } elseif (!empty($this->input->post('old_staff_img'))) {
            $this->img_file2 = $this->input->post('old_staff_img');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $service_dt = array();
            $service_dt['sp_name'] = $this->input->post('sprovider');
            $service_dt['sp_cat'] = $this->input->post('scategory');
            $service_dt['sp_summery'] = $this->input->post('ssummary');
            $service_dt['sp_cost'] = $this->input->post('scost');
            $service_dt['sp_phn'] = $this->input->post('scontact');
            $service_dt['sp_key'] = $this->input->post('skey');
            $service_dt['sp_list'] = $this->input->post('slist');
            $service_dt['sp_add'] = $this->input->post('saddress');
            $service_dt['sp_hrs'] = $this->input->post('shours');
            $service_dt['sp_after_phn'] = $this->input->post('safternum');
                $service_dt['sp_email'] = $this->input->post('email');
            $service_dt['sp_img'] = $this->img_file1;
            $service_dt['sp_staff_img'] = $this->img_file2;
            $service_dt['sp_web'] = $this->input->post('sweb');
            $service_dt['sp_level'] = $this->input->post('slevel');
            
            $this->load->model('service_model');
            $rt_msg = $this->service_model->edit_service($service_dt, $this->sp_id);
            if ($rt_msg) {
                $this->session->set_flashdata('success_msg', 'Service directory edit successfully');
                $this->index();
            } else {
                $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                $this->index();
            }
        }
    }

    public function file_check($str, $field_name) {
        $this->load->helper('db_helper');
        $enc_file = upload_media_file($this->input->post('sprovider'), $field_name, "img", 120, 120);
        if (isset($enc_file['file_error'])) {
            $this->form_validation->set_message('file_check', $enc_file['file_error']);
            return FALSE;
        } else {
            if ($field_name == "image") {
                $this->img_file1 = $enc_file['enc_img'];
            } elseif ($field_name == "staff_image") {
                $this->img_file2 = $enc_file['enc_img'];
            }
        }
    }

}
