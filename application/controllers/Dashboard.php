<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('dashboard_model');
        $this->load->model('service_model');
        $session = $this->session->userdata('username');
        if (!isset($session)) {
            $this->isSetSession = FALSE;
        } else {
            $this->isSetSession = TRUE;
        }
    }

    public function index() {

        if ($this->isSetSession == FALSE) {
            redirect(base_url());
        }
        if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') {
            $data['new_members'] = $this->dashboard_model->load_newly_registered_mem();
            $data['top_events'] = $this->dashboard_model->load_top_performing_events();
            $data['survey_alerts'] = $this->dashboard_model->survey_allerts();
            $data['recent_surveys'] = $this->dashboard_model->recent_surveys();
            $data['top_leaders'] = $this->dashboard_model->top_leaders();
            $data['top_members'] = $this->dashboard_model->top_members();
            $data['up_coming_ev'] = $this->dashboard_model->up_coming_events();
            $data['blog_post'] = $this->dashboard_model->load_blog_posts();
            $data['top_rewarders'] = $this->dashboard_model->load_highest_rewards();
            $data['main_content'] = 'super_dashboard';
            $this->load->view('include/dashboard_template', $data);
        } elseif ($this->session->userdata('user_role') === '3') {
            $data['my_events'] = $this->dashboard_model->load_my_skawd_events();
            $data['service_cat'] = $this->service_model->load_services_according_survey();
            $data['top_leaders'] = $this->dashboard_model->top_leaders();
            $data['top_members'] = $this->dashboard_model->top_members();
            $data['leads_not_project'] = $this->dashboard_model->load_leaders_not_have_project();
            $data['leaders_derectory'] = $this->dashboard_model->load_leaders_directory();
            $data['blog_post'] = $this->dashboard_model->load_blog_posts();
            $data['top_rewarders'] = $this->dashboard_model->load_highest_rewards();
            $data['daily_survey'] = $this->dashboard_model->load_daily_surveys_on_public();
            $data['main_content'] = 'leader_dashboard';
            $this->load->view('include/dashboard_template', $data);
        } elseif ($this->session->userdata('user_role') === '4') {
            $data['my_events'] = $this->dashboard_model->load_my_skawd_events();
            $data['service_cat'] = $this->service_model->load_services_according_survey();
            $data['top_leaders'] = $this->dashboard_model->top_leaders();
            $data['top_members'] = $this->dashboard_model->top_members();
            $data['blog_post'] = $this->dashboard_model->load_blog_posts();
            $data['top_rewarders'] = $this->dashboard_model->load_highest_rewards();
            $data['daily_survey'] = $this->dashboard_model->load_daily_surveys_on_public();
            $data['main_content'] = 'member_dashboard';
            $this->load->view('include/dashboard_template', $data);
        }
    }

}
