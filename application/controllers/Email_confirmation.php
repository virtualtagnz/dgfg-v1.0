<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_confirmation extends CI_Controller {

    public $conf_key;

    public function __construct() {
        parent::__construct();
        $this->conf_key = end($this->uri->segments);
        $this->load->library('form_validation');
        $this->load->model('registration_model');
        $this->load->model('survey_model');
    }

    public function index() {
        $this->load->view('include/header');
        $this->load->view('email_confirmation_view');
    }

    public function key_confirmation() {
        $this->form_validation->set_rules('user_name', 'Email', 'trim|valid_email|required');
        $this->form_validation->set_rules('user_pwd', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $user = array();
            $user['uname'] = $this->input->post('user_name');
            $user['pwd'] = md5($this->input->post('user_pwd'));
            $user['conf_key'] = $this->conf_key;
            $msg = $this->registration_model->conf_key_compare($user);
            if ($msg === '01CFT') {
                $this->session->set_flashdata('success_msg', 'done');
                redirect(current_url());
            } elseif ($msg === '01ACF') {
                $this->session->set_flashdata('conf_msg', 'aconfd');
                redirect(current_url());
            } elseif ($msg === '01CFE') {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            } elseif ($msg === '01CFF') {
                $this->session->set_flashdata('fail_msg', 'faild');
                redirect(current_url());
            }
        }
    }

    public function next_step($param) {
        $data['survey_info'] = $this->survey_model->get_survey_section();
        $this->load->view('include/header');
        $this->load->view('survey_view', $data);
    }

}
