<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_pending_event extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['main_content'] = 'view_pending_event_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function pending_events() {
        $this->load->model('event_model');
        $rtn_arr = $this->event_model->get_pending_events();
        echo $rtn_arr;
    }
    
}
