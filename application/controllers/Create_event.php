<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_event extends CI_Controller {

    public $img_file;
    public $vid_file;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['main_content'] = 'create_event_view';
        $this->load->view('include/dashboard_template', $data);
    }

    public function add_event() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('evname', 'Event Name', 'trim|required');
        $this->form_validation->set_rules('evdate', 'Event Date', 'trim|required');
        $this->form_validation->set_rules('evstime', 'Event Start Time', 'trim|required');
        $this->form_validation->set_rules('evetime', 'Event End time', 'trim|required');
        $this->form_validation->set_rules('evloc', 'Event Location', 'trim|required');
        $this->form_validation->set_rules('ev_task', 'ev_task', 'callback_task_validator');
        if (!empty($_FILES['evid']['name'])) {
            $this->form_validation->set_rules('evid', 'Upload video', 'trim|callback_file_check[vid]');
        }
        if (!empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Upload image', 'trim|callback_file_check[img]');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $event_data = array();
            $event_data['name'] = $this->input->post('evname');
            $event_data['dt'] = $this->input->post('evdate');
            $event_data['stime'] = $this->input->post('evstime');
            $event_data['etime'] = $this->input->post('evetime');
            $event_data['loc'] = $this->input->post('evloc');
            $event_data['promo1'] = $this->input->post('evpromo1');
            $event_data['promo2'] = $this->input->post('evpromo2');
            $event_data['promo3'] = $this->input->post('evpromo3');
            $event_data['promo4'] = $this->input->post('evpromo4');
            $event_data['evplan'] = $this->input->post('evplan');
            $event_data['cmt'] = $this->input->post('evcomments');
            $event_data['evid'] = $this->vid_file;
            $event_data['img'] = $this->img_file;
            $event_data['tasks'] = $this->input->post('json_str');

            $this->load->model('event_model');
            $rt_msg = $this->event_model->create_event($event_data);


            if ($rt_msg) {
                if ($this->session->userdata('user_role') === '3') {
                    $sent_id = get_user_id_by_user_name($this->session->userdata('username'));
                    $dn_rtn = dashboard_notification('2', '1', $this->session->userdata('user_role'), $sent_id, 'New event created by: ' . $this->session->userdata('real_name'));
                    if ($dn_rtn) {
                        $this->session->set_flashdata('success_msg', 'Event create successfully');
                        redirect(current_url());
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                        redirect(current_url());
                    }
                } else {
                    $this->session->set_flashdata('success_msg', 'Event create successfully');
                    redirect(current_url());
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Something is going wrong, try again');
                redirect(current_url());
            }
        }
    }

    public function file_check($str, $type) {
        $this->load->helper('db_helper');
        $enc_file = array();
        if ($type === 'vid') {
            $enc_file = upload_media_file($this->input->post('evname'), "evid", $type);
        } elseif ($type === 'img') {
            $enc_file = upload_media_file($this->input->post('evname'), "image", $type, 851, 315);
        }

        if (isset($enc_file['file_error'])) {
            $this->form_validation->set_message('file_check', $enc_file['file_error']);
            return FALSE;
        } else {
            if (isset($enc_file['enc_vid'])) {
                $this->vid_file = $enc_file['enc_vid'];
            }
            if (isset($enc_file['enc_img'])) {
                $this->img_file = $enc_file['enc_img'];
            }
        }
    }
    
    public function task_validator(){
        if($this->input->post('json_str')==""){
            $this->form_validation->set_message('task_validator', 'Please enter task(s)');
            return FALSE;
        }  else {
            return TRUE;
        }
    }

}
