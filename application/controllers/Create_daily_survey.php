<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_daily_survey extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('daily_survey_model');
        $this->load->library('form_validation');
    }
    
    public function index() {
        $data['main_content'] = 'create_daily_survey_view';
        $this->load->view('include/dashboard_template', $data);
    }
    
    public function add_daily_survey() {
        $this->form_validation->set_rules('dsurvey_name', 'Survey Name', 'trim|required');
        $this->form_validation->set_rules('dsurvey_date', 'Date of survey', 'trim|required');
        $this->form_validation->set_rules('dsurvey_pnts', 'Points for survey', 'trim|required');
        $this->form_validation->set_rules('dsurvey_tab', 'dsurvey_tab', 'callback_task_validator');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        }  else {
            $ds_arr = array();
            $ds_arr['dsurvey_name'] = $this->input->post('dsurvey_name');
            $ds_arr['dsurvey_date'] = $this->input->post('dsurvey_date'); 
            $ds_arr['dsurvey_points'] = $this->input->post('dsurvey_pnts');
            $ds_arr['dsurvey_json'] = $this->input->post('json_str');
            $rtn_val = $this->daily_survey_model->create_daily_survey($ds_arr);
            if($rtn_val){
                $this->session->set_flashdata('success_msg', 'success');
                redirect(current_url());
            }  else {
                $this->session->set_flashdata('error_msg', 'error');
                redirect(current_url());
            }
        }
    }
    
    public function task_validator(){
        if($this->input->post('json_str')==""){
            $this->form_validation->set_message('task_validator', 'Please enter question(s) and answer(s)');
            return FALSE;
        }  else {
            return TRUE;
        }
    }
    
}

