<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_single extends CI_Controller {

    public $para1 = array();
    public $para2 = array();

    public function __construct() {
        parent::__construct();
        $this->para1 = $this->uri->segment(2);
        $this->para2 = $this->uri->segment(3);
    }

    public function index() {
        $rtn_arr = array();
        if ($this->para2 === '1') {
            $this->load->model('create_coordinator');
            $rtn_arr = $this->create_coordinator->getSingleCoodinator($this->para1);
        } elseif ($this->para2 === '2') {
            $this->load->model('create_leader');
            $rtn_arr = $this->create_leader->getSingleLeader($this->para1);
        }elseif ($this->para2 === '3') {
            $this->load->model('create_member');
            $rtn_arr = $this->create_member->getSingleMember($this->para1);
        }

        foreach ($rtn_arr as $key => $value) {
            $data[$key] = $value;
        }

        $data['user_type'] = $this->para2;
        switch ($data['user_type']) {
            case '1':
                $data['page_title'] = 'YOUTH COORDINATOR PROFILE: ' . $data['fname'] . ' ' . $data['lname'];
                break;
            case '2':
                $data['page_title'] = 'SKWAD LEADER PROFILE: ' . $data['fname'] . ' ' . $data['lname'];
                break;
            case '3':
                $data['page_title'] = 'SKWAD MEMBER PROFILE: ' . $data['fname'] . ' ' . $data['lname'];
                break;
        }

        $this->load->view('single_user_view', $data);
    }

}
