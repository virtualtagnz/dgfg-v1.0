<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        redirect(site_url());
    }
    
    public function index(){
//        $this->session->sess_destroy();
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('user_role');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_img');
        $this->session->unset_userdata('real_name');
        if(count($this->session->userdata)==0){
            redirect(site_url());
        } 
    }
}
