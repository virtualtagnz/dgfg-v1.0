<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php if (!empty($data[0]['blog_image'])) { ?>
                <div class="col-lg-12">
                    <img class="img-responsive center-block" src="<?php echo base_url() . $data[0]['blog_image'] ?>"/>
                </div>
            <?php }; ?>
            <p><h2 class="text-center"><i><?php echo $data[0]['blog_name']; ?></i></h2></p>
            <p class="text-center"><?php echo $data[0]['blog_body'] ?></p>
            <table class="table centered">
                <tbody>
                    <tr>
                        <td>Created By:</td>
                        <td><?php echo $data[0]['blog_created_by'] ?></td>
                    </tr>
                    <tr>
                        <td>Created Date:</td>
                        <td><?php echo $data[0]['blog_created_date'] ?></td>
                    </tr>
                    <tr>
                        <td>Approved By:</td>
                        <td><?php echo $data[0]['blog_modified_by'] ?></td>
                    </tr>
                    <tr>
                        <td>Approved Date:</td>
                        <td><?php echo $data[0]['blog_modified_date'] ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('p img').removeAttr("style");
        $('p img').addClass("img-responsive");

    });

</script>

