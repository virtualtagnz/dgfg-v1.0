<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            AVAILABLE SERVICES
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-12">
                <div class="row">
                    <?php foreach ($service_cat as $values) { ?>
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <?php if ($values['sp_img']) { ?>
                                    <img src="<?php echo base_url() . 'public/uploads/' . $values['sp_name'] . '/' . $values['sp_img'] ?>" alt="...">
                                <?php } else { ?>
                                    <img src="<?php echo base_url() . 'public/images/default_service.jpg' ?>">
                                <?php } ?>
                                <div class="caption">
                                    <h4><strong><?php echo $values['sp_name'] ?></strong></h4>
                                    <p class="txt-wrap"><?php echo $values['sp_summery'] ?></p>
                                    <p><button class="btn btn-primary sing-det" role="button" id="<?php echo $values['sp_id'] ?>">More Details</button></p>
                                    <input type="hidden" name="sid" id="sid" value="<?php echo $values['sp_id'] ?>">
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<?php if ($this->session->flashdata('success_msg')) { ?>
    <script>
        swal("Good job!", "Message sent successfully!", "success")
    </script>
    <?php } elseif ($this->session->flashdata('error_msg')) { ?>
    <script>
        swal("Error!", "Something is going wrong!", "error")
    </script>
    <?php } ?>
    <script>
        $(document).ready(function () {
            $('button').on('click', function () {
                var sid = this.id;
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url() . '/single_service/'; ?>" + sid,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#service_modal').modal('show');
                }
            });
        });
    });
</script>

