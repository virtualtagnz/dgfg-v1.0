<body class="loginbg">
    <div class="container container-table">
        <div class="row vertical-center-row">
            <div class="col-md-4 col-md-offset-4">
<!--                <div class="text-center"><img src=""/></div>-->
                <div class="login-form">
                    <h3 class="text-center" style="color: #FFFFFF;">Activate Your Account</h3>
                    <form name="login-form" id="login-form" action="<?php echo site_url('email_confirmation/key_confirmation/' . end($this->uri->segments)); ?>" method="post">
                        <div class="form-group">
                            <input class="form-control" type="text" name="user_name" id="user_name" placeholder="Email"  autocomplete="off" value="<?php echo set_value('user_name') ?>">
                            <div id="infoMessage"><?php echo form_error('user_name'); ?></div>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" name="user_pwd" id="user_pwd" placeholder="Password"  autocomplete="off" value="<?php echo set_value('user_pwd') ?>">
                            <div id="infoMessage"><?php echo form_error('user_pwd'); ?></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control">CONFIRM</button>
                        </div>
                    </form>
                </div>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal({title: "Activated!", text: "Your account has been activated!", type: "success", showConfirmButton: true, confirmButtonText: "OK"}, function () {
                            window.location.href = "<?php echo site_url('login/user_login') ?>";
                        });
                    </script>
                <?php } elseif ($this->session->flashdata('conf_msg')) { ?>
                    <script>
                        swal("Error!", "Your account already activated, please login!", "error")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Activation faild!", "error")
                    </script>
                <?php } elseif ($this->session->flashdata('fail_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </div>
</body>

