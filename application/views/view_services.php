<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            VIEW / EDIT SERVICE DIRECTORIES
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="sd_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Service Directory Provider</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>View / Edit Service</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url().'/view_services/get_services';?>",
            success: function (results) {
                console.log(results);
                $('#sd_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "sp_name"},
                        {"mData": "sp_cat"},
                        {"mData": "sp_summery"},
                        {"mData": "sp_id", "mRender": function (data) {
                            return '<a href=<?php echo site_url()?>/edit_service/' + data +'>Edit Service Directory</a>';
                        }}
                    ]
                });
            }
        });
    });
</script>