<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="single_survey">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" name="com_survey_frm" id="com_survey_frm">
                <?php for ($i = 0; $i < count($survey_info); $i++) { ?>
                    <div class="text-center"><h3><?php echo $survey_info[$i]['wellness_sec_name']; ?></h3></div>
                    <?php for ($x = 0; $x < count($survey_info[$i]['wellness_quesions']); $x++) { ?>
                        <?php $qid = $survey_info[$i]['wellness_quesions'][$x]['wellness_quesions_id'] ?>
                        <div class="form-group">
                            <ul class="list-group" id="<?php echo 'sv' . strval($x + 1) ?>">
                                <?php if ($this->session->userdata('user_role') === '2') { ?>
                                    <h4 class="list-group-item active <?php echo (($survey_answers[$qid] == '5') ? "critic-5" : (($survey_answers[$qid] == '4') ? "critic-4" : (($survey_answers[$qid] == '3') ? "critic-3" : (($survey_answers[$qid] == '2') ? "critic-2" : (($survey_answers[$qid] == '1') ? "critic-1" : ""))))) ?>"><?php echo $qid . '. ' . $survey_info[$i]['wellness_quesions'][$x]['wellness_quesion'] ?></h4>
                                <?php } else { ?>
                                    <h4 class="list-group-item active"><?php echo $qid . '. ' . $survey_info[$i]['wellness_quesions'][$x]['wellness_quesion'] ?></h4>
                                <?php } ?>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="5"<?php echo $survey_answers[$qid] == '5' ? ' checked="checked"' : ''; ?>><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans1'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans1_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="4"<?php echo $survey_answers[$qid] == '4' ? ' checked="checked"' : ''; ?>><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans2'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans2_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="3"<?php echo $survey_answers[$qid] == '3' ? ' checked="checked"' : ''; ?>><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans3'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans3_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="2"<?php echo $survey_answers[$qid] == '2' ? ' checked="checked"' : ''; ?>><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans4'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans4_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="1"<?php echo $survey_answers[$qid] == '1' ? ' checked="checked"' : ''; ?>><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans5'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans5_support'] ?>"></i></li>
                            </ul>
                            <div id="infoMessage"><?php echo form_error('Q' . $qid); ?></div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </form>
        </div>
    </div>
</div>