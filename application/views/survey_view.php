<?php
//var_dump($survey_info[1]);
//var_dump($survey_info[1]['wellness_sec_name']);
//var_dump($survey_info[1]['wellness_quesions'][0]);
//var_dump($survey_info[1]['wellness_quesions'][0]['wellness_quesions_id']);
//for ($i = 0; $i < count($survey_info); $i++) {
//    echo $survey_info[$i]['wellness_sec_name'] . '<br>';
//    for ($x = 0; $x < count($survey_info[$i]['wellness_quesions']); $x++) {
//        echo $survey_info[$i]['wellness_quesions'][$x]['wellness_quesions_id'] . '<br>';
//        echo $survey_info[$i]['wellness_quesions'][$x]['wellness_quesion'] . '<br>';
//        echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans1'] . '<br>';
//        echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans2'] . '<br>';
//        echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans3'] . '<br>';
//        echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans4'] . '<br>';
//    }
//}
?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false" id="survey_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <form class="form-horizontal" name="the-test-frm" id="the-test-frm" method="post" action="<?php echo site_url('survey/save_survey'); ?>">
                <?php for ($i = 0; $i < count($survey_info); $i++) { ?>
                    <div class="text-center"><h3><?php echo $survey_info[$i]['wellness_sec_name']; ?></h3></div>
                    <?php for ($x = 0; $x < count($survey_info[$i]['wellness_quesions']); $x++) { ?>
                        <div class="form-group">
                            <ul class="list-group" id="<?php echo 'sv' . strval($x + 1) ?>">
                                <?php $qid = $survey_info[$i]['wellness_quesions'][$x]['wellness_quesions_id'] ?>
                                <h4 class="list-group-item active"><?php echo $qid . '. ' . $survey_info[$i]['wellness_quesions'][$x]['wellness_quesion'] ?></h4>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="5"><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans1'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans1_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="4"><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans2'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans2_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="3"><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans3'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans3_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="2"><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans4'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans4_support'] ?>"></i></li>
                                <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $qid . '[]' ?>" value="1"><?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans5'] ?><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="right" title="<?php echo $survey_info[$i]['wellness_quesions'][$x]['wellness_ans5_support'] ?>"></i></li>
                            </ul>
                            <div id="infoMessage"><?php echo form_error('Q' . $qid); ?></div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" id="btn-tst" class="btn btn-primary pull-right">Finish</button>
                    </div>
                </div>
            </form>
            <?php if ($this->session->flashdata('success_msg')) { ?>
                <script>
                    swal({title: "Good job!", text: "Thank you for submitting wellness survey!", type: "success", showConfirmButton: true, confirmButtonText: "OK"}, function () {
                        window.location.href="<?php echo site_url('dashboard')?>";
                    });
                </script>
            <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                <script>
                    swal("Error!", "Something is going wrong!", "error");
                </script>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#survey_modal').modal('show');
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
</script>


