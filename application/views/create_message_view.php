<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CREATE MESSAGE
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="msg_frm" id="msg_frm" method="post" action="<?php echo site_url('create_message/create_msg'); ?>">
                    <div class="form-group">
                        <label for="msg_subject" class="col-sm-3 control-label">Subject</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="msg_subject" id="msg_subject" value="<?php echo set_value('msg_subject'); ?>">
                            <div id="infoMessage"><?php echo form_error('msg_subject'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="msg_to" class="col-sm-3 control-label">To</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="msg_to" id="msg_to" multiple="true" value="<?php echo set_value('msg_to'); ?>">
                            <div id="infoMessage"><?php echo form_error('msg_to'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="msg" class="col-sm-3 control-label">Message</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="msg" id="msg" rows="4" cols="50"></textarea>
                            <div id="infoMessage"><?php echo form_error('msg'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right"><i class="fa fa-paper-plane fa-fw" aria-hidden="true"></i> Send Message</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Message sent successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('msg');

        var senders = <?php echo $senders_arr; ?>;
        var dt = senders.length;
        //console.log(senders);
        var senders_arr = new Array();
        for (var i = 0; i < senders.length; i++) {
            senders_arr[i] = '' + senders[i].uname + '<' + senders[i].email + '>' + '';
        }
        console.log(senders_arr);
        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }
        $("#msg_to")
                // don't navigate away from the field on tab when selecting an item
                .bind("keydown", function (event) {
                    if (event.keyCode === $.ui.keyCode.TAB &&
                            $(this).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
            minLength: 0,
            source: function (request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(
                        senders_arr, extractLast(request.term)));
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }
        });



    });
</script>



