<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blog Posts
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="blog_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Blog Title</th>
                            <th>Blog Body</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                            <th>Modified By</th>
                            <th>Modified Date</th>
                            <th>Blog Id</th>
                            <th>Full View</th>
                            <th>Approve</th>
                            <th>Reject</th>
                        </tr>
                    </thead>
                </table>
                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/blog/blog_admin_view'; ?>",
            success: function (results) {
                console.log(results);
                $('#blog_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "blog_name"},
                        {"mData": "blog_body"},
                        {"mData": "blog_created_by"},
                        {"mData": "blog_created_date"},
                        {"mData": "blog_modified_by"},
                        {"mData": "blog_modified_date"},
                        {"mData": "blog_id", "visible": false},
                        {"mData": "", "mRender": function (data) {
                                return '<a class="view">View Full Blog</a>';
                            }},
                        {"mData": "is_approved", "mRender": function (data) {
                                return '<a class="approve">Approve</a>';
                            }},
                        {"mData": "is_approved", "mRender": function (data) {
                                return '<a class="reject">Reject</a>';
                            }}

                    ]
                });
            }
        });

        $('#blog_tab').on('click', 'a.approve', function () {
            var tab = $('#blog_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "DO YOU WANT TO APPROVE THE BLOG?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/blog/blog_toggle_approve_post/'; ?>" + data.blog_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results === "1") {
                                        swal("Good job!", "Approved successfully!", "success")
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });

        $('#blog_tab').on('click', 'a.reject', function () {
            var tab = $('#blog_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "DO YOU WANT TO REJECT THE BLOG?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/blog/blog_toggle_reject_post/'; ?>" + data.blog_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results === "1") {
                                        swal("Good job!", "Rejected successfully!", "success")
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });

        $('#blog_tab').on('click', 'a.view', function () {
            var tab = $('#blog_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/blog/blog_single_view/'; ?>" + data.blog_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#myModal').modal('show');
                }
            });
        });
    });
</script>