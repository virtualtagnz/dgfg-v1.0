<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            APPROVE/ REJECT IMAGES FOR EVENT
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content-header">
        <h1>
            APPROVAL PENDING/ REJECTED PHOTOS
            <small>All photos which are not approved yet or rejected</small>
        </h1>
    </section>
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12"><p id="no_images_uploaded" class="text-center text-warning"> No photos for approval...</p></div>
            <div class="grid">
            <!--div-->
                <?php $json_str1 = json_decode($pending_images); ?>
                <?php if ($json_str1 != null) { ?>
                    <?php foreach ($json_str1 as $key1 => $value1) { ?>
                        <div class="grid-item grid-item--width1">
                        <!--div style="padding: 0 4px"-->
                            <!--div class="thumbnail col-lg-3 col-md-4 col-sm-6 col-xs-12"-->
                            <div>
                                <div>
                                    <?php if ($value1->img_linq != NULL) { ?>
                                        <a class="magnifine_view" rel="<?php echo $value1->img_linq; ?>"><img class="img-responsive" src="<?php echo $value1->img_linq; ?>" alt=""/></a>
                                    <?php } ?>
                                </div>
                                <div class="text-justify caption">
                                <!--div-->
                                    <i class="fa fa-clock-o"></i> <?php echo $value1->create_date; ?><br/><i class="fa fa-pencil-square-o"></i> <?php echo $value1->create_by; ?>
                                </div>
                                <div class="caption">
                                <!--div-->
                                    <a class="magnifine_view" rel="<?php echo $value1->img_linq; ?>"><i class="fa fa-eye"></i>&nbsp;View &nbsp;&nbsp;&nbsp;</a>
                                    <a class="delete_img" rel="<?php echo $value1->galary_id; ?>"><i class="fa fa-trash-o"></i>&nbsp;Delete &nbsp;&nbsp;&nbsp;</a>
                                    <a class="approve_single_img" rel="<?php echo $value1->galary_id; ?>"><i class="fa fa-check-circle-o"></i>&nbsp;Approve</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="hidden" name="check_img_uploaded" id="check_img_uploaded" value="<?php echo TRUE; ?>"/>
                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $value1->event_id; ?>"/>
                <?php } ?>
            </div>
            <div class="col-md-12">
                <form id="send_approval" action="<?php echo site_url() . "/edit_event/approve_eventgallery/" . $event_id; ?>" method="POST" enctype="multipart/form-data">
                    <button type="button" id="btn_send_approv" class="btn btn-primary pull-right"><i class="fa fa-check-circle-o fa-fw" aria-hidden="true"></i> Approve All</button>
                </form>
            </div>
        </div>
    </section>
    <section class="content-header">
        <h1>
            APPROVED PHOTOS
            <small>All photos which are already approved</small>
        </h1>
    </section>
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12"><p id="no_images_submit" class="text-center text-warning"> No approved photos...</p></div>
            <div class="grid">
            <!--div-->
                <?php $json_str2 = json_decode($approve_images); ?>
                <?php if ($json_str2 != null) { ?>
                    <?php foreach ($json_str2 as $key2 => $value2) { ?>
                        <div class="grid-item grid-item--width1">
                        <!--div style="padding: 0 4px"-->
                            <!--div class="thumbnail col-lg-3 col-md-4 col-sm-6 col-xs-12"-->
                            <div>
                                <div>
                                    <?php if ($value2->img_linq != NULL) { ?>
                                        <a class="magnifine_view" rel="<?php echo $value2->img_linq; ?>"><img class="img-responsive" src="<?php echo $value2->img_linq; ?>" alt=""/></a>
                                    <?php } ?>
                                </div>
                                <div class="text-justify caption">
                                <!--div-->
                                    <i class="fa fa-clock-o"></i> <?php echo $value2->create_date; ?><br/><i class="fa fa-pencil-square-o"></i> <?php echo $value2->create_by; ?>
                                </div>
                                <div class="caption">
                                <!--div-->
                                    <a class="magnifine_view" rel="<?php echo $value2->img_linq; ?>"><i class="fa fa-eye"></i>&nbsp;View &nbsp;&nbsp;&nbsp;</a>
                                    <a class="delete_img" rel="<?php echo $value2->galary_id; ?>"><i class="fa fa-trash-o"></i>&nbsp;Delete &nbsp;&nbsp;&nbsp;</a>
                                    <a class="reject_single_img" rel="<?php echo $value2->galary_id; ?>"><i class="fa fa-remove"></i>&nbsp;Reject</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="hidden" name="check_img_submit" id="check_img_submit" value="<?php echo TRUE; ?>"/>
                <?php } ?>
            </div>
        </div>
    </section>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="imgModal">
        <div class="modal-dialog modal-lg">
            <img src="" alt="img event" id="magnifinedimg" class="img-responsive center-block img-bordered"/>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var has_value_upload = $("#check_img_uploaded").val();
        $("#btn_send_approv").prop('disabled', true);
        if (has_value_upload == 1) {
            $("#btn_send_approv").prop('disabled', false);
            $("#no_images_uploaded").hide();
        }

        var has_value_submit = $("#check_img_submit").val();
        if (has_value_submit == 1) {
            $("#no_images_submit").hide();
        }

        $('p img').removeAttr("style");
        $('p img').addClass("img-responsive");
        $('.grid').masonry({
            // options
            itemSelector: '.grid-item',
            columnWidth: 200
        });

        $(".magnifine_view").click(function () {
            var image = $(this).get(0).rel;
            $("#magnifinedimg").attr("src", image);
            $('#imgModal').modal('show');
        });

        $(".delete_img").click(function () {
            var galary_id = $(this).get(0).rel;
            swal({title: "DO YOU WANT TO DELETE THE IMAGE?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/delete_image/'; ?>" + galary_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results === "1") {
                                        swal("Good job!", "Deleted successfully!", "success")
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });

        $(".approve_single_img").click(function () {
            var galary_id = $(this).get(0).rel;
            swal({title: "DO YOU WANT TO APPROVE THIS IMAGE?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/approve_single_image/'; ?>" + galary_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results === "1") {
                                        swal("Good job!", "Approved successfully!", "success")
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });

        $(".reject_single_img").click(function () {
            var galary_id = $(this).get(0).rel;
            swal({title: "DO YOU WANT TO REJECT THIS IMAGE?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/reject_single_image/'; ?>" + galary_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results === "1") {
                                        swal("Good job!", "Rejected successfully!", "success")
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });

        $("#btn_send_approv").click(function () {
            var event_id = $("#event_id").val();
            swal({title: "DO YOU WANT TO APPROVE THEM ALL?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/approve_all_image/'; ?>" + event_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results === "1") {
                                        swal("Good job!", "Approved successfully!", "success")
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 200,
            gutter: 8
        });
    });

//    function expand_view(id) {
//        $.ajax({
//            type: "POST",
//            url: "<?php echo site_url() . '/blog/blog_single_view/'; ?>" + id,
//            success: function (results) {
//                console.log(results);
//                $('#mod').html(results);
//                $('#myModal').modal('show');
//            }
//        });
//    }
</script>