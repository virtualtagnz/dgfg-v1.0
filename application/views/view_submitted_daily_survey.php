<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            VIEW SUBMITTED QUICK SURVEYS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="sub_sur_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sid</th>
                            <th>Profile Image</th>
                            <th>User Name</th>
                            <th>Date Submitted</th>
                            <th>Role</th>
                            <th>email</th>
                            <th>View Survey</th>
                        </tr>
                    </thead>
                </table>
                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/view_submitted_daily_surveys/load_submitted_daily_surveys/'; ?>",
            success: function (results) {
                $('#sub_sur_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "sid", "visible": false},
                        {"mData": "img", "mRender": function (data) {
                                if (data !== "") {
                                    return '<img class="img-circle img-responsive" src="<?php echo base_url() ?>public/uploads/' + data + '"></>';
                                } else {
                                    return '<img class="img-circle img-responsive" src="<?php echo base_url() ?>public/uploads/default-user.jpg"></>';
                                }
                            }},
                        {"mData": "uname"},
                        {"mData": "dt"},
                        {"mData": "role"},
                        {"mData": "email","visible": false},
                        {"mData": "sid", "mRender": function (data) {
                                return '<a class="pop-sur">View Survey</a>';
                                //return '<a href=<?php echo site_url() ?>/single_daily_survey/view_completed_single_daily_surveys/' + data + '>View Survey</a>';
                            }}
                    ]
                });
            }
        });
        
        
        $('#sub_sur_tab').on('click', 'a.pop-sur', function () {
            var tab = $('#sub_sur_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_daily_survey/view_completed_single_daily_surveys/'; ?>" + data.sid,
                data: "email="+data.email,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#com_daily_survey_modal').modal('show');
                }
            });
        });

    });
</script>