<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="single_active_events">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php if (!empty($event_img_linq)) { ?>
                <div class="col-lg-12">
                    <img class="img-responsive center-block" src="<?php echo base_url() . 'public/uploads/' . $event_name . '/' . $event_img_linq ?>"/>
                </div>
            <?php }; ?>
            <p><h2 class="text-center"><i><?php echo $event_name; ?></i></h2></p>
        <?php if ($event_video_linq !== "") { ?>
                                <video width="100%" height="240" controls>
                                    <source src="<?php echo base_url() . 'public/uploads/' . $event_name . '/' . $event_video_linq ?>" type="video/mp4">
                                </video>
        <?php }?>
            <p class="text-center"><?php echo $event_comments ?></p>
            <table class="table centered">
                <tbody>
                    <tr>
                        <td>Date:</td>
                        <td><?php echo $event_date ?></td>
                    </tr>
                    <tr>
                        <td>Time:</td>
                        <td><?php echo $event_start . ' - ' . $event_end ?></td>
                    </tr>
                    <tr>
                        <td>Location:</td>
                        <td><?php echo $event_loc ?></td>
                    </tr>
                    <tr>
                        <td>Promotion Links:</td>
                        <td><?php
                            echo '<a href=' . $event_promo1 . '>Promo Link 1</a><br>'
                            . '<a href=' . $event_promo2 . '>Promo Link 2</a><br>'
                            . '<a href=' . $event_promo3 . '>Promo Link 3</a><br>'
                            . '<a href=' . $event_promo4 . '>Promo Link 4</a><br>'
                            ?></td>
                    </tr>
                    <tr>
                        <td>Event Plan:</td>
                        <td><?php echo '<a href=' . $event_plan . '>Event Plan</a><br>' ?></td>
                    </tr>
                    <!--event video took up of the page-->
                    <tr>
                        <td>SKWAD Leader:</td>
                        <td><?php echo $skwad_leder ?></td>
                    </tr>
                    <tr>
                        <td>SKWAD Members:</td>
                        <td><?php
                            $mem_str = json_decode($skwad_members);
                            if (count($mem_str) > 0) {
                                foreach ($mem_str as $mem_value) {
                                    echo $mem_value->u_name . '<br>';
                                }
                            } else {
                                echo 'No members selected!';
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Event Tasks:</td>
                        <td>
                            <table id="tab_inner" class="table">
                                <tbody>
                                    <?php
                                    $json1 = json_decode($json_str);
                                    foreach ($json1 as $key => $value) {
                                        ?>
                                        <tr>
                                            <td style="width:40%;" align="left"><?php echo $value->task_des?></td>
                                            <td style="width:40%;" align="center"><?php echo $value->due_date?></td>
                                            <td style="width:20%;<?php echo (($value->stat=="Active")?"color:blue;":(($value->stat=="Complete")?"color:green;":"color:red;"))?>" align="center" ><?php echo $value->stat?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


