<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ACTIVE EVENTS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="act_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Event Id</th>
                            <th>Total Tasks</th>
                            <th>Complete Tasks</th>
                            <th>Percentage Completed(%)</th>
                            <th>Detail View</th>
                        </tr>
                    </thead>
                </table>

                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/active_events/load_active_events'; ?>",
            success: function (results) {
                console.log(results);
                $('#act_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "event_name"},
                        {"mData": "event_id","visible": false},
                        {"mData": "task_cnt"},
                        {"mData": "task_comp"},
                        {"mData": "precent"},
                        {"mData": "", "mRender": function (data) {
                                return '<a class="vtevt">Detail View</a>';
                            }}
                    ]
                });
            }
        });
        
        $('#act_tab').on('click', 'a.vtevt', function () {
            var tab = $('#act_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_event/active_events_view/'; ?>" + data.event_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#single_active_events').modal('show');
                }
            });
        });
    });
</script>
