<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" id="dashboard">
        <!--        <h1>
        <?php
        var_dump($this->session->userdata);
        echo count($this->session->userdata)
        ?>
                    <small>No controller yet</small>
                </h1>-->
        <div class="row">
            <!--Widget 1 -->
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Newly Registered Members</h4></div>
                    <div class="panel-body">
                        <?php foreach ($new_members as $value) { ?>
                            <div class="col-md-4 text-center" id="<?php echo $value['mem_id'] ?>"><img class="img-responsive img-circle center-block" src="<?php echo ($value['mem_img_link']) ?  $value['mem_img_link'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['mem_name'] . '<br>' ?>
                                <a class="vsu" id="<?php echo $value['mem_id'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <!--Widget 2 -->
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top Performing Events</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php foreach ($top_events as $value) { ?>
                                    <tr>
                                        <td><?php echo $value['event_name'] ?><span><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-event" id="<?php echo $value['event_id']; ?>">View Event</a></td>
                                        <td><span class="pull-right"><?php echo $value['precent'] . '%' ?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--Widget 3 -->
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Alerts (Based on High Risk)</h4></div>
                    <div class="panel-body">
                        <?php foreach ($survey_alerts as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['led_img_link']) ? $value['led_img_link'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['name'] ?><br>
    <!--                                <small><?php echo 'High risk marks -' . $value['cnt'] ?></small><br>-->
                                <a class="v-sur" id="<?php echo $value['survey_map_id'] ?>">View Survey</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!--Widget 4 -->
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Most Recent Wellness Surveys</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php foreach ($recent_surveys as $value) { ?>
                                    <tr>
                                        <td><?php echo $value['uname'] ?><span><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-sur" id="<?php echo $value['survey_map_id']; ?>">View Survey</a></td>
                                        <td><span class="pull-right"><?php echo $value['dt'] ?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--Widget 5 -->
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top SKWAD Leaders</h4></div>
                    <div class="panel-body">
                        <?php foreach ($top_leaders as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['led_img_link']) ? $value['led_img_link'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['lname'] . '<br>' ?>
                                <small><?php echo 'Completed Events - ' . $value['ev_cnt'] . '<br>' ?></small>
                                <a class="v-led" id="<?php echo $value['led_id'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <!--Widget 6 -->
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top SKWAD Members</h4></div>
                    <div class="panel-body">
                        <?php foreach ($top_members as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['mem_img_link']) ?  $value['mem_img_link'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['mem_name'] . '<br>' ?>
                                <small><?php echo 'Success Rate - ' . $value['task_completion_precentage'] . '%' . '<br>' ?></small>
                                <a class="vsu" id="<?php echo $value['mem_id'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Up Coming Events</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php foreach ($up_coming_ev as $value) { ?>
                                    <tr>
                                        <td><?php echo $value['event_name'] ?><span><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-event" id="<?php echo $value['event_id']; ?>">View Event</a></td>
                                        <td><span class="pull-right"><?php echo $value['event_date']?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Blog Posts</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php foreach ($blog_post as $value) { ?>
                                    <tr>
                                        <td><?php echo $value['blog_name'] ?><span><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-blog" id="<?php echo $value['blog_id']; ?>">View Blog Post</a></td>
                                        <td><span class="pull-right"><?php echo $value['blog_created_date']?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top Rewarders</h4></div>
                    <div class="panel-body">
                        <?php foreach ($top_rewarders as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['img']) ? $value['img'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['uname'] . '<br>' ?>
                                <small><?php echo 'Points - ' . $value['tot_points'].'<br>'?></small>
                                <a class="<?php echo ($value['role']=='3')?"v-led":"vsu";?>" id="<?php echo $value['uid'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="dmod"></div>
        <div id="mod"></div>
    </section>
</div>
<script>
    $(document).ready(function () {
        /*
         * view single members
         */
        $('.vsu').on('click', function () {
            var uid = $(this).attr('id');
            console.log(uid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/view_single/" + uid + '/3',
                success: function (results) {
                    console.log(results);
                    $('#dmod').html(results);
                    $('#user_modal').modal('show');
                }
            });
        });

        /*
         * view single members
         */
        $('.v-led').on('click', function () {
            var uid = $(this).attr('id');
            console.log(uid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/view_single/" + uid + '/2',
                success: function (results) {
                    console.log(results);
                    $('#dmod').html(results);
                    $('#user_modal').modal('show');
                }
            });
        });

        $('.v-event').on('click', function () {
            var uid = $(this).attr('id');
            console.log(uid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/single_event/active_events_view/" + uid,
                success: function (results) {
                    console.log(results);
                    $('#dmod').html(results);
                    $('#single_active_events').modal('show');
                }
            });
        });

        $('.v-sur').on('click', function () {
            var sid = $(this).attr('id');
            console.log(sid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/survey/display_single_survay_completed/" + sid,
                success: function (results) {
                    console.log(results);
                    $('#dmod').html(results);
                    $('#single_survey').modal('show');
                }
            });
        });

        $('.v-blog').on('click', function () {
            var sid = $(this).attr('id');
            console.log(sid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/blog/blog_single_view/" + sid,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#myModal').modal('show');
                }
            });
        });
    });
</script>

