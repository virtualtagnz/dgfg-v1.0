<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ASSIGN TASKS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="task_frm" id="event_frm" method="post" action="<?php echo site_url('assign_task/task_assign'); ?>">
                    <div class="form-group">
                        <label for="ev_name" class="col-sm-3 control-label"><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="left" title="Please select project to view tasks list"></i>Event Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" list="listone" name="ev_name" id="ev_name">
                            <datalist id="listone">
                                <?php if (count($ev_info) > 0) { ?>
                                    <?php for ($i = 0; $i < count($ev_info); $i++) { ?>
                                        <option id="<?php echo $ev_info[$i]['event_id'] ?>" value="<?php echo $ev_info[$i]['event_name'] ?>"></option>
                                    <?php } ?>;
                                <?php } else { ?>
                                    <option id="0" value="No available events"></option>
                                <?php } ?>;
                            </datalist>
                            <div id="infoMessage"><?php echo form_error('ev_name'); ?></div>
                        </div>
                    </div>
                    <input type="hidden" name="event_id" id="event_id">


                    <div class="form-group">
                        <label for="ev_tasks" class="col-sm-3 control-label">Task List</label>
                        <div class="col-sm-9">
                            <ul class="list-group" id="tlist">

                            </ul>
                            <div id="infoMessage"><?php echo form_error('ev_tasks'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="as_role" class="col-sm-3 control-label">Select Role Assign</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="as_role" id="as_role">
                                <option value="0">--Please Select--</option>
                                <option value="3">SKAWD Leader</option>
                                <option value="4">SKWAD Member</option>
                            </select>
                            <div id="infoMessage"><?php echo form_error('as_role'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="uname" class="col-sm-3 control-label">User Name</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="uname" id="uname">
                            </select>
                            <div id="infoMessage"><?php echo form_error('uname'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right">Assign Tasks</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Tasks Assign Successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('#ev_name').bind('input', function () {
            var x = $(this).val();
            var z = $('#listone');
            var val = $(z).find('option[value="' + x + '"]');
            var endval = val.attr('id');
            if (endval) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url() . '/assign_task/get_event_tasks/'; ?>" + endval,
                    success: function (results) {
                        if (results != "") {
                            var tasks = JSON.parse(results);
                            console.log(tasks);
                            if (tasks.length > 0) {
                                $("#tlist").empty();
                                for (var i = 0; i < tasks.length; i++) {
                                    $('#event_id').val(endval);
                                    $('#tlist').append('<li class="list-group-item"><label><input type="checkbox" value="' + tasks[i].event_task_id + '" name="ev_tasks[]" id="' + tasks[i].event_task_id + '">' + tasks[i].task_des + '</label></li>');
                                }
                            }
                        } else {
                            $("#tlist").empty();
                            $('#tlist').append('<li class="list-group-item"><p><strong>Sorry,</strong> No tasks to display. Please select other event </p></li>');
                        }

                    }
                });
            }

        });

        $('#as_role').change(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/assign_task/get_users/'; ?>" + $(this).val() +'/'+$('#event_id').val(),
                success: function (results) {
                    var users = JSON.parse(results);
                    console.log(users);
                    if (users.length > 0) {
                        $("#uname").empty();
                        for (var i = 0; i < users.length; i++) {
                            $('#uname').append($("<option>").val(users[i].uid).html(users[i].u_name));
                        }
                    }
                }
            });
        });
    });
</script>
