<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            MY TASKS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="task_frm" id="event_frm" method="post" action="<?php echo site_url('load_tasks/task_complete'); ?>">
                    <div class="form-group">
                        <label for="ev_name" class="col-sm-3 control-label"><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="left" title="Please select project to view tasks list"></i>Event Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" list="listone" name="ev_name" id="ev_name">
                            <datalist id="listone">
                                <?php if (count($ev_info) > 0) { ?>
                                    <?php foreach ($ev_info as $key => $value) { ?>
                                        <option id="<?php echo $key ?>" value="<?php echo $value ?>"></option>
                                    <?php } ?>;
                                <?php } else { ?>
                                    <option id="0" value="No available events!"></option>
                                <?php } ?>;
                            </datalist>
                            <div id="infoMessage"><?php echo form_error('ev_name'); ?></div>
                            <input type="hidden" name="event_id" id="event_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ev_tasks" class="col-sm-3 control-label">Task List</label>
                        <div class="col-sm-9">
                            <ul class="list-group" id="tlist">

                            </ul>
                            <div id="infoMessage"><?php echo form_error('ev_tasks'); ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right">Complete Marked Tasks</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Tasks Update As Completed!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('#ev_name').bind('input', function () {
            var x = $(this).val();
            var z = $('#listone');
            var val = $(z).find('option[value="' + x + '"]');
            var endval = val.attr('id');
            if (endval) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url() . '/load_tasks/load_my_tasks/'; ?>" + endval,
                    success: function (results) {
                        if (results != "") {
                            var tasks = JSON.parse(results);
                            console.log(tasks);
                            if (tasks.length > 0) {
                                $("#tlist").empty();
                                for (var i = 0; i < tasks.length; i++) {
                                    $('#event_id').val(endval);
                                    $('#tlist').append('<li class="list-group-item"><label><input type="checkbox" value="' + tasks[i].event_task_id + '" name="ev_tasks[]" id="' + tasks[i].event_task_id + '">' + tasks[i].task_des + '</label></li>');
                                }
                            }
                        } else {
                            $("#tlist").empty();
                            $('#tlist').append('<li class="list-group-item"><p><strong>Sorry,</strong> No tasks to display. Please select other event </p></li>');
                        }

                    }
                });
            }

        });
    });
</script>
