<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            MY MESSAGES
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="msg_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Message id</th>
                            <th>Sender</th>
                            <th>Message Title</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                </table>

                <div id="mod"></div>
            </div>
        </div>
        
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/view_messages/load_msg'; ?>",
            success: function (results) {
                console.log($.parseJSON(results));
                $('#msg_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "msg_id", "visible": false},
                        {"mData": "sender_email"},
                        {"mData": "msg_title", "mRender": function (data) {
                                return '<a class="pop-msg">' + data + '</a>';
                            }},
                        {"mData": "msg_date"}
                    ]
                });
            }
        });

        $('#msg_tab').on('click', 'a.pop-msg', function () {
            var tab = $('#msg_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            window.location.href = "<?php echo site_url() . '/view_messages/get_single_msg/'; ?>" + data.msg_id;
//            $.ajax({
//                type: "POST",
//                url: "<?php echo site_url() . '/view_messages/get_single_msg/'; ?>" + data.msg_id,
//                success: function (results) {
//                    console.log(results);
////                    $('#mod').html(results);
////                    $('#msg_modal').modal('show');
//                }
//            });
        });
    });
</script>

