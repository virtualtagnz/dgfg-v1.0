<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blog Posts
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">&nbsp;</div>
            <div class="grid">
                <?php foreach ($info as $row) { ?>
                    <div class="grid-item grid-item--width3 grid-item--height2">
                        <div>
                            <?php if ($row['blog_image'] != NULL) { ?>
                                <img class="img-responsive" src="<?php echo base_url() . $row['blog_image']; ?>" alt=""/>
                                <ul class="list-inline over-img">
                                    <li><i class="fa fa-clock-o"></i> <?php echo $row['blog_created_date']; ?></li>
                                </ul>
                            <?php }else{ ?>
                                <img class="img-responsive" src="<?php echo base_url() . 'public/uploads/blog/no-image.jpg' ?>" alt=""/>
                                <ul class="list-inline over-img">
                                    <li><i class="fa fa-clock-o"></i> <?php echo $row['blog_created_date']; ?></li>
                                </ul>
                            <?php } ?>
                            <div class="blog-body">
                                <h2><a href="#" onclick="expand_view(<?php echo $row['blog_id']; ?>);"><?php echo $row['blog_name']; ?></a></h2>
                                <p><?php echo substr($row['blog_body'], 0, 200) . "..."; ?></p>

                                    <!--a class="likes" href="#"><i class="fa fa-heart"></i>239</a-->
                            </div>
                            <ul class="list-inline">
                                <li><a href="#" onclick="expand_view(<?php echo $row['blog_id']; ?>);">Read More...</a></li>
                                <!--li><i class="fa fa-comments-o"></i> <a href="#">7 Comments</a></li-->
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="mod"></div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('p img').removeAttr("style");
        $('p img').addClass("img-responsive");
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 8,
            gutter: 8
        });
    });

    function expand_view(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/blog/blog_single_view/'; ?>" + id,
            success: function (results) {
                console.log(results);
                $('#mod').html(results);
                $('#myModal').modal('show');
            }
        });
    }
</script>
<?php
//echo base_url()."blog/blog_single_view/".$row['blog_id']; ?>