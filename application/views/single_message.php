<!--<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="msg_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">-->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            MY MESSAGES
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <?php

                function extract_senders_receiver($input_arr) {
                    $arr = array();
                    $i = 0;
                    foreach ($input_arr as $value) {
                        $arr['sender'] = $value['sender_email'];
                        //if ($value['receiver_email'] != $this->session->userdata('username')) {
                        $arr['sender_img'] = $value['sender_img'];
                        $temp[$i] = $value['receiver_email'];
                        $arr['res'][$i] = $temp[$i];
                        $i++;
                        //}
                    }
                    return $arr;
                }
                ?>
                <p><h2 class="text-center"><i><?php echo $msg[0]['msg_title']; ?></i></h2></p>
                <?php $rainco = extract_senders_receiver($send_res) ?>
                <div class="msg-wrapper">
                    <div><img class="img-responsive img-circle" width="60" src="<?php echo $rainco['sender_img'] ?>"></div>
                    <div><strong><?php echo 'From : ' . $rainco['sender'] . '<br>'; ?></strong></div>
                    <div><i>To : 
                            <?php
                            $str = implode(", ", $rainco['res']);
                            echo $str;
                            ?></i></div>
                    <div><?php echo $msg[0]['msg_date']; ?></div>
                    <!--                    <div class="col-md-12"><hr></div>-->
                    <p><?php echo $msg[0]['msg_body']; ?></p>
                    <div class="col-md-12"><hr></div>
                </div>


                <?php if ($reply_msg) { ?>
                    <div class="msg-wrapper">
                        <?php foreach ($reply_msg as $value) { ?>
                            <?php $exsr = extract_senders_receiver($value['rep_rs']); ?>
                            <div><img class="img-responsive img-circle" width="60" src="<?php echo $exsr['sender_img'] ?>"></div>
                            <div class=""><strong><?php echo 'From : ' . $exsr['sender'] . '<br>'; ?></strong></div>
                            <div class=""><i>To : 
                                    <?php
                                    $str = implode(", ", $exsr['res']);
                                    echo $str;
                                    ?></i></div>
                            <div class=""><?php echo $value['msg_date']; ?></div>
                            <p><?php echo $value['msg_body']; ?></p>
                            <div class="col-md-12"><hr></div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <div class="container-fluid" id="event">
                    <div class="col-md-10">
                        <form class="form-horizontal center-block" name="reply_frm" id="reply_frm" method="post" action="<?php echo site_url('create_message/reply_msg/' . $msg[0]['msg_id']); ?>">
                            <div class="form-group">
                                <label for="msg_to" class="col-sm-3 control-label">To</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="msg_to" id="msg_to" multiple="true" value="<?php echo set_value('msg_to'); ?>">
                                    <div id="infoMessage"><?php echo form_error('msg_to'); ?></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="msg" class="col-sm-3 control-label">Reply Message</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="msg" id="msg" rows="4" cols="50"></textarea>
                                    <div id="infoMessage"><?php echo form_error('msg'); ?></div>
                                </div>
                            </div>

                            <input type="hidden" name="msd" id="msd" value="<?php echo $msg[0]['msg_id']; ?>">
                            <input type="hidden" name="msg_sub" id="msg_sub" value="<?php echo $msg[0]['msg_title']; ?>">
                            <div class="form-group">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9">
                                    <button type="submit" id="btnev" class="btn btn-primary pull-right"><i class="fa fa-paper-plane fa-fw" aria-hidden="true"></i> Send Reply</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Replied successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>

<!--        </div>
    </div>
</div>-->
<script>
    $(document).ready(function () {
        CKEDITOR.replace('msg');

//        $('#msg_modal').modal('show');

        var senders = <?php echo $senders_arr; ?>;
        var dt = senders.length;
        //console.log(senders);
        var senders_arr = new Array();
        for (var i = 0; i < senders.length; i++) {
            senders_arr[i] = '' + senders[i].uname + '<' + senders[i].email + '>' + '';
        }
        console.log(senders_arr);
        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }
        $("#msg_to")
                // don't navigate away from the field on tab when selecting an item
                .bind("keydown", function (event) {
                    if (event.keyCode === $.ui.keyCode.TAB &&
                            $(this).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
            minLength: 0,
            source: function (request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(
                        senders_arr, extractLast(request.term)));
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }
        });
    });
</script>
