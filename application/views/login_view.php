<body class="loginbg">

    <div class="container-table">
        <div class="row vertical-center-row">

        <div class="col-md-8 center-block">
            <img class="img-responsive center-block" width="22%" style="padding: 10px;" src="<?php echo base_url('public/images/logo_main.png') ?>">
            <div class="event-ov">
                <h2 class="text-center">UPCOMING EVENTS</h2>
                <?php if (count($up_coming_ev) > 0) { ?>
                    <table class="table centered">
                        <thead>
                            <tr>
                                <th>Event Date</th>
                                <th>Event Name</th>
                                <th>View Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($up_coming_ev as $value) { ?>
                            <tr>
                                <td><h4><?php echo $value['event_date']; ?></h4></td>
                                <td><h4><?php echo $value['event_name']; ?></h4></td>
                                <td><button><a class="v-event" id="<?php echo $value['event_id']; ?>">View Event</a></button></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-4 center-block" style="margin-top: 20px;">
<!--            <img class="img-responsive center-block" width="45%" style="padding: 10px;" src="<?php echo base_url('public/images/logo_main.png') ?>">-->
<!--                <div class="text-center"><img src=""/></div>-->
            <div class="login-form">
                <form name="login-form" id="login-form" action="<?php echo site_url('login/user_login'); ?>" method="post">
                    <div class="form-group">
                        <input class="form-control" type="text" name="user_name" id="user_name" placeholder="User Name"  autocomplete="off" value="<?php echo set_value('user_name') ?>">
                        <div id="infoMessage"><?php echo form_error('user_name'); ?></div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="user_pwd" id="user_pwd" placeholder="Password"  autocomplete="off" value="<?php echo set_value('user_pwd') ?>">
                        <div id="infoMessage"><?php echo form_error('user_pwd'); ?></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control">LOG IN</button>
                    </div>
                </form>
                <div class="text-center pwd-lost col-xs-12" id="lst-pwd"><a href="">Lost Your Password?</a></div>
            </div>
            <div class="text-center" style="font-size: 15px; color: #fff;">
                <?php if ($this->session->flashdata('error_msg1')) { ?>
                    <div> <?= $this->session->flashdata('error_msg1') ?> </div>
                <?php } else { ?>
                    <div> <?= $this->session->flashdata('error_msg2') ?> </div>
                <?php } ?>
            </div>
            <br/>
            <div class="form-group">
                <a href="<?php echo $login_url; ?>" class="btn btn-facebook form-control">Login with Facebook</a>
            </div>

            <div class="register-form">
                <form name="reg-form" id="reg-form" action="<?php echo site_url('login/user_registration'); ?>" method="post">
                    <h4>New to DGFG? Sign up</h4>
                    <div class="form-group">
                        <input class="form-control" type="text" name="first_name" id="first_name" placeholder="First Name"  autocomplete="off" value="<?php echo set_value('first_name') ?>">
                        <div id="infoMessage"><?php echo form_error('first_name'); ?></div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="last_name" id="last_name" placeholder="Last Name"  autocomplete="off" value="<?php echo set_value('last_name') ?>">
                        <div id="infoMessage"><?php echo form_error('last_name'); ?></div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="reg_user_name" id="reg_user_name" placeholder="Email"  autocomplete="off" value="<?php echo set_value('reg_user_name') ?>">
                        <div id="infoMessage"><?php echo form_error('reg_user_name'); ?></div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="reg_user_pwd" id="reg_user_pwd" placeholder="Password"  autocomplete="off" value="<?php echo set_value('reg_user_pwd') ?>">
                        <div id="infoMessage"><?php echo form_error('reg_user_pwd'); ?></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control">SIGN UP</button>
                    </div>
                </form>
            </div>

            <?php if ($this->session->flashdata('success_msg')) { ?>
                <script>
                    swal("Thank You!", "Please check your email!", "success")
                </script>
            <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                <script>
                    swal("Error!", "Something is going wrong!", "error")
                </script>
            <?php } ?>
        </div>

        <!--        <div class="col-md-4 center-block pull-right">
                    <br/>
                    <div class="">
                        <div class="form-group">
                            <a href="<?php echo $login_url; ?>" class="btn btn-facebook form-control">Login with Facebook</a>
                        </div>
                    </div>
                </div>-->
                    </div>
    </div>
<div id="dmod"></div>
</body>
<script>
    $(document).ready(function(){
        $('.v-event').on('click', function () {
            var uid = $(this).attr('id');
            console.log(uid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/single_event/active_events_view/" + uid,
                success: function (results) {
                    console.log(results);
                    $('#dmod').html(results);
                    $('#single_active_events').modal('show');
                }
            });
        });
    });
</script>

