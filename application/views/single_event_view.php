<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ev_carousel">
            <?php if (!empty($event_img_linq)) { ?>
                <div class="col-lg-12">
                    <img class="img-responsive center-block" src="<?php echo base_url() . 'public/uploads/' . $event_name . '/' . $event_img_linq ?>"/>
                </div>
            <?php }; ?>
            <p><h2 class="text-center"><i><?php echo $event_name; ?></i></h2></p>
            <?php if ($event_video_linq !== "") { ?>
                <video width="100%" height="" controls>
                    <source src="<?php echo base_url() . 'public/uploads/' . $event_name . '/' . $event_video_linq ?>" type="video/mp4">
                </video>
            <?php } ?>
            <p class="text-center"><?php echo $event_comments ?></p>
            <table class="table centered">
                <tbody>
                    <tr>
                        <td>Date:</td>
                        <td><?php echo $event_date ?></td>
                    </tr>
                    <tr>
                        <td>Time:</td>
                        <td><?php echo $event_start . ' - ' . $event_end ?></td>
                    </tr>
                    <tr>
                        <td>Location:</td>
                        <td><?php echo $event_loc ?></td>
                    </tr>
                    <tr>
                        <td>Promotion Links:</td>
                        <td><?php
                            echo '<a href=' . $event_promo1 . '>Promo Link 1</a><br>'
                            . '<a href=' . $event_promo2 . '>Promo Link 2</a><br>'
                            . '<a href=' . $event_promo3 . '>Promo Link 3</a><br>'
                            . '<a href=' . $event_promo4 . '>Promo Link 4</a><br>'
                            ?></td>
                    </tr>
                    <tr>
                        <td>Event Plan:</td>
                        <td><?php echo '<a href=' . $event_plan . '>Event Plan</a><br>' ?></td>
                    </tr>
                    <!--event video take up-->
                    <tr>
                        <td>Event Tasks:</td>
                        <td>
                            <?php
                            $json1 = json_decode($json_str);
                            foreach ($json1 as $key => $value) {
                                echo $value->task_des . ' - ' . $value->due_date . '<br>';
                            }
                            ?>  
                        </td>
                    </tr>
                    <tr>
                        <td>Share:</td>
                        <td>
                            <button class="btn btn-facebook" onclick="location.href = '<?php echo $return_url . $event_id; ?>'">Share this Event on Facebook</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
            $img_arr = json_decode($img_json);
            if(count($img_arr) > 0){
            ?>
            <div id="ev-carousel" style="margin: 10px auto !important;width: 600px !important;">
                <div id="carousel_custom" class="carousel slide" data-ride="carousel">
                    <div class="carousel-outer" style="position: relative !important;">
                        <div class="carousel-inner">
                            <?php $k = 0; ?>
                            <?php foreach ($img_arr as $img_value2) { ?>
                                <div class="item <?php echo ($k == 0) ? "active" : "" ?>">
                                    <img class="center-block" src="<?php echo $img_value2->img_linq ?>" alt="...">
                                </div>
                                <?php $k++; ?>
                            <?php } ?>
                        </div>

                        <a class="left carousel-control" href="#carousel_custom" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel_custom" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>  
                    </div>
                    <ol class='carousel-indicators' style="margin: 10px 0 0 !important;overflow: auto !important;position: static !important;text-align: left !important;white-space: nowrap !important;width: 100% !important;">
                        <?php $j = 0; ?>
                        <?php foreach ($img_arr as $img_value) { ?>
                            <li data-target="#carousel_custom" data-slide-to="<?php echo $j ?>" class="<?php echo ($j == 0) ? "active" : "" ?>" style="background-color: transparent;-webkit-border-radius: 0;border-radius: 0;display: inline-block;height: auto;margin: 0;width: auto;">
                                <img class="center-block" style="display: block;" src="<?php echo $img_value->img_linq ?>" alt="..." width="100" height="50">
                            </li>
                            <?php $j++; ?>
                        <?php } ?>
                    </ol>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.carousel').carousel({
            interval: 2000
        });
    });
</script>