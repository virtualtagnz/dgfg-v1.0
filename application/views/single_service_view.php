<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="service_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <h4 class="text-center"><strong><?php echo $sp_name ?></strong></h4>
            <?php if (($sp_img)) { ?>
                <img class="img-responsive center-block" src="<?php echo base_url() . 'public/uploads/' . $sp_name . '/' . $sp_img ?>" alt="...">
            <?php } ?>
            <p><?php echo $sp_summery ?></p>
            <p><?php echo $sp_list ?></p>
            <table class="table centered">
                <tbody>
                    <tr>
                        <td>Web Site:</td>
                        <td><a href="<?php echo $sp_web ?>" target="_blank"><?php echo $sp_web ?></a></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td><?php echo $sp_phn; ?></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><?php echo $sp_add; ?></td>
                <div class="col-sm-12" id="map" style="height: 200px;"></div>
                </tr>
                <tr>
                    <td>Provider Contact Name:</td>
                    <td><?php echo $sp_key; ?></td>
                </tr>
                <tr>
                    <td>Opening hours:</td>
                    <td><?php echo $sp_hrs; ?></td>
                </tr>
                <tr>
                    <td>Cost:</td>
                    <td><?php echo $sp_cost; ?></td>
                </tr>
                </tbody>
            </table>
            <div class="col-md-12"><hr></div>
            <h4 class="text-center">Contact - <strong><?php echo $sp_name ?></strong></h4>
            <form class="form-horizontal center-block" name="msg_frm" id="msg_frm" method="post" action="<?php echo site_url('single_service/send_service_message/' . $service_id); ?>">
                <div class="form-group">
                    <label for="subject" class="col-sm-3 control-label">Subject</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="subject" id="subject" value="<?php echo set_value('subject'); ?>">
                        <div id="infoMessage"><?php echo form_error('subject'); ?></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">Message</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" type="text" name="message" id="message" value="<?php echo set_value('message'); ?>"></textarea>
                        <div id="infoMessage"><?php echo form_error('message'); ?></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email Address</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="email" id="email" value="<?php echo set_value('email'); ?>">
                        <div id="infoMessage"><?php echo form_error('email'); ?></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="telephone" class="col-sm-3 control-label">Telephone Number</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="telephone" id="telephone" value="<?php echo set_value('telephone'); ?>">
                        <div id="infoMessage"><?php echo form_error('telephone'); ?></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" id="btnmsg" class="btn btn-primary pull-right"><i class="fa fa-paper-plane fa-fw" aria-hidden="true"></i> Send Message</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#service_modal').modal('show');
    });
</script>
<script>
// This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('saddress')),
                {types: ['geocode']});
    }

    function initMap() {
        var address = <?php echo "'$sp_add'" ?>;
        GetLatLong(address);
    }

    function GetLatLong(address) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var latlng = results[0].geometry.location;
                var mapOptions = {
                    zoom: 15,
                    center: latlng
                };

                var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                var marker = new google.maps.Marker({
                    title: <?php echo "'$sp_add'" ?>,
                    map: map,
                    position: results[0].geometry.location
                });

            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initMap"
async defer></script>