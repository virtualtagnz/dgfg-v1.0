<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Headers (Page header) -->
    <section class="content-header">
        <h1>
            Add a Blog Post
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="blog_frm" id="msg_frm" method="post" action="<?php echo site_url('blog/blog_add_post'); ?>"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="blog_title" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="blog_title" id="msg_subject" value="<?php //echo set_value('blog_title'); ?>">
                            <div id="infoMessage"><?php echo form_error('blog_title'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-3 control-label">Featured Image</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="image" id="image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 851*315 only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="blog_body" class="col-sm-3 control-label">Body</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="blog_body" id="msg" rows="4" cols="50"></textarea>
                            <div id="infoMessage"><?php echo form_error('blog_body'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i> Post for Review</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('blog_body', {
            filebrowserBrowseUrl: '/browser/browse.php?type=Files',
            filebrowserUploadUrl: '/uploader/upload.php?type=Files'
        });

    });
</script>
<?php if ($this->session->flashdata('success_msg')) { ?>
    <script>
        swal("Good job!", "Post added successfully!", "success")
    </script>
<?php } elseif ($this->session->flashdata('error_msg')) { ?>
    <script>
        swal("Error!", "Something is going wrong!", "error")
    </script>
<?php } ?>



