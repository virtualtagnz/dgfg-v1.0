<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title ?>
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->
        <div class="container-fluid" id="add-user">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="user-frm" id="user-frm" method="post" action="<?php echo site_url('user_edit/modify_user/' . $seg1 . '/' . $seg2); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if ($img !== "") { ?>
                                <img id="prof-img" class="img-responsive img-circle" src="<?php echo $img ?>"/>
                            <?php } else { ?>
                                <img id="prof-img" class="img-responsive img-circle" src="<?php echo base_url() . 'public/uploads/default-user.jpg' ?>"/>   
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="user-email" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="email" name="user_email" id="user_email" value="<?php echo $uemail; ?>">
                            <div id="infoMessage"><?php echo form_error('user_email'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="pwd" class="col-sm-3 control-label">Change Password</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" name="pwd" id="pwd"  value="<?php echo set_value('pwd'); ?>" size="32">
                            <div id="infoMessage"><?php echo form_error('pwd'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="re-pwd" class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" name="re_pwd" id="re_pwd"  value="<?php echo set_value('re_pwd'); ?>" size="32">
                            <div id="infoMessage"><?php echo form_error('re_pwd'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status" id="status">
                                <option value="1"<?= $act_stat == '1' ? ' selected="selected"' : ''; ?>>Active</option>
                                <option value="0"<?= $act_stat == '0' ? ' selected="selected"' : ''; ?>>Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="first_name" class="col-sm-3 control-label">First Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="test" name="first_name" id="first_name"  value="<?php echo $fname; ?>">
                            <div id="infoMessage"><?php echo form_error('first_name'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="test" name="last_name" id="last_name"  value="<?php echo $lname; ?>">
                            <div id="infoMessage"><?php echo form_error('last_name'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="gender" class="col-sm-3 control-label">Gender  </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" id="male" value="male"<?= $gender == 'male' ? ' checked="checked"' : ''; ?>> Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" id="male" value="female"<?= $gender == 'female' ? ' checked="checked"' : ''; ?>> Female
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="mobile" class="col-sm-3 control-label">Mobile No</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="tel" name="mobile" id="mobile" value="<?php echo $mob; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-3 control-label">Change Profile Image</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="image" id="image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : jpg, jpeg and png only</label><br>
                            <label>Accept size   : 120*120px only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="des" class="col-sm-3 control-label">Profile Description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="des" id="des" rows="4" cols="50"><?php echo $des; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fb_linq" class="col-sm-3 control-label">Facebook Link</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="url" name="fb_linq" id="fb_linq" value="<?php echo $fb; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inst_linq" class="col-sm-3 control-label">Instagram Link</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="url" name="inst_linq" id="inst_linq"  value="<?php echo $inst; ?>">
                        </div>
                    </div>

                    <?php if ($user_type === '1') { ?>
                        <div class="form-group">
                            <label for="smc" class="col-sm-3 control-label">Social media challenge</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="test" name="smc" id="smc"  value="<?php echo $sm; ?>">
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <label for="ethnicity" class="col-sm-3 control-label">Ethnicity</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="ethnicity" id="ethnicity">
                                    <option value="0">--Please Select--</option>
                                    <option value="1"<?= $sm == '1' ? ' selected="selected"' : ''; ?>>New Zealand European</option>
                                    <option value="2"<?= $sm == '2' ? ' selected="selected"' : ''; ?>>Māori</option>
                                    <option value="3"<?= $sm == '3' ? ' selected="selected"' : ''; ?>>Samoan</option>
                                    <option value="4"<?= $sm == '4' ? ' selected="selected"' : ''; ?>>Tongan</option>
                                    <option value="5"<?= $sm == '5' ? ' selected="selected"' : ''; ?>>Niuean</option>
                                    <option value="6"<?= $sm == '6' ? ' selected="selected"' : ''; ?>>Chinese</option>
                                    <option value="7"<?= $sm == '7' ? ' selected="selected"' : ''; ?>>Indian</option>
                                    <option value="8"<?= $sm == '8' ? ' selected="selected"' : ''; ?>>Other</option>
                                </select>
                            </div>
                        </div>
                    <?php } ?>

                    <input type="hidden" name="old_img" id="old_img" value="<?php echo $img !== "" ? $img : "" ?>">

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btn-user" class="btn btn-primary pull-right">Edit User Profile</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "User Info Update Successfully!", "success")
                    </script>
    <!--                    <div class="alert alert-success text-center msg-success pull-right"> <?= $this->session->flashdata('success_msg') ?> </div>-->
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
    <!--                    <div class="alert alert-danger text-center msg-success pull-right"> <?= $this->session->flashdata('error_msg') ?> </div>-->
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script>
    $(document).ready(function () {
    });
</script>



