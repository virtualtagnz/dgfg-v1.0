<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="invite_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" name="invite_frm" id="invite_frm" method="post" action="<?php echo site_url('send_invitation/send_member_invitation/'.$uid); ?>">
                <div class="form-group">
                    <label for="ev_name" class="col-sm-3 control-label">Please Select Event Name</label>
                    <div class="col-sm-9">
                        <input class="form-control" list="listone" name="ev_name" id="ev_name">
                        <datalist id="listone">
                            <?php if (count(my_event) > 0) { ?>
                                <?php for ($i = 0; $i < count($my_event); $i++) { ?>
                                    <option id="<?php echo $my_event[$i]['event_id'] ?>" value="<?php echo $my_event[$i]['event_name'] ?>"></option>
                                <?php } ?>;
                            <?php } else { ?>
                                <option id="0" value="No available events!"></option>
                            <?php } ?>;
                        </datalist>
                        <div id="infoMessage"><?php echo form_error('ev_name'); ?></div>
                    </div>
                    <input type="hidden" name="event_id" id="event_id">
                </div>
                <div class="form-group">
                    <label for="user-email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="email" name="user_email" id="user_email" readonly="true" value="<?php echo $email; ?>">
                        <div id="infoMessage"><?php echo form_error('user_email'); ?></div>
                        <input type="hidden" name="u_id" id="u_id" value="<?php echo $uid; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" id="btn-user" class="btn btn-primary pull-right">Send Invitation</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('#ev_name').bind('input', function () {
            var x = $(this).val();
            var z = $('#listone');
            var val = $(z).find('option[value="' + x + '"]');
            var endval = val.attr('id');
            $('#event_id').val(endval);

        });

    });
</script>
