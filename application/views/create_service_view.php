<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CREATE SERVICE DIRECTORY
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Page Content Here -->
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="service_frm" id="event_frm" method="post" action="<?php echo site_url('create_service/add_service'); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="sprovider" class="col-sm-3 control-label">Name of Provider</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="sprovider" id="sprovider" value="<?php echo set_value('sprovider'); ?>">
                            <div id="infoMessage"><?php echo form_error('sprovider'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="scategory" class="col-sm-3 control-label">Provider Category</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="scategory" id="scategory">
                                <option value="0"<?php echo set_select('scategory', "0", TRUE); ?>>-- Please Select --</option>
                                <?php foreach ($service_cat as $key => $value) {?>
                                    <option value="<?php echo $value['wellness_sec_id'];?>"><?php echo $value['wellness_sec_name']?></option>
                                <?php }?>
                            </select>
                            <div id="infoMessage"><?php echo form_error('scategory'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ssummary" class="col-sm-3 control-label">Summary of Services</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="ssummary" id="ssummary" rows="4" cols="50"></textarea>
                            <div id="infoMessage"><?php echo form_error('ssummary'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="scost" class="col-sm-3 control-label">Cost of Service</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="scost" id="scost" value="<?php echo set_value('scost'); ?>">
                            <div id="infoMessage"><?php echo form_error('scost'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="scontact" class="col-sm-3 control-label">Contact Number</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="tel" name="scontact" id="scontact" value="<?php echo set_value('scontact'); ?>">
                            <div id="infoMessage"><?php echo form_error('scontact'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="skey" class="col-sm-3 control-label">Provider Key Contact Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="skey" id="skey" value="<?php echo set_value('skey'); ?>">
                            <div id="infoMessage"><?php echo form_error('skey'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="slist" class="col-sm-3 control-label">List Of Services Offered</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="slist" id="slist" rows="4" cols="50"></textarea>
                            <div id="infoMessage"><?php echo form_error('slist'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="saddress" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="saddress" id="saddress" placeholder="" value="<?php echo set_value('saddress'); ?>">
                            <div id="infoMessage"><?php echo form_error('saddress'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="shours" class="col-sm-3 control-label">Opening hours</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="shours" id="shours" value="<?php echo set_value('shours'); ?>">
                            <div id="infoMessage"><?php echo form_error('shours'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="safternum" class="col-sm-3 control-label">After Hours Number</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="tel" name="safternum" id="safternum" value="<?php echo set_value('safternum'); ?>">
                            <div id="infoMessage"><?php echo form_error('safternum'); ?></div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Contact Email</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="tel" name="email" id="safternum" value="<?php echo set_value('email'); ?>">
                            <div id="infoMessage"><?php echo form_error('email'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-3 control-label">Photo of Provider</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="image" id="image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 120*120px only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="staff_image" class="col-sm-3 control-label">Photo of staff</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="staff_image" id="staff_image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 120*120px only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sweb" class="col-sm-3 control-label">Provider Website</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="url" name="sweb" id="sweb" value="<?php echo set_value('sweb'); ?>">
                            <div id="infoMessage"><?php echo form_error('sweb'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="slevel" class="col-sm-3 control-label">Level</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="slevel" id="slevel">
                                <option value="0"<?php echo set_select('scategory', "0", TRUE); ?>>-- Please Select --</option>
                                <option value="1">High</option>
                                <option value="2">Medium</option>
                                <option value="3">Low</option>
                            </select>
                            <div id="infoMessage"><?php echo form_error('slevel'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="sbtn_save" class="btn btn-primary pull-right">Create Service Directory</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <div class="alert alert-success text-center msg-success pull-right"> <?= $this->session->flashdata('success_msg') ?> </div>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <div class="alert alert-danger text-center msg-success pull-right"> <?= $this->session->flashdata('error_msg') ?> </div>
                <?php } ?>
            </div>
        </div>
    </section>
</div>

<script>
// This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('saddress')),
                {types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete.addListener('place_changed', fillInAddress);
    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initAutocomplete"
async defer></script>

