<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            INVITED EVENTS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="invit_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Event Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Event Location</th>
                            <th>Event Id</th>
                            <th>Accept Invitation</th>
                            <th>View Event</th>
                        </tr>
                    </thead>
                </table>

                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/invited_events/view_invited_events'; ?>",
            success: function (results) {
                $('#invit_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "event_name"},
                        {"mData": "event_date"},
                        {"mData": "event_start"},
                        {"mData": "event_end"},
                        {"mData": "event_loc"},
                        {"mData": "event_id", "visible": false},
                        {"mData": "", "mRender": function () {
                                return '<a class="intevt">Accept Invitation</a>';
                            }},
                        {"mData": "", "mRender": function (data) {
                                return '<a class="vtevt">View Full Event Info</a>';
                            }}
                    ]
                });
            }
        });
        
        $('#invit_tab').on('click', 'a.vtevt', function () {
            var tab = $('#invit_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_event/'; ?>" + data.event_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#myModal').modal('show');
                }
            });
        });
        
        $('#invit_tab').on('click', 'a.intevt', function () {
            var tab = $('#invit_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "DO ACCEPT INVITATION?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Accept!", cancelButtonText: "No, Cancel!", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/invited_events/accept_invitation/'; ?>" + data.event_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results == "true") {
                                        swal("Accepted!", "Invitation accepted successfully.", "success");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });
    });
</script>