<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            UPLOAD NEW PHOTOS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <form id="upload" action="<?php echo site_url() . "/edit_event/upload_images/" . $event_id; ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="evname" id="evname" value="<?php echo $event_name; ?>"/>
                    <input type="hidden" name="files" id="files" value=""/>
                    <div class="form-group">
                        <label for="image" class="col-sm-3 control-label">Event photos</label>
                        <div class="col-sm-6">
                            <input class="form-control file" type="file" name="image[]" id="image" value="<?php echo set_value('image'); ?>" multiple="multiple" accept="image/*"/>
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 851*315 px only</label>
                            <div id="infoMessage"><?php echo form_error('files'); ?></div>
                        </div>
                        <div class="col-sm-3"><button type="submit" id="btnev" class="btn btn-primary pull-left">Upload Photos</button></div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Photos uploaded successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <?php if (($this->session->flashdata('detail'))) { ?>
                        <script>
                            //swal("Error!", "<?php echo $this->session->flashdata('detail'); ?>", "error", html:true)
                            swal({title: "Error!", text: "<?php echo $this->session->flashdata('detail'); ?>", type: "error", html: true});
                        </script>
                    <?php } else { ?>
                        <script>
                            swal("Error!", "Something is going wrong!", "error")
                        </script>
                    <?php } ?>
                <?php } elseif ($this->session->flashdata('form_msg')) { ?>
                    <script>
                        swal("Error!", "Please choose images before upload!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="content-header">
        <h1>
            UPLOADED PHOTOS
            <small>Submit your photos to get the approval</small>
        </h1>
    </section>
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12"><p id="no_images_uploaded" class="text-center text-warning"> You have no uploaded photos to submit for approval...</p></div>
            <div class="grid">
                <!--div-->
                <?php $json_str1 = json_decode($pending_images); ?>
                <?php if ($json_str1 != null) { ?>
                    <?php foreach ($json_str1 as $key1 => $value1) { ?>
                        <div class="grid-item grid-item--width1">
                            <!--div style="padding: 0 4px">
                                <div class="thumbnail col-lg-3 col-md-4 col-sm-6 col-xs-12"-->
                            <div>
                                <div>
                                    <?php if ($value1->img_linq != NULL) { ?>
                                        <a class="magnifine_view" rel="<?php echo $value1->img_linq; ?>"><img class="img-responsive" src="<?php echo $value1->img_linq; ?>" alt=""/></a>
                                    <?php } ?>
                                </div>
                                <div class="text-justify caption">
                                    <!--div-->
                                    <i class="fa fa-clock-o"></i> <?php echo $value1->create_date; ?>
                                </div>
                                <div class="caption">
                                    <!--div-->
                                    <a rel="<?php echo $value1->img_linq; ?>" class="magnifine_view"><i class="fa fa-eye"></i>&nbsp;View &nbsp;&nbsp;&nbsp;</a>
                                    <a rel="<?php echo $value1->galary_id; ?>" class="submit_img"><i class="fa fa-arrow-circle-o-right"></i>&nbsp;Submit &nbsp;&nbsp;&nbsp;</a>
                                    <a rel="<?php echo $value1->galary_id; ?>" class="delete_img"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="hidden" name="check_img_uploaded" id="check_img_uploaded" value="<?php echo TRUE; ?>"/>
                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $value1->event_id; ?>"/>
                <?php } ?>
            </div>
            <div class="col-md-12">
                <button type="button" id="btn_submit" class="btn btn-primary pull-right"><i class="fa fa-arrow-circle-o-right fa-fw" aria-hidden="true"></i> Submit for approval</button>
            </div>
        </div>
    </section>
    <section class="content-header">
        <h1>
            SUBMITTED PHOTOS
            <small>You can see your photos on the event view after the approval</small>
        </h1>
    </section>
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12"><p id="no_images_submit" class="text-center text-warning"> You have no submitted photos for approval...</p></div>
            <div class="grid">
                <!--div-->
                <?php $json_str2 = json_decode($approve_images); ?>
                <?php if ($json_str2 != null) { ?>
                    <?php foreach ($json_str2 as $key2 => $value2) { ?>
                        <div class="grid-item grid-item--width1">
                            <!--div style="padding: 0 4px">
                                <div class="thumbnail col-lg-3 col-md-4 col-sm-6 col-xs-12"-->
                            <div>
                                <div>
                                    <?php if ($value2->img_linq != NULL) { ?>
                                        <a class="magnifine_view" rel="<?php echo $value2->img_linq; ?>"><img class="img-responsive" src="<?php echo $value2->img_linq; ?>" alt=""/></a>
                                    <?php } ?>
                                </div>
                                <div class="text-justify caption">
                                    <!--div-->
                                    <i class="fa fa-clock-o"></i> <?php echo $value2->create_date; ?>
                                </div>
                                <div class="caption">
                                    <!--div-->
                                    <a rel="<?php echo $value2->img_linq; ?>" class="magnifine_view"><i class="fa fa-eye"></i>&nbsp;View &nbsp;&nbsp;&nbsp;</a>
                                    <a rel="<?php echo $value2->galary_id; ?>" class="delete_img"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="hidden" name="check_img_submit" id="check_img_submit" value="<?php echo TRUE; ?>"/>
                <?php } ?>
            </div>
        </div>
    </section>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="imgModal">
        <div class="modal-dialog modal-lg">
            <img src="" alt="img event" id="magnifinedimg" class="img-responsive center-block img-bordered"/>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var has_value_upload = $("#check_img_uploaded").val();
        $("#btn_submit").prop('disabled', true);
        if (has_value_upload == 1) {
            $("#btn_submit").prop('disabled', false);
            $("#no_images_uploaded").hide();
        }

        var has_value_submit = $("#check_img_submit").val();
        if (has_value_submit == 1) {
            $("#no_images_submit").hide();
        }

        $("#image").change(function () {
            var names = [];
            var i = 0;
            for (i; i < $(this).get(0).files.length; ++i) {
                names.push($(this).get(0).files[i].name);
            }
            $("input[name=files]").val(names);
        });

        $('p img').removeAttr("style");
        $('p img').addClass("img-responsive");
        $('.grid').masonry({
            // options
            itemSelector: '.grid-item',
            columnWidth: 200
        });

        $(".magnifine_view").click(function () {
            var image = $(this).get(0).rel;
            $("#magnifinedimg").attr("src", image);
            $('#imgModal').modal('show');
        });

        $(".delete_img").click(function () {
            var galary_id = $(this).get(0).rel;
            swal({title: "DO YOU WANT TO DELETE THE IMAGE?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url() . '/edit_event/delete_image/'; ?>" + galary_id,
                        success: function (results) {
                            console.log(results);
                            if (results === "1") {
                                swal("Good job!", "Deleted successfully!", "success")
                                setTimeout(function () {
                                    location.reload();
                                }, 3000);
                            } else {
                                swal("Error!!!", "Something is going wrong, Please try again", "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
        });
        $(".submit_img").click(function () {
            var galary_id = $(this).get(0).rel;
            swal({title: "DO YOU WANT TO SUBMIT THE IMAGE?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url() . '/edit_event/submit_single_image/'; ?>" + galary_id,
                        success: function (results) {
                            console.log(results);
                            if (results === "1") {
                                swal("Good job!", "Submitted successfully!", "success")
                                setTimeout(function () {
                                    location.reload();
                                }, 3000);
                            } else {
                                swal("Error!!!", "Something is going wrong, Please try again", "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
        });
        $("#btn_submit").click(function () {
            var event_id = $("#event_id").val();
            swal({title: "DO YOU WANT TO SUBMIT THE ALBUM?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url() . '/edit_event/submit_images/'; ?>" + event_id,
                        success: function (results) {
                            console.log(results);
                            if (results === "1") {
                                swal("Good job!", "Submitted successfully!", "success")
                                setTimeout(function () {
                                    location.reload();
                                }, 3000);
                            } else {
                                swal("Error!!!", "Something is going wrong, Please try again", "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
        });

        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 200,
            gutter: 8
        });
    });

</script>