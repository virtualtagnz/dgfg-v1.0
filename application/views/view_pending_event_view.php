<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PENDING EVENTS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="pev_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Event Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Event Location</th>
                            <th>Approve Event</th>
                            <th>Approve Reject</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/view_pending_event/pending_events'; ?>",
            success: function (results) {
                console.log(results);
                $('#pev_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "event_name"},
                        {"mData": "event_date"},
                        {"mData": "event_start"},
                        {"mData": "event_end"},
                        {"mData": "event_loc"},
                        {"mData": "event_id", "mRender": function (data) {
                                return '<a class="aprv" id="' + data + '">Approve Event</a>';
                            }},
                        {"mData": "event_id", "mRender": function (data) {
                                return '<a class="rjct">Reject Event</a>';
                            }}
                    ]
                });
            }
        });

        /*
         * Calling approve event function
         */
        $('#pev_tab').on('click', 'a.aprv', function () {
            var tab = $('#pev_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "DO YOU WANT TO APPROVE THIS EVENT?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Approve it!", cancelButtonText: "No, Cancel!", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/approve_event/'; ?>" + data.event_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results == "true") {
                                        swal("Approved !", data.event_name + " has been approved successfully.", "success");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });
        
        /*
         * Calling reject event function 
         */
        $('#pev_tab').on('click', 'a.rjct', function () {
            var tab = $('#pev_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "DO YOU WANT TO REJECT THIS EVENT?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Reject it!", cancelButtonText: "No, Cancel!", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/reject_event/'; ?>" + data.event_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results == "true") {
                                        swal("Rejected !", data.event_name + " has been rejected successfully.", "success");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });
    });
</script>