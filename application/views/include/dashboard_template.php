<?php $this->load->view('include/header'); ?>

<?php $this->load->view('include/dashboard_header'); ?>

<?php $this->load->view('include/dashboard_links'); ?>

<?php $this->load->view($main_content); ?>

<?php $this->load->view('include/dashboard_footer'); ?>

<?php $this->load->view('include/dashboard_control_sidebar'); ?>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>/dashboard_notification",
            success: function (results) {
                //console.log(results);
                var end_pos = results.indexOf("+");
                var notify_str = results.substring(0, end_pos);
                var msg_arr = results.substring(results.indexOf("+") + 1);
                console.log(notify_str);
                console.log(msg_arr);
                if (notify_str != '"0"') {
                    var dt = JSON.parse(notify_str);
                    console.log(dt);
                    $('#noti_cnt').html(dt.length);
                    $('#noti_title').html('You have ' + dt.length + ' notifications');
                    $('#notify_links').empty();
                    for (var i = 0; i < dt.length; i++) {
                        $('#notify_links').append('<li id=' + dt[i].notify_id + '><a href="#"><div class="pull-left"><i class="fa fa-dot-circle-o text-aqua"></i></div><h4>' + dt[i].notification_msg + '</h4></a></li>')
                    }
                } else {
                    $('#noti_cnt').html('0');
                    $('#noti_title').html('You don\'t have new notifications');
                }

                if (msg_arr != '"0"') {
                    var dt = JSON.parse(msg_arr);
                    $('#msg_cnt').html(dt.length);
                    $('#msg_tot').html('You have ' + dt.length + ' messages');
                    $('#msg_link').empty();
                    for (var j = 0; j < dt.length; j++) {
                        $('#msg_link').append('<li id=' + dt[j].msg_id + '><a href="#"><div class="pull-left"><i class="fa fa-envelope" aria-hidden="true"></i></div><h4>' + dt[j].msg_title + '</h4><p>' + dt[j].msg_date + '</p></a></li>');
                    }
                } else {
                    $('#msg_cnt').html('0');
                    $('#msg_tot').html('You don\'t have new messages');
                }

            }
        });

        $('#notify_links').on('click', 'li', function () {
            console.log($(this).attr('id'));
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/dashboard_notification/mark_as_view/" + $(this).attr('id'),
                success: function (results) {
                    console.log(results);
                    if (results != '"0"') {
                        var dt = JSON.parse(results);
                        console.log(dt);
                        $('#noti_cnt').html(dt.length);
                        $('#noti_title').html('You have ' + dt.length + ' notifications');
                        $('#notify_links').empty();
                        for (var i = 0; i < dt.length; i++) {
                            $('#notify_links').append('<li id=' + dt[i].notify_id + '><a href="#"><i class="fa fa-dot-circle-o text-aqua"></i>' + dt[i].notification_msg + '</a></li>')
                        }
                    } else {
                        $('#notify_links').empty();
                        $('#noti_cnt').html('0');
                        $('#noti_title').html('You don\'t have new notifications');
                    }

                }
            });

        });

        $('#msg_link').on('click', 'li', function () {
            console.log($(this).attr('id'));
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/dashboard_notification/set_as_mark/" + $(this).attr('id'),
                success: function (results) {
                    console.log(results);
                    if (results != '"0"') {
                        var dt = JSON.parse(results);
                        console.log(dt);
                        $('#msg_cnt').html(dt.length);
                        $('#msg_tot').html('You have ' + dt.length + ' messages');
                        $('#msg_link').empty();
                        for (var j = 0; j < dt.length; j++) {
                            $('#msg_link').append('<li id=' + dt[j].msg_id + '><a href="#"><div class="pull-left"><i class="fa fa-envelope" aria-hidden="true"></i></div><h4>' + dt[j].msg_title + '</h4><p>' + dt[j].msg_date + '</p></a></li>');
                        }
                    } else {
                        $('#msg_link').empty();
                        $('#msg_cnt').html('0');
                        $('#msg_tot').html('You don\'t have new messages');
                    }

                }
            });

        });
    });
</script>