<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>DGFG</title>
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Add JQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

        <!-- Bootstrap-->
        <link href="<?php echo base_url() ?>/bootstrap/css/bootstrap.css" rel="stylesheet">
        <script src="<?php echo base_url() ?>bootstrap/js/bootstrap.js"></script>

        <!--jquery datatable-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
        <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>

        <!-- Local style -->
        <link rel="stylesheet"  href="<?php echo base_url() ?>public/css/dgfg-styles.css">
        <link rel="stylesheet"  href="<?php echo base_url() ?>public/css/skin-yellow.css">
        <link rel="stylesheet"  href="<?php echo base_url() ?>public/css/AdminLTE.css">
        <link rel="stylesheet"  href="<?php echo base_url() ?>public/css/sweetalert.css">
        <link rel="stylesheet"  href="<?php echo base_url() ?>public/fullcalendar/fullcalendar.css">

        <!-- Local JS -->
        <script src="<?php echo base_url() ?>public/js/dgfg-js.js"></script>
        <script src="<?php echo base_url() ?>public/js/app.js"></script>
        <script src="<?php echo base_url() ?>public/js/sweetalert.min.js"></script>
        <script src="<?php echo base_url() ?>public/fullcalendar/lib/moment.min.js"></script>
        <script src="<?php echo base_url() ?>public/fullcalendar/fullcalendar.js"></script>
        <script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
        <script src="<?php echo base_url() ?>public/js/plugin-ckeditor.js"></script>
        <script src="<?php echo base_url() ?>public/js/masonry-js.js"></script>
        <!-- File upload JS -->
<!--        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>-->

        <!-- Font awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
</html>

