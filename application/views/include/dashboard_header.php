<div class="skin-yellow sidebar-mini">
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
<!--                <span class="logo-mini"><img src="<?php echo base_url('public/images/logo-small.png')?>"></span>-->
                <img src="<?php echo base_url('public/images/logo-small.png')?>" style="width: 42px;">
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"></span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <!--email link deactivate-->
                        <li class="dropdown messages-menu">
<!--                             Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success" id="msg_cnt"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header" id="msg_tot"></li>
                                <li>
<!--                                     inner menu: contains the messages -->
                                    <ul class="menu" id="msg_link">
                                        
                                    </ul> <!--menu -->
                                </li>
                                <li class="footer"><a href="<?php echo site_url('view_messages')?>">See All Messages</a></li>
                            </ul>
                        </li> <!--messages-menu -->

                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning" id="noti_cnt"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header" id="noti_title"></li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu" id="notify_links">
<!--                                        <li> start notification 
                                            <a href="#">
                                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                            </a>
                                        </li> end notification -->
                                    </ul>
                                </li>
<!--                                <li class="footer"><a href="#">View all</a></li>-->
                            </ul>
                        </li>
                        <!-- Tasks Menu -->
<!--                        <li class="dropdown tasks-menu">
                             Menu Toggle Button 
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                                <li>
                                     Inner menu: contains the tasks 
                                    <ul class="menu">
                                        <li> Task item 
                                            <a href="#">
                                                 Task title and progress text 
                                                <h3>
                                                    Design some buttons
                                                    <small class="pull-right">20%</small>
                                                </h3>
                                                 The progress bar 
                                                <div class="progress xs">
                                                     Change the css width attribute to simulate progress 
                                                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">20% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li> end task item 
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li>-->
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="<?php echo $this->session->userdata('user_img') === "" ? base_url().'public/uploads/profile_imgs/default-user.jpg' : $this->session->userdata('user_img') ?>" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><?php echo $this->session->userdata('real_name') ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="<?php echo $this->session->userdata('user_img') === "" ? base_url().'public/uploads/profile_imgs/default-user.jpg' : $this->session->userdata('user_img') ?>" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo $this->session->userdata('real_name') ?> - <?php
                                        switch ($this->session->userdata('user_role')) {
                                            case '1':
                                                echo 'Super Admin';
                                                break;
                                            case '2':
                                                echo 'Co-odinator';
                                                break;
                                            case '3':
                                                echo 'SKAWD Leader';
                                                break;
                                            case '4':
                                                echo 'SKWAD Member';
                                                break;
                                        }
                                        ?>
<!--                                        <small>Member since Nov. 2012</small>-->
                                    </p>
                                </li>
                                <!-- Menu Body -->
<!--                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <!--                                        <a href="#" class="btn btn-default btn-flat" id="logout">Sign out</a>-->
                                        <button type="button" class="btn btn-default btn-flat" onclick="window.location = '<?php echo site_url("logout") ?>'">Sign out</button>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

