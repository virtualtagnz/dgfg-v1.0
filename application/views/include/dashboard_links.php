<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $this->session->userdata('user_img') === "" ? base_url().'public/uploads/profile_imgs/default-user.jpg' : $this->session->userdata('user_img') ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('real_name') ?></p>
                <!-- Status -->
<!--                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            <?php if ($this->session->userdata('user_role') === '1' || $this->session->userdata('user_role') === '2') { ?>
<!--                <li class="header"><?php //echo $this->session->userdata('user_role') === '1' ? 'Super Admin Dashboard' : 'Co-ordinator Dashboard'  ?></li>-->
                <!-- Optionally, you can add icons to the links -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-user-plus"></i> <span>Create Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('create_user/1') ?>">Youth Coordinator</a></li>
                        <li><a href="<?php echo site_url('create_user/2') ?>">SKWAD Leader</a></li>
                        <li><a href="<?php echo site_url('create_user/3') ?>">SKWAD Member</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-user"></i> <span>View Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('view_user/1') ?>">Youth Coordinator</a></li>
                        <li><a href="<?php echo site_url('view_user/2') ?>">SKWAD Leader</a></li>
                        <li><a href="<?php echo site_url('view_user/3') ?>">SKWAD Member</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-calendar"></i> <span>Event</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('create_event') ?>">Create New Event</a></li>
                        <li><a href="<?php echo site_url('view_events') ?>">View / Edit Events</a></li>
                        <li><a href="<?php echo site_url('view_pending_event') ?>">Approve Pending Events</a></li>
                        <li><a href="<?php echo site_url('active_events') ?>">View Active Events</a></li>
                        <li><a href="<?php echo site_url('completed_events') ?>">View Completed Events</a></li>
                        <li><a href="<?php echo site_url('view_events/control_image_gallery') ?>">View / Approve Event Gallery</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-info-circle"></i> <span>Service Directory</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('create_service') ?>">Create New Service Directory</a></li>
                        <li><a href="<?php echo site_url('view_services') ?>">View / Edit Services</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-list"></i> <span>Wellness Survey</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('survey/load_all_surveys/3') ?>">SKAWD Leaders Surveys</a></li>
                        <li><a href="<?php echo site_url('survey/load_all_surveys/4') ?>">SKAWD Members Surveys</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-comments"></i> <span>Messages</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('create_message') ?>">Create Message</a></li>
                        <li><a href="<?php echo site_url('view_messages')?>">View Message</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-list-alt"></i> <span>Daily Survey</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('create_daily_survey') ?>">Create Survey</a></li>
                        <li><a href="<?php echo site_url('view_daily_survey') ?>">View/ Edit Survey</a></li>
                        <li><a href="<?php echo site_url('View_submitted_daily_surveys') ?>">View Completed Surveys</a></li>
                    </ul>
                </li>
    <!--                    <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
                <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="#">Link in level 2</a></li>
                        <li><a href="#">Link in level 2</a></li>
                    </ul>
                </li>-->
            <?php } elseif ($this->session->userdata('user_role') === '3') { ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-calendar"></i> <span>Event</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('create_event') ?>">Create New Event</a></li>
                        <li><a href="<?php echo site_url('view_events') ?>">View / Edit Event</a></li>
                        <li><a href="<?php echo site_url('available_events') ?>">Available Events</a></li>
                        <li><a href="<?php echo site_url('view_user/3') ?>">Available Members</a></li>
                        <li><a href="<?php echo site_url('available_members') ?>">Assign Events</a></li>
                        <li><a href="<?php echo site_url('assign_task') ?>">Assign Task</a></li>
                        <li><a href="<?php echo site_url('load_tasks') ?>">My Tasks</a></li>
                        <li><a href="<?php echo site_url('active_events') ?>">My Active Events</a></li>
                        <li><a href="<?php echo site_url('completed_events') ?>">My Completed Events</a></li>
                        <li><a href="<?php echo site_url('view_events/control_image_gallery') ?>">View/ Edit/ Approve Event Gallery</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo site_url('survey/display_survey')?>"><i class="fa fa-list"></i><span>My Wellness Survey</span></a></li>
                <li><a href="<?php echo site_url('load_service')?>"><i class="fa fa-sitemap"></i><span>My Services</span></a></li>
                <li><a href="<?php echo site_url('view_messages')?>"><i class="fa fa-comments"></i><span>My Messages</span></a></li>
            <?php } elseif ($this->session->userdata('user_role') === '4') {?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-calendar"></i> <span>Event</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('available_events') ?>">Available Events</a></li>
                        <li><a href="<?php echo site_url('invited_events') ?>">Invited Events</a></li>
                        <li><a href="<?php echo site_url('load_tasks') ?>">My Tasks</a></li>
                        <li><a href="<?php echo site_url('view_events/control_image_gallery') ?>">View/ Edit Event Gallery</a></li>
                    </ul>
                </li>
<!--                <li><a href="<?php echo site_url('available_events') ?>"><i class="fa fa-calendar"></i><span>Available Events</span></a></li>-->
                <li><a href="<?php echo site_url('survey/display_survey')?>"><i class="fa fa-list"></i><span>My Wellness Survey</span></a></li>
                <li><a href="<?php echo site_url('load_service')?>"><i class="fa fa-sitemap"></i><span>My Services</span></a></li> 
                <li><a href="<?php echo site_url('view_messages')?>"><i class="fa fa-comments"></i><span>My Messages</span></a></li>
            <?php } ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-pencil"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('blog/blog_timeline_view/1') ?>">View Posts</a></li>
                        <li><a href="<?php echo site_url('blog/blog_add_post') ?>">Write a Post</a></li>
                        <?php if ($this->session->userdata('user_role') === '2') { ?>
                        <li><a href="<?php echo site_url('blog/blog_approve_post_view') ?>">Approve Posts</a></li>
                        <?php } ?>
                    </ul>
                </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
