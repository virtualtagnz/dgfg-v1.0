<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            EVENT GALLERY CONTROL VIEW
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="ev_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Event Date</th>
                            <th>Event Location</th>
                            <th>Event Status</th>
                            <th>Event Id</th>
                            <th>View Event</th>
                            <!--th>Number of Images Exist</th-->
                            <th>Upload Images</th>
                        </tr>
                    </thead>
                </table>
                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/view_events/load_gallery_control'; ?>",
            success: function (results) {
                console.log(results);
                $('#ev_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "event_name"},
                        {"mData": "event_date"},
                        {"mData": "event_loc"},
                        {"mData": "ev_stat"},
                        {"mData": "event_id", "visible": false},
                        {"mData": "", "mRender": function (data) {
                                return '<a class="vtevt">View Full Event</a>';
                            }},
                        //{"mData": "event_image_count"},
                        {"mData": "event_id", "mRender": function (data) {
                                return '<a href=<?php echo site_url() ?>/edit_event/upload_image_view/' + data + '>Upload Images</a>';
                            }}
                    ]
                });
            }
        });

        $('#ev_tab').on('click', 'a.vtevt', function () {
            var tab = $('#ev_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_event/'; ?>" + data.event_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#myModal').modal('show');
                }
            });
        });
    });
</script>
