<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CREATE DAILY SURVEY
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="dsurvey_frm" id="dsurvey_frm" method="post" action="<?php echo site_url('create_daily_survey/add_daily_survey'); ?>">
                    <div class="form-group">
                        <label for="dsurvey_name" class="col-sm-3 control-label">Survey Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="dsurvey_name" id="dsurvey_name" value="<?php echo set_value('dsurvey_name'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_name'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dsurvey_date" class="col-sm-3 control-label">Date of survey</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="date" name="dsurvey_date" id="dsurvey_date" value="<?php echo set_value('dsurvey_date'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_date'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dsurvey_pnts" class="col-sm-3 control-label">Points for survey</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="number" name="dsurvey_pnts" id="dsurvey_pnts" min="1" value="<?php echo set_value('dsurvey_pnts'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_pnts'); ?></div>
                        </div>
                    </div>

                    <div class="col-sm-3"></div>
                    <legend class="col-sm-9"><h4>Questions & Answers List <small>(Maximum number of questions - 5)</small></h4></legend>
                    <div class="form-group">
                        <label for="dsurvey_q" class="col-sm-3 control-label">Question</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="dsurvey_q" id="dsurvey_q" value="<?php echo set_value('dsurvey_q'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_q'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dsurvey_a1" class="col-sm-3 control-label">Answer 1</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="dsurvey_a1" id="dsurvey_a1" value="<?php echo set_value('dsurvey_a1'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_a1'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dsurvey_a2" class="col-sm-3 control-label">Answer 2</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="dsurvey_a2" id="dsurvey_a2" value="<?php echo set_value('dsurvey_a2'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_a2'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dsurvey_a3" class="col-sm-3 control-label">Answer 3</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="dsurvey_a3" id="dsurvey_a3" value="<?php echo set_value('dsurvey_a3'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_a3'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dsurvey_a4" class="col-sm-3 control-label">Answer 4</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="dsurvey_a4" id="dsurvey_a4" value="<?php echo set_value('dsurvey_a4'); ?>">
                            <div id="infoMessage"><?php echo form_error('dsurvey_a4'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9" >
                            <button type="button" id="dsurvey_remove" class="btn btn-primary pull-right">Remove Question</button>
                            <button type="button" id="dsurvey_set" class="btn btn-primary pull-right btn-space">Add Question</button>
                            <span class="pull-right" style="padding-top: 7px;"><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="left" title="You can add multiple task to this section by clicking on the 'Create Task' button.If you want remove tasks please select on the table row and delete by clicking the 'Remove Task' button"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <table id="dsurvey_tab" name="dsurvey_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th>Answer 1</th>
                                        <th>Answer 2</th>
                                        <th>Answer 3</th>
                                        <th>Answer 4</th>
                                    </tr>
                                </thead>
                            </table>
                            <div id="infoMessage"><?php echo form_error('dsurvey_tab'); ?></div>
                        </div>
                    </div>
                    <input type="hidden" name="json_str" id="json_str" value="">
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right">Create Survey</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Daily Survey Created!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        var counter = 0;
        /*
         * Create datatable
         */
        var tab = $('#dsurvey_tab').DataTable({
            "paging": false,
            "searching": false,
            "info": false
        });

        /*
         * Set data to a datatable
         */
        
        $('#dsurvey_set').click(function () {
            var col1 = $('#dsurvey_q').val();
            var col2 = $('#dsurvey_a1').val();
            var col3 = $('#dsurvey_a2').val();
            var col4 = $('#dsurvey_a3').val();
            var col5 = $('#dsurvey_a4').val();
            console.log(counter);
            if (counter < 5) {
                if (col1.length !== 0 && col2.length !== 0 && col3.length !== 0 && col4.length !== 0 && col5.length !== 0) {
                    tab.row.add([
                        col1,
                        col2,
                        col3,
                        col4,
                        col5,
                    ]).draw(true);
                    createJson();
                    clearTest();
                    counter++;
                } else {
                    swal({title: "Error!",
                        text: "You can't add empty values to table! <strong>Please fill all</strong> fields and then press 'Add Question' button",
                        type: "error",
                        html: true});
                }
            } else {
                swal({title: "Error!",
                    text: "<strong>Questions limit is 5 for daily servey</strong>",
                    type: "error",
                    html: true});
            }
        });

        /*
         * Clear all test boxes
         */
        var clearTest = function () {
            $('#dsurvey_q').val('');
            $('#dsurvey_a1').val('');
            $('#dsurvey_a2').val('');
            $('#dsurvey_a3').val('');
            $('#dsurvey_a4').val('');
            $('#dsurvey_a5').val('');
        };

        /*
         * Cretes JSON string
         */
        function createJson() {
            var rowData = tab.data();
            jsonObj = [];
            for (var i = 0; i < tab.data().length; i++) {
                item = {};
                item["col1"] = rowData[i][0];
                item["col2"] = rowData[i][1];
                item["col3"] = rowData[i][2];
                item["col4"] = rowData[i][3];
                item["col5"] = rowData[i][4];
                jsonObj.push(item);
            }
            var json_str = JSON.stringify(jsonObj);
            console.log(json_str);
            $('#json_str').val(json_str);
        }

        /*
         * Select row of datatable
         */
        $('#dsurvey_tab tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                tab.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        /*
         * Delete selected row of datatable
         */
        $('#dsurvey_remove').click(function () {
            tab.row('.selected').remove().draw(false);
            createJson();
            counter--;
        });
    });
</script>

