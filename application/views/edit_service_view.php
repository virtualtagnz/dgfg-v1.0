<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            EDIT SERVICE DIRECTORY
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Page Content Here -->
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="service_frm" id="event_frm" method="post" action="<?php echo site_url('edit_service/modify_service/' . end($this->uri->segments)); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="sprovider" class="col-sm-3 control-label">Name of Provider</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="sprovider" id="sprovider" value="<?php echo $sp_name; ?>">
                            <div id="infoMessage"><?php echo form_error('sprovider'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="scategory" class="col-sm-3 control-label">Provider Category</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="scategory" id="scategory">
                                <option value="0"<?php echo set_select('scategory', "0", TRUE); ?>>-- Please Select --</option>
                                <?php foreach ($service_cat as $key => $value) { ?>
                                    <option value="<?php echo $value['wellness_sec_id']; ?>"<?= $sp_cat == '' . $value['wellness_sec_id'] . '' ? ' selected="selected"' : ''; ?>><?php echo $value['wellness_sec_name'] ?></option>
                                <?php } ?>
<!--                                <option value="1"<?= $sp_cat == '1' ? ' selected="selected"' : ''; ?>>Test Category 1</option>
                            <option value="2"<?= $sp_cat == '2' ? ' selected="selected"' : ''; ?>>Test Category 2</option>
                            <option value="3"<?= $sp_cat == '3' ? ' selected="selected"' : ''; ?>>Test Category 3</option>
                            <option value="4"<?= $sp_cat == '4' ? ' selected="selected"' : ''; ?>>Test Category 4</option>
                            <option value="5"<?= $sp_cat == '5' ? ' selected="selected"' : ''; ?>>Test Category 5</option>-->
                            </select>
                            <div id="infoMessage"><?php echo form_error('scategory'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ssummary" class="col-sm-3 control-label">Summary of Services</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="ssummary" id="ssummary" rows="4" cols="50"><?php echo $sp_summery; ?></textarea>
                            <div id="infoMessage"><?php echo form_error('ssummary'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="scost" class="col-sm-3 control-label">Cost of Service</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="scost" id="scost" value="<?php echo $sp_cost; ?>">
                            <div id="infoMessage"><?php echo form_error('scost'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="scontact" class="col-sm-3 control-label">Contact Number</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="tel" name="scontact" id="scontact" value="<?php echo $sp_phn; ?>">
                            <div id="infoMessage"><?php echo form_error('scontact'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="skey" class="col-sm-3 control-label">Provider Key Contact Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="skey" id="skey" value="<?php echo $sp_key; ?>">
                            <div id="infoMessage"><?php echo form_error('skey'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="slist" class="col-sm-3 control-label">List Of Services Offered</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="slist" id="slist" rows="4" cols="50"><?php echo $sp_list; ?></textarea>
                            <div id="infoMessage"><?php echo form_error('slist'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="saddress" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="saddress" id="saddress" onFocus="initAutocomplete()" placeholder="" value="<?php echo $sp_add; ?>">
                            <div id="infoMessage"><?php echo form_error('saddress'); ?></div>
                        </div>
                    </div>

                    <?php if (!empty($sp_add)) { ?>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <div class="col-sm-12" id="map" style="height: 300px;"></div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label for="shours" class="col-sm-3 control-label">Opening hours</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="shours" id="shours" value="<?php echo $sp_hrs; ?>">
                            <div id="infoMessage"><?php echo form_error('shours'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="safternum" class="col-sm-3 control-label">After Hours Number</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="tel" name="safternum" id="safternum" value="<?php echo $sp_after_phn; ?>">
                            <div id="infoMessage"><?php echo form_error('safternum'); ?></div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Contact Email</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="email" id="email" value="<?php echo $sp_email; ?>">
                            <div id="infoMessage"><?php echo form_error('email'); ?></div>
                        </div>
                    </div>

                    <?php if (!empty($sp_img)) { ?>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <img class="img-responsive" src="<?php echo base_url() . 'public/uploads/' . $sp_name . '/' . $sp_img ?>"/>
                            </div>
                        </div>
                    <?php }; ?>

                    <div class="form-group">
                        <label for="image" class="col-sm-3 control-label">Photo of Provider</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="image" id="image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 120*120px only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <?php if (!empty($sp_staff_img)) { ?>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <img class="img-responsive" src="<?php echo base_url() . 'public/uploads/' . $sp_name . '/' . $sp_staff_img ?>"/>
                            </div>
                        </div>
                    <?php }; ?>

                    <div class="form-group">
                        <label for="staff_image" class="col-sm-3 control-label">Photo of staff</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="staff_image" id="staff_image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 120*120px only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sweb" class="col-sm-3 control-label">Provider Website</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="url" name="sweb" id="sweb" value="<?php echo $sp_web; ?>">
                            <div id="infoMessage"><?php echo form_error('sweb'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="slevel" class="col-sm-3 control-label">Level</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="slevel" id="slevel">
                                <option value="0"<?php echo set_select('scategory', "0", TRUE); ?>>-- Please Select --</option>
                                <option value="1"<?= $sp_level == '1' ? ' selected="selected"' : ''; ?>>High</option>
                                <option value="2"<?= $sp_level == '2' ? ' selected="selected"' : ''; ?>>Medium</option>
                                <option value="3"<?= $sp_level == '3' ? ' selected="selected"' : ''; ?>>Low</option>
                            </select>
                            <div id="infoMessage"><?php echo form_error('slevel'); ?></div>
                        </div>
                    </div>

                    <input type="hidden" name="old_img" id="old_img" value="<?php echo $sp_img ?>">
                    <input type="hidden" name="old_staff_img" id="old_staff_img" value="<?php echo $sp_staff_img ?>">

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="sbtn_save" class="btn btn-primary pull-right">Edit Service Directory</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Service edit successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>

<script>
// This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete;

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('saddress')),
                {types: ['geocode']});
    }

    function initMap() {
        var address = <?php echo "'$sp_add'" ?>;
        GetLatLong(address);
    }

    function GetLatLong(address) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var latlng = results[0].geometry.location;
                var mapOptions = {
                    zoom: 15,
                    center: latlng
                };

                var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                var marker = new google.maps.Marker({
                    title: <?php echo "'$sp_add'" ?>,
                    map: map,
                    position: results[0].geometry.location
                });

            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU5pwsCrj8K5Sf7dvZ1fmq2VRfG4CMg64&libraries=places&callback=initMap"
async defer></script>


