<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            AVAILABLE EVENTS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="avi_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Event Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Event Location</th>
                            <th>Event Id</th>
                            <th>Get Event</th>
                            <th>View Event</th>
                        </tr>
                    </thead>
                </table>
                
                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        var log_role=<?php echo $this->session->userdata('user_role')?>;
        console.log(log_role);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/available_events/available_events_view'; ?>",
            success: function (results) {
                console.log(results);
                $('#avi_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "event_name"},
                        {"mData": "event_date"},
                        {"mData": "event_start"},
                        {"mData": "event_end"},
                        {"mData": "event_loc"},
                        {"mData": "event_id", "visible": false},
                        {"mData": "", "mRender": function () {
                                if(log_role===3){
                                    return '<a class="gtevt">Get This Event</a>';
                                }else if(log_role===4){
                                    return '<a class="stevt">Send Interest</a>';
                                }
                                
                            }},
                        {"mData": "", "mRender": function (data) {
                                return '<a class="vtevt">View Full Event Info</a>';
                            }}
                    ]
                });
            }
        });
        $('#avi_tab').on('click', 'a.gtevt', function () {
            var tab = $('#avi_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "DO YOU WANT TO SELECT THIS EVENT?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Select it!", cancelButtonText: "No, Cancel!", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/select_event/'; ?>" + data.event_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results == "true") {
                                        swal("Selected!", data.event_name + " has been selected successfully.", "success");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });
        
        $('#avi_tab').on('click', 'a.vtevt', function () {
            var tab = $('#avi_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_event/'; ?>" + data.event_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#myModal').modal('show');
                }
            });
        });
        
        $('#avi_tab').on('click', 'a.stevt', function () {
            var tab = $('#avi_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            swal({title: "ARE YOU INTERESTED IN THIS EVENT?", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Select it!", cancelButtonText: "No, Cancel!", closeOnConfirm: false, closeOnCancel: false},
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url() . '/edit_event/send_member_interest/'; ?>" + data.event_id,
                                success: function (results) {
                                    console.log(results);
                                    if (results == "TR") {
                                        swal("Done !", "sent interest successfully for"+ data.event_name + " event", "success");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else if(results == "AI") {
                                        swal("Error!!!", "Sorry, you already selected this event as, interested in", "error");
                                    }else{
                                        swal("Error!!!", "Something is going wrong, Please try again", "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
        });
    });
</script>
