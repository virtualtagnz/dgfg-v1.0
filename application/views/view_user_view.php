<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title ?>
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="cod-tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Profile Image</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>id</th>
                            <th>View Profile</th>
                            <?php if ($this->session->userdata('user_role') === '2') { ?>
                                <th>Edit Profile</th>
                            <?php } elseif ($this->session->userdata('user_role') === '3') { ?>
                                <th>Send Invitation</th>
                            <?php } ?>
                        </tr>
                    </thead>
                </table>
                <div id="umod"></div>
            </div>
            <?php if ($this->session->flashdata('success_msg')) { ?>
                <script>
                    swal("Good job!", "Invitation Sent Successfully!", "success")
                </script>
            <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                <script>
                    swal("Error!", "Something is going wrong!", "error")
                </script>
            <?php } elseif ($this->session->flashdata('validation_err')) { ?>
                <script>
                    swal("Error!", "Please select event to send invitation!", "error")
                </script>
            <?php } elseif ($this->session->flashdata('already_invite')) { ?>
                <script>
                    swal("Error!", "Already sent a invitation for this member to this project", "error")
                </script>
            <?php } elseif ($this->session->flashdata('already_interest')) { ?>
                <script>
                    swal("Error!", "Selected user already interested of this project, No need to send invitation", "error")
                </script>
            <?php } ?>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {
        var u_role = <?php echo $this->session->userdata('user_role') ?>;
        var call = <?php echo end($this->uri->segments) ?>;
        var url;
        switch (call) {
            case 1:
                url = "<?php echo site_url('view_user/view_coordinator/1'); ?>";
                break;
            case 2:
                url = "<?php echo site_url('view_user/view_leader/2'); ?>";
                break;
            case 3:
                url = "<?php echo site_url('view_user/view_member/3'); ?>";
                break;
        }
        $.ajax({
            type: "POST",
            url: url,
            success: function (results) {
                console.log(results);
                $('#cod-tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "img", "mRender": function (data) {
                                if (data !== "") {
                                    return '<img class="img-circle img-responsive" src="'+data+'"></>';
                                } else {
                                    return '<img class="img-circle img-responsive" src="<?php echo base_url() ?>public/uploads/default-user.jpg"></>';
                                }
                            }},
                        {"mData": "u_name"},
                        {"mData": "act_stat"},
                        {"mData": "uid", "visible": false},
                        {"mData": "uid", "mRender": function (data) {
                                return '<a class="vsu">View Profile</a>';
                                //return '<a href=<?php echo site_url() ?>/view_single/' + data +'/'+ call+'>View Profile</a>';
                            }},
                        {"mData": "uid", "mRender": function (data) {
                                if (u_role == 2) {
                                    var enc_p1 = encodeURIComponent('rec=' + data + '/tap=' + call);
                                    return '<a href=<?php echo site_url() ?>/user_edit/' + data + '/' + call + '>Edit Profile</a>';
                                } else if (u_role == 3) {
                                    return '<a class="invite">Send Invitaion</a>';
                                }
                            }}
                    ]
                });
            }
        });

        $('#cod-tab').on('click', 'a.vsu', function () {
            var tab = $('#cod-tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/view_single/" + data.uid + '/' + call,
                success: function (results) {
                    console.log(results);
                    $('#umod').html(results);
                    $('#user_modal').modal('show');
                }
            });
        });

        $('#cod-tab').on('click', 'a.invite', function () {
            var tab = $('#cod-tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/send_invitation/" + data.uid,
                success: function (results) {
                    console.log(results);
                    $('#umod').html(results);
                    $('#invite_modal').modal('show');
                }
            });
        });

        $('#ev_name').bind('input', function () {
            var x = $(this).val();
            var z = $('#listone');
            var val = $(z).find('option[value="' + x + '"]');
            var endval = val.attr('id');
            $('#event_id').val(endval);

        });


    });
</script>

