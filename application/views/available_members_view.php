<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ASSIGN EVENTS TO MEMBERS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-10">
                <form class="form-horizontal" name="task_frm" id="event_frm" method="post" action="<?php echo site_url('available_members/set_events_to_members'); ?>">
                    <div class="form-group">
                        <label for="ev_name" class="col-sm-3 control-label">Event Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" list="listone" name="ev_name" id="ev_name" autocomplete="off">
                            <datalist id="listone">
                                <?php if (count($ev_info) > 0) { ?>
                                    <?php for ($i = 0; $i < count($ev_info); $i++) { ?>
                                        <option id="<?php echo $ev_info[$i]['event_id'] ?>" value="<?php echo $ev_info[$i]['event_name'] ?>"></option>
                                    <?php } ?>;
                                <?php } else { ?>
                                    <option id="0" value="No available events!"></option>
                                <?php } ?>;
                            </datalist>
                            <div id="infoMessage"><?php echo form_error('ev_name'); ?></div>
                        </div>
                        <input type="hidden" name="event_id" id="event_id">
                    </div>
                    <div class="form-group">
                        <label for="av_mem" class="col-sm-3 control-label">Member Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" list="listtwo" name="av_mem" id="av_mem" autocomplete="off">
                            <datalist id="listtwo">
                                <!--                                <option id="" value=""></option>
                                                                <option id="0" value="Sorry no available members!"></option>-->
                            </datalist>
                            <div id="infoMessage"><?php echo form_error('av_mem'); ?></div>
                        </div>
                        <input type="hidden" name="mem_id" id="mem_id">
                        <div id="infoMessage"><?php echo form_error('mem_id'); ?></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right">Assign Event</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Good job!", "Event Assign Successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        $('#ev_name').bind('input', function () {
            var x = $(this).val();
            var z = $('#listone');
            var val = $(z).find('option[value="' + x + '"]');
            var endval = val.attr('id');
            $('#event_id').val(endval);

        });

        $('#av_mem').bind('input', function () {
            var x = $(this).val();
            var z = $('#listtwo');
            var val = $(z).find('option[value="' + x + '"]');
            var endval = val.attr('id');
            $('#mem_id').val(endval);
        });

        $('#ev_name').bind('input', function () {
            var eid = $('#event_id').val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/available_members/load_members/'; ?>" + eid,
                success: function (results) {
                    console.log(results);
                    if(results!=""){
                        var dt = JSON.parse(results);
                        $('#listtwo').empty();
                        for (var i = 0; i < dt.length; i++) {
                            $('#listtwo').append($("<option id='" + dt[i].uid + "' value='" + dt[i].u_name + "'>"));
                        }
                    }else{
                        $('#listtwo').empty();
                        $('#listtwo').append($("<option>No Members to display"));
                    }


                }
            });
        });

    });
</script>
