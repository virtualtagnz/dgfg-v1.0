<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            VIEW / EDIT DAILY SURVEYS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="ds_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Survey Id</th>
                            <th>Daily Survey Name</th>
                            <th>Daily Survey Date</th>
                            <th>View Survey</th>
                            <th>Edit Survey</th>
                        </tr>
                    </thead>
                </table>
                <div id="mod"></div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() . '/view_daily_survey/load_all_surveys'; ?>",
            success: function (results) {
                console.log(results);
                $('#ds_tab').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                        {"mData": "daily_survey_id", "visible": false},
                        {"mData": "daily_survey_name"},
                        {"mData": "daily_survey_date"},
                        {"mData": "daily_survey_id", "mRender": function (data) {
                                return '<a class="view_ds">View Survey</a>';
                            }},
                        {"mData": "daily_survey_id", "mRender": function (data) {
                                return '<a href=<?php echo site_url() ?>/edit_daily_surveys/' + data + '>Edit Survey</a>';
                            }}
                    ]
                });
            }
        });
        
        $('#ds_tab').on('click', 'a.view_ds', function () {
            var tab = $('#ds_tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_daily_survey/'; ?>" + data.daily_survey_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#daily_survey_modal').modal('show');
                }
            });
        });
    });
</script>