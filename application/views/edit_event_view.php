<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            EDIT EVENT
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Page Content -->
        <div class="container-fluid" id="event">
            <div class="page-title text-center" id="title"><h3></h3></div>
            <div class="col-md-10">
                <form class="form-horizontal" name="event_frm" id="event_frm" method="post" action="<?php echo site_url('edit_event/modify_event/' . end($this->uri->segments)); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="evname" class="col-sm-3 control-label">Event name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evname" id="evname" readonly="true" value="<?php echo $event_name; ?>">
                            <input type="hidden" name="hide_evname" id="hide_evname" value="<?php echo $event_name; ?>">
                            <div id="infoMessage"><?php echo form_error('evname'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evdate" class="col-sm-3 control-label">Event date</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="date" name="evdate" id="evdate" value="<?php echo $event_date; ?>">
                            <div id="infoMessage"><?php echo form_error('evdate'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evstime" class="col-sm-3 control-label">Event start time</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="time" name="evstime" id="evstime" value="<?php echo $event_start; ?>">
                            <div id="infoMessage"><?php echo form_error('evstime'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evetime" class="col-sm-3 control-label">Event end time</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="time" name="evetime" id="evetime" value="<?php echo $event_end; ?>">
                            <div id="infoMessage"><?php echo form_error('evetime'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evloc" class="col-sm-3 control-label">Event location</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evloc" id="evloc" value="<?php echo $event_loc; ?>">
                            <div id="infoMessage"><?php echo form_error('evloc'); ?></div>
                        </div>
                    </div>

                    <div class="col-sm-3"></div>
                    <legend class="col-sm-9">Event promotion</legend>
                    <div class="form-group">
                        <label for="evpromo1" class="col-sm-3 control-label">Promotion link 1</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evpromo1" id="evpromo1" value="<?php echo $event_promo1; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evpromo2" class="col-sm-3 control-label">Promotion link 2</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evpromo2" id="evpromo2" value="<?php echo $event_promo2; ?>">
                            <div id="infoMessage"><?php echo form_error('evpromo2'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evpromo3" class="col-sm-3 control-label">Promotion link 3</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evpromo3" id="evpromo3" value="<?php echo $event_promo3; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evpromo4" class="col-sm-3 control-label">Promotion link 4</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evpromo4" id="evpromo4" value="<?php echo $event_promo4; ?>">
                            <div id="infoMessage"><?php echo form_error('evpromo4'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evplan" class="col-sm-3 control-label">Event Plan</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="url" name="evplan" id="evplan" value="<?php echo $event_plan; ?>">
                            <div id="infoMessage"><?php echo form_error('evplan'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evcomments" class="col-sm-3 control-label">Comments</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="evcomments" id="evcomments" rows="4" cols="50"><?php echo $event_comments; ?></textarea>
                        </div>
                    </div>

                    <?php if (!empty($event_video_linq)) { ?>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <video width="320" height="240" controls>
                                    <source src="<?php echo base_url() . 'public/uploads/' . $event_name . '/' . $event_video_linq ?>" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    <?php }; ?>
                    <div class="form-group">
                        <label for="evid" class="col-sm-3 control-label">Event video</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="evid" id="evid" value="<?php echo set_value('evid'); ?>">
                            <label>Accept Formats : mp4 only   </label><br>
                            <div id="infoMessage"><?php echo form_error('evid'); ?></div>
                        </div>
                    </div>

                    <?php if (!empty($event_img_linq)) { ?>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <img class="img-responsive" src="<?php echo base_url() . 'public/uploads/' . $event_name . '/' . $event_img_linq ?>"/>
                            </div>
                        </div>
                    <?php }; ?>

                    <div class="form-group">
                        <label for="image" class="col-sm-3 control-label">Event photo</label>
                        <div class="col-sm-9">
                            <input class="form-control file" type="file" name="image" id="image" value="<?php echo set_value('image'); ?>">
                            <label>Accept Formats : gif|jpg|png only</label><br>
                            <label>Accept Dimensions : 851*315 only</label>
                            <div id="infoMessage"><?php echo form_error('image'); ?></div>
                        </div>
                    </div>

                    <div class="col-sm-3"></div>
                    <legend class="col-sm-9">Task List</legend>
                    <div class="form-group">
                        <label for="evtask" class="col-sm-3 control-label">Task name</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="evtask" id="evtask" value="<?php echo set_value('evtask'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="evduedate" class="col-sm-3 control-label">Due date</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="date" name="evduedate" id="evduedate" value="<?php echo set_value('evdate'); ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evpoints" class="col-sm-3 control-label">Points</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="number" name="evpoints" id="evpoints" min="1" value="<?php echo set_value('evpoints'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9" >
                            <button type="button" id="btnrm_task" class="btn btn-primary pull-right">Remove Task</button>
                            <button type="button" id="btnset_task" class="btn btn-primary pull-right btn-space">Add More Task</button>
                            <span class="pull-right" style="padding-top: 7px;"><i class="fa fa-question-circle fa-fw" data-toggle="tooltip" data-placement="left" title="You can add multiple task to this section by clicking on the 'Create Task' button.If you want remove tasks please select on the table row and delete by clicking the 'Remove Task' button"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <table id="ev_task" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Task Name</th>
                                        <th>Due Date</th>
                                        <th>Task Points</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <input type="hidden" name="json_str" id="json_str" value="">
                    <input type="hidden" name="old_vid" id="old_vid" value="<?php echo $event_video_linq ?>">
                    <input type="hidden" name="old_img" id="old_img" value="<?php echo $event_img_linq ?>">

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btnev" class="btn btn-primary pull-right">Edit Event</button>
                        </div>
                    </div>
                </form>
                <?php if ($this->session->flashdata('success_msg')) { ?>
                    <script>
                        swal("Done!", "Event edit successfully!", "success")
                    </script>
                <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                    <script>
                        swal("Error!", "Something is going wrong!", "error")
                    </script>
                <?php } ?>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {
        console.log(<?php echo $json_str ?>);
        /*
         * Create datatable
         */
        var tab = $('#ev_task').DataTable({
            "paging": false,
            "searching": false,
            "info": false,
            "destroy": true,
            "aaData": <?php echo $json_str ?>,
            "aoColumns": [
                {"mData": "task_des"},
                {"mData": "due_date"},
                {"mData": "task_point"}
            ]
        });
        /*
         * Select row of datatable
         */



        $('#ev_task tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                tab.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        function createJson() {
            var rowData = tab.data();
            console.log(rowData);
            jsonObj = [];
            for (var i = 0; i < tab.data().length; i++) {
                item = {};
                item["col1"] = rowData[i]['task_des'];
                item["col2"] = rowData[i]['due_date'];
                item["col3"] = rowData[i]['task_point'];
                jsonObj.push(item);
            }
            var json_str = JSON.stringify(jsonObj);
            console.log(json_str);
            $('#json_str').val('');
            $('#json_str').val(json_str);
        }

        /*
         * Set data to a datatable
         */
        $('#btnset_task').click(function () {
            var c1 = $('#evtask').val();
            var c2 = $('#evduedate').val();
            var c3 = $('#evpoints').val();
            if (c1.length !== 0 && c2.length !== 0 && c3.length !== 0) {
                tab.rows.add([{
                        "task_des": c1,
                        "due_date": c2,
                        "task_point":c3}

                ]).draw(true);
                createJson();
                clearTest();
            } else {
                swal({title: "Error!",
                    text: "You can't add empty values to table! Please fill <strong>Task Name, Due Date and Points</strong> fields and then create task",
                    type: "error",
                    html: true});
            }
        });
        /*
         * Clear all test boxes
         */
        var clearTest = function () {
            $('#evtask').val('');
            $('#evduedate').val('');
            $('#evpoints').val('');
        };

        /*
         * Delete selected row of datatable
         */

        $('#btnrm_task').click(function () {
            tab.row('.selected').remove().draw(false);
            createJson();
        });
    });
</script>
