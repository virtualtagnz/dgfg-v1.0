<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="user_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php if ($img) { ?>
                <img class="img-responsive img-circle center-block" src="<?php echo $img ?>"/>
            <?php } else { ?>
                <img class="img-responsive img-circle center-block" src="<?php echo base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>   
            <?php } ?>
            <h4 class="text-center"><?php echo $fname . ' ' . $lname . '<br>' ?>
                <?php if (end($this->uri->segments) === '1') { ?>
                    <small>Project Co-ordinator</small>
                <?php } elseif (end($this->uri->segments) === '2') { ?>
                    <small>SKWAD Leader</small>
                <?php } elseif (end($this->uri->segments) === '3') { ?>
                    <small>SKWAD Member</small>
                <?php } ?></h4>
            <p><?php echo $des ?></p>
            <table class="table centered">
                <tbody>
                    <tr>
                        <td>Email:</td>
                        <td><?php echo $uemail ?></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td><?php echo $mob ?></td>
                    </tr>
                    <tr>
                        <td>Facebook Link:</td>
                        <td><a href='<?php echo  $fb?>' target="_blank"><?php echo  $fb?></a></td>
                    </tr>
                     <tr>
                        <td>instagram Link:</td>
                        <td><a href='<?php echo  $inst?>' target="_blank"><?php echo  $inst?></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>