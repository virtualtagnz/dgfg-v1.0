<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            VIEW SUBMITTED WELLNESS SURVEYS
<!--            <small>Optional description</small>-->
        </h1>
        <ol class="breadcrumb">
<!--            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" id="admin-view">
            <div class="col-md-12">
                <table id="sur_tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Survey User</th>
                            <th>User Name</th>
                            <th>Date Submitted</th>
                            <th>View Survey</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('#sur_tab').DataTable({
                    "aaData": <?php echo $json_str ?>,
                    "aoColumns": [
                        {"mData": "survey_id","visible": false},
                        {"mData": "user_name"},
                        {"mData": "sub_date"},
                        {"mData": "survey_id", "mRender": function (data) {
                                return '<a href=<?php echo site_url() ?>/survey/display_survey_cod/' + data + '>View Survey</a>';
                            }}
                    ]
                });
    });
</script>