<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 870px;">
    <!-- Content Header (Page header) -->
    <section class="content-header" id="dashboard">
        <!--        <h1>
                    MEMBER DASHBOARD
        <?php var_dump($this->session->userdata);
        echo gettype(count($this->session->userdata))
        ?>
                    <small>No controller yet</small>
                </h1>-->
        <div class="row">
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">My Events & Tasks</h4></div>
                    <div class="panel-body">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php
                                $i = 0;
                                foreach ($my_events as $value) {
                                    ?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" class="<?php echo ($i == 0) ? "active" : "" ?>"></li>
                                    <?php
                                    $i++;
                                };
                                ?>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <?php $j = 0; ?>
                                    <?php foreach ($my_events as $value) { ?>
                                    <div class="item <?php echo ($j == 0) ? "active" : "" ?>">
                                        <?php if ($value['event_img_linq']) { ?>
                                            <img class="center-block" src="<?php echo base_url() . 'public/uploads/' . $value['event_name'] . '/' . $value['event_img_linq'] ?>" alt="...">
                                            <?php } ?>
                                        <div class="carousel-caption">
                                            <h3><?php echo $value['event_name'] ?></h3>
                                            <p><?php echo $value['event_comments'] ?></p>
                                            <h4>My Tasks</h4>
                                            <table class="table table-striped">
                                                <tbody>
                                                    <?php foreach ($value['my_task_list'] as $value1) { ?>
                                                        <tr>
                                                            <td><?php echo $value1['task_des'] ?></td>
                                                            <td><?php echo $value1['stat'] ?></td>
                                                        </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="col-md-12"><a class="vtevt" id="<?php echo $value['event_id'] ?>">View Full Event</a></div>
                                        </div>
                                    </div>
                                    <?php
                                    $j++;
                                };
                                ?>  
                            </div>
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">My Services</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php foreach ($service_cat as $values) { ?>
                                    <tr>
                                        <td><?php echo $values['sp_name'] ?><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-service" id="<?php echo $values['sp_id']; ?>">Go</a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top SKWAD Leaders</h4></div>
                    <div class="panel-body">
                        <?php foreach ($top_leaders as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['led_img_link']) ? $value['led_img_link'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['lname'] . '<br>' ?>
                                <small><?php echo 'Completed Events - ' . $value['ev_cnt'] . '<br>' ?></small>
                                <a class="v-led" id="<?php echo $value['led_id'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top SKWAD Members</h4></div>
                    <div class="panel-body">
                        <?php foreach ($top_members as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['mem_img_link']) ? $value['mem_img_link'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['mem_name'] . '<br>' ?>
                                <small><?php echo 'Success Rate - ' . $value['task_completion_precentage'] . '%' . '<br>' ?></small>
                                <a class="vsu" id="<?php echo $value['mem_id'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default" id="widget1">
                    <div class="panel-heading"><h4 class="text-center">Event Calender</h4></div>
                    <div class="panel-body">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Blog Posts</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php foreach ($blog_post as $value) { ?>
                                    <tr>
                                        <td><?php echo $value['blog_name'] ?><span><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-blog" id="<?php echo $value['blog_id']; ?>">View Blog Post</a></td>
                                        <td><span class="pull-right"><?php echo $value['blog_created_date']?></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" id="widget2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Quick Surveys</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <?php if(count($daily_survey)>0){?>
                                <?php foreach ($daily_survey as $value) { ?>
                                    <tr>
                                        <td><?php echo $value['daily_survey_name'] ?><span><i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i><a class="v-dsurveys" id="<?php echo $value['daily_survey_id']; ?>">Take Survey</a></td>
                                        <td><span class="pull-right"><?php echo $value['daily_survey_date']?></span></td>
                                    </tr>
                                <?php } ?>
                                <?php } else {?>
                                    <p class="text-center">Surveys are not available</p>                          
                                <?php  }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="widget1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center">Top Rewarders</h4></div>
                    <div class="panel-body">
                        <?php foreach ($top_rewarders as $value) { ?>
                            <div class="col-md-4 text-center"><img class="img-responsive img-circle center-block" src="<?php echo ($value['img']) ? $value['img'] : base_url() . 'public/uploads/profile_imgs/default-user.jpg' ?>"/>
                                <?php echo $value['uname'] . '<br>' ?>
                                <small><?php echo 'Points - ' . $value['tot_points'].'<br>'?></small>
                                <a class="<?php echo ($value['role']=='3')?"v-led":"vsu";?>" id="<?php echo $value['uid'] ?>">View Profile</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="mod"></div>
    </section>
</div>
<script>
    $(document).ready(function () {
//        $('.carousel').carousel({
//            interval: false
//        });

        $('.vtevt').on('click', function () {
            var eid = $(this).attr('id');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_event/active_events_view/'; ?>" + eid,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#single_active_events').modal('show');
                }
            });
        });

        $('.v-service').on('click', function () {
            var sid = $(this).attr('id');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() . '/single_service/'; ?>" + sid,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#service_modal').modal('show');
                }
            });
        });

        $('.vsu').on('click', function () {
            var uid = $(this).attr('id');
            console.log(uid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/view_single/" + uid + '/3',
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#user_modal').modal('show');
                }
            });
        });

        $('.v-led').on('click', function () {
            var uid = $(this).attr('id');
            console.log(uid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/view_single/" + uid + '/2',
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#user_modal').modal('show');
                }
            });
        });

        $('.v-blog').on('click', function () {
            var sid = $(this).attr('id');
            console.log(sid);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/blog/blog_single_view/" + sid,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#myModal').modal('show');
                }
            });
        });
        
        $('.v-dsurveys').on('click', function () {
            var sur_id = $(this).attr('id');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url() ?>/single_daily_survey/" + sur_id,
                success: function (results) {
                    console.log(results);
                    $('#mod').html(results);
                    $('#daily_survey_modal').modal('show');
                }
            });
        });
        
        $('#calendar').fullCalendar({
            eventSources: [{
                    url: "<?php echo site_url() . '/view_events/load_events_calender'; ?>",
                    type: 'POST',
                    success: function (results) {
                        console.log(results);
                    },
                    error: function () {
                        alert('there was an error while fetching events!');
                    }
                }],
            eventClick: function (eventSources) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url() . '/single_event/'; ?>" + eventSources.id,
                    success: function (results) {
                        console.log(results);
                        $('#mod').html(results);
                        $('#myModal').modal('show');
                    }
                });
            }
        });
    });
</script>
