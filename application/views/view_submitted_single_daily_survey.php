<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="com_daily_survey_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <p><h2 class="text-center"><i><?php echo $s_name; ?></i></h2></p>
            <form class="form-horizontal" name="ds-frm" id="ds-frm" method="post" action="<?php echo site_url('single_daily_survey/save_daily_survey_answers/' . end($this->uri->segments)); ?>">
                <?php $json1 = json_decode($json_str); ?>
                <?php $x = 1; ?>
                <?php foreach ($json1 as $key => $value) { ?>
                    <div class="form-group">
                        <ul class="list-group" id="<?php echo 'grp' . strval($x) ?>">
                            <h4 class="list-group-item active"><?php echo strval($x) . '. ' . $value->question ?></h4>
                            <input type="hidden" name="<?php echo 'real_qv' . strval($x) ?>" id="<?php echo 'real_qv' . strval($x) ?>" value="<?php echo $value->qa_id ?>">
                            <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $x . '[]' ?>" value="1"<?php echo $value->ds_ans == '1' ? ' checked="checked"' : ''; ?>><?php echo $value->ans1 ?></li>
                            <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $x . '[]' ?>" value="2"<?php echo $value->ds_ans == '2' ? ' checked="checked"' : ''; ?>><?php echo $value->ans2 ?></li>
                            <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $x . '[]' ?>" value="3"<?php echo $value->ds_ans == '3' ? ' checked="checked"' : ''; ?>><?php echo $value->ans3 ?></li>
                            <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . $x . '[]' ?>" value="4"<?php echo $value->ds_ans == '4' ? ' checked="checked"' : ''; ?>><?php echo $value->ans4 ?></li>
                        </ul>
                        <div id="infoMessage"><?php echo form_error('Q' . $x); ?></div>
                    </div>
                    <?php $x++; ?>
                <?php } ?><!--end main loop-->
                <?php if ($this->session->userdata('user_role') !== '2') { ?>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="submit" id="btn-go" class="btn btn-primary pull-right">Finish & Submit</button>
                        </div>
                    </div>
                <?php } ?>
            </form>
            <?php if ($this->session->flashdata('success_msg')) { ?>
                <script>
                    swal({title: "Good job!", text: "Thank you for taking survey!", type: "success", showConfirmButton: true, confirmButtonText: "OK"}, function () {
                        window.location.href = "<?php echo site_url('dashboard') ?>";
                    });
                </script>
            <?php } elseif ($this->session->flashdata('error_msg')) { ?>
                <script>
                    swal("Error!", "Something is going wrong!", "error");
                </script>
            <?php } ?>
        </div>
    </div>
</div>
