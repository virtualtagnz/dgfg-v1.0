CREATE DATABASE  IF NOT EXISTS `dgfg_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dgfg_db`;
-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: dgfg_db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assign_events_tab`
--

DROP TABLE IF EXISTS `assign_events_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assign_events_tab` (
  `aev_id` int(11) NOT NULL AUTO_INCREMENT,
  `aev_event_ref` int(11) NOT NULL,
  `aev_task_id` int(11) NOT NULL,
  `aev_role_code` tinyint(1) NOT NULL,
  `aev_uid` int(11) NOT NULL,
  `assign_date` datetime NOT NULL,
  `assign_role` tinyint(1) NOT NULL,
  `assign_by` varchar(45) NOT NULL,
  PRIMARY KEY (`aev_id`),
  KEY `aev_event_ref_idx` (`aev_event_ref`),
  KEY `aev_task_id_idx` (`aev_task_id`),
  KEY `aev_role_code_idx` (`aev_role_code`),
  CONSTRAINT `aev_event_ref` FOREIGN KEY (`aev_event_ref`) REFERENCES `event_tab` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `aev_role_code` FOREIGN KEY (`aev_role_code`) REFERENCES `user_roles` (`role_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `aev_task_id` FOREIGN KEY (`aev_task_id`) REFERENCES `event_task_tab` (`event_task_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assign_events_tab`
--

LOCK TABLES `assign_events_tab` WRITE;
/*!40000 ALTER TABLE `assign_events_tab` DISABLE KEYS */;
INSERT INTO `assign_events_tab` VALUES (15,1,59,3,1,'2016-06-09 05:52:27',3,'aaronkemp@inbound.plus'),(16,1,60,3,1,'2016-06-09 05:52:27',3,'aaronkemp@inbound.plus'),(17,2,53,3,1,'2016-06-09 23:57:11',3,'aaronkemp@inbound.plus'),(18,2,58,3,1,'2016-06-09 23:57:11',3,'aaronkemp@inbound.plus'),(19,2,54,3,1,'2016-06-12 15:05:51',3,'aaronkemp@inbound.plus'),(20,2,55,3,1,'2016-06-12 15:05:51',3,'aaronkemp@inbound.plus'),(21,2,56,3,1,'2016-06-12 15:05:51',3,'aaronkemp@inbound.plus'),(22,2,57,3,1,'2016-06-12 15:05:51',3,'aaronkemp@inbound.plus'),(23,4,67,3,2,'2016-06-12 16:00:42',3,'alexanderharper@inbound.plus'),(24,4,68,3,2,'2016-06-12 16:00:42',3,'alexanderharper@inbound.plus'),(25,4,69,3,2,'2016-06-12 16:00:42',3,'alexanderharper@inbound.plus'),(26,5,70,4,2,'2016-06-20 13:11:12',3,'aaronkemp@inbound.plus'),(27,5,71,4,2,'2016-06-20 13:11:12',3,'aaronkemp@inbound.plus'),(28,5,72,3,1,'2016-06-20 13:11:44',3,'aaronkemp@inbound.plus'),(29,7,77,4,1,'2016-06-20 13:37:30',3,'alexanderharper@inbound.plus'),(30,7,79,4,1,'2016-06-20 13:37:30',3,'alexanderharper@inbound.plus'),(31,7,80,4,1,'2016-06-20 13:37:30',3,'alexanderharper@inbound.plus'),(32,7,78,4,3,'2016-06-22 13:59:10',3,'alexanderharper@inbound.plus'),(33,8,81,4,4,'2016-06-22 15:29:32',3,'aaronkemp@inbound.plus'),(34,8,83,4,4,'2016-06-22 15:29:32',3,'aaronkemp@inbound.plus'),(35,11,92,4,5,'2016-06-24 07:17:14',3,'alexanderharper@inbound.plus'),(36,11,93,4,5,'2016-06-24 07:17:14',3,'alexanderharper@inbound.plus'),(37,11,94,4,5,'2016-06-24 07:17:14',3,'alexanderharper@inbound.plus'),(38,10,89,4,5,'2016-07-04 05:45:43',3,'aaronkemp@inbound.plus'),(39,10,90,4,5,'2016-07-04 05:45:43',3,'aaronkemp@inbound.plus'),(40,10,91,4,5,'2016-07-04 05:45:43',3,'aaronkemp@inbound.plus'),(41,12,95,3,1,'2016-07-04 07:29:16',3,'aaronkemp@inbound.plus'),(42,12,96,3,1,'2016-07-04 07:29:16',3,'aaronkemp@inbound.plus'),(43,12,97,3,1,'2016-07-04 07:29:16',3,'aaronkemp@inbound.plus');
/*!40000 ALTER TABLE `assign_events_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_tab`
--

DROP TABLE IF EXISTS `blog_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_tab` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_name` varchar(45) NOT NULL,
  `blog_body` text NOT NULL,
  `blog_image` text,
  `is_approved` tinyint(1) DEFAULT NULL,
  `blog_created_by` varchar(25) DEFAULT NULL,
  `blog_created_role` int(11) DEFAULT NULL,
  `blog_created_date` datetime DEFAULT NULL,
  `blog_modified_by` varchar(25) DEFAULT NULL,
  `blog_modified_role` int(11) DEFAULT NULL,
  `blog_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_tab`
--

LOCK TABLES `blog_tab` WRITE;
/*!40000 ALTER TABLE `blog_tab` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_survey_answers`
--

DROP TABLE IF EXISTS `daily_survey_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daily_survey_answers` (
  `ds_ans_id` int(11) NOT NULL AUTO_INCREMENT,
  `ds_ans_main_ref` int(11) NOT NULL,
  `ds_ans_question_ref` int(11) NOT NULL,
  `ds_ans` int(11) NOT NULL,
  PRIMARY KEY (`ds_ans_id`),
  KEY `ds_ans_question_ref_idx` (`ds_ans_question_ref`),
  KEY `ds_ans_main_ref_idx` (`ds_ans_main_ref`),
  CONSTRAINT `ds_ans_main_ref` FOREIGN KEY (`ds_ans_main_ref`) REFERENCES `daily_survey_map` (`ds_map_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ds_ans_question_ref` FOREIGN KEY (`ds_ans_question_ref`) REFERENCES `daily_survey_qa` (`qa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_survey_answers`
--

LOCK TABLES `daily_survey_answers` WRITE;
/*!40000 ALTER TABLE `daily_survey_answers` DISABLE KEYS */;
INSERT INTO `daily_survey_answers` VALUES (6,1,14,1),(7,1,15,3),(8,1,16,2),(9,1,17,4),(10,1,18,4),(11,2,9,1),(12,2,10,3),(13,2,11,1),(14,2,12,5),(15,2,13,1),(16,3,9,2),(17,3,10,4),(18,3,11,5),(19,3,12,5),(20,3,13,2);
/*!40000 ALTER TABLE `daily_survey_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_survey_main`
--

DROP TABLE IF EXISTS `daily_survey_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daily_survey_main` (
  `daily_survey_id` int(11) NOT NULL,
  `daily_survey_name` varchar(250) NOT NULL,
  `daily_survey_date` date NOT NULL,
  `daily_survey_points` int(11) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `mod_by` varchar(255) DEFAULT NULL,
  `mod_date` datetime DEFAULT NULL,
  PRIMARY KEY (`daily_survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_survey_main`
--

LOCK TABLES `daily_survey_main` WRITE;
/*!40000 ALTER TABLE `daily_survey_main` DISABLE KEYS */;
INSERT INTO `daily_survey_main` VALUES (1,'Customer Satisfaction','2016-07-17',5,'jamiecraig@inbound.plus','2016-07-13 04:29:50',NULL,NULL),(2,'Sample Survey','2016-07-19',7,'jamiecraig@inbound.plus','2016-07-14 01:27:25',NULL,NULL);
/*!40000 ALTER TABLE `daily_survey_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_survey_map`
--

DROP TABLE IF EXISTS `daily_survey_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daily_survey_map` (
  `ds_map_id` int(11) NOT NULL,
  `ds_map_survey_ref` int(11) NOT NULL,
  `ds_map_user` varchar(255) NOT NULL,
  `ds_map_sur_date` datetime NOT NULL,
  PRIMARY KEY (`ds_map_id`),
  KEY `ds_map_survey_ref_idx` (`ds_map_survey_ref`),
  CONSTRAINT `ds_map_survey_ref` FOREIGN KEY (`ds_map_survey_ref`) REFERENCES `daily_survey_main` (`daily_survey_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_survey_map`
--

LOCK TABLES `daily_survey_map` WRITE;
/*!40000 ALTER TABLE `daily_survey_map` DISABLE KEYS */;
INSERT INTO `daily_survey_map` VALUES (1,2,'alexanderharper@inbound.plus','2016-07-15 03:23:42'),(2,1,'aaronkemp@inbound.plus','2016-07-15 04:01:43'),(3,1,'tiafraser@gmail.com','2016-07-15 04:04:53');
/*!40000 ALTER TABLE `daily_survey_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_survey_qa`
--

DROP TABLE IF EXISTS `daily_survey_qa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daily_survey_qa` (
  `qa_id` int(11) NOT NULL AUTO_INCREMENT,
  `daily_survey_main_ref` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `ans1` varchar(500) NOT NULL,
  `ans2` varchar(500) NOT NULL,
  `ans3` varchar(500) NOT NULL,
  `ans4` varchar(500) NOT NULL,
  `ans5` varchar(500) NOT NULL,
  PRIMARY KEY (`qa_id`),
  KEY `daily_survey_main_ref_idx` (`daily_survey_main_ref`),
  CONSTRAINT `daily_survey_main_ref` FOREIGN KEY (`daily_survey_main_ref`) REFERENCES `daily_survey_main` (`daily_survey_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_survey_qa`
--

LOCK TABLES `daily_survey_qa` WRITE;
/*!40000 ALTER TABLE `daily_survey_qa` DISABLE KEYS */;
INSERT INTO `daily_survey_qa` VALUES (9,1,'with the appropriateness of the documentation to your needs?','Very Satisfied','Satisfied','Neutral','Dissatisfied','Very Dissatisfied'),(10,1,'with the quality of the documentation delivered with your system?','Very Satisfied','Satisfied','Neutral','Dissatisfied','Very Dissatisfied'),(11,1,'with the usability of the documentation provided?','Very Satisfied','Satisfied','Neutral','Dissatisfied','Very Dissatisfied'),(12,1,'How many school-age children do you have (K-12)?','1','2','3','4','5'),(13,1,'My child/children attend(s)','Public school','Non-local public school','Charter school','Private school','Does not attend school yet'),(14,2,'How likely are you to continue doing business with us?','Very likely','Somewhat likely','Neutral','Somewhat unlikely','Very unlikely'),(15,2,'How satisfi ed are you overall with our customer support?','Very satisfi ed','Somewhat satisfi ed','Neutral','Somewhat dissatisfi ed','Very dissatisfi ed'),(16,2,'Please rate our customer service representative on the following attributes.','Very Poor','Poor','Fair','Good','Very Good'),(17,2,'How often is your coworker late to work?','Always','Most of the time','About half the time','Once in a while',' Never'),(18,2,'How often does your coworker meet his deadlines?','Always',' Most of the time','About half the time','Once in a while',' Never');
/*!40000 ALTER TABLE `daily_survey_qa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dgfg_coordinator_tab`
--

DROP TABLE IF EXISTS `dgfg_coordinator_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgfg_coordinator_tab` (
  `cod_id` int(11) NOT NULL,
  `cod_user_ref` int(11) NOT NULL,
  `cod_fname` varchar(45) NOT NULL,
  `cod_lname` varchar(45) NOT NULL,
  `cod_gender` varchar(10) DEFAULT NULL,
  `cod_mobile` varchar(10) NOT NULL,
  `cod_img_link` varchar(100) DEFAULT NULL,
  `cod_description` varchar(500) DEFAULT NULL,
  `cod_fb_link` varchar(200) DEFAULT NULL,
  `cod_ig_link` varchar(200) DEFAULT NULL,
  `cod_sm_challenge` varchar(45) DEFAULT NULL,
  `create_by` varchar(45) NOT NULL,
  `role_create` tinyint(1) NOT NULL,
  `date_create` datetime NOT NULL,
  `mod_by` varchar(45) NOT NULL,
  `mod_role` tinyint(1) NOT NULL,
  `mod_date` datetime NOT NULL,
  PRIMARY KEY (`cod_id`),
  KEY `cod_user_ref_idx` (`cod_user_ref`),
  CONSTRAINT `cod_user_ref` FOREIGN KEY (`cod_user_ref`) REFERENCES `user_tab` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgfg_coordinator_tab`
--

LOCK TABLES `dgfg_coordinator_tab` WRITE;
/*!40000 ALTER TABLE `dgfg_coordinator_tab` DISABLE KEYS */;
INSERT INTO `dgfg_coordinator_tab` VALUES (1,2,'Jamie','Craig','female','0210214563','657e051d773ae5982bdabb235ad43d80.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut ipsum orci. Donec vehicula, tellus ac sodales fringilla, dui libero vestibulum erat, sed pulvinar nibh massa at ante','http://fb-linq.com','http://inst-linq.com','challenge','super@gmail.com',1,'2016-05-11 10:32:34','jamiecraig@inbound.plus',2,'2016-05-30 17:31:21'),(2,3,'Niamh','Blackburn','male','0921458963','','Quisque vulputate viverra metus nec molestie. Nullam ut dolor lacinia, consequat lacus laoreet, imperdiet lorem. Vestibulum vitae quam at massa','http://fb-linq.com','','','super@gmail.com',1,'2016-05-11 10:33:56','super@gmail.com',1,'2016-05-11 10:33:56'),(3,7,'Aaron','Davis','male','0215553334','7dd14c523357489257d058a1490f8ada.jpg','asdasdasd11111','http://fb-linq.com','http://inst-linq.com','','super@gmail.com',1,'2016-05-11 16:33:16','super@gmail.com',1,'2016-05-13 14:30:10'),(4,8,'Mary','Rhinehart','','','','','','','','super@gmail.com',1,'2016-05-12 14:39:35','super@gmail.com',1,'2016-05-12 14:39:35');
/*!40000 ALTER TABLE `dgfg_coordinator_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dgfg_leader_tab`
--

DROP TABLE IF EXISTS `dgfg_leader_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgfg_leader_tab` (
  `led_id` int(11) NOT NULL,
  `led_user_ref` int(11) NOT NULL,
  `led_fname` varchar(45) NOT NULL,
  `led_lname` varchar(45) NOT NULL,
  `led_gender` varchar(10) DEFAULT NULL,
  `led_mobile` varchar(10) NOT NULL,
  `led_img_link` varchar(100) DEFAULT NULL,
  `led_description` varchar(500) DEFAULT NULL,
  `led_fb_link` varchar(200) DEFAULT NULL,
  `led_ig_link` varchar(200) DEFAULT NULL,
  `led_ethnicity` varchar(45) DEFAULT NULL,
  `create_by` varchar(45) NOT NULL,
  `role_create` tinyint(1) NOT NULL,
  `date_create` datetime NOT NULL,
  `mod_by` varchar(45) NOT NULL,
  `mod_role` tinyint(1) NOT NULL,
  `mod_date` datetime NOT NULL,
  PRIMARY KEY (`led_id`),
  KEY `led_user_ref_idx` (`led_user_ref`),
  CONSTRAINT `led_user_ref` FOREIGN KEY (`led_user_ref`) REFERENCES `user_tab` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgfg_leader_tab`
--

LOCK TABLES `dgfg_leader_tab` WRITE;
/*!40000 ALTER TABLE `dgfg_leader_tab` DISABLE KEYS */;
INSERT INTO `dgfg_leader_tab` VALUES (1,4,'Aaron','Kemp','male','0921365423','3c625f74a0d53154b8165c7cabd13dbb.jpg','aliquet suscipit id et nisi. Integer sit amet nisl et nunc ullamcorper accumsan. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas asdasda','http://fb-linq.com','http://inst-linq.com','5','super@gmail.com',1,'2016-05-11 10:48:18','jamiecraig@inbound.plus',2,'2016-05-31 10:24:16'),(2,5,'Alexander','Harper','male','0278523695','d1565c464fdb89eafb6e5432d4cef6cf.jpg','Vivamus dictum justo non elit dapibus, sed vehicula libero sodales. Suspendisse sed magna vitae augue efficitur gravida eu ut erat','http://fb-linq.com','','1','super@gmail.com',1,'2016-05-11 10:49:43','',0,'2016-05-31 11:42:06'),(3,13,'Lisa','Crowe','female','502-609-21','','Donec venenatis ullamcorper odio, in gravida sapien maximus eget. Integer vestibulum eu enim id laoreet. Proin eget varius ligula. Cras libero nisi, rutrum et tempus et, tincidunt at magna. Mauris libero urna, hendrerit eget dolor vel, rhoncus condimentum enim. Maecenas aliquam congue nulla ut suscipit.dsfsdfsdf','https://www.facebook.com/C-ksksk-163988917072730/','https://www.instagram.com/?hl=en','3','jamiecraig@inbound.plus',2,'2016-06-27 09:55:21','jamiecraig@inbound.plus',2,'2016-06-27 09:55:42'),(4,14,'Ramona','Hughes','female','212-313-39','','Vestibulum pretium tortor arcu, non elementum orci consectetur nec. Mauris ullamcorper lacus eget consequat tincidunt. Duis quis sapien id ante aliquet luctus. sdfsdfs','https://www.facebook.com/C-ksksk-163988917072730/','https://www.instagram.com/?hl=en','1','jamiecraig@inbound.plus',2,'2016-06-27 15:51:10','jamiecraig@inbound.plus',2,'2016-06-27 16:18:55');
/*!40000 ALTER TABLE `dgfg_leader_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dgfg_member_tab`
--

DROP TABLE IF EXISTS `dgfg_member_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgfg_member_tab` (
  `mem_id` int(11) NOT NULL,
  `mem_user_ref` int(11) NOT NULL,
  `mem_fname` varchar(45) NOT NULL,
  `mem_lname` varchar(45) NOT NULL,
  `mem_gender` varchar(10) DEFAULT NULL,
  `mem_mobile` varchar(10) NOT NULL,
  `mem_img_link` varchar(100) DEFAULT NULL,
  `mem_description` varchar(500) DEFAULT NULL,
  `mem_fb_link` varchar(200) DEFAULT NULL,
  `mem_ig_link` varchar(200) DEFAULT NULL,
  `mem_ethnicity` varchar(45) DEFAULT NULL,
  `create_by` varchar(45) NOT NULL,
  `role_create` tinyint(1) NOT NULL,
  `date_create` datetime NOT NULL,
  `mod_by` varchar(45) NOT NULL,
  `mod_role` tinyint(1) NOT NULL,
  `mod_date` datetime NOT NULL,
  PRIMARY KEY (`mem_id`),
  KEY `mem_user_ref_idx` (`mem_user_ref`),
  CONSTRAINT `mem_user_ref` FOREIGN KEY (`mem_user_ref`) REFERENCES `user_tab` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgfg_member_tab`
--

LOCK TABLES `dgfg_member_tab` WRITE;
/*!40000 ALTER TABLE `dgfg_member_tab` DISABLE KEYS */;
INSERT INTO `dgfg_member_tab` VALUES (1,6,'Elise D','Stephenson','female','0402145896','a95e8a961455e2913881a05e4efd24b3.jpg','estibulum venenatis lorem id augue scelerisque, non auctor odio mollis. Morbi diam odio, dignissim a tristique nec, hendrerit sit amet ante asdasdas','','','7','super@gmail.com',1,'2016-05-11 10:51:17','jamiecraig@inbound.plus',2,'2016-06-20 13:08:08'),(2,9,'Denise C.','Sweeney','female','769-38-025','','test','http://fb-linq.com','http://inst-linq.com','1','jamiecraig@inbound.plus',2,'2016-06-09 14:12:34','jamiecraig@inbound.plus',2,'2016-06-09 14:12:34'),(3,10,'Cynthia','Hart','female','020 5767-4','','sdfsdsdf sdfsdfsdf sdfsdfsdfsdf','http://fb-linq.com','http://inst-linq.com','1','jamiecraig@inbound.plus',2,'2016-06-19 09:03:17','jamiecraig@inbound.plus',2,'2016-06-19 09:03:17'),(4,11,'Tia','Fraser','female','','','test test','http://fb-linq.com','http://inst-linq.com','1','jamiecraig@inbound.plus',2,'2016-06-19 09:04:28','jamiecraig@inbound.plus',2,'2016-06-19 09:04:28'),(5,12,'Jack','Plant','male','020-9591-8','cf0322ba69024b237d56ea33f6af4a26.jpg','Duis posuere purus in metus placerat, vitae posuere augue luctus. Curabitur pulvinar molestie nisi ut consectetur. Proin id ante eget arcu fringilla euismod nec at magna. In finibus augue ligula, sodales ultricies magna suscipit quis. Donec at suscipit nisl, ac facilisis ante. Praesent ultrices maximus libero, non bibendum elit. In cursus quam in ante rutrum, et convallis sapien tincidunt. Quisque nec ornare est, ','https://www.facebook.com/C-ksksk-163988917072730/','https://www.instagram.com/?hl=en','1','jamiecraig@inbound.plus',2,'2016-06-24 10:30:02','jamiecraig@inbound.plus',2,'2016-06-24 10:30:02'),(6,15,'Scott','Davies','male','021-5988-5','','Mauris leo est, dictum et elit a, porttitor mollis mauris. Duis molestie aliquet velit ut porta. Sed eget dolor aliquet, tempor tortor et, luctus nunc. Nullam consectetur posuere orci, at','https://www.facebook.com/C-ksksk-163988917072730/','https://www.instagram.com/?hl=en','8','jamiecraig@inbound.plus',2,'2016-06-27 15:58:17','jamiecraig@inbound.plus',2,'2016-06-27 15:58:17');
/*!40000 ALTER TABLE `dgfg_member_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_galary`
--

DROP TABLE IF EXISTS `event_galary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_galary` (
  `galary_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `img_linq` text NOT NULL,
  `is_approved` tinyint(4) NOT NULL,
  `is_visible` tinyint(4) NOT NULL,
  `is_submited` tinyint(4) NOT NULL,
  `create_by` varchar(25) DEFAULT NULL,
  `create_role` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `approve_by` varchar(25) DEFAULT NULL,
  `approve_role` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  PRIMARY KEY (`galary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_galary`
--

LOCK TABLES `event_galary` WRITE;
/*!40000 ALTER TABLE `event_galary` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_galary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_invitation`
--

DROP TABLE IF EXISTS `event_invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_invitation` (
  `ev_invite_id` int(11) NOT NULL AUTO_INCREMENT,
  `ev_invite_event_ref` int(11) NOT NULL,
  `ev_invite_mem_ref` int(11) NOT NULL,
  `ev_invite_accept` tinytext NOT NULL,
  `ev_invite_date` datetime NOT NULL,
  `ev_invite_by` varchar(45) NOT NULL,
  PRIMARY KEY (`ev_invite_id`),
  KEY `ev_invite_event_ref_idx` (`ev_invite_event_ref`),
  KEY `ev_invite_mem_ref_idx` (`ev_invite_mem_ref`),
  CONSTRAINT `ev_invite_event_ref` FOREIGN KEY (`ev_invite_event_ref`) REFERENCES `event_tab` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ev_invite_mem_ref` FOREIGN KEY (`ev_invite_mem_ref`) REFERENCES `dgfg_member_tab` (`mem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_invitation`
--

LOCK TABLES `event_invitation` WRITE;
/*!40000 ALTER TABLE `event_invitation` DISABLE KEYS */;
INSERT INTO `event_invitation` VALUES (2,5,1,'Y','2016-06-19 12:38:51','aaronkemp@inbound.plus'),(3,1,1,'N','2016-06-20 01:55:46','aaronkemp@inbound.plus'),(4,7,3,'Y','2016-06-22 02:41:52','alexanderharper@inbound.plus'),(5,8,4,'Y','2016-06-22 15:26:34','aaronkemp@inbound.plus'),(6,7,5,'Y','2016-06-24 07:00:33','alexanderharper@inbound.plus'),(7,11,5,'Y','2016-06-24 07:12:10','alexanderharper@inbound.plus'),(8,10,5,'Y','2016-07-04 05:29:18','aaronkemp@inbound.plus'),(9,10,6,'N','2016-07-04 05:29:33','aaronkemp@inbound.plus'),(10,10,4,'Y','2016-07-04 05:29:51','aaronkemp@inbound.plus');
/*!40000 ALTER TABLE `event_invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_tab`
--

DROP TABLE IF EXISTS `event_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_tab` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `event_date` date NOT NULL,
  `event_start` time NOT NULL,
  `event_end` time NOT NULL,
  `event_loc` varchar(250) NOT NULL,
  `event_promo1` varchar(500) DEFAULT NULL,
  `event_promo2` varchar(500) DEFAULT NULL,
  `event_promo3` varchar(500) DEFAULT NULL,
  `event_promo4` varchar(500) DEFAULT NULL,
  `event_plan` varchar(500) DEFAULT NULL,
  `event_comments` varchar(500) DEFAULT NULL,
  `event_video_linq` varchar(350) DEFAULT NULL,
  `event_img_linq` varchar(350) DEFAULT NULL,
  `event_stat` tinytext NOT NULL,
  `event_approve_stat` tinytext NOT NULL,
  `event_approved_by` varchar(45) NOT NULL,
  `event_approved_date` datetime NOT NULL,
  `event_approved_role` tinyint(1) NOT NULL,
  `event_create_by` varchar(45) NOT NULL,
  `event_create_role` tinyint(1) NOT NULL,
  `event_create_date` datetime NOT NULL,
  `event_mod_by` varchar(45) NOT NULL,
  `event_mod_role` tinyint(1) NOT NULL,
  `event_mod_date` datetime NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_tab`
--

LOCK TABLES `event_tab` WRITE;
/*!40000 ALTER TABLE `event_tab` DISABLE KEYS */;
INSERT INTO `event_tab` VALUES (1,'The June Hayes Experience','2016-06-30','13:00:00','17:00:00','Pt Chevalier RSA','http://samplelinq.com/232111','http://samplelinq.com/212','http://samplelinq.com/768678','http://samplelinq.com/674563','http://eventpaln.com/674563','The June Hayes Experience’ blends a top line-up of musicians into a smoothly mixed cocktail where ‘The Great American Songbook’ classic jazz mixes well with classic pops and original works. Accompanying June is Alex Ward – piano/vocals, David Hodkinson – double-bass, Jono Sawyer – drums, and special guest Thabani Gapara on saxophone.','756ba865a7f0079c6eeba072ec2d9697.mp4','2c6adb924d82c9f162e71f63ba03ddf8.jpg','S','A','jamiecraig@inbound.plus','2016-06-07 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-06-07 15:25:29','jamiecraig@inbound.plus',2,'2016-06-07 15:54:27'),(2,'K-Pop Showdown','2016-07-31','16:15:00','18:00:00','Westfield Albany','http://samplelinq.com/232111','http://samplelinq.com/212','http://samplelinq.com/768678','http://samplelinq.com/674563','http://eventpaln.com/674563','Fresh from LA where they\'ve been dancing with Beyoncé, Justin Bieber, Parris Goebel and Korean pop-stars CL and Boa, choreographerS Rina Chae and Kiel Tutin face off in an explosive, ground-shaking half-hour at Westfield Albany.\r\nFour shows featuring Rina Chae, Kiel Tutin and Street Candee Dance Co.\r\nFriday 10 June: 4pm & 7pm \r\nSaturday 11 June: 12pm & 1pm\r\nWe\'re also offering 30 lucky people the chance to take part in a six-week K-pop dance course with Rina Chae, who has co-choreographed for K-','80cf19a05beb1d18c72b3999e19d9917.mp4','d828fc7f183898c9c69d7ac4b4f57d6e.jpg','C','A','jamiecraig@inbound.plus','2016-06-07 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-06-07 15:29:48','jamiecraig@inbound.plus',2,'2016-06-07 15:53:31'),(3,'Eb and Sparrow','2016-06-30','13:00:00','15:00:00','St Peters Hall, Paekakariki','http://samplelinq.com/232111','http://samplelinq.com/212','http://samplelinq.com/768678','http://samplelinq.com/674563','http://eventpaln.com/674563','Eb & Sparrow have traversed a pathway that has led them onto stages with an astonishing string of folk-heroes: Rodriguez, Beth Orton, Pokey LaFarge, Wagons, The Warratahs, Tami Neilson and Marlon Williams to name a few. Eb & Sparrow have carved out a niche, creating a soundscape that is at once spacious and lush, nuanced alt-country music that swings out into folk, soul, and rockabilly. Ebony’s voice slides along the edges of Cat Power and Gillian Welch, and with a captivating presence, she deli','','','N','R','jamiecraig@inbound.plus','2016-06-10 12:49:57',2,'alexanderharper@inbound.plus',3,'2016-06-10 12:42:54','alexanderharper@inbound.plus',3,'2016-06-10 12:42:54'),(4,'Snarky Puppy','2016-07-31','17:00:00','20:00:00','The Opera House,Te Aro, Wellington','http://samplelinq.com/232111','http://samplelinq.com/212','http://samplelinq.com/768678','http://samplelinq.com/674563','http://eventpaln.com/674563','Get down to the new jazz sound from the underground and revel in a night of raw “animal magic” (The Telegraph).\r\nThis two-time Grammy Award-winning Brooklyn-based outfit is ready to bring the Festival funk with a jazzed up rock concert of synth-driven grooves, “horn-hollering soul hooks, floor-shaking beats and punchy improv” - (The Guardian).\r\nSnarky Puppy has worked with some of the music industry’s biggest players including Snoop Dogg and Justin Timberlake, and won fans worldwide including a ','','','C','A','jamiecraig@inbound.plus','2016-06-10 12:58:27',2,'aaronkemp@inbound.plus',3,'2016-06-10 12:58:00','aaronkemp@inbound.plus',3,'2016-06-10 12:58:00'),(5,'Fly My Pretties - String Theory','2016-06-22','18:00:00','20:00:00','The Paramount Theatre','http://samplelinq.com/232111','http://samplelinq.com/212','http://samplelinq.com/768678','http://samplelinq.com/674563','http://eventpaln.com/674563','String Theory will enlist a new 14-strong cast of FMP favourites and new faces to explore the fabric of our existence through music. Each show will be accompanied by breath-taking interactive visuals, examining everything from the smallest particles of energy, to the largest galaxies in the universe through original storytelling.\r\n','dbc262c154b981b8509106ff913fb7f4.mp4','6e09f1bdd99f8d84dbd07d906d7c3473.jpg','C','A','jamiecraig@inbound.plus','2016-06-10 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-06-10 13:32:11','jamiecraig@inbound.plus',2,'2016-06-10 13:32:11'),(6,'The Amazing Nina Simone','2016-07-29','17:00:00','20:00:00','Light House Cuba Cinema','http://samplelinq.com/232111','http://samplelinq.com/212','http://samplelinq.com/768678','http://samplelinq.com/674563','http://eventpaln.com/674563','The story of Americaʼs most overlooked musical genius is finally brought to light. Director Jeff L Lieberman brings audiences on singer, songwriter, pianist and activist Nina Simone’s journey from the segregated South, through the worlds of classical music, jazz joints and international concert halls.','','','S','A','jamiecraig@inbound.plus','2016-06-18 14:38:11',2,'alexanderharper@inbound.plus',3,'2016-06-10 13:38:41','alexanderharper@inbound.plus',3,'2016-06-10 13:38:41'),(7,'Te Aro Pā Walking Tour','2016-07-31','16:00:00','19:00:00','Taranaki St Wharf','http://promo1.co.nz','http://promo2.co.nz','http://promo3.co.nz','http://promo4.co.nz','http://event_plan.co.nz','Our tour begins at Te Wharewaka o Pōneke the cultural centre for Taranaki Whānui on the Wellington Waterfront. Our staff will share their stories and take you on a journey introducing you to the first Polynesian explorer, Kupe and his wife Hineaparangi, to Te Aro Pā where you will discover and explore the preserved foundations of two whareponga (buildings) and other items for the old pā site uncovered during construction in 2005.','','','C','A','jamiecraig@inbound.plus','2016-06-20 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-06-20 14:07:42','jamiecraig@inbound.plus',2,'2016-06-20 14:07:42'),(8,'The 39 Steps','2016-07-28','17:00:00','20:00:00','Gaslight Theatre','http://promo1.co.nz','http://promo2.co.nz','http://promo3.co.nz','','http://event_plan.co.nz','Patrick Barlow\'s Olivier Award-winning stage adaptation, based on John Buchan\'s gripping whodunit - memorably filmed by Alfred Hitchcock in 1935. Follow the incredible adventures of our handsome hero Richard Hannay, complete with stiff-upper-lip, British gung-ho and pencil moustache as he encounters dastardly murders, double-crossing secret agents, and, of course, devastatingly beautiful women.','','','S','A','jamiecraig@inbound.plus','2016-06-20 14:46:07',2,'alexanderharper@inbound.plus',3,'2016-06-20 14:45:23','alexanderharper@inbound.plus',3,'2016-06-20 14:45:23'),(9,'Trivia Tuesdays','2016-06-30','17:00:00','19:00:00','The Library','http://promo1.co.nz','http://promo2.co.nz','','','http://event_plan.co.nz','Come with me and you\'ll be in a world of pure imagination \r\nFor the month of June we will be offering an extra special menu of candy and chocolate-inspired cocktails.\r\nOompa loompa doompety dee, if you are wise you\'ll drink two or three.','','','N','R','jamiecraig@inbound.plus','2016-06-22 11:05:38',2,'alexanderharper@inbound.plus',3,'2016-06-21 16:37:46','alexanderharper@inbound.plus',3,'2016-06-21 16:37:46'),(10,'Beneath The Words','2016-07-31','13:00:00','17:00:00','Wellington Irish Society','http://promo1.co.nz','http://promo2.co.nz','http://promo3.co.nz','http://promo4.co.nz','http://event_plan.co.nz','An indigenous performance art collaboration of the Mãori & Irish cultures presented through music, storytelling, language and kapahaka. This project \"Beneath The Words\" specifically crosses a Taranaki Iwi with a Tipperary Clan. The show has been well received in Ireland, August 2015 at shows in counties Kildare, Kerry and Cork. With planned tours of Aotearoa New Zealand\'s North & South Islands in 2016 you can expect a fusion of te reo Mãori waiata, haka with Irish hard hitting beats and tunes, c','','','C','A','jamiecraig@inbound.plus','2016-06-22 10:49:31',2,'alexanderharper@inbound.plus',3,'2016-06-21 16:49:09','alexanderharper@inbound.plus',3,'2016-06-21 16:49:09'),(11,'Green Living','2016-07-28','07:00:00','17:00:00','ASB Showgrounds','http://promo1.co.nz','http://promo2.co.nz','http://promo3.co.nz','http://promo4.co.nz','http://event_plan.co.nz','Ecobuild - Healthy Homes & Healthy Buildings - new build, renovation, retrofit. Bring your ideas, concepts and plans to life. Get Expert Advice. Future Proof your home! Home owners will be able to sit with skilled architects, eco design advisors and discuss how you can make your renovation or new build as sustainable as possible.','88827f0301e0fdd1bfd628d2db6f4325.mp4','1c156ea57f4042b6c54099bd769a7007.jpg','S','A','jamiecraig@inbound.plus','2016-06-24 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-06-24 17:08:10','jamiecraig@inbound.plus',2,'2016-06-24 17:08:10'),(12,'Pulse in Heart','2016-07-19','17:00:00','20:00:00','Eva Beva','http://promo1.co.nz','http://promo2.co.nz','http://promo3.co.nz','http://promo4.co.nz','http://event_plan.co.nz','To show our support for the victims and their loved ones of the Orlando shooting at Pulse Bar, the rainbow community and friends are coming together to bring you a fabulous show hosted by Lee from L.Y.C.\r\nSo come along and show your support for your favorite performers and to help raise as much money as we can for a great cause.\r\nDoors open at 12pm.\r\nThere will be a silent auction and a raffle from some amazing groups organisations and business so keep an eye out on the Facebook event age to kee','','','S','A','jamiecraig@inbound.plus','2016-06-24 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-06-24 17:11:10','jamiecraig@inbound.plus',2,'2016-06-24 17:11:10'),(13,'Knightshade','2016-08-31','20:00:00','15:00:00','Altitude Bar','http://promo1.co.nz','http://promo2.co.nz','http://promo3.co.nz','','http://event_plan.co.nz','Knightshade are hanging up their gloves - One final concert in Hamilton, A show not to be missed. \r\nA night of awesome rock \'n roll. Signing and photo sessions, merchandise and memorabilia. \r\nCome down and be part of a historical send-off for Waikato\'s Heavy-Weight Rock Band','','','A','A','jamiecraig@inbound.plus','2016-07-01 00:00:00',2,'jamiecraig@inbound.plus',2,'2016-07-01 15:04:32','jamiecraig@inbound.plus',2,'2016-07-01 15:54:45');
/*!40000 ALTER TABLE `event_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_task_tab`
--

DROP TABLE IF EXISTS `event_task_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_task_tab` (
  `event_task_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_ref` int(11) NOT NULL,
  `task_des` varchar(500) NOT NULL,
  `due_date` datetime NOT NULL,
  `task_point` int(11) NOT NULL,
  `task_stat` tinytext NOT NULL,
  `date_complete` datetime NOT NULL,
  PRIMARY KEY (`event_task_id`),
  KEY `event_ref_idx` (`event_ref`),
  CONSTRAINT `event_ref` FOREIGN KEY (`event_ref`) REFERENCES `event_tab` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_task_tab`
--

LOCK TABLES `event_task_tab` WRITE;
/*!40000 ALTER TABLE `event_task_tab` DISABLE KEYS */;
INSERT INTO `event_task_tab` VALUES (53,2,'Curabitur at enim vehicula, aliquam est sed, rhoncus nulla','2016-06-15 00:00:00',0,'C','2016-06-12 15:06:52'),(54,2,'Donec tempor velit eu vulputate feugiat','2016-06-15 00:00:00',0,'C','2016-06-12 15:07:05'),(55,2,'Nullam fringilla tortor tristique, consectetur augue quis, posuere sapien','2016-06-16 00:00:00',0,'C','2016-06-12 15:06:52'),(56,2,'Maecenas eu nisi egestas mi elementum euismod a et dui','2016-06-23 00:00:00',0,'C','2016-06-12 15:07:05'),(57,2,'Etiam eu dolor sed nunc congue dapibus','2016-06-29 00:00:00',0,'C','2016-06-12 15:06:52'),(58,2,'Aenean eu arcu id eros dictum convallis a ut orci','2016-07-07 00:00:00',0,'C','2016-06-10 02:07:28'),(59,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.','2016-06-14 00:00:00',0,'C','2016-06-10 02:02:08'),(60,1,'In nec purus et sapien pellentesque dapibus.','2016-06-23 00:00:00',0,'C','2016-06-11 00:10:34'),(61,1,'Duis a diam a elit congue condimentum vitae ut risus.','2016-06-30 00:00:00',0,'O','0000-00-00 00:00:00'),(62,1,'Aenean eu arcu id eros dictum convallis a ut orci','2016-06-29 00:00:00',0,'O','0000-00-00 00:00:00'),(63,3,'Capture Biometric Identifier 1 ','2016-06-21 00:00:00',0,'O','0000-00-00 00:00:00'),(64,3,'Capture Biometric Identifier 2 ','2016-06-15 00:00:00',0,'O','0000-00-00 00:00:00'),(65,3,'Compare document to shared information within INS regarding fraudulent document trends, recent fraud issues, etc.','2016-06-16 00:00:00',0,'O','0000-00-00 00:00:00'),(66,3,'Review results of automated presentation of relevant passport/visa/travel document fraud data as presented by PAU','2016-06-24 00:00:00',0,'O','0000-00-00 00:00:00'),(67,4,'Visually analyze travel document for all required seals, watermarks, etc.','2016-06-28 00:00:00',0,'C','2016-06-12 16:01:23'),(68,4,'Review results of automated comparison of biometric','2016-06-23 00:00:00',0,'C','2016-06-12 16:01:23'),(69,4,'Visually review biographic information in passport and other traveler documents','2016-06-30 00:00:00',0,'C','2016-06-13 00:04:10'),(70,5,'Capture Biometric Identifier 1 (Active/Fingerprint)','2016-06-23 00:00:00',0,'C','2016-06-20 13:31:22'),(71,5,'Capture Biometric Identifier 2 (Active/Photograph)','2016-06-22 00:00:00',0,'C','2016-06-20 13:31:22'),(72,5,'Capture Biometric Identifier 3 (Active)','2016-06-23 00:00:00',0,'C','2016-06-20 14:23:19'),(73,6,'Adverse action','2016-06-24 00:00:00',0,'O','0000-00-00 00:00:00'),(74,6,'Process for Expedited Removal','2016-06-23 00:00:00',0,'O','0000-00-00 00:00:00'),(75,6,'Process for Withdrawal of Application','2016-06-30 00:00:00',0,'O','0000-00-00 00:00:00'),(76,6,'Process for Deferred Inspection','2016-06-30 00:00:00',0,'O','0000-00-00 00:00:00'),(77,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.','2016-06-22 00:00:00',0,'C','2016-06-20 13:41:52'),(78,7,'Phasellus vitae lorem elementum, tincidunt odio nec, aliquam lorem','2016-06-24 00:00:00',0,'C','2016-06-22 15:23:49'),(79,7,'Proin et sem eu justo euismod tincidunt.','2016-06-29 00:00:00',0,'C','2016-06-20 13:43:38'),(80,7,'Nunc ornare ligula sed erat ullamcorper, sed vestibulum ex facilisis','2016-06-30 00:00:00',0,'C','2016-06-20 13:45:34'),(81,8,'Morbi tempor tortor sed elit lacinia, eu ullamcorper massa tincidunt','2016-06-29 00:00:00',0,'C','2016-06-22 15:36:54'),(82,8,'Integer quis est sed nulla aliquet rutrum et eget lacus','2016-06-30 00:00:00',0,'O','0000-00-00 00:00:00'),(83,8,'Integer ut velit quis neque elementum pharetra non sed ligula.','2016-07-04 00:00:00',0,'C','2016-06-22 15:36:54'),(84,9,'Identify resources to be monitored.','2016-06-23 00:00:00',0,'O','0000-00-00 00:00:00'),(85,9,'Define users and workflow','2016-06-25 00:00:00',0,'O','0000-00-00 00:00:00'),(86,9,'Identify event sources by resource type.','2016-06-24 00:00:00',0,'O','0000-00-00 00:00:00'),(87,9,'Define the relationship between resources and business systems','2016-06-26 00:00:00',0,'O','0000-00-00 00:00:00'),(88,9,'Identify tasks and URLs by resource type','2016-06-30 00:00:00',0,'O','0000-00-00 00:00:00'),(89,10,'Define the server configuration.','2016-06-23 00:00:00',10,'C','2016-07-04 07:18:51'),(90,10,'Identify the implementation team.','2016-06-23 00:00:00',12,'C','2016-07-04 07:18:54'),(91,10,'Order the server hardware for production as well as test/quality assurance (QA).','2016-06-30 00:00:00',15,'C','2016-07-04 07:22:37'),(92,11,'Set up and test jobs on the database server to produce the database backup.','2016-06-27 00:00:00',0,'A','0000-00-00 00:00:00'),(93,11,'Set up and test jobs to copy backup databases to the history server','2016-06-30 00:00:00',0,'A','0000-00-00 00:00:00'),(94,11,'Set up and test jobs to replicate events to the history server','2016-06-29 00:00:00',0,'C','2016-06-24 07:18:01'),(95,12,'Install the Tivoli Business Systems Manager health monitor software.','2016-07-01 00:00:00',0,'C','2016-07-04 07:29:29'),(96,12,'Customize the health monitor to match your environment.','2016-07-05 00:00:00',0,'C','2016-07-04 07:29:29'),(97,12,'Test the health monitor client functions.','2016-07-12 00:00:00',0,'A','0000-00-00 00:00:00'),(101,13,'Tivoli Business Systems Manager SQL server jobs','2016-07-07 00:00:00',10,'O','0000-00-00 00:00:00'),(102,13,'Customize the health monitor to match your environment','2016-08-18 00:00:00',20,'O','0000-00-00 00:00:00'),(103,13,'Test the health monitor client functions','2016-08-17 00:00:00',10,'O','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `event_task_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_interest`
--

DROP TABLE IF EXISTS `events_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_interest` (
  `events_interest_id` int(11) NOT NULL AUTO_INCREMENT,
  `events_id_ref` int(11) NOT NULL,
  `member_id_ref` int(11) NOT NULL,
  `date_send_interest` datetime NOT NULL,
  PRIMARY KEY (`events_interest_id`),
  KEY `events_id_ref_idx` (`events_id_ref`),
  KEY `member_id_ref_idx` (`member_id_ref`),
  CONSTRAINT `events_id_ref` FOREIGN KEY (`events_id_ref`) REFERENCES `event_tab` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `member_id_ref` FOREIGN KEY (`member_id_ref`) REFERENCES `dgfg_member_tab` (`mem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_interest`
--

LOCK TABLES `events_interest` WRITE;
/*!40000 ALTER TABLE `events_interest` DISABLE KEYS */;
INSERT INTO `events_interest` VALUES (4,5,2,'2016-06-18 04:16:04'),(5,6,2,'2016-06-18 04:39:14'),(6,7,1,'2016-06-20 04:21:34'),(7,8,1,'2016-06-20 04:47:24'),(8,5,1,'2016-06-20 05:26:43'),(12,7,3,'2016-06-22 12:47:59'),(13,8,4,'2016-06-22 15:28:28'),(14,7,5,'2016-06-24 07:01:49'),(15,11,5,'2016-06-24 07:16:17'),(16,10,4,'2016-07-04 05:37:08'),(17,10,5,'2016-07-04 05:38:19');
/*!40000 ALTER TABLE `events_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_role_map`
--

DROP TABLE IF EXISTS `events_role_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_role_map` (
  `tab_id` int(11) NOT NULL,
  `event_id_ref` int(11) NOT NULL,
  `role_id_ref` int(11) NOT NULL,
  `selected_date` datetime NOT NULL,
  PRIMARY KEY (`tab_id`),
  KEY `event_id_ref_idx` (`event_id_ref`),
  KEY `role_id_ref_idx` (`role_id_ref`),
  CONSTRAINT `event_id_ref` FOREIGN KEY (`event_id_ref`) REFERENCES `event_tab` (`event_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `role_id_ref` FOREIGN KEY (`role_id_ref`) REFERENCES `dgfg_leader_tab` (`led_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_role_map`
--

LOCK TABLES `events_role_map` WRITE;
/*!40000 ALTER TABLE `events_role_map` DISABLE KEYS */;
INSERT INTO `events_role_map` VALUES (1,2,1,'2016-06-09 09:47:18'),(2,1,1,'2016-06-09 10:13:16'),(3,4,2,'2016-06-10 13:00:04'),(4,4,2,'2016-06-10 13:23:34'),(5,6,2,'2016-06-18 14:43:31'),(6,5,1,'2016-06-19 14:29:11'),(7,7,2,'2016-06-20 14:29:11'),(8,8,1,'2016-06-23 01:26:16'),(9,11,2,'2016-06-24 17:11:52'),(10,10,1,'2016-07-04 15:04:55'),(11,12,1,'2016-07-04 17:25:28');
/*!40000 ALTER TABLE `events_role_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_tab`
--

DROP TABLE IF EXISTS `notification_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_tab` (
  `notify_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_recive_role` tinyint(1) NOT NULL,
  `notification_receive_id` int(11) NOT NULL,
  `notification_sent_role` tinyint(1) NOT NULL,
  `notification_sent_id` int(11) NOT NULL,
  `notification_sent_date` datetime NOT NULL,
  `is_watch` tinytext NOT NULL,
  `notification_msg` varchar(100) NOT NULL,
  PRIMARY KEY (`notify_id`),
  KEY `notification_recive_role_idx` (`notification_recive_role`),
  KEY `notification_sent_role_idx` (`notification_sent_role`),
  CONSTRAINT `notification_recive_role` FOREIGN KEY (`notification_recive_role`) REFERENCES `user_roles` (`role_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `notification_sent_role` FOREIGN KEY (`notification_sent_role`) REFERENCES `user_roles` (`role_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_tab`
--

LOCK TABLES `notification_tab` WRITE;
/*!40000 ALTER TABLE `notification_tab` DISABLE KEYS */;
INSERT INTO `notification_tab` VALUES (8,2,1,4,3,'2016-06-21 15:57:07','Y','Wellness survey completed by: Cynthia Hart'),(9,2,1,3,2,'2016-06-21 16:49:09','Y','Event created by: Alexander Harper'),(10,3,2,2,1,'2016-06-22 10:49:32','Y','Beneath The Words - has approved'),(11,3,2,2,1,'2016-06-22 11:05:38','Y','Trivia Tuesdayshas rejected'),(12,4,3,3,2,'2016-06-22 12:41:52','N','You have project invitation'),(13,3,2,4,3,'2016-06-22 22:48:12','Y','Your invitation accepted for event- Te Aro P? Walking Tour'),(14,4,3,3,2,'2016-06-22 23:08:17','N','You have 1 event assigned'),(15,4,3,3,2,'2016-06-22 23:59:40','N','You have assigned 1 tasks'),(16,4,4,3,1,'2016-06-23 01:26:34','N','You have project invitation'),(17,2,1,4,4,'2016-06-23 01:28:04','N','Wellness survey completed by: Tia Fraser'),(18,3,1,4,4,'2016-06-23 01:28:28','N','Your invitation accepted for event- The 39 Steps'),(19,4,4,3,1,'2016-06-23 01:29:15','N','You have 1 event assigned'),(20,4,4,3,1,'2016-06-23 01:29:32','N','You have assigned 2 tasks'),(21,3,1,4,4,'2016-06-23 01:37:23','N','2 Task completed by: Tia Fraser'),(22,2,1,4,5,'2016-06-24 14:21:51','Y','Wellness survey completed by: Jack Plant'),(23,4,5,3,2,'2016-06-24 17:00:33','Y','You have project invitation'),(24,3,2,4,5,'2016-06-24 17:01:49','Y','Your invitation accepted for event- Te Aro P? Walking Tour'),(25,4,5,3,2,'2016-06-24 17:02:28','Y','You have 1 event assigned'),(26,4,5,3,2,'2016-06-24 17:12:10','Y','You have project invitation'),(27,3,2,4,5,'2016-06-24 17:16:17','N','Your invitation accepted for event- Green Living'),(28,4,5,3,2,'2016-06-24 17:16:56','Y','You have 1 event assigned'),(29,4,5,3,2,'2016-06-24 17:17:15','Y','You have assigned 3 tasks'),(30,3,2,4,5,'2016-06-24 17:18:01','N','1 Task completed by: Jack Plant'),(31,4,5,3,1,'2016-07-04 15:29:18','Y','You have project invitation'),(32,4,6,3,1,'2016-07-04 15:29:33','N','You have project invitation'),(33,4,4,3,1,'2016-07-04 15:29:51','N','You have project invitation'),(34,3,1,4,4,'2016-07-04 15:37:08','N','Your invitation accepted for event- Beneath The Words'),(35,3,1,4,5,'2016-07-04 15:38:19','N','Your invitation accepted for event- Beneath The Words'),(36,4,4,3,1,'2016-07-04 15:39:15','Y','You have 1 event assigned'),(37,4,4,3,1,'2016-07-04 15:39:28','N','You have 1 event assigned'),(38,4,4,3,1,'2016-07-04 15:39:38','N','You have 1 event assigned'),(39,4,5,3,1,'2016-07-04 15:39:49','N','You have 1 event assigned'),(40,4,5,3,1,'2016-07-04 15:45:43','N','You have assigned 3 tasks'),(42,3,1,4,5,'2016-07-04 17:19:06','N','2 Task completed by: Jack Plant'),(43,3,1,4,5,'2016-07-04 17:22:37','N','1 Task completed by: Jack Plant'),(44,3,1,3,1,'2016-07-04 17:29:16','N','You have assigned 3 tasks'),(45,3,1,3,1,'2016-07-04 17:29:29','N','2 Task completed by: Aaron Kemp');
/*!40000 ALTER TABLE `notification_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `private_msg`
--

DROP TABLE IF EXISTS `private_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `private_msg` (
  `msg_id` int(11) NOT NULL,
  `msg_reply_id` int(11) DEFAULT NULL,
  `msg_title` varchar(200) NOT NULL,
  `msg_body` text NOT NULL,
  `msg_date` datetime NOT NULL,
  PRIMARY KEY (`msg_id`),
  KEY `msg_reply_id_idx` (`msg_reply_id`),
  CONSTRAINT `msg_reply_id` FOREIGN KEY (`msg_reply_id`) REFERENCES `private_msg` (`msg_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `private_msg`
--

LOCK TABLES `private_msg` WRITE;
/*!40000 ALTER TABLE `private_msg` DISABLE KEYS */;
INSERT INTO `private_msg` VALUES (1,NULL,'Test Message','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam molestie purus ut urna rutrum pulvinar. Suspendisse porttitor aliquam nunc et egestas. Pellentesque tempor lorem vitae nunc bibendum imperdiet. Donec ac hendrerit <strong>quam, sit amet consectetur sapien. Etiam varius erat nec posuere sagittis. Duis ultricies vestibulum facilisis. Nullam enim diam, posuere ut suscipit et, euismod a urna</strong>. Morbi at sollicitudin nulla, in facilisis justo.</p>','2016-07-07 00:13:48'),(2,NULL,'Hand Drawn: A Relaxed Yet Artistic Style','<p>Hand drawn blogs are all over the place. Some of these designs are very doodle-esque, while others are more like fine art. They can lend a more relaxed feeling to a blog&rsquo;s design, as well as a healthy dose of creativity. One of the best parts of this style is that it&rsquo;s so customizable. The illustrations used can really reflect the style of the owners or writers.</p>\\r\\n\\r\\n<p><img alt=\\\"\\\" src=\\\"https://newevolutiondesigns.com/images/freebies/cool-wallpaper-1.jpg\\\" style=\\\"height:175px; width:280px\\\" /></p>','2016-07-07 05:05:09'),(3,NULL,'Samsung\'s new 256GB microSD card','<p><img alt=\"\" src=\"http://i.amz.mshcdn.com/U5JLUW7nBMEB68jhEYxvo64RNXI=/950x534/https%3A%2F%2Fblueprint-api-production.s3.amazonaws.com%2Fuploads%2Fcard%2Fimage%2F137732%2Fsamsung-ufs-microsd-card.jpg\" style=\"float:right; height:112px; width:200px\" /></p>\r\n\r\n<p>Samsung&#39;s new UFS (Universal Flash Storage) microSD cards come in 32, 64, 128 and 256GB capacities.</p>\r\n\r\n<p>Designed for high-resolution 3D gaming and high-resolution movies, the cards boast read speeds of 530 megabytes per second (MB/s), which is about five times faster than most microSD cards. The cards also have write speeds of 170MB/s.</p>\r\n\r\n<p>In comparison, SanDisk&#39;s week-old &quot;world&#39;s fastest 256GB microSD card&quot; (classified as UFS-1 speed) has a mere 100MB/s read speed and 90MB/s write speed.</p>\r\n\r\n<p><a href=\"http://mashable.com/2015/03/02/sandisk-crammed-200gb-into-a-fingernail-sized-microsd-card/\">SanDisk crammed 200GB into a fingernail-sized microSD card</a></p>','2016-07-08 01:16:21'),(4,3,'Samsung\'s new 256GB microSD card','<p>Suspendisse ac nulla ut massa iaculis condimentum vitae ultrices ex. Pellentesque id sapien tincidunt, blandit elit eu, tristique odio. In sed felis metus. Etiam varius lacinia massa eu scelerisque. Ut at justo rhoncus, euismod dui at, imperdiet lacus. Aenean suscipit condimentum ante,&nbsp;</p>','2016-07-11 04:20:39'),(5,3,'Samsung\'s new 256GB microSD card','<p>lacus. Aenean suscipit condimentum ante, a mollis dui hendrerit ac. Donec pretium quam vel magna dignissim congue. Integer ut odio accumsan, tempor sem ac, accumsan diam.</p>','2016-07-11 04:31:22'),(6,3,'Samsung\'s new 256GB microSD card','<p>elementum quam sodales. Nulla in lobortis lorem. Vivamus sollicitudin tellus ac quam porttitor, sed eleifend turpis bibendum. Donec sed cursus purus.</p>','2016-07-11 05:05:43'),(7,1,'Test Message','<p>cvbbxcxc</p>','2016-07-11 05:54:00'),(8,NULL,'Somewhere I Belong','<p>asasdasdasd</p>','2016-07-13 03:08:50');
/*!40000 ALTER TABLE `private_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `private_msg_users`
--

DROP TABLE IF EXISTS `private_msg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `private_msg_users` (
  `msg_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id_ref` int(11) NOT NULL,
  `sender_email` varchar(255) NOT NULL,
  `receiver_email` varchar(255) NOT NULL,
  `is_watch` tinytext NOT NULL,
  PRIMARY KEY (`msg_user_id`),
  KEY `msg_id_ref_idx` (`msg_id_ref`),
  CONSTRAINT `msg_id_ref` FOREIGN KEY (`msg_id_ref`) REFERENCES `private_msg` (`msg_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `private_msg_users`
--

LOCK TABLES `private_msg_users` WRITE;
/*!40000 ALTER TABLE `private_msg_users` DISABLE KEYS */;
INSERT INTO `private_msg_users` VALUES (1,1,'jamiecraig@inbound.plus','tiafraser@gmail.com','N'),(2,1,'jamiecraig@inbound.plus','jackpplant@gmail.com','Y'),(3,1,'jamiecraig@inbound.plus','aaronkemp@inbound.plus','N'),(4,2,'jamiecraig@inbound.plus','tiafraser@gmail.com','N'),(5,2,'jamiecraig@inbound.plus','aaronkemp@inbound.plus','N'),(6,3,'jamiecraig@inbound.plus','tiafraser@gmail.com','N'),(7,3,'jamiecraig@inbound.plus','lisarcrowe@gmail.com','N'),(8,3,'jamiecraig@inbound.plus','jackpplant@gmail.com','Y'),(9,4,'tiafraser@gmail.com','jamiecraig@inbound.plus','N'),(10,4,'tiafraser@gmail.com','lisarcrowe@gmail.com','N'),(11,4,'tiafraser@gmail.com','jackpplant@gmail.com','N'),(12,5,'tiafraser@gmail.com','jamiecraig@inbound.plus','N'),(13,5,'tiafraser@gmail.com','lisarcrowe@gmail.com','N'),(14,5,'tiafraser@gmail.com','jackpplant@gmail.com','N'),(15,6,'jackpplant@gmail.com','jamiecraig@inbound.plus','Y'),(16,6,'jackpplant@gmail.com','tiafraser@gmail.com','N'),(17,6,'jackpplant@gmail.com','lisarcrowe@gmail.com','N'),(18,7,'jackpplant@gmail.com','jamiecraig@inbound.plus','N'),(19,7,'jackpplant@gmail.com','tiafraser@gmail.com','N'),(20,7,'jackpplant@gmail.com','aaronkemp@inbound.plus','N'),(21,8,'jamiecraig@inbound.plus','lisarcrowe@gmail.com','N');
/*!40000 ALTER TABLE `private_msg_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rewards`
--

DROP TABLE IF EXISTS `rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rewards` (
  `rewads_id` int(11) NOT NULL AUTO_INCREMENT,
  `re_user_role` tinyint(1) NOT NULL,
  `re_user_id` int(11) NOT NULL,
  `reward_des` varchar(250) DEFAULT NULL,
  `reward` int(11) NOT NULL,
  `reward_date` datetime NOT NULL,
  PRIMARY KEY (`rewads_id`),
  KEY `re_user_role_idx` (`re_user_role`),
  CONSTRAINT `re_user_role` FOREIGN KEY (`re_user_role`) REFERENCES `user_roles` (`role_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rewards`
--

LOCK TABLES `rewards` WRITE;
/*!40000 ALTER TABLE `rewards` DISABLE KEYS */;
INSERT INTO `rewards` VALUES (1,4,5,'89 Task Complete',10,'2016-07-04 07:18:51'),(2,4,5,'90 Task Complete',12,'2016-07-04 07:18:54'),(3,4,5,'91 Task Complete',15,'2016-07-04 07:22:37'),(4,3,1,'95 Task Complete',0,'2016-07-04 07:29:29'),(5,3,1,'96 Task Complete',0,'2016-07-04 07:29:29'),(6,3,2,'Survey id: 2 Survey Done',7,'2016-07-15 03:23:42'),(7,3,1,'Survey id: 1 Survey Done',5,'2016-07-15 04:01:43'),(8,4,4,'Survey id: 1 Survey Done',5,'2016-07-15 04:04:53');
/*!40000 ALTER TABLE `rewards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selected_members`
--

DROP TABLE IF EXISTS `selected_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selected_members` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sel_mem_id_ref` int(11) NOT NULL,
  `sel_event_id_ref` int(11) NOT NULL,
  `selected_by` varchar(45) NOT NULL,
  `selected_role` tinyint(1) NOT NULL,
  `selected_date` datetime NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `sel_mem_id_ref_idx` (`sel_mem_id_ref`),
  KEY `sel_event_id_ref_idx` (`sel_event_id_ref`),
  CONSTRAINT `sel_event_id_ref` FOREIGN KEY (`sel_event_id_ref`) REFERENCES `event_tab` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sel_mem_id_ref` FOREIGN KEY (`sel_mem_id_ref`) REFERENCES `dgfg_member_tab` (`mem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selected_members`
--

LOCK TABLES `selected_members` WRITE;
/*!40000 ALTER TABLE `selected_members` DISABLE KEYS */;
INSERT INTO `selected_members` VALUES (1,1,1,'aaronkemp@inbound.plus',0,'2016-06-09 04:57:24'),(3,1,2,'aaronkemp@inbound.plus',0,'2016-06-09 04:59:16'),(4,2,2,'aaronkemp@inbound.plus',0,'2016-06-09 05:03:17'),(15,2,5,'aaronkemp@inbound.plus',3,'2016-06-20 13:10:37'),(16,1,7,'alexanderharper@inbound.plus',3,'2016-06-20 13:37:08'),(17,3,7,'alexanderharper@inbound.plus',3,'2016-06-22 13:07:44'),(18,4,8,'aaronkemp@inbound.plus',3,'2016-06-22 15:29:15'),(19,5,7,'alexanderharper@inbound.plus',3,'2016-06-24 07:02:28'),(20,5,11,'alexanderharper@inbound.plus',3,'2016-06-24 07:16:56'),(21,4,10,'aaronkemp@inbound.plus',3,'2016-07-04 05:39:15'),(24,5,10,'aaronkemp@inbound.plus',3,'2016-07-04 05:39:49');
/*!40000 ALTER TABLE `selected_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_tab`
--

DROP TABLE IF EXISTS `service_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_tab` (
  `sp_id` int(11) NOT NULL,
  `sp_name` varchar(45) NOT NULL,
  `sp_cat` int(11) NOT NULL,
  `sp_summery` varchar(500) DEFAULT NULL,
  `sp_cost` varchar(45) DEFAULT NULL,
  `sp_phn` varchar(45) DEFAULT NULL,
  `sp_key` varchar(45) DEFAULT NULL,
  `sp_list` varchar(500) DEFAULT NULL,
  `sp_add` varchar(150) DEFAULT NULL,
  `sp_hrs` varchar(45) DEFAULT NULL,
  `sp_after_phn` varchar(45) DEFAULT NULL,
  `sp_img` varchar(150) DEFAULT NULL,
  `sp_staff_img` varchar(150) DEFAULT NULL,
  `sp_web` varchar(500) DEFAULT NULL,
  `sp_level` int(11) NOT NULL,
  `sp_create_by` varchar(45) NOT NULL,
  `sp_create_role` tinyint(1) NOT NULL,
  `sp_create_date` datetime NOT NULL,
  `sp_mod_by` varchar(45) NOT NULL,
  `sp_mod_role` tinyint(1) NOT NULL,
  `sp_mod_date` datetime NOT NULL,
  PRIMARY KEY (`sp_id`),
  KEY `sp_cat_idx` (`sp_cat`),
  CONSTRAINT `sp_cat` FOREIGN KEY (`sp_cat`) REFERENCES `wellnes_sections` (`wellness_sec_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_tab`
--

LOCK TABLES `service_tab` WRITE;
/*!40000 ALTER TABLE `service_tab` DISABLE KEYS */;
INSERT INTO `service_tab` VALUES (1,'AIMHI Alternative Education  Consortium',2,'dapibus arcu a ultrices iaculis. Donec sed ex ultrices, pretium sapien id, vehicula enim. Nam euismod luctus imperdiet. Ut pharetra volutpat tellus a tempor. Praesent ut sem ut lorem hendrerit laoreet. Duis quam leo, cursus quis odio eu, mollis dapibus justo.','Free','09-270-8066',' Moana Taane','Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\nDuis vitae augue tempus, viverra felis quis, suscipit felis.\r\nSed mattis elit nec nulla vehicula, non lobortis urna fermentum.\r\nNullam ac nunc sagittis, facilisis ipsum id, tristique lorem.','10 Beatty Street, Otahuhu, Auckland, New Zealand','8am - 5pm','0800 214 222','3884b167d04ec7df38d0b9e89a1580ff.jpg','0239c79a833885a0b7192bba6e75d154.jpg','http://mtaane@aimhi.ac.nz',1,'jamiecraig@inbound.plus',2,'2016-06-17 11:19:35','jamiecraig@inbound.plus',2,'2016-06-17 11:19:35'),(2,'Rainbow Youth',4,'Morbi sed ex eget libero dictum hendrerit et non nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu ipsum eu sapien tempor tristique et viverra massa. Nulla magna nisl,','100$ - For one engagement','09-376-4155','Tanu Gago','Fusce venenatis velit nec placerat ullamcorper.\r\nPhasellus ac felis iaculis, tempus ligula id, interdum elit.','281 Karangahape Road, Auckland, New Zealand','Monday - Friday 10AM - 6PM','021-162-7393','38a6ca4ec1c5e7ff357056fde72bae9a.jpg','a8362d20f5b191fcb7a179f03eaa691e.jpg','http://tanu.gago@RY.org.nz ',2,'jamiecraig@inbound.plus',2,'2016-06-17 11:23:24','jamiecraig@inbound.plus',2,'2016-06-17 11:23:24'),(3,'AKL Council - Leisure Services',4,'Sed purus augue, rhoncus vel faucibus id, lobortis id nisi. Vivamus vel tempor erat, scelerisque pulvinar nisi. Donec at posuere mauris. Praesent hendrerit magna et leo porttitor, posuere faucibus metus molestie','Free',' 09-275-8979','Malaina Vaotuua','Sed quis nisi dictum, molestie lorem et, efficitur odio.\r\nSed lacinia leo quis enim semper fermentum.\r\nNam sagittis massa ac vestibulum dignissim.','Mascot Ave, Mangere, Auckland, New Zealand','Monday - Friday 8AM - 3PM','','0652210fb1feca28c4e4da7b45b5c88c.png','fd12bddbd4ac3b7391daeac17ac7617d.png','http:// malaina.vaotu@aucklandcouncil.govt.nz',1,'jamiecraig@inbound.plus',2,'2016-06-17 12:07:01','jamiecraig@inbound.plus',2,'2016-06-17 14:53:10'),(4,'Youth Law',11,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.','$40','09-250-2670','Mira Taitz','Mauris pharetra ex a mauris accumsan, nec efficitur mi tincidunt.\r\nMauris id ipsum malesuada, dapibus ligula eget, feugiat eros.\r\nNulla eleifend ex sed ultricies aliquet.','145 St George St, Papatoetoe, Auckland, New Zealand','Monday - Friday 8AM - 3PM','','1c8c0b601bd434ab6f81743e04ed6cf0.jpg','7d494b4d20af039d019a31ed43f8e7c9.jpg','http://mira@youthlaw.co.nz',2,'jamiecraig@inbound.plus',2,'2016-06-17 12:41:43','jamiecraig@inbound.plus',2,'2016-06-17 12:41:43'),(5,'Vaka Tautua',6,'Vaka Tautua is a charitable organisation that aims to help improve the health and wellbeing of Pacific people in New Zealand.','Free','09-250 -1812','Alphonso Afoa-Scotter','Foster resilience and recovery\r\nPlace individuals and families at its core\r\nProvide culturally effective support\r\nEnable individuals with a disability or mental illness to live, work, learn, and participate fully in communities of their choice','586 Great South Road, Manukau, Auckland, New Zealand','Monday - Friday 8AM - 3PM','0800 214 222','0f80ef1250e187e44cc40b5022a2a1c5.jpg','f63ccdf7f84767fe5e9f0156f526f56f.jpg','http://Alphonso.A@vakatautua.co.nz',1,'jamiecraig@inbound.plus',2,'2016-06-17 12:45:53','jamiecraig@inbound.plus',2,'2016-06-17 12:45:53'),(6,'Solomon Group',1,'sollicitudin ultricies hendrerit. Proin accumsan interdum odio, sit amet gravida erat efficitur ut. Mauris luctus, turpis vitae tincidunt dictum, velit eros posuere odio, nec rutrum magna orci quis tortor.','Negotiable','09-268-9210 ','Kiri Solomon','Donec nec massa et orci iaculis condimentum.\r\nAliquam imperdiet massa ut quam luctus, id accumsan velit convallis.\r\nDonec mattis felis quis tellus elementum, in pellentesque lacus vulputate.','236 Great South Rd, Manurewa, Auckland, New Zealand','Monday - Friday 8AM - 5PM','','d59ec6d03a1f5591a866eea0f025b886.jpg','d94ba8a7ddf154eb4045018a75094700.jpg','http://kiri@solomongroup.co.nz',1,'jamiecraig@inbound.plus',2,'2016-06-17 12:52:45','jamiecraig@inbound.plus',2,'2016-06-17 12:52:45'),(7,'TYLA Youth Trust Otara',6,'Donec vel purus augue. Sed gravida et lectus pulvinar faucibus. Vivamus lobortis quis enim vitae egestas. Quisque maximus vel lorem nec mattis.','Free','09-271-6098','Anastasia Meredith','Donec sed est eleifend ante vulputate \r\ncondimentum sagittis vitae felis. \r\nCras sagittis feugiat luctus.\r\nPhasellus quam felis, feugiat sit amet nisi aliquet, malesuada aliquet orci.','121 Bairds Road, Otara, Auckland, New Zealand','Monday - Friday 10AM - 6PM','021-164-9119','','','http://anastasia@tyla.org.nz',2,'jamiecraig@inbound.plus',2,'2016-06-17 12:57:22','jamiecraig@inbound.plus',2,'2016-06-17 12:57:22');
/*!40000 ALTER TABLE `service_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_answers`
--

DROP TABLE IF EXISTS `survey_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_answers` (
  `survey_ans_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_no_ref` int(11) NOT NULL,
  `survey_question_ref` int(11) NOT NULL,
  `survey_ans` int(11) NOT NULL,
  PRIMARY KEY (`survey_ans_id`),
  KEY `survey_no_ref_idx` (`survey_no_ref`),
  KEY `survey_question_ref_idx` (`survey_question_ref`),
  CONSTRAINT `survey_no_ref` FOREIGN KEY (`survey_no_ref`) REFERENCES `survey_map` (`survey_map_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `survey_question_ref` FOREIGN KEY (`survey_question_ref`) REFERENCES `wellness_quesions` (`wellness_quesions_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=599 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_answers`
--

LOCK TABLES `survey_answers` WRITE;
/*!40000 ALTER TABLE `survey_answers` DISABLE KEYS */;
INSERT INTO `survey_answers` VALUES (183,1,1,5),(184,1,2,4),(185,1,3,3),(186,1,4,3),(187,1,5,2),(188,1,6,2),(189,1,7,4),(190,1,8,5),(191,1,9,2),(192,1,10,2),(193,1,11,4),(194,1,12,4),(195,1,13,2),(196,1,14,3),(197,1,15,4),(198,1,16,2),(199,1,17,4),(200,1,18,2),(201,1,19,4),(202,1,20,2),(203,1,21,2),(204,1,22,5),(205,1,23,3),(206,1,24,4),(207,1,25,4),(208,1,26,4),(235,2,1,3),(236,2,2,3),(237,2,3,1),(238,2,4,1),(239,2,5,5),(240,2,6,5),(241,2,7,3),(242,2,8,2),(243,2,9,1),(244,2,10,1),(245,2,11,1),(246,2,12,1),(247,2,13,1),(248,2,14,1),(249,2,15,4),(250,2,16,2),(251,2,17,5),(252,2,18,4),(253,2,19,3),(254,2,20,4),(255,2,21,3),(256,2,22,4),(257,2,23,2),(258,2,24,2),(259,2,25,1),(260,2,26,1),(261,3,1,3),(262,3,2,3),(263,3,3,5),(264,3,4,5),(265,3,5,2),(266,3,6,2),(267,3,7,5),(268,3,8,5),(269,3,9,2),(270,3,10,3),(271,3,11,5),(272,3,12,5),(273,3,13,5),(274,3,14,4),(275,3,15,4),(276,3,16,4),(277,3,17,5),(278,3,18,4),(279,3,19,5),(280,3,20,4),(281,3,21,3),(282,3,22,2),(283,3,23,4),(284,3,24,5),(285,3,25,4),(286,3,26,2),(287,4,1,3),(288,4,2,3),(289,4,3,1),(290,4,4,1),(291,4,5,2),(292,4,6,2),(293,4,7,2),(294,4,8,2),(295,4,9,1),(296,4,10,1),(297,4,11,1),(298,4,12,1),(299,4,13,1),(300,4,14,1),(301,4,15,1),(302,4,16,1),(303,4,17,2),(304,4,18,2),(305,4,19,2),(306,4,20,2),(307,4,21,2),(308,4,22,2),(309,4,23,1),(310,4,24,1),(311,4,25,1),(312,4,26,1),(521,5,1,5),(522,5,2,5),(523,5,3,4),(524,5,4,4),(525,5,5,1),(526,5,6,1),(527,5,7,3),(528,5,8,3),(529,5,9,4),(530,5,10,3),(531,5,11,3),(532,5,12,1),(533,5,13,1),(534,5,14,1),(535,5,15,2),(536,5,16,4),(537,5,17,3),(538,5,18,2),(539,5,19,4),(540,5,20,3),(541,5,21,2),(542,5,22,4),(543,5,23,1),(544,5,24,3),(545,5,25,4),(546,5,26,4),(547,6,1,5),(548,6,2,3),(549,6,3,4),(550,6,4,3),(551,6,5,2),(552,6,6,1),(553,6,7,4),(554,6,8,2),(555,6,9,2),(556,6,10,1),(557,6,11,4),(558,6,12,2),(559,6,13,3),(560,6,14,2),(561,6,15,2),(562,6,16,4),(563,6,17,3),(564,6,18,5),(565,6,19,3),(566,6,20,1),(567,6,21,2),(568,6,22,3),(569,6,23,4),(570,6,24,4),(571,6,25,2),(572,6,26,3),(573,7,1,1),(574,7,2,1),(575,7,3,2),(576,7,4,3),(577,7,5,5),(578,7,6,5),(579,7,7,5),(580,7,8,4),(581,7,9,3),(582,7,10,3),(583,7,11,1),(584,7,12,1),(585,7,13,1),(586,7,14,1),(587,7,15,4),(588,7,16,3),(589,7,17,5),(590,7,18,4),(591,7,19,5),(592,7,20,5),(593,7,21,5),(594,7,22,5),(595,7,23,3),(596,7,24,5),(597,7,25,5),(598,7,26,5);
/*!40000 ALTER TABLE `survey_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_map`
--

DROP TABLE IF EXISTS `survey_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_map` (
  `survey_map_id` int(11) NOT NULL,
  `survey_map_user` varchar(255) NOT NULL,
  `survey_date` datetime NOT NULL,
  PRIMARY KEY (`survey_map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_map`
--

LOCK TABLES `survey_map` WRITE;
/*!40000 ALTER TABLE `survey_map` DISABLE KEYS */;
INSERT INTO `survey_map` VALUES (1,'aaronkemp@inbound.plus','2016-06-15 07:08:55'),(2,'alexanderharper@inbound.plus','2016-06-16 01:32:09'),(3,'denisecsweeney@inbound.plus','2016-06-16 03:40:25'),(4,'elisestephenson@inbound.plus','2016-06-20 03:09:42'),(5,'cynthiah@gmail.coms','2016-06-21 05:56:59'),(6,'tiafraser@gmail.com','2016-06-22 15:28:04'),(7,'jackpplant@gmail.com','2016-06-24 04:21:51');
/*!40000 ALTER TABLE `survey_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `role_code` tinyint(1) NOT NULL,
  `role_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'super-admin'),(2,'coordinator'),(3,'leader'),(4,'member');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tab`
--

DROP TABLE IF EXISTS `user_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tab` (
  `user_id` int(11) NOT NULL,
  `user_role_ref` tinyint(1) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pwd` varchar(32) NOT NULL,
  `active_stat` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `role_create` varchar(45) NOT NULL,
  `rcode_create` tinyint(1) NOT NULL,
  `date_modify` datetime NOT NULL,
  `role_mod` varchar(45) NOT NULL,
  `rcode_mod` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_role_ref_idx` (`user_role_ref`),
  CONSTRAINT `user_role_ref` FOREIGN KEY (`user_role_ref`) REFERENCES `user_roles` (`role_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tab`
--

LOCK TABLES `user_tab` WRITE;
/*!40000 ALTER TABLE `user_tab` DISABLE KEYS */;
INSERT INTO `user_tab` VALUES (1,1,'super@gmail.com','cc03e747a6afbbcbf8be7668acfebee5',1,'2016-05-11 10:00:00','super',1,'2016-05-11 10:00:00','super',1),(2,2,'jamiecraig@inbound.plus','cc03e747a6afbbcbf8be7668acfebee5',1,'2016-05-11 10:32:34','super@gmail.com',1,'2016-05-30 17:31:21','jamiecraig@inbound.plus',2),(3,2,'niamhblackburn@inbound.plus','cc03e747a6afbbcbf8be7668acfebee5',1,'2016-05-11 10:33:56','super@gmail.com',1,'2016-05-11 10:33:56','super@gmail.com',1),(4,3,'aaronkemp@inbound.plus','e99a18c428cb38d5f260853678922e03',1,'2016-05-11 10:48:18','super@gmail.com',1,'2016-05-31 10:24:16','jamiecraig@inbound.plus',2),(5,3,'alexanderharper@inbound.plus','e99a18c428cb38d5f260853678922e03',1,'2016-05-11 10:49:43','super@gmail.com',1,'2016-05-31 11:42:06','',0),(6,4,'elisestephenson@inbound.plus','632a7986caabc711b49ef5ba86bfff4b',1,'2016-05-11 10:51:17','super@gmail.com',1,'2016-06-20 13:08:08','jamiecraig@inbound.plus',2),(7,2,'aarondavis@inbound.plus','53d171d430fe26e6773cde2161f551fc',1,'2016-05-11 16:33:16','super@gmail.com',1,'2016-05-13 14:30:10','super@gmail.com',1),(8,2,'WilliamMCampbell@inbound.plus','e4a6222cdb5b34375400904f03d8e6a5',1,'2016-05-12 14:39:35','super@gmail.com',1,'2016-05-12 14:39:35','super@gmail.com',1),(9,4,'denisecsweeney@inbound.plus','a9deecb03c239120e2b74951becc72f6',1,'2016-06-09 14:12:34','jamiecraig@inbound.plus',2,'2016-06-09 14:12:34','jamiecraig@inbound.plus',2),(10,4,'cynthiah@gmail.coms','8c3bdbbb62a223bc3d00b079c40a30df',1,'2016-06-19 09:03:17','jamiecraig@inbound.plus',2,'2016-06-19 09:03:17','jamiecraig@inbound.plus',2),(11,4,'tiafraser@gmail.com','6b5fbf288fbf074542018af78445d932',1,'2016-06-19 09:04:28','jamiecraig@inbound.plus',2,'2016-06-19 09:04:28','jamiecraig@inbound.plus',2),(12,4,'jackpplant@gmail.com','a07dc682a60c1f45c871cfb3ca0ce967',1,'2016-06-24 10:30:02','jamiecraig@inbound.plus',2,'2016-06-24 10:30:02','jamiecraig@inbound.plus',2),(13,3,'lisarcrowe@gmail.com','53d171d430fe26e6773cde2161f551fc',1,'2016-06-27 09:55:21','jamiecraig@inbound.plus',2,'2016-06-27 09:55:42','jamiecraig@inbound.plus',2),(14,3,'ramonakhughes@gmail.com','e4a6222cdb5b34375400904f03d8e6a5',1,'2016-06-27 15:51:10','jamiecraig@inbound.plus',2,'2016-06-27 16:18:55','jamiecraig@inbound.plus',2),(15,4,'scottdavies@gmail.com','7c973a94a1050241cc9efb12f3177462',1,'2016-06-27 15:58:17','jamiecraig@inbound.plus',2,'2016-06-27 15:58:17','jamiecraig@inbound.plus',2);
/*!40000 ALTER TABLE `user_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wellnes_sections`
--

DROP TABLE IF EXISTS `wellnes_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wellnes_sections` (
  `wellness_sec_id` int(11) NOT NULL AUTO_INCREMENT,
  `wellness_sec_name` varchar(100) NOT NULL,
  PRIMARY KEY (`wellness_sec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wellnes_sections`
--

LOCK TABLES `wellnes_sections` WRITE;
/*!40000 ALTER TABLE `wellnes_sections` DISABLE KEYS */;
INSERT INTO `wellnes_sections` VALUES (1,'Housing'),(2,'Schooling or work'),(3,'Family Relationships'),(4,'Social Connections'),(5,'Physical Health'),(6,'Drug and Alcohol Use'),(7,'Mental Health'),(8,'Culture'),(9,'Parenting'),(10,'Disability'),(11,'Safety and the Law');
/*!40000 ALTER TABLE `wellnes_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wellness_quesions`
--

DROP TABLE IF EXISTS `wellness_quesions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wellness_quesions` (
  `wellness_quesions_id` int(11) NOT NULL AUTO_INCREMENT,
  `wellness_quesions_sec_ref` int(11) NOT NULL,
  `wellness_quesion` varchar(400) NOT NULL,
  `wellness_ans1` varchar(200) NOT NULL,
  `wellness_ans1_support` varchar(500) DEFAULT NULL,
  `wellness_ans2` varchar(200) NOT NULL,
  `wellness_ans2_support` varchar(500) DEFAULT NULL,
  `wellness_ans3` varchar(200) DEFAULT NULL,
  `wellness_ans3_support` varchar(500) NOT NULL,
  `wellness_ans4` varchar(200) DEFAULT NULL,
  `wellness_ans4_support` varchar(500) DEFAULT NULL,
  `wellness_ans5` varchar(200) NOT NULL,
  `wellness_ans5_support` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`wellness_quesions_id`),
  KEY `wellness_quesions_sec_ref_idx` (`wellness_quesions_sec_ref`),
  CONSTRAINT `wellness_quesions_sec_ref` FOREIGN KEY (`wellness_quesions_sec_ref`) REFERENCES `wellnes_sections` (`wellness_sec_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wellness_quesions`
--

LOCK TABLES `wellness_quesions` WRITE;
/*!40000 ALTER TABLE `wellness_quesions` DISABLE KEYS */;
INSERT INTO `wellness_quesions` VALUES (1,1,'What is your current housing situation?','High risk','e.g. High risk of violence, abuse, harassment, unsafe shelter (homeless) or facilities, sleeping rough, imminent risk of homelessness','Moderate risk','e.g. Moderate risk of violence, abuse, harassment, inadequate shelter or facilities, couch surfing, tenancy failing','Low risk','e.g. Low risk of violence, abuse, harassment, adequate shelter or facilities','Safe','e.g. Safe­ no known risk , good shelter or facilities','Safe and stable – no known risk','e.g. Very safe and stable, very good shelter or facilities'),(2,1,'How does the situation i mpact on your wellbeing?','Can be a lot better','e.g. significantly affecting physical and emotional health','Can be better','','OK','','Doing well','e .g. Housing provides appropriate facilities for daily activities','Doing great',''),(3,2,'What is your current participation in employment or learning?','No Participation','e.g. History of repeated suspension/ exclusion from school, long period of nonattendance. Unable to maintain a job, no access to employment support, no access to income support. Not learning or working.','Low Participation','.g. Recent history of exclusion/ suspension from school, intermittent non­attendance. Unable to maintain a job, poor access to employment support,transport barriers.','Inconsistent Participation','e.g. At risk of suspension/ expulsion from school, difficulties or improvements in interaction with education/ training staff. Learning difficulty, minor transport or housing problems. Accessing employment assistance/ training.','Active Participation','e.g. Regular attendance, good access to school or training facility, participates in school or training activities. Participates in school or training activities. Participating in employment programs, maintaining work, sustained access to Centrelink.','Demonstrated\nParticipation','e.g. Regular attendance, consistent completion of tasks, participation/ re­engaged in school/ training activities for min 10 weeks. Achieving qualifications, good experience and references, independent with transport and housing.'),(4,2,'How does the situation impact on your wellbeing?','Can be a lot better','e.g. Very limited literacy and numeracy, significant barrier to completing education or training, very limited future employment options because of education','Can be better','e.g. L ow literacy and numeracy, significant barrier to completing education or training, very limited future employment options because of education','OK','e.g. Adequate literacy and numeracy, low barriers/ some opportunities to complete education or training, seek employment in the future','Doing well','e.g. No difficulties with literacy or numeracy, education enables to access or will provide future options in training or employment','Doing great','e.g. Education/ training or work enhance other areas in life and will provide future options, good literacy and numeracy skills.'),(5,3,'What is your situation with family relationships?','Unstable','e.g. No supportive relationships within family, constant conflict / volatile relationships, identified domestic violence, abuse or neglect, no access to recreation activities as a family','Limited Support','e.g. Limited supportive relationships within family, frequent conflict in relationships, at risk of domestic violence, abuse or neglect limited access to recreation activities as a family','Some support','e.g. Relationships within family are ok at times , challenging or rebuilding relationships with family , some opportunities to access to recreation activities as a family','Stable','e.g. Stable relationship within family , accessing recreation activities as a family','Stable and Supportive','e.g. Good relationships within family, supportive network of extended family, participation in recreation activities as a family'),(6,3,'How does the situation impact on your well­being?','Can be a lot better','','Can be better','','OK','','Doing well','','Doing great',''),(7,4,'What is the current level of your social connection with peers?','Limited social connections','e.g. Lacking supportive relationships; interactions or contact with peers, not accessing social/ recreational activities; no or limited access to public or private transport','Loose s ocial connections','e.g. Limited relationships, interactions or contact with peers, not accessing social/ recreational activities; limited access to public or private transport','Some social connections','e.g. Some supportive relationships, interactions or contact with peers, accessing social/ recreational activities; inconsistent access to public or private transport','Reasonable social connections','e.g. Supportive relationships, interactions or contact with peers, accessing social/ recreational activities; accessing public or private transport','Excellent social connections','e.g. A wide network of friends and community relationships,  participating in social/ recreational activities; accessing transport'),(8,4,'How does the situation impact on your well­being?','Can be a lot better','e.g. Socially isolated; dependent on family relationship(s)','Can be better','e.g. Socially isolated; mostly dependent on family relationship(s)','OK','e.g. Challenging or engaging in relationships with peers or others in the community','Doing well','e.g. Good relationships with peers and others in the community','Doing great','e.g. Maintained relationships with peers and others in the community'),(9,5,'What is your current health situation?','Condition is in c risis','e.g. Hygiene, nutrition, exercise, heavy smoking), risky sexual behaviour, severe dental issues; morbidly obese (BMI >35), frequent and serious poorly controlled chronic illness (diabetes, asthma etc.)','Condition is u nmanaged','e.g. Hygiene, nutrition, exercise, heavy smoking), risky sexual behaviour ,serious dental issues; obese (BMI >30), poorly controlled chronic illness (diabetes, asthma etc.)','Condition is n ot well managed','e.g. Hygiene, nutrition, exercise, smoking, risky sexual behaviour, some dental issues, at risk of obesity; overweight (BMI >25), inconsistent management/ prevention of chronic illness (diabetes, asthma etc.)','Condition is m anaged','e.g. Reasonable hygiene, nutrition, exercise, social smoking, etc., good sexual health practice, routine dental hygiene; good weight (BMI 23­25), good management of chronic illness (diabetes, asthma etc.)','Condition is w ell managed or absent','e.g. Good hygiene, nutrition, exercise, ceased smoking, safe sexual health, good dental hygiene and treatment access; good weight (BMI 19­23), good prevention and early intervention of chronic illness (diabetes, asthma etc.)'),(10,5,'How does the situation impact on your well­being?','Can be a lot better','e.g. Consistently intoxicated, lengthy irregular attendance; regular, lengthy hospitalisations','Can be better','e.g. Regularly intoxicated, irregular attendance; lengthy hospitalisations','OK','e.g. Occasionally intoxicated, some days off school, work or training','Doing well','e.g. Never intoxicated at school/training; regular attendance; few days off school','Doing great','e.g. Never intoxicated at school/training; regular'),(11,6,'What are the substances that you use?','Multiple simultaneous drug use/chronic use of C lass A or B s ubstances','e.g. Alcohol, tobacco, cannabis, inhalants, ice, illegal prescription drugs. No regard for drug interaction. Unsafe methods','Multiple non­simultaneous drug use/chronic use of Class C s ubstance','e.g. Alcohol, tobacco, cannabis, inhalants, ice, illegal prescription drugs. Multiple drug types not taken simultaneously. Unsafe usage methods','Controlled drug use','e.g. Alcohol, tobacco, cannabis, inhalants, ice, illegal prescription drugs interactions known. Understands and implements harm minimisation usage methods','Substance under control','e.g. Alcohol, tobacco, cannabis, . Regard for drug interaction. Understands and uses harm minimisation methods.','No usage',NULL),(12,6,'What is your usage pattern?','Compulsive use','Used today/ Daily usage e.g. Frequent high doses resulting in psychological/ physiological dependence, used within the previous 24 hours','Situational use','Used this week e.g. Used when there’s a need to be alert, calm, to relieve anxiety, physical or emotional pain; used on multiple occasions this week, not daily','Recreational use','Used this month e.g. Controlled use on social occasions; used weekly, 2 or 3 times a week','Experimental use','Used last 2 months ago e.g. Short term use motivated by curiosity or desire to experience new feelings or moods; mostly weekend usage, social occasions','No u sage in the last 6 months','e.g. Extended periods of abstinence or not using at all'),(13,6,'What is your family and peers usage of drugs and alcohol?','Family/ peers encourage/supply drugs','e.g. Consistent exposure high levels of drug use with family/peers, drug use encouraged/supported','Family/peers, use consistently','e.g. Exposure to high levels of drug use, drug use not encouraged but not discouraged','Family/peers use sometimes','e.g. Exposure to moderate levels of drug use, drug use discouraged','Family/peers engage in occasional social drug use','e.g.Exposure to low-level responsible drug use, drug use discouraged','Family/peers don’t use Supportively discourage use',NULL),(14,6,'How does the situation impact on your well­being?','Substance use makes focus on other goals impossible','e.g. Consistently intoxicated, unable to get to school/work or appointments','Substance use makes focus on other goals unlikely','e.g. Regularly intoxicated, unable to consistently attend school/work or appointments','Substance use makes focus on other goals a bit difficult','e.g. Occasionally intoxicated, some days of school/work or missed appointments','Substance use does not prevent achievement of other goals',NULL,'No substance use',NULL),(15,7,'What is your current mental health situation?','In critical crisis','e.g. Hospitalization due to psychosis or manic episode (depressive anxiety; schizophrenia); behaviour disorders (ADHD, ODD; conduct disorder) or suicide attempts, at critical risk of harm','In crisis','e.g. Experiencing of psychosis or manic episode (depressive anxiety; schizophrenia); behaviour disorders (ADHD, ODD, conduct disorder); suicidal ideation, at significant risk of harm','Resolving situation/ getting worse','e.g. Accessing treatment/therapy/ medication','Situation mainly resolved','e.g. Maintaining treatment plan','No problem or well managed mental health wellbeing','e.g. No symptoms, good level of mood, behavioural and conduct'),(16,7,'How does the situation impact on your wellbeing?','Mental Health has a serious impact on current and future options','e.g. Two or more of: difficulty in maintaining relationships, personal care or distorted perception of reality and cognition ','Mental Health has a moderate impact on current and future options','e.g. One of: difficulty in maintaining relationships, personal care or distorted perception of reality cognition','Mental Health has a l ow impact on current and future options','e.g. Resolving mental health status allows for rebuilding damaged relationships, maintaining personal care and uptake of education or employment opportunities','Mental Health s upports current and future options','e.g. Effort previously expended in resolving mental health status is now able to be focussed into achievement of educational/ training goals ','Mental Health e nhances current and future options','e.g. Good coping plans and distress tolerance, coping skills have been generalised to other areas '),(17,8,'What is the current level of your cultural connection?','Limited cultural connections due to:','Social isolation; family breakdown; exposure to discrimination, racism, bullying; experience of adversity, trauma; limited language/ networks; financial stress; settlement/ migration or refugee experience ','Loose cultural connections due to:','Social isolation; family breakdown; exposure to discrimination, racism, bullying; experience of adversity, trauma; limited language/ networks; financial stress; settlement/ migration or refugee experience ','Some cultural connections','e.g. Some social isolation or making connections; some family issues or being addressed; some exposure to­ discrimination/ racism/ bullying/ trauma/ settlement or migration experience and beginning to recognise issue ','Reasonable cultural connections','e.g. Making cultural (language/ spiritual/ custom/ community) connections; addressing negative experiences; has reasonable cultural supports','Stable cultural connections','e.g. Maintaining cultural (language/ spiritual/ custom/ community) connection; managing negative cultural experiences; stable cultural connections in the community  '),(18,8,'How does the situation impact on your wellbeing?','Poor cultural connections makes focus on goals unlikely',' ','Loose cultural connections makes focus on goals difficult',' ','Some cultural connections makes focus on goals somewhat likely','','Cultural connections supports focus on achievement of goals',' ','Cultural connections enhances focus on achievement of goals',' '),(19,9,'What is your current parenting situation?','Limited Parenting Capacity:','e.g. Basic care, safety, emotional warmth, stimulation, guidance/ boundaries, stability of child is compromised.','Minimum Parenting Capacity:','e.g. Basic care, safety, emotional warmth, stimulation, guidance/ boundaries, stability is provided','Some Parenting Capacity:','e.g. Basic care, safety, emotional warmth, stimulation, guidance, stability is provided','Good Parenting Capacity:','e.g. Basic care, safety, emotional warmth, stimulation, guidance/ boundaries, stability is provided','Great Parenting Capacity:',' e.g. Basic care, safety, emotional warmth, stimulation, guidance/ boundaries, stability is provided'),(20,9,'Child Development?','Limited Child Development','Child(ren’s) health, education, emotional/behavioural/social development, identity and self-care skills','Minimum Child Development','Child(ren’s) health, education, emotional/behavioural/social development, identity and self-care skills','Some Child Development:','Child(ren’s) health, education, emotional/behavioural/social development, identity and self ­ care skills ','Good Child Development','Child(ren’s) health, education,emotional/behavioural/social development, identity and self­care skills ','Great Child Development','emotional/behavioural/social development, identity and self-care skills '),(21,9,'Pregnancy Care?','Limited Pregnancy Care','Antenatal care, low/no risk taking behaviours, (Males: Unsupportive of partner during antenatal period) ','Minimum Pregnancy Care','Antenatal care, low/no risk taking behaviours (Males: Somewhat supportive of partner during antenatal period) ','Some Pregnancy Care','Antenatal care, low/no risk taking behaviours (Males: Generally supportive of partner during antenatal period) ','Good Pregnancy Care','Antenatal care, low/no risk taking behaviours (Males: Consistently supportive of partner during antenatal period) ','Great Pregnancy Care','Antenatal care, low/no risk taking behaviours (Males: Highly supportive of partner during antenatal period) \r\n'),(22,9,'How does the situation impact on your wellbeing?','Focus on parenting unlikely','e.g. Child Safety involvement, no child care, custody disputes, drug and alcohol issues, unmanaged disability/ mental health issues, financial stress','Focus on parenting difficult','e .g. Child Safety involvement, no child care, custody disputes, drug and alcohol issues, unmanaged disability/ mental health issues, financial stress','Parenting issues have minor impact','e.g. Child Safety involvement reduced/ resolved, child care, custody being resolved, drug/ alcohol issues, mental health issues, disability being managed','Parenting abilities support achievement of own goals','e.g. Child care in place, children’s issues are well­managed, accessing support for personal or parenting needs','Parenting enhances family life & achievement of goals','e .g. Parenting has\r\n\r\npositive impact on family life and personal aspirations'),(23,10,'What is your current disability situation?','\r\nUnmanaged diagnosed or undiagnosed condition','e.g. Speech & language, ASD, visual & hearing impairments, learning difficulties, sensory and auditory processing issues, cognitive, physical or functional disability','Condition not well managed diagnosed or undiagnosed','e.g. Speech & language, ASD, visual & hearing impairments, learning difficulties, sensory and auditory processing issues, cognitive, physical or functional disability','Inconsistent management of diagnosed or undiagnosed condition ','e.g. Speech & language, ASD, visual & hearing impairments, learning difficulties, sensory and auditory processing issues, cognitive, physical or functional disability','Condition reasonably managed','e.g. Speech & language, ASD, visual & hearing impairments, learning difficulties, sensory and auditory processing issues,, cognitive, physical or functional disability','Condition well managed or absent','e.g. Speech & language, ASD, visual & hearing impairments, learning difficulties, sensory and auditory processing issues,, cognitive, physical or functional disability'),(24,10,'How does the situation impact on your wellbeing?','Situation makes focus on goals unlikely','e.g. significant barriers to: completing education or training, other life skills','Situation makes focus on goals difficult','e.g. difficulty in: completing education or training, other life skills because of mismanaged disability','Situation makes focus on goals somewhat likely','e.g. adequate literacy and numeracy, low barriers/ some opportunities to: complete education or training, obtain other life skills','Situation supports achievement of goals','e.g. no difficulties with literacy or numeracy, situation enables access to future options in life','Situation enhances achievement of goals','\r\ne.g. management of disability enhance other areas in life and will provide good future options.'),(25,11,'What is your current involvement with Youth Justice/ criminal justice agencies?','High Risk','e.g. Multiple prior criminal history (in detention/ police custody), recent offending, court order, high risk environment (peer offending, disengaged from training/ employment, regular use of drugs or alcohol or violence)','Moderate Risk','e.g. One prior criminal history (in detention/ police custody), recent or current court order, moderate risk environment (limited support, at risk of or disengagement, regular use of AOD, some aggression or violence)','Low Risk','e.g. recent or current court order, police caution, low risk environment (some support, no offending history, low motivation to engage with training or employment, low drug or alcohol use, low aggression/violence)','Protective Factors','e.g. Completed court order, no offending or involvement with police in the last 6mths, living in a supportive environment, engaged in education/ training, employment, reduced drug or alcohol, no display of aggression or violence < 6mths','Good Protective Factors','e.g. No offending or involvement with police in the last 12mths, lives in a supportive place, engaged in training/ employment, no AOD, no display of aggression or violence < 12mths '),(26,11,'How does the situation impact on your wellbeing?','Situation makes focus on educational/ training goals unlikely','e.g. Lack of stability; restrictions due to YJ order; interrupts ability to achieve goals; causes significant distress','Situation makes focus on educational/ training goals difficult','e.g. Lack of stability; restrictions on employment due to YJ order; interrupts ability to achieve goals; causes some distress','Situation makes focus on educational/ training goals somewhat likely','e.g. Adequate stability; compliance with YJ order does not interrupt ability to achieve goals; causes occasional distress','Situation supports achievement of educational/ training goals','e.g. Good stability of placement; YJ order nearing completion; able to participate in range of activities; gets support from YJ officers','Situation enhances achievement of educational/ training goals',' e .g. Stable and consistent placement; YJ order completed; able to participate in range of activities; no distress ');
/*!40000 ALTER TABLE `wellness_quesions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-18 16:34:46
