

-----------############### Not Realesed #################--------------
--------------------------------------------------------------------------------
-- Changes upto 26-July-2016 - Nirmal Perera

ALTER TABLE `dgfg_db`.`blog_tab` 
CHANGE COLUMN `blog_name` `blog_name` TEXT NOT NULL ;


-----------############### Realased 25 July 2016 #################--------------
--------------------------------------------------------------------------------
-- Changes upto 17-July-2016 - Nirmal Perera

CREATE TABLE event_galary(
    galary_id INT PRIMARY KEY AUTO_INCREMENT,
    event_id INT NOT NULL,
    img_linq TEXT NOT NULL,
    is_approved TINYINT NOT NULL,
    is_visible TINYINT NOT NULL,
    create_by VARCHAR(25),
    create_role INT,
    create_date DATETIME,
    approve_by VARCHAR(25),
    approve_role INT,
    approve_date DATETIME
);

ALTER TABLE `dgfg_db`.`event_galary` 
ADD COLUMN `is_submited` TINYINT(4) NOT NULL AFTER `is_visible`;
ALTER TABLE `dgfg_db`.`event_galary` 
CHANGE COLUMN `create_by` `create_by` VARCHAR(50) NULL DEFAULT NULL ;

--------------------------------------------------------------------------------
-- Changes upto 11-July-2016 - Chris Bandara
DROP TABLE IF EXISTS `private_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `private_msg` (
 `msg_id` int(11) NOT NULL,
 `msg_reply_id` int(11) DEFAULT NULL,
 `msg_title` varchar(200) NOT NULL,
 `msg_body` text NOT NULL,
 `msg_date` datetime NOT NULL,
 PRIMARY KEY (`msg_id`),
 KEY `msg_reply_id_idx` (`msg_reply_id`),
 CONSTRAINT `msg_reply_id` FOREIGN KEY (`msg_reply_id`) REFERENCES `private_msg` (`msg_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--------------------------------------------------------------------------------
-- Changes upto 6-July-2016 - Nirmal Perera
ALTER TABLE `dgfg_db`.`service_tab` 
ADD COLUMN `sp_email` TEXT NULL AFTER `sp_after_phn`;

CREATE TABLE blog_tab(
blog_id INT PRIMARY KEY AUTO_INCREMENT,
blog_name VARCHAR(45) NOT NULL,
blog_body TEXT NOT NULL,
is_approved TINYINT(1),
blog_created_by VARCHAR(25),
blog_created_role INT,
blog_created_date DATETIME,
blog_approved_by VARCHAR(25),
blog_approved_role INT,
blog_approved_date DATETIME
);

ALTER TABLE `dgfg_db`.`blog_tab` 
ADD COLUMN `blog_image` TEXT NULL DEFAULT NULL AFTER `blog_body`;

ALTER TABLE `dgfg_db`.`blog_tab` 
CHANGE COLUMN `blog_approved_by` `blog_modified_by` VARCHAR(25) NULL DEFAULT NULL ,
CHANGE COLUMN `blog_approved_role` `blog_modified_role` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `blog_approved_date` `blog_modified_date` DATETIME NULL DEFAULT NULL ;
